import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import * as global from '../global.variables';
import { environment } from '../../environments/environment';

@Injectable()
export class AdministrationService {
  private base_url: string;
  private header: Headers;
  constructor(private http: Http) {
    if (environment.production) {
      this.base_url = global.AppURLSettings.SANDBOX_URL;
    } else {
      this.base_url = global.AppURLSettings.DEV_URL;
    }
    this.header = new Headers();
    this.header.append('token', sessionStorage.getItem('token'));
  }


  getTopLevelOrgUnit() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}AdministrationService/getTopLevelOrgUnit?sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }
  getTranslations() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}AdministrationService/getTranslations?sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  saveTranslation(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}AdministrationService/saveTranslation?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value, { headers: this.header });
  }
  deleteTranslation(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}AdministrationService/deleteTranslation?id=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }

  getSubLevelOrgUnits(subOrgId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}AdministrationService/getSubLevelOrgUnits?orgId=${subOrgId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  saveOrgUnit(data) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}AdministrationService/saveOrgUnit?sysdatetime=${fulldatetime}`;
    return this.http.post(url, data, { headers: this.header }).map(res => res);
  }

  errorHandler(error: Response) {
    return Observable.throw(error || 'Server Error');
  }
  getAllSubLevelOrgUnits(orgId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}AdministrationService/getAllSubLevelOrgUnits?orgId=${orgId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }
  getAddOns() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}AdministrationService/getAddOns?sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }
  searchLDAPUsers(user) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}AdministrationService/searchLDAPUsers?user=${user}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  encryptConfig(text) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}AdministrationService/encryptConfig?text=${text}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  getLicenseInfo() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}AdministrationService/getLicenseInfo?sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
}
