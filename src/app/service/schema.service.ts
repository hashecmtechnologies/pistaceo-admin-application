import { Injectable } from '@angular/core';
import { Http, Response, Headers, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import * as global from '../global.variables';
import { UserService } from './user.service';
import { HttpInterceptor } from './interceptor.service';
import { environment } from '../../environments/environment';

@Injectable()
export class SchemaService {
  private base_url: string;
  private header: Headers;
  constructor(private http: HttpInterceptor) {
    if (environment.production) {
      this.base_url = global.AppURLSettings.SANDBOX_URL;
    } else {
      this.base_url = global.AppURLSettings.DEV_URL;
    }
    this.header = new Headers();
    this.header.append('token', sessionStorage.getItem('token'));
  }
  getWorkTypes(): Observable<any> {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/getWorkTypes?sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }
  getRegisterWorkTypes(): Observable<any> {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/getRegisterWorkTypes?sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }
  getWorkTypeFromID(activityId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/getWorkTypeFromID?id=${activityId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }
  getActivityTypeFromID(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/getActivityTypeFromID?id=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }
  getWorkCategories() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/getWorkCategories?sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }
  getWorkRegisterTypes() {
    // const sysDateTime = new Date();
    // const fulldatetime = sysDateTime.getTime();
    // const url = `${this.base_url}SchemaService/getWorkRegisterTypeSearch?id=${id}`;
    // return this.http.get(url, {headers: this.header}).map(
    //   res => res
    // );
  }
  saveWorkCategory(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/saveWorkCategory?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value, { headers: this.header }).map(res => res);
  }
  getWorkTypeSearch(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/getWorkTypeSearch?id=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }
  getWorkTypesForSearch() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/getWorkTypesForSearch?sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }
  getDocumentTypes() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/getDocumentTypes?sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  saveDocumentType(documentType) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/saveDocumentType?sysdatetime=${fulldatetime}`;
    return this.http.post(url, documentType, { headers: this.header });
  }
  getResponseTypes() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/getResponseTypes?sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  getSimpleForms() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/getSimpleForms?sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  saveResponseType(responseType) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/saveResponseType?sysdatetime=${fulldatetime}`;
    return this.http.post(url, responseType, { headers: this.header });
  }
  getTemplates() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/getTemplates?sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  saveConfigDocument(saveName) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/saveConfigDocument?sysdatetime=${fulldatetime}`;
    return this.http.post(url, saveName, { headers: this.header });
  }
  addTemplate(form) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/addTemplate?sysdatetime=${fulldatetime}`;
    return this.http.post(url, form, { headers: this.header });
  }
  addImage(form) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/addImage?sysdatetime=${fulldatetime}`;
    return this.http.post(url, form, { headers: this.header });
  }
  addSignature(form) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/addSignature?sysdatetime=${fulldatetime}`;
    return this.http.post(url, form, { headers: this.header });
  }
  addSimpleForm(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/addSimpleForm?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value, { headers: this.header });
  }
  getForms() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/getForms?sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  addform(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/addForm?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value, { headers: this.header });
  }
  importForm(formdata) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/importForm?sysdatetime=${fulldatetime}`;
    return this.http.post(url, formdata, { headers: this.header });
  }
  getWorkTypeDefinitions() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/getWorkTypeDefinitions?sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  addWorkTypeDefinition(worktype) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/addWorkTypeDefinition?sysdatetime=${fulldatetime}`;
    return this.http.post(url, worktype, { headers: this.header });

  }
  setConfigDocumentAsDraft(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/setConfigDocumentAsDraft?id=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  validateWorktype(docId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/validateWorkType?docid=${docId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  commitWorkType(docId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/commitWorkType?docid=${docId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  removeWorkTypeRoles(role, objid) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/removeWorkTypeRole?id=${objid}&roleid=${role.id}&type=${role.type}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  getWorkTypeRoles(objId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/getWorkTypeRoles?id=${objId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  addWorkTypeRoles(formdata) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/addWorkTypeRole?id=${formdata.id}&roleid=${formdata.roleid}&type=${formdata.type}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  exportWorkType(objId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/exportWorkType?id=${objId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header, responseType: ResponseContentType.Blob, });
  }

  importWorkTypeDefinition(formdata) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/importWorkTypeDefinition?sysdatetime=${fulldatetime}`;
    return this.http.post(url, formdata, { headers: this.header });
  }
  inactiveWorkType(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/inactivateWorkType?id=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  inactiveDraftWorkType(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/inactivateDraftWorkType?docid=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  addWorkType(activity) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/addWorkType?sysdatetime=${fulldatetime}`;
    return this.http.post(url, activity, { headers: this.header });
  }
  getWorkTypesForEdit() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/getWorkTypesForEdit?sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  addNewWorkTypeVersion(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/addNewWorkTypeVersion?id=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  deleteWorkType(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/deleteWorkType?sysdatetime=${fulldatetime}`;
    return this.http.post(url, id, { headers: this.header });
  }
  validateDraftWorkType(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/validateDraftWorkType?id=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  commitDraftWorkType(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/commitDraftWorkType?id=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  updateWorkType(workType) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/updateWorkType?sysdatetime=${fulldatetime}`;
    return this.http.post(url, workType, { headers: this.header });
  }
  addActivityType(activity) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/addActivityType?sysdatetime=${fulldatetime}`;
    return this.http.post(url, activity, { headers: this.header });
  }
  getWorkTypeInitiatingForm(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/getWorkTypeInitiatingForm?id=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  updateActivityType(activity) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/updateActivityType?sysdatetime=${fulldatetime}`;
    return this.http.post(url, activity, { headers: this.header });
  }
  GetDocumentTypes() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/getDocumentTypes?sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header });
  }
  addDocTypeToActivityType(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/addDocTypeToActivityType?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value, { headers: this.header });
  }
  updateActivityTypeDocType(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/updateActivityTypeDocType?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value, { headers: this.header });
  }
  addResponseToActivityType(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/addResponseToActivityType?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value, { headers: this.header });
  }
  updateActivityTypeResponse(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/updateActivityTypeResponse?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value, { headers: this.header });
  }
  addRouteRuleToActivityType(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/addRouteRuleToActivityType?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value, { headers: this.header });
  }
  updateActivityTypeRouteRule(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/updateActivityTypeRouteRule?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value, { headers: this.header });
  }
  addConditionToRouteRule(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/addConditionToRouteRule?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value, { headers: this.header });
  }
  updateRouteRuleCondition(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/updateRouteRuleCondition?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value, { headers: this.header });
  }
  deleteRouteRuleCondition(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/deleteRouteRuleCondition?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value, { headers: this.header });
  }
  deleteActivityTypeRouteRule(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/deleteActivityTypeRouteRule?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value, { headers: this.header });
  }
  addActionToActivityType(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/addActionToActivityType?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value, { headers: this.header });
  }
  updateActivityTypeAction(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/updateActivityTypeAction?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value, { headers: this.header });
  }
  deleteActivityTypeAction(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/getDocumentTypes?sysdatetime=${fulldatetime}&id=${id}`;
    return this.http.get(url, { headers: this.header });
  }
  addWorkTypePermission(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/addWorkTypePermission?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value, { headers: this.header });
  }
  updateWorkTypePermission(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/updateWorkTypePermission?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value, { headers: this.header });
  }
  deleteWorkTypePermission(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}SchemaService/deleteWorkTypePermission?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value, { headers: this.header });
  }
}
