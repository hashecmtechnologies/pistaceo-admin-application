import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { AppRoutes } from './app.routes';
import 'rxjs/add/operator/toPromise';

import { AccordionModule } from 'primeng/primeng';
import { AutoCompleteModule } from 'primeng/primeng';
import { BreadcrumbModule } from 'primeng/primeng';
import { ButtonModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { CarouselModule } from 'primeng/primeng';
import { ChartModule } from 'primeng/primeng';
import { CheckboxModule } from 'primeng/primeng';
import { ChipsModule } from 'primeng/primeng';
import { CodeHighlighterModule } from 'primeng/primeng';
import { ConfirmDialogModule } from 'primeng/primeng';
import { ColorPickerModule } from 'primeng/primeng';
import { SharedModule } from 'primeng/primeng';
import { ContextMenuModule } from 'primeng/primeng';
import { DataGridModule } from 'primeng/primeng';
import { DataListModule } from 'primeng/primeng';
import { DataScrollerModule } from 'primeng/primeng';
import { DataTableModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';
import { DragDropModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { EditorModule } from 'primeng/primeng';
import { FieldsetModule } from 'primeng/primeng';
import { GalleriaModule } from 'primeng/primeng';
import { GMapModule } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { InputMaskModule } from 'primeng/primeng';
import { InputSwitchModule } from 'primeng/primeng';
import { InputTextModule } from 'primeng/primeng';
import { InputTextareaModule } from 'primeng/primeng';
import { LightboxModule } from 'primeng/primeng';
import { ListboxModule } from 'primeng/primeng';
import { MegaMenuModule } from 'primeng/primeng';
import { MenuModule } from 'primeng/primeng';
import { MenubarModule } from 'primeng/primeng';
import { MessagesModule } from 'primeng/primeng';
import { MultiSelectModule } from 'primeng/primeng';
import { OrderListModule } from 'primeng/primeng';
import { OrganizationChartModule } from 'primeng/primeng';
import { OverlayPanelModule } from 'primeng/primeng';
import { PaginatorModule } from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';
import { PanelMenuModule } from 'primeng/primeng';
import { PasswordModule } from 'primeng/primeng';
import { PickListModule } from 'primeng/primeng';
import { ProgressBarModule } from 'primeng/primeng';
import { RadioButtonModule } from 'primeng/primeng';
import { RatingModule } from 'primeng/primeng';
import { ScheduleModule } from 'primeng/primeng';
import { SelectButtonModule } from 'primeng/primeng';
import { SlideMenuModule } from 'primeng/primeng';
import { SliderModule } from 'primeng/primeng';
import { SpinnerModule } from 'primeng/primeng';
import { SplitButtonModule } from 'primeng/primeng';
import { StepsModule } from 'primeng/primeng';
import { TabMenuModule } from 'primeng/primeng';
import { TabViewModule } from 'primeng/primeng';
import { TerminalModule } from 'primeng/primeng';
import { TieredMenuModule } from 'primeng/primeng';
import { ToggleButtonModule } from 'primeng/primeng';
import { ToolbarModule } from 'primeng/primeng';
import { TooltipModule } from 'primeng/primeng';
import { TreeModule } from 'primeng/primeng';
import { TreeTableModule } from 'primeng/primeng';
import { FileUploadModule } from 'ng2-file-upload';

import { AppComponent } from './app.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SigninComponent } from './components/app-components/signin/signin.component';
import { UserService } from './service/user.service';
import { ContentService } from './service/content.service';
import { DocumentService } from './service/document.service';
import { AdministrationService } from './service/Administration.service';
import { SchemaService } from './service/schema.service';
import { IntegrationService } from './service/integration.service';
import { WorkService } from './service/work.service';
import { HttpInterceptor } from './service/interceptor.service';
import { AdminLayoutComponent } from './components/app-components/layout/admin-layout/admin-layout.component';
import { FooterComponent } from './components/app-components/layout/footer/footer.component';
import { TopbarComponent } from './components/app-components/layout/topbar/topbar.component';
import { BreadcrumbComponent } from './components/app-components/layout/breadcrumb/breadcrumb.component';
import { BreadcrumbService } from './components/app-components/layout/breadcrumb/breadcrumb.service';
import { MenuItemsComponent, SubMenuMenuItemsComponent } from './components/app-components/layout/menu-items/menu-items.component';
import { ToastrModule } from 'ngx-toastr';
import { AngularDraggableModule } from 'angular2-draggable';
import { ReportService } from './service/report.service';
import { ConfigurationService } from './service/Configuration.service';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { AuthGuard } from './service/auth.guard';
import { DataService } from './service/data.service';
import { UsersComponent } from './components/app-components/users/users.component';
import { OrgtreeComponent } from './components/generic-components/orgtree/orgtree.component';
import { OrgunitsComponent } from './components/app-components/orgunits/orgunits.component';
import { RolesComponent } from './components/app-components/roles/roles.component';
import { RolesListComponent } from './components/app-components/roles-list/roles-list.component';
import { DelegationsComponent } from './components/delegations/delegations.component';
import { DocumentTypesComponent } from './components/document-types/document-types.component';
import { ResponseTypesComponent } from './components/response-types/response-types.component';
import { TemplatesComponent } from './components/app-components/templates/templates.component';
import { RepositoryComponent } from './components/repository/repository.component';
import { PropertyTemplateComponent } from './components/property-template/property-template.component';
import { ClassesComponent } from './components/classes/classes.component';
import { RootFoldersComponent } from './components/root-folders/root-folders.component';
import { RecycleBinComponent } from './components/recycle-bin/recycle-bin.component';
import { ConfiguarationsComponent } from './components/configuarations/configuarations.component';
import { TranslationsComponent } from './components/translations/translations.component';
import { LicenseComponent } from './components/license/license.component';
import { ReportsComponent } from './components/reports/reports.component';
import { FilterFormsComponent } from './components/app-components/filter-forms/filter-forms.component';
import { FormsComponent } from './components/forms/forms.component';
import { WorkTypesComponent } from './components/work-types/work-types.component';


export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutes,
        HttpModule,
        BrowserAnimationsModule,
        AccordionModule,
        AutoCompleteModule,
        BreadcrumbModule,
        ButtonModule,
        CalendarModule,
        CarouselModule,
        ChartModule,
        CheckboxModule,
        ChipsModule,
        CodeHighlighterModule,
        ConfirmDialogModule,
        ColorPickerModule,
        SharedModule,
        ContextMenuModule,
        DataGridModule,
        DataListModule,
        DataScrollerModule,
        DataTableModule,
        DialogModule,
        DragDropModule,
        DropdownModule,
        EditorModule,
        FieldsetModule,
        GalleriaModule,
        GMapModule,
        GrowlModule,
        InputMaskModule,
        InputSwitchModule,
        InputTextModule,
        InputTextareaModule,
        LightboxModule,
        ListboxModule,
        MegaMenuModule,
        MenuModule,
        MenubarModule,
        MessagesModule,
        MultiSelectModule,
        OrderListModule,
        OrganizationChartModule,
        OverlayPanelModule,
        PaginatorModule,
        PanelModule,
        PanelMenuModule,
        PasswordModule,
        PickListModule,
        ProgressBarModule,
        RadioButtonModule,
        RatingModule,
        ScheduleModule,
        SelectButtonModule,
        SlideMenuModule,
        SliderModule,
        SpinnerModule,
        SplitButtonModule,
        StepsModule,
        TabMenuModule,
        TabViewModule,
        TerminalModule,
        TieredMenuModule,
        ToggleButtonModule,
        ToolbarModule,
        TooltipModule,
        TreeModule,
        TreeTableModule,
        HttpClientModule,
        ReactiveFormsModule,
        FileUploadModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        }),
        FileUploadModule,
        ToastrModule.forRoot({
            timeOut: 5000,
            positionClass: 'toast-bottom-right',
            preventDuplicates: true
        }),
        AngularDraggableModule,
        NgxSmartModalModule.forRoot(),
        Ng4LoadingSpinnerModule.forRoot()
    ],
    declarations: [
        SigninComponent,
        AppComponent,
        AdminLayoutComponent,
        FooterComponent,
        TopbarComponent,
        BreadcrumbComponent,
        MenuItemsComponent,
        SubMenuMenuItemsComponent,
        MenuItemsComponent,
        UsersComponent,
        OrgtreeComponent,
        OrgunitsComponent,
        RolesComponent,
        RolesListComponent,
        DelegationsComponent,
        DocumentTypesComponent,
        ResponseTypesComponent,
        TemplatesComponent,
        RepositoryComponent,
        PropertyTemplateComponent,
        ClassesComponent,
        RootFoldersComponent,
        RecycleBinComponent,
        ConfiguarationsComponent,
        TranslationsComponent,
        LicenseComponent,
        ReportsComponent,
        FilterFormsComponent,
        FormsComponent,
        WorkTypesComponent,

    ],
    providers: [
        { provide: LocationStrategy, useClass: HashLocationStrategy },
        UserService, BreadcrumbService,
        ContentService,
        DocumentService,
        AdministrationService,
        SchemaService,
        IntegrationService,
        WorkService,
        HttpInterceptor,
        ReportService,
        ConfigurationService,
        AuthGuard,
        DataService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
