import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroupDirective, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { ContentService } from '../../service/content.service';
import { UserService } from '../../service/user.service';

@Component({
  selector: 'app-repository',
  templateUrl: './repository.component.html',
  styleUrls: ['./repository.component.css']
})
export class RepositoryComponent implements OnInit {
  @ViewChild(FormGroupDirective) formDirective: FormGroupDirective;
  public permissionList = true;
  public permissionForm: FormGroup;
  public accessLevelDrop = [];
  public documentResult = [];
  public editId: any;
  public singleSelectDropdown: any;
  public dbvalue = [];
  public dbDummyValue = [];
  public isAdd = false;
  public isView = true;
  public repositoryResults: any;
  public deletepermissionsValue: any;
  public accessTypeDrop = [];
  public selectedTab = 0;
  public selectedTab1 = true;
  public selectedTab2 = false;
  constructor(private cs: ContentService, private fb: FormBuilder, private tr: ToastrService, private us: UserService,
    private spinner: Ng4LoadingSpinnerService, private ngxSmartModalService: NgxSmartModalService) {
    this.accessLevelDrop = [
      { label: 'Full Control (Delete)', value: 127 },
      { label: 'Owner (Permissions)', value: 63 },
      { label: 'Unfile', value: 31 },
      { label: 'File', value: 51 },
      { label: 'Create', value: 7 },
      { label: 'Modify', value: 3 },
      { label: 'View', value: 1 }
    ];
    this.accessTypeDrop = [
      { label: 'Inherit', value: 1 }
    ];
  }

  ngOnInit() {
    this.callRepository();
    this.permissionForm = this.fb.group({
      proproleid: [null, Validators.compose([Validators.required])],
      propaccesslevel: [127],
      propaccesstype: [1],
    });
  }
  reindexAlert(event) {
    this.ngxSmartModalService.getModal('reindexAlertModel').open();
  }
  reindexConfirmed() {
    this.spinner.show();
    this.cs.indexAllDocuments().subscribe(data => {
      this.spinner.hide();
      this.tr.success('Reindex is done');
    }, error => { this.spinner.hide(); });
  }
  callRepository() {
    this.spinner.show();
    this.cs.getRepositories().subscribe(data => { this.getResults(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
  getResults(data) {
    this.repositoryResults = JSON.parse(data._body);
  }
  clickedOnAdd() {
    this.isAdd = true;
    this.isView = false;
    this.permissionForm.reset();
  }
  clickedOnView() {
    this.isView = true;
    this.isAdd = false;
  }
  cancelAdd() {
    this.permissionForm.reset();
    this.permissionForm.patchValue({ propaccesslevel: 127, propaccesstype: 1 });
  }
  editRepository(event) {
    this.isView = false;
    this.isAdd = false;
    this.editId = event;
  }
  deleteAlert(event) {
    this.deletepermissionsValue = event;
    this.ngxSmartModalService.getModal('deleteConfirmationAlert').open();
  }
  permissionFormSubmitted(event) {
    this.selectedTab1 = false;
    this.selectedTab2 = true;
    this.spinner.show();
    const value = {
      id: this.editId.id,
      permissions: [{
        roleId: this.permissionForm.value.proproleid.value,
        accessMask: this.permissionForm.value.propaccesslevel,
        inheritDepth: this.permissionForm.value.propaccesstype
      }]
    };
    this.cs.updateRespositorySecurity(value).subscribe(data => { this.permissionupdated(data); this.tr.success('', 'Updated permissions'); this.spinner.hide(); this.selectedTab2 = false; this.selectedTab1 = true; }, error => { this.spinner.hide(); });
  }
  deleteConfirmed() {
    const value = {
      id: this.editId.id,
      permissions: [{
        roleId: this.deletepermissionsValue.roleId,
        accessMask: 0,
        inheritDepth: this.deletepermissionsValue.inheritDepth,
        aclId: this.deletepermissionsValue.aclId
      }]
    };
    this.spinner.show();
    this.cs.updateRespositorySecurity(value).subscribe(data => { this.permissionupdated(data); this.tr.success('', 'Deleted permissions'); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
  clicked(event) {
    this.selectedTab = 0;
  }
  permissionupdated(data) {
    this.permissionList = true;
    this.isView = false;
    this.isAdd = false;
    this.permissionForm.reset();
    this.permissionForm.patchValue({ propaccesslevel: 127, propaccesstype: 1 });
    this.callRepository();
  }
  onSearchAutoStextTA(event) {
    let stringTyped = '';
    stringTyped = event.query;
    if (stringTyped.length > 2) {
      this.spinner.show();
      this.us.searchRoles(stringTyped).subscribe(data => { this.getAllRoles(data); }, error => { this.spinner.hide(); });
    }
  }
  getAllRoles(data) {
    this.dbvalue = [];
    this.dbDummyValue = [];
    const getrolecategory = JSON.parse(data._body);
    for (let index = 0; index < getrolecategory.length; index++) {
      const dbValues = {
        'value': getrolecategory[index].id,
        'name': getrolecategory[index].name
      };
      this.dbvalue.push(dbValues);
    }
    this.spinner.hide();
  }
  selectSingle(event) {

  }
  accessTypeChanged(event) {
  }
  accessLevelChanged(event) {

  }
}
