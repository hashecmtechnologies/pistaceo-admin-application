import { UserService } from '../../../service/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  public form: FormGroup;
  errorMessage = false;
  signIn: boolean;
  private user: any = sessionStorage.getItem('user');
  public capsLock = false;
  public capsOn;
  public showcookiepolicy = true;
  constructor(private fb: FormBuilder, private router: Router, private userservice: UserService) {

  }

  ngOnInit() {
    if (sessionStorage.getItem('token') === undefined || sessionStorage.getItem('token') === null) {
      this.router.navigateByUrl('/');
    } else {
      this.router.navigateByUrl('/users');
    }
    this.signIn = false;
    this.form = this.fb.group({
      uname: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required])],
      saveUser: [null]
    });
    this.form.patchValue({ saveUser: true });
    if (environment.sso) {
      let value = 0;
      if (this.showcookiepolicy === true) {
        value = 1;
      }
      this.userservice.authenticateUser('test', 'test', value).subscribe(data => this.singleSignOn(data), error => {
        document.getElementById('spinner').style.display = 'none';
      });
    } else {
      document.getElementById('spinner').style.display = 'none';
    }
  }
  getDeploymentType(data) {
    if (data._body === 'CLOUD') {
      const emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
    }
  }

  singleSignOn(data) {
    this.userservice.getUserDetails('qw').subscribe(datares => this.signInUser(datares));
  }

  onSubmit(form) {
    document.getElementById('spinner').style.display = 'block';
    const username = btoa(form.controls.uname.value);
    const password = btoa(form.controls.password.value);
    sessionStorage.clear();
    let value = 0;
    if (this.showcookiepolicy === true) {
      value = 1;
    }
    this.userservice.authenticateUser(username, password, value).subscribe(data => {
      this.login(username, data);
    }, error => {
      this.signIn = true;
      document.getElementById('spinner').style.display = 'none';
      sessionStorage.removeItem('token');
    });
  }
  login(username, datares) {
    this.userservice.getUserDetails(username).subscribe(
      data => this.signInUser(data), error => {
        this.errorMessage = true;
        document.getElementById('spinner').style.display = 'none';
        sessionStorage.removeItem('token');
      });
  }
  signTyped() {
    this.signIn = false;
  }

  signInUser(data) {
    if (data._body !== '') {
      this.user = JSON.parse(data._body);
      sessionStorage.setItem('user', JSON.stringify(this.user));
      if (this.user.settings !== undefined) {
        for (let index = 0; index < this.user.settings.length; index++) {
          if (this.user.settings[index].key === 'Default Document View') {
            sessionStorage.setItem(this.user.settings[index].key, this.user.settings[index].val);
            break;
          }
        }
        for (let index = 0; index < this.user.settings.length; index++) {
          if (this.user.settings[index].key === 'Default Theme') {
            sessionStorage.setItem(this.user.settings[index].key, this.user.settings[index].val);
            break;
          }
        }
        for (let other = 0; other < this.user.settings.length; other++) {
          if (this.user.settings[other].key !== 'Default Document View' && this.user.settings[other].key !== 'Default Theme') {
            sessionStorage.setItem(this.user.settings[other].key, this.user.settings[other].val);
          }
        }
      }
      this.router.navigateByUrl(`/users`);
    } else {
      this.signIn = true;
      document.getElementById('spinner').style.display = 'none';
    }
  }


  detectCapsLock(event) {
    if (event.getModifierState('CapsLock')) {
      this.capsLock = true;
    } else {
      this.capsLock = false;
    }
  }
  saveLoginDetails(event) {
    if (event === true) {
      this.showcookiepolicy = true;
    } else {
      this.showcookiepolicy = false;
    }
  }
  policy() {
    window.open('../../../../assets/CookiePolicy/Cookie Policy.html');
  }
}
