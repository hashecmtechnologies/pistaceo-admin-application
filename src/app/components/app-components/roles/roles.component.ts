import { Component, OnInit, ViewChild, Output, Input } from '@angular/core';
import { FormGroupDirective, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EventEmitter } from 'events';
import { AdministrationService } from '../../../service/Administration.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../../service/user.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ConfigurationService } from '../../../service/Configuration.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {
  @ViewChild(FormGroupDirective) formDirective: FormGroupDirective;
  @Output() showUserSpinner = new EventEmitter();
  @Output() userResult = new EventEmitter();
  @Output() RepositoryValue = new EventEmitter();
  @Input() selectedTree: any;
  @Input() addingNewNode: any;
  public formRole: FormGroup;
  public addUserToRoleForm: FormGroup;
  public repository: any;
  public rolesValue = [];
  public roleList = [];
  public rolesList = [];
  public isAdd = false;
  public isView = true;
  public roleNgModel: any;
  public selectedUser;
  public getUserRole: any;
  public websocketNotOpen = false;
  public dbDummyValue = [];
  public userTypeOptions: any;
  public deploymentType = 'NotCloud';
  public dbvalue;
  public existingSupervisor: any;
  public roles = [];
  public rolesToChoose = [];
  public rolesSelect: any;
  public displayRolePrivacy = [];
  public source = [];
  public username: any;
  public getUserToAdd;
  public showRoleUsers = [];
  public filterUser;
  public callingName = 'roles';
  constructor(private fb: FormBuilder, private as: AdministrationService, private tr: ToastrService, private us: UserService,
    public ngxSmartModalService: NgxSmartModalService, private loading: Ng4LoadingSpinnerService,
    private config: ConfigurationService) { }

  ngOnInit() {
    this.loading.show();
    this.us.getRoles().subscribe(data => { this.getRolesEvent(data); this.loading.hide(); }, error => { this.loading.hide(); });
    this.as.getTopLevelOrgUnit().subscribe(data => this.getOrgUnit(data), error => { });
    this.us.getUsers().subscribe(data => this.getAllUsersResult(data));
    this.formRole = this.fb.group({
      orgName: [null, Validators.compose([Validators.required])],
      name: [null, Validators.compose([Validators.required])],
      orgId: [null],
      id: [null],
      privacy: [0, Validators.compose([Validators.required])]
    });
    this.addUserToRoleForm = this.fb.group({
      userName: [null, Validators.compose([Validators.required])]
    });
    this.isView = true;
    this.isAdd = false;

  }
  clickedOnAdd() {
    this.isAdd = true;
    this.isView = false;
    this.formRole.reset();
  }
  clickedOnView() {
    // this.isAdd = false;
    // this.isView = true;
  }
  editRoles(event) {
    this.isAdd = false;
    this.isView = false;
    this.dbvalue = null;
    this.formRole.patchValue({
      name: event.data.name,
      orgId: event.data.orgId,
      orgName: event.data.orgName,
      id: event.data.id,
      privacy: 0
    });
    this.loading.show();
    this.us.getRoleMembers(this.formRole.controls.id.value).subscribe(data => { this.roleMemberResult(data); this.loading.hide(); }, error => { this.loading.hide(); });
  }
  getRolesEvent(data) {
    this.displayRolePrivacy = [];
    const getprivacy = JSON.parse(data._body);
    for (let i = 0; i < getprivacy.length; i++) {
      const matchprivacy = {
        id: getprivacy[i].id,
        name: getprivacy[i].name,
        orgName: getprivacy[i].orgName,
        privacy: 0,
        orgId: getprivacy[i].orgId
      };
      this.displayRolePrivacy.push(matchprivacy);
    }
    this.source = [];
    this.source = this.displayRolePrivacy;
  }
  getOrgUnit(data) {
    this.repository = JSON.parse(data._body);
    console.log("REPOSITORIES: " + this.repository);
    console.log("JSON REPOSITORIES: " + JSON.stringify(this.repository));
  }
  getAllUsersResult(data) {
    this.username = '';
    this.getUserToAdd = [];
    this.getUserToAdd = JSON.parse(data._body);
    this.addUserToRoleForm.patchValue({ userName: this.getUserToAdd[0].fulName });
  }
  editTree(event) {
    this.selectedTree = event;
    console.log("EVENT: " + event.label);
    if (this.selectedTree !== null) {
      this.formRole.patchValue({
        orgName: this.selectedTree.label,
        orgId: this.selectedTree.id
      });
      this.selectedTree = null;
    }
  }
  roleFormSubmitted() {
    const addRole = {
      'name': this.formRole.controls.name.value,
      'orgId': this.formRole.controls.orgId.value,
      'id': this.formRole.controls.id.value,
      'privacy': 0,
    };
    this.loading.show();
    this.us.saveRole(addRole).subscribe(data => { this.userSave(data); this.loading.hide(); }, error => { this.loading.hide(); });
  }
  userSave(data) {
    this.formRole.reset();
    this.ngOnInit();
    this.tr.success('', 'Role added successfully');
  }
  selectOrg() {
    this.tr.info('', 'Please select organization from organization tree');
  }
  cancelAdd() {
    this.formRole.reset();
    this.formRole.enable();
    this.formDirective.resetForm();
  }
  roleMemberResult(data) {
    this.showRoleUsers = [];
    this.showRoleUsers = JSON.parse(data._body);
    this.filteredUser();
  }
  filteredUser() {
    this.filterUser = [];
    if (this.showRoleUsers.length > 0) {
      for (let gt = 0; gt < this.getUserToAdd.length; gt++) {
        let flag = 0;
        for (let su = 0; su < this.showRoleUsers.length; su++) {
          if (this.showRoleUsers[su].EmpNo !== this.getUserToAdd[gt].EmpNo) {
            flag = 1;
          } else {
            flag = 0;
            break;
          }
        }
        if (flag === 1) {
          const objct = {
            value: this.getUserToAdd[gt].EmpNo,
            label: this.getUserToAdd[gt].fulName
          };
          this.filterUser.push(objct);
        }
      }
    } else {
      const objct = {
        value: this.getUserToAdd[0].EmpNo,
        label: this.getUserToAdd[0].fulName
      };
      this.filterUser.push(objct);
    }
  }
  userNamechanged(event) {

  }
  deleteConfirmed() {
    this.loading.show();
    this.us.removeUserFromRole(this.dbvalue.EmpNo, this.formRole.controls.id.value).subscribe(data => { this.callRoleUsers(data); this.tr.success('User Romoved'); this.loading.hide(); }, error => { this.loading.hide(); });
  }
  removeUser(value) {
    this.ngxSmartModalService.getModal('deleteRoleUsers').open();
    this.dbvalue = value;
  }
  callRoleUsers(data) {
    this.dbvalue = null;
    this.loading.show();
    this.us.getRoleMembers(this.formRole.controls.id.value).subscribe(datas => { this.roleMemberResult(datas); this.loading.hide(); }, error => { this.loading.hide(); });
  }
  addUserToRole(event) {
    this.loading.show();
    this.us.addUserToRole(event.value, this.formRole.controls.id.value).subscribe(data => { this.addedUsers(data); this.tr.success('User Added'); this.loading.hide(); }, error => { this.loading.hide(); });
  }
  addedUsers(data) {
    this.addUserToRoleForm.reset();
    this.addUserToRoleForm.patchValue({ userName: '' });
    this.callRoleUsers(data);
  }
  cancelRoleAdd() {
    this.addUserToRoleForm.reset();
    this.addUserToRoleForm.patchValue({ userName: '' });
  }

}
