import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DocumentService } from '../../../service/document.service';
import { SchemaService } from '../../../service/schema.service';
import { IntegrationService } from '../../../service/integration.service';
import { ContentService } from '../../../service/content.service';
import { ToastrService } from 'ngx-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { FormProps } from '../../../models/formProps.model';

@Component({
  selector: 'app-filter-forms',
  templateUrl: './filter-forms.component.html',
  styleUrls: ['./filter-forms.component.css']
})
export class FilterFormsComponent implements OnInit {
  public isAdd = false;
  public isView = true;
  public SimpleFormsResult = [];
  public editProp = false;
  public editPropname = '';
  public selectedIndex = -1;
  public docId: any;
  public adddatatableitems = false;
  public editDatatableItems = false;
  public addDatatableBtn = false;
  public datatableIndex = -1;
  public addDatatableForm: FormGroup;
  public datatableList = [];
  public simpleForm: FormGroup;
  public addFormProps: FormGroup;
  public propsList = [];
  public closeResult;
  public singleSelectDropdown;
  public multipleSelectDropdown;
  public singleSelectDropdownDisabled;
  public multipleSelectDropdownDisabled;
  public propsIndex;
  public selectedProp = null;
  public addProps = false;
  public calcArray = [];
  public autoFillArray = [];
  public dateDifferenceProps = [];
  public propTemplatesDrop = [];
  public selectedType = 'TEXT';
  public selectedActionDrop = 'NONE';
  public selectedPropType = 'TEXT';
  public showFormBuildTab = false;
  public formListTab = true;
  public addFormListTab = false;
  public templateDropDown = [];
  public typeDropDown = [];
  public uiTypeDropDown = [];
  public propNames = [];
  public dbTypeDropDown = [];
  public actionsDropDwon = [];
  public activeIndexNumber = 0;
  public proplookupArray = [];
  constructor(private fb: FormBuilder, private ds: DocumentService, private schemaService: SchemaService,
    private integrationservice: IntegrationService, private ss: SchemaService, private cs: ContentService,
    private tr: ToastrService, private spinner: Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.simpleForm = this.fb.group({
      name: [null, Validators.compose([Validators.required])]
    });
    this.typeDropDown = [
      { label: 'Text', value: 'TEXT' },
      { label: 'Number', value: 'NUMBER' },
      { label: 'Date', value: 'DATE' }
    ];
    this.dbTypeDropDown = [
      { label: 'Sql Server', value: 'SQLSERVER' },
      { label: 'Oracle', value: 'ORACLE' },
      { label: 'Mysql', value: 'MYSQL' }
    ];
    this.actionsDropDwon = [
      { label: 'None', value: 'NONE' },
      { label: 'Auto Fill', value: 'autoFill' }
    ];
    this.uiTypeDropDown = [
      { value: 'TEXT', label: 'Text' },
      { value: 'NUMBER', label: 'NUMBER' },
      { value: 'DATE', label: 'DATE' },
      { value: 'DATERANGE', label: 'Date Range' },
      { value: 'TIME', label: 'Time' },
      { value: 'LOOKUP', label: 'Lookup' },
      { value: 'LOOKUPCONDITION', label: 'Conditional Lookup' },
      { value: 'DBLOOKUP', label: 'DbLookup' },
      { value: 'TEXTAREA', label: 'Textarea' },
      { value: 'CHECKBOX', label: 'Checkbox' },
      { value: 'STEXTTA', label: 'Single Text Type Ahead' },
      { value: 'MTEXTTA', label: 'Multi Text Type Ahead' },
      { value: 'ROWSUM', label: 'Sum' },
      { value: 'MULTIPLICATION', label: 'Multiplication' },
      { value: 'ROWSUB', label: 'Subtraction' },
      { value: 'ROWDIV', label: 'Division' },
      { value: 'ROWAVG', label: 'Average' },
      { value: 'ROWDATEDIFF', label: 'Date Difference' }
    ];
    this.getSimpleFormsApi();
    this.callPropertyTemplates();
    this.addFormProps = this.fb.group({
      propname: [null, (Validators.maxLength(12), Validators.required)],
      proplabel: [null, (Validators.maxLength(15), Validators.required)],
      proptype: ['TEXT', Validators.required],
      proplength: [null],
      propreadonly: ['FALSE'],
      proprequired: ['FALSE'],
      propvisible: ['TRUE'],
      proplookup: [null],
      proplookupTemp: [null],
      propdburl: [null],
      propdbtype: ['SQLSERVER'],
      propuser: [null],
      proppassword: [null],
      propsql: [null],
      propnamecol: [null],
      propvaluecol: [null],
      propdbvalue: [null],
      propwidth: [null],
      condition: [[]],
      calc: [[]],
      colcalc: [{
        type: 'NONE',
        label: 'None'
      }],
      colcalcType: ['NONE'],
      fillprops: [[]],
      isDefaultDB: [true],
      propTemplate: [null, Validators.required],
      actiondrop: ['NONE'],
      uitype: ['TEXT', Validators.required],
      actions: [[]],
      propTemplateSelected: [{}]
    });
    this.addDatatableForm = this.fb.group({
      key: [null, Validators.required],
      value: [null, Validators.required]
    });
    this.selectedProp = null;
  }
  callPropertyTemplates() {
    this.cs.getPropertyTemplates().subscribe(data => { this.proptemdrop(data); });
  }
  proptemdrop(data) {
    this.propTemplatesDrop = [];
    this.propTemplatesDrop = JSON.parse(data._body);
    this.templateDropDown = [];
    const value = JSON.parse(data._body);
    value.forEach(element => {
      const template = {
        label: element.name,
        value: element.id
      };
      this.templateDropDown.push(template);
    });
  }
  getSimpleFormsApi() {
    this.spinner.show();
    this.ss.getSimpleForms().subscribe(data => { this.gotSimpleForms(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
  gotSimpleForms(data) {
    this.SimpleFormsResult = [];
    this.SimpleFormsResult = JSON.parse(data._body);
  }
  clickedOnAdd() {
    this.isAdd = true;
    this.isView = false;
    this.showFormBuildTab = true;
    this.addFormListTab = true;
    this.formListTab = false;
    this.simpleForm.reset();
    this.propsList = [];
  }
  clickedOnView() {
    this.isAdd = false;
    this.isView = true;
  }
  onFilterFormSelect(event) {
    this.isAdd = false;
    this.isView = false;
    this.docId = event.data.docId;
    this.spinner.show();
    this.callPropertyTemplates();
    this.selectedProp = null;
    this.addProps = false;
    this.adddatatableitems = false;
    this.editDatatableItems = false;
    this.addDatatableForm.reset();
    this.cs.getJSONFromDocument(event.data.docId).subscribe(data => { this.editSimpleFormValue(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
  editSimpleFormValue(data) {
    this.proplookupArray = [];
    this.selectedIndex = -1;
    const result = JSON.parse(data._body);
    this.simpleForm.patchValue({ name: result.name });
    this.propsList = [];
    this.propsList = result.properties;
    this.showFormBuildTab = true;
    this.addFormListTab = true;
    this.formListTab = false;
    this.datatableIndex = -1;
    this.datatableList = [];
    if (result.datatable !== undefined) {
      this.datatableList = result.datatable;
    }

  }
  onTabChange(event) {

  }
  onSimpleFormSubmit(simpleForm) {
    const sForm = {
      name: this.simpleForm.controls.name.value,
      properties: this.propsList,
      datatable: this.datatableList
    };
    const docInfo = {
      docclass: '',
      docTitle: this.simpleForm.controls.name.value,
      props: [{
        'name': 'Document Title',
        'symName': 'DocumentTitle',
        'dtype': 'STRING',
        'mvalues': [this.simpleForm.controls.name.value],
        'mtype': 'N',
        'len': 255,
        'rOnly': 'false',
        'hidden': 'false',
        'req': 'false'
      }],
      accessPolicies: []
    };
    const formData = new FormData();
    formData.append('DocInfo', JSON.stringify(sForm));
    this.spinner.show();
    if (this.isAdd) {
      this.ss.addSimpleForm(sForm).subscribe(data => { this.documentAdded(data); this.tr.success('Add Success'); this.spinner.hide(); }, error => { this.spinner.hide(); });
    } else {
      this.ds.checkOut(this.docId).subscribe(data => { this.checkinfile(data); });
    }
  }
  checkinfile(data) {
    const sForm = {
      name: this.simpleForm.controls.name.value,
      properties: this.propsList
    };
    const docInfo = {
      id: data._body,
      docclass: 'ProductivitiConfig',
      docTitle: this.simpleForm.controls.name.value,
      props: [{
        'name': 'Document Title',
        'symName': 'DocumentTitle',
        'dtype': 'STRING',
        'mvalues': [this.simpleForm.controls.name.value],
        'mtype': 'N',
        'len': 255,
        'rOnly': 'false',
        'hidden': 'false',
        'req': 'false'
      }],
      accessPolicies: []
    };
    const name = this.simpleForm.controls.name.value;
    const formData = new FormData();
    formData.append('DocInfo', JSON.stringify(docInfo));
    formData.append('file', new Blob([JSON.stringify(sForm)]), name);
    this.ds.checkIn(formData).subscribe(dataRes => { this.documentAdded(dataRes); this.tr.success('Edit Success'); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
  documentAdded(data) {
    this.addFormListTab = false;
    this.showFormBuildTab = false;
    this.formListTab = true;
    this.getSimpleFormsApi();
    this.isAdd = false;
    this.isView = true;
    // call the API and refresh list
  }

  addNewProperty(addNewProps) {
    this.adddatatableitems = false;
    this.editDatatableItems = false;
    this.addFormProps.reset();
    this.addFormProps.patchValue({
      proptype: 'TEXT', uitype: 'TEXT', propTemplate: this.propTemplatesDrop[0].id, propTemplateSelected: this.propTemplatesDrop[0],
      propname: this.propTemplatesDrop[0].symName, proplabel: this.propTemplatesDrop[0].name, propreadonly: 'FALSE',
      proprequired: 'FALSE', propvisible: 'TRUE', condition: [], actions: [], calc: [], colcalc: { type: 'NONE', label: 'None' },
      actiondrop: 'NONE'
    });
    this.addProps = true;
    this.editProp = false;
    this.selectedIndex = -1;
    this.selectedProp = null;
    if (this.propTemplatesDrop[0].dtype === 'INTEGER') {
      this.selectedType = 'NUMBER';
      this.selectedPropType = 'NUMBER';
      this.selectedTypeIsNumber();
      this.addFormProps.patchValue({ proptype: 'NUMBER', uitype: 'NUMBER' });
    } else if (this.propTemplatesDrop[0].dtype === 'STRING') {
      this.selectedType = 'TEXT';
      this.selectedPropType = 'TEXT';
      this.selectedTypeIsText();
      this.addFormProps.patchValue({ proptype: 'TEXT', uitype: 'TEXT' });
    } else if (this.propTemplatesDrop[0].dtype === 'DATE' || this.propTemplatesDrop[0].dtype === 'DATETIME') {
      this.selectedType = 'DATE';
      this.selectedPropType = 'DATE';
      this.selectedTypeIsDate();
      this.addFormProps.patchValue({ proptype: 'DATE', uitype: 'DATE' });
    }
  }
  selectedTypeIsNumber() {
    this.uiTypeDropDown = [];
    this.uiTypeDropDown = [
      { value: 'NUMBER', label: 'Number' },
      { value: 'CHECKBOX', label: 'Checkbox' },
      { value: 'ROWSUM', label: 'Sum' },
      { value: 'MULTIPLICATION', label: 'Multiplication' },
      { value: 'ROWSUB', label: 'Subtraction' },
      { value: 'ROWDIV', label: 'Division' },
      { value: 'ROWAVG', label: 'Average' },
    ];
  }
  selectedTypeIsText() {
    this.uiTypeDropDown = [];
    this.uiTypeDropDown = [
      { value: 'TEXT', label: 'Text' },
      { value: 'LOOKUP', label: 'Lookup' },
      { value: 'LOOKUPCONDITION', label: 'Conditional Lookup' },
      { value: 'DBLOOKUP', label: 'DbLookup' },
      { value: 'TEXTAREA', label: 'Textarea' },
      { value: 'CHECKBOX', label: 'Checkbox' },
      { value: 'STEXTTA', label: 'Single Text Type Ahead' },
      { value: 'MTEXTTA', label: 'Multi Text Type Ahead' },
    ];
  }
  selectedTypeIsDate() {
    this.uiTypeDropDown = [];
    this.uiTypeDropDown = [
      { value: 'DATE', label: 'Date' },
      { value: 'DATERANGE', label: 'Date Range' },
      { value: 'TIME', label: 'Time' },
      { value: 'ROWDATEDIFF', label: 'Date Difference' }
    ];
  }
  addDatatable() {
    this.adddatatableitems = true;
    this.editDatatableItems = false;
    this.addDatatableForm.reset();
    this.addProps = false;
    this.datatableIndex = -1;
    this.selectedIndex = -1;
  }
  propertyTemplateChanged(event) {
    for (let i = 0; i < this.propTemplatesDrop.length; i++) {
      if ((+this.propTemplatesDrop[i].id) === (+event.value)) {
        this.addFormProps.patchValue({ propTemplate: event.value, propname: this.propTemplatesDrop[i].symName, proplabel: this.propTemplatesDrop[i].name, propTemplateSelected: this.propTemplatesDrop[i] });
        if (this.propTemplatesDrop[i].dtype === 'INTEGER') {
          this.selectedType = 'NUMBER';
          this.selectedPropType = 'NUMBER';
          this.selectedTypeIsNumber();
          this.addFormProps.patchValue({ proptype: 'NUMBER', uitype: 'NUMBER' });
        } else if (this.propTemplatesDrop[i].dtype === 'STRING') {
          this.selectedType = 'TEXT';
          this.selectedPropType = 'TEXT';
          this.selectedTypeIsText();
          this.addFormProps.patchValue({ proptype: 'TEXT', uitype: 'TEXT' });
        } else if (this.propTemplatesDrop[i].dtype === 'DATE' || this.propTemplatesDrop[i].dtype === 'DATETIME') {
          this.selectedType = 'DATE';
          this.selectedPropType = 'DATE';
          this.selectedTypeIsDate();
          this.addFormProps.patchValue({ proptype: 'DATE', uitype: 'DATE' });
        }
        this.onTypeChange(event);
        break;
      }
    }
  }
  onTypeChange(event) {
    if (this.selectedType === 'TEXT') {
      this.selectedPropType = 'TEXT';
      this.addFormProps.patchValue({ uitype: 'TEXT' });
    } else if (this.selectedType === 'NUMBER') {
      this.selectedPropType = 'NUMBER';
      this.addFormProps.patchValue({ uitype: 'NUMBER' });
      this.addFormProps.controls.colcalcType.patchValue('NONE');
      this.addFormProps.controls.colcalc.patchValue({ type: 'NONE', label: 'None' });
    }
  }
  onUITypeChange(event) {
    this.selectedPropType = event.value;
    this.addFormProps.controls.calc.patchValue([]);
    this.addFormProps.controls.fillprops.patchValue([]);
    this.proplookupArray = [];
    this.addFormProps.patchValue({ propdburl: 'DEFAULT', propdbtype: 'SQLSERVER', propuser: 'DEFAULT', proppassword: 'DEFAULT', isDefaultDB: true });
    if (event.value === 'MULTIPLICATION' || event.value === 'AUTONUMBER' || event.value === 'ROWSUB' || event.value === 'ROWSUM' || event.value === 'ROWDIV' || event.value === 'ROWAVG') {
      this.pushRowCalced();
      this.addFormProps.controls.colcalcType.patchValue('NONE');
      this.addFormProps.controls.colcalc.patchValue({ type: 'NONE', label: 'None' });
      this.addFormProps.controls.propreadonly.disable();
      this.addFormProps.controls.propreadonly.patchValue('TRUE');
    } else {
      this.addFormProps.controls.propreadonly.enable();
    }
    if (event.value === 'NUMBER') {
      this.addFormProps.controls.colcalcType.patchValue('NONE');
      this.addFormProps.controls.colcalc.patchValue({ type: 'NONE', label: 'None' });
    } if (event.value === 'TEXTAF') {
      // this.textAutoFilled();
    }
    if (event.value === 'ROWDATEDIFF') {
      this.addFormProps.controls.propreadonly.disable();
      this.addFormProps.controls.propreadonly.patchValue('TRUE');
      this.addFormProps.controls.colcalcType.patchValue('NONE');
      this.addFormProps.controls.colcalc.patchValue({ type: 'NONE', label: 'None' });
      this.pushDateDiffValue();
    }
  }
  pushRowCalced() {
    this.calcArray = [];
    const arrayValue = this.propsList;
    for (let a = 0; a < arrayValue.length; a++) {
      for (let b = 0; b < arrayValue[a].length; b++) {
        if (arrayValue[a].type === 'NUMBER') {
          this.calcArray.push(arrayValue[a].name);
        }
      }
    }
  }
  pushDateDiffValue() {
    this.dateDifferenceProps = [];
    const arrayValue = this.propsList;
    for (let a = 0; a < arrayValue.length; a++) {
      for (let b = 0; b < arrayValue[a].length; b++) {
        if (arrayValue[a].type === 'DATE') {
          this.dateDifferenceProps.push(arrayValue[a].name);
        }
      }
    }
  }
  removeCalcValue(value) {
    const pushValue = this.addFormProps.value.calc;
    for (let d = 0; d < pushValue.length; d++) {
      if (pushValue[d] === value) {
        this.addFormProps.value.calc.splice(d, 1);
      }
    }
  }
  toggleCalced(id) {
    document.getElementById(id).classList.toggle('hide');
  }
  filterFunctionCalced(calcId, event) {
    let input, filter, a, i;
    input = document.getElementById(calcId);
    filter = input.children['0'].value.toUpperCase();
    const div = document.getElementById(calcId);
    a = div.getElementsByTagName('a');
    for (i = 0; i < a.length; i++) {
      const txtValue = a[i].textContent || a[i].innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = '';
      } else {
        a[i].style.display = 'none';
      }
    }
  }
  selectedOptionsCalced(options, index) {
    const pushValue = this.addFormProps.value.calc;
    let calcFlag = 0;
    if (pushValue.length > 0) {
      for (let a = 0; a < pushValue.length; a++) {
        if (pushValue[a] === options) {
          calcFlag = 0;
          break;
        } else {
          calcFlag = 1;
        }
      }
      if (calcFlag === 1 && (this.selectedPropType === 'ROWSUB' || this.selectedPropType === 'ROWDIV') && this.addFormProps.value.calc.length < 2) {
        this.addFormProps.value.calc.push(options);
      } else if (calcFlag === 1 && this.selectedPropType !== 'ROWSUB' && this.selectedPropType !== 'ROWDIV') {
        this.addFormProps.value.calc.push(options);
      } else {
        this.tr.warning('', 'Only two properties you can select');
      }
    } else {
      this.addFormProps.value.calc.push(options);
    }
  }
  dateDiffCalced(options, index) {
    const pushValue = this.addFormProps.value.calc;
    let calcFlag = 0;
    if (pushValue.length > 0) {
      for (let a = 0; a < pushValue.length; a++) {
        if (pushValue[a] === options) {
          calcFlag = 0;
          break;
        } else {
          calcFlag = 1;
        }
      }
      if (calcFlag === 1 && this.addFormProps.value.calc.length < 2) {
        this.addFormProps.value.calc.push(options);
      } else {
        this.tr.warning('', 'Only two properties you can select');
      }
    } else {
      this.addFormProps.value.calc.push(options);
    }
  }
  addValue(event, index) {
    const pushValue = this.addFormProps.controls.condition.value;
    for (let i = 0; i < pushValue.length; i++) {
      if (i === index) {
        pushValue[i].val = event.target.value;
      }

    }
    // this.addFormProps.controls.condition.value = pushValue;
  }
  removeShowValue(value, index) {
    const pushValue = this.addFormProps.controls.condition.value;
    for (let i = 0; i < pushValue.length; i++) {
      if (i === index) {
        for (let a = 0; a < pushValue[i].show.length; a++) {
          if (value === pushValue[i].show[a]) {
            pushValue[i].show.splice(a, 1);
          }
        }
      }
    }
  }
  showToggle(i) {
    document.getElementById(i).classList.toggle('show');
  }
  hideToggle(i, id) {
    document.getElementById(id).classList.toggle('hide');
  }
  toggleAutoFilled(id) {
    document.getElementById(id).classList.toggle('hide');
  }
  toggleActionAutoFilled(id, r) {
    document.getElementById(id).classList.toggle('hide');
  }
  filterFunction(index, event) {
    let input, filter, a, i;
    input = document.getElementById(index + 'id');
    filter = input.value.toUpperCase();
    const div = document.getElementById(index);
    a = div.getElementsByTagName('a');
    for (i = 0; i < a.length; i++) {
      const txtValue = a[i].textContent || a[i].innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = '';
      } else {
        a[i].style.display = 'none';
      }
    }
  }
  filterFunctionHide(index, event) {
    let input, filter, a, i;
    input = document.getElementById(index + 'idHide');
    filter = input.value.toUpperCase();
    const div = document.getElementById(index + 'hide');
    a = div.getElementsByTagName('a');
    for (i = 0; i < a.length; i++) {
      const txtValue = a[i].textContent || a[i].innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = '';
      } else {
        a[i].style.display = 'none';
      }
    }
  }
  filterFunctionAutoFilled(id, event) {
    let input, filter, a, i;
    input = document.getElementById(id);
    filter = input.children['0'].value.toUpperCase();
    const div = document.getElementById(id);
    a = div.getElementsByTagName('a');
    for (i = 0; i < a.length; i++) {
      const txtValue = a[i].textContent || a[i].innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = '';
      } else {
        a[i].style.display = 'none';
      }
    }
  }
  filterFunctionActionAutoFilled(id, event, index) {
    let input, filter, a, i;
    input = document.getElementById(id);
    filter = input.children['0'].value.toUpperCase();
    const div = document.getElementById(id);
    a = div.getElementsByTagName('a');
    for (i = 0; i < a.length; i++) {
      const txtValue = a[i].textContent || a[i].innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = '';
      } else {
        a[i].style.display = 'none';
      }
    }
  }
  selectedOptions(options, index) {
    const pushValue = this.addFormProps.controls.condition.value;
    for (let i = 0; i < pushValue.length; i++) {
      if (i === index) {
        for (let a = 0; a < pushValue[i].hide.length; a++) {
          if (pushValue[i].hide.length > 0 && options === pushValue[i].hide[a]) {
            pushValue[i].hide.splice(a, 1);
          }
        }
        let flag = true;
        for (let t = 0; t < pushValue[i].show.length; t++) {
          if (options !== pushValue[i].show[t]) {
            flag = true;
          } else {
            flag = false;
            break;
          }
        }
        if (flag) {
          pushValue[i].show.push(options);
        }
      }
    }
    // this.arrayToShow[0].sections[this.secIndex].columns[this.colIndex].properties[this.propsIndex].condition = pushValue;
  }
  selectedOptionsHide(options, index) {
    const pushValue = this.addFormProps.controls.condition.value;
    for (let i = 0; i < pushValue.length; i++) {
      if (i === index) {
        for (let a = 0; a < pushValue[i].show.length; a++) {
          if (pushValue[i].show.length > 0 && options === pushValue[i].show[a]) {
            pushValue[i].show.splice(a, 1);
          }
        }
        let flag = true;
        for (let t = 0; t < pushValue[i].hide.length; t++) {
          if (options !== pushValue[i].hide[t]) {
            flag = true;
          } else {
            flag = false;
            break;
          }
        }
        if (flag) {
          pushValue[i].hide.push(options);
        }
      }
    }
    // this.arrayToShow[0].sections[this.secIndex].columns[this.colIndex].properties[this.propsIndex].condition = pushValue;
  }
  removeHideValue(value, index) {
    const pushValue = this.addFormProps.controls.condition.value;
    for (let i = 0; i < pushValue.length; i++) {
      if (i === index) {
        for (let a = 0; a < pushValue[i].hide.length; a++) {
          if (value === pushValue[i].hide[a]) {
            pushValue[i].hide.splice(a, 1);
          }
        }
      }
    }
    // this.arrayToShow[0].sections[this.secIndex].columns[this.colIndex].properties[this.propsIndex].condition = pushValue;
  }
  addCondition() {
    const pushvalue = {
      val: '',
      show: [],
      hide: []
    };
    if (this.addFormProps.controls.condition.value === undefined) {
      this.addFormProps.patchValue({ condition: [] });
    }
    this.addFormProps.controls.condition.value.push(pushvalue);

  }
  deafultDbChanged(event) {
    this.addFormProps.controls.isDefaultDB.patchValue(event);
    // console.log(this.addFormProps.value.isDefaultDB);
    if (event === true) {
      this.addFormProps.patchValue({ propdburl: 'DEFAULT', propdbtype: 'SQLSERVER', propuser: 'DEFAULT', proppassword: 'DEFAULT', isDefaultDB: true });
    }
  }
  deafultActionDbChanged(event, act, index) {
    this.addFormProps.controls.actions.value[index].isDefaultDB = event;
    if (event === true) {
      this.addFormProps.controls.actions.value[index].autoFill.dburl = 'DEFAULT';
      this.addFormProps.controls.actions.value[index].autoFill.dbtype = 'SQLSERVER';
      this.addFormProps.controls.actions.value[index].autoFill.user = 'DEFAULT';
      this.addFormProps.controls.actions.value[index].autoFill.password = 'DEFAULT';
    }
  }
  onActionDbTypeChanged(event, index) {
    this.addFormProps.controls.actions.value[index].autoFill.dbtype = event.target.value;
  }
  dbUrl(event, index) {
    this.addFormProps.controls.actions.value[index].autoFill.dburl = event.value;
  }
  dbUser(event, index) {
    this.addFormProps.controls.actions.value[index].autoFill.user = event.value;
  }
  dbPassword(event, index) {
    this.addFormProps.controls.actions.value[index].autoFill.password = event.value;
  }
  dbSql(event, index) {
    this.addFormProps.controls.actions.value[index].autoFill.sql = event.value;
  }
  onDbTypeChanged(event) {
    this.addFormProps.patchValue({ propdbtype: event.value });
  }
  removeAutoFilledValue(value) {
    const autoFilled = this.addFormProps.value.fillprops;
    for (let af = 0; af < autoFilled.length; af++) {
      if (autoFilled[af] === value) {
        this.addFormProps.value.fillprops.splice(af, 1);
      }
    }
  }
  selectedOptionsAutoFilled(options, index) {
    const pushValue = this.addFormProps.value.fillprops;
    let AfFlag = 0;
    if (pushValue.length > 0) {
      for (let a = 0; a < pushValue.length; a++) {
        if (pushValue[a] === options) {
          AfFlag = 0;
          break;
        } else {
          AfFlag = 1;
        }
      }
      if (AfFlag === 1 && this.addFormProps.value.fillprops.length < 4) {
        this.addFormProps.value.fillprops.push(options);
      } else if (AfFlag === 1 && this.addFormProps.value.fillprops.length === 4) {
        this.tr.warning('', 'Only 4 parameters you can have');
      }
    } else {
      this.addFormProps.value.fillprops.push(options);
    }
  }
  actionTypeChanged(event) {
    this.selectedActionDrop = event.value;
    if (event.value === 'autoFill') {
      // this.textAutoFilled();
      this.addFormProps.patchValue({ actions: [] });
      const actionOne = {
        action: 'autoFill',
        isDefaultDB: true,
        autoFill: {
          dburl: 'DEFAULT',
          dbtype: 'SQLSERVER',
          user: 'DEFAULT',
          password: 'DEFAULT',
          sql: '',
          fillprops: [],
          fillparams: [],
        }
      };
      this.addFormProps.controls.actions.value.push(actionOne);
    } else {
      this.addFormProps.patchValue({ actions: [] });
    }
  }
  addAction() {
    const actionOne = {
      action: 'autoFill',
      isDefaultDB: true,
      autoFill: {
        dburl: 'DEFAULT',
        dbtype: 'SQLSERVER',
        user: 'DEFAULT',
        password: 'DEFAULT',
        sql: '',
        fillprops: [],
        fillparams: [],
      }
    };
    this.addFormProps.controls.actions.value.push(actionOne);
  }
  removeActionAutoFilledValue(value, r) {
    const autoFilled = this.addFormProps.controls.actions.value[r].autoFill.fillprops;
    for (let af = 0; af < autoFilled.length; af++) {
      if (autoFilled[af] === value) {
        this.addFormProps.controls.actions.value[r].autoFill.fillprops.splice(af, 1);
      }
    }
  }
  selectedOptionsActionAutoFilled(options, index, r) {
    const pushValue = this.addFormProps.controls.actions.value[r].autoFill.fillprops;
    let AfFlag = 0;
    if (pushValue.length > 0) {
      for (let a = 0; a < pushValue.length; a++) {
        if (pushValue[a] === options) {
          AfFlag = 0;
          break;
        } else {
          AfFlag = 1;
        }
      }
      if (AfFlag === 1 && this.addFormProps.controls.actions.value[r].autoFill.fillprops.length < 4) {
        this.addFormProps.controls.actions.value[r].autoFill.fillprops.push(options);
      } else if (AfFlag === 1 && this.addFormProps.controls.actions.value[r].autoFill.fillprops.length === 4) {
        this.tr.warning('', 'Only 4 parameters you can have');
      }
    } else {
      this.addFormProps.controls.actions.value[r].autoFill.fillprops.push(options);
    }
  }
  addProperties(props) {
    const time = new Date();
    const lookup = [];
    let number = 1;
    let splitproplookup = [];

    if (props.value.proplookup !== null && props.value.proplookup !== undefined) {
      if (props.value.proplookup.includes(',')) {
        splitproplookup = props.value.proplookup.split(',');
      } else {
        splitproplookup.push(props.value.proplookup);
      }

      for (let u = 0; u < splitproplookup.length; u++) {
        const propss = {
          name: splitproplookup[u],
          value: number
        };
        lookup.push(propss);
        number++;
      }
    }
    const propts = new FormProps();

    //  propts.unique = time.getTime();
    propts.name = props.value.propname;
    propts.label = props.value.proplabel;
    propts.value = [];
    propts.type = props.value.proptype;
    propts.length = props.value.proplength;
    propts.rOnly = props.value.propreadonly;
    propts.req = props.value.proprequired;
    propts.visible = props.value.propvisible;
    propts.uitype.uitype = props.value.uitype;
    propts.prop = props.value.propTemplateSelected;
    propts.action = props.value.actiondrop;
    propts.actions = props.value.actions;
    if (props.value.uitype === 'LOOKUP' || props.value.uitype === 'LOOKUPCONDITION') {
      propts.uitype.lookups = lookup;
      delete propts.uitype.dblookup;
      delete propts.dbvalue;
      delete propts.length;
      if (props.value.uitype === 'LOOKUPCONDITION') {
        propts.uitype.condition = props.value.condition;
      }
    } if (props.value.uitype === 'DBLOOKUP' || props.value.uitype === 'MTEXTTA' || props.value.uitype === 'STEXTTA') {
      //  const filter = 'empty';
      propts.uitype.dblookup.dburl = props.value.propdburl;
      propts.uitype.dblookup.dbtype = props.value.propdbtype;
      propts.uitype.dblookup.sql = props.value.propsql;
      propts.uitype.dblookup.user = props.value.propuser;
      propts.uitype.dblookup.password = props.value.proppassword;
      propts.uitype.dblookup.filter = null;
      propts.uitype.dblookup.namecol = props.value.propnamecol;
      propts.uitype.dblookup.valuecol = props.value.propvaluecol;
      propts.isDefaultDB = props.value.isDefaultDB;
      propts.dbvalue = [];
      delete propts.uitype.lookups;
      delete propts.length;
    } if (props.value.uitype === 'TEXTAF') {
      propts.uitype.autofill.dburl = props.value.propdburl;
      propts.uitype.autofill.dbtype = props.value.propdbtype;
      propts.uitype.autofill.sql = props.value.propsql;
      propts.uitype.autofill.user = props.value.propuser;
      propts.uitype.autofill.password = props.value.proppassword;
      propts.uitype.autofill.fillprops = props.value.fillprops;
      propts.isDefaultDB = props.value.isDefaultDB;
      // propts.autofill.filter = null;
      // propts.autofill.namecol = props.value.propnamecol;
      // propts.autofill.valuecol = props.value.propvaluecol;
    } if (props.value.uitype !== 'DBLOOKUP' && props.value.uitype !== 'MTEXTTA' && props.value.uitype !== 'STEXTTA' && props.value.uitype !== 'LOOKUP' && props.value.uitype !== 'LOOKUPCONDITION') {
      delete propts.uitype.dblookup;
      delete propts.dbvalue;
      delete propts.uitype.lookups;
      delete propts.uitype.condition;
      // delete propts.isDefaultDB;
    } if (props.value.uitype !== 'TEXTAF') {
      delete propts.uitype.autofill;
      // delete propts.isDefaultDB;

    } if (props.value.uitype === 'MULTIPLICATION' || props.value.uitype === 'ROWSUB' || props.value.uitype === 'ROWSUM' || props.value.uitype === 'ROWDIV' || props.value.uitype === 'ROWAVG' || props.value.uitype === 'ROWDATEDIFF') {
      propts.uitype.calc = props.value.calc;
      propts.uitype.colcalc = props.value.colcalc;
      propts.rOnly = 'TRUE';
      delete propts.colcalcType;
    }
    if (props.value.uitype !== 'MULTIPLICATION' && props.value.uitype !== 'ROWSUB' && props.value.uitype !== 'ROWSUM' && props.value.uitype !== 'ROWDIV' && props.value.uitype !== 'ROWAVG' && props.value.uitype !== 'ROWDATEDIFF') {
      delete propts.uitype.calc;
      delete propts.uitype.colcalc;
      delete propts.colcalcType;
    }
    // delete propts.colcalc;
    // delete propts.calc;
    // delete propts.condition;
    // delete propts.dblookup;
    // delete propts.lookups;
    // delete propts.autofill;
    if (this.selectedProp === null || this.selectedProp === undefined) {
      this.propsList.push(propts);
      this.selectedProp = null;
    } else {
      for (let index = 0; index < this.propsList.length; index++) {
        if (this.propsList[index].name === this.selectedProp.name) {
          this.propsList[index] = propts;
          break;
        }
      }
    }
    if (props.value.proptype === 'DBLOOKUP') {
      this.propsIndex = this.propsList.findIndex(x => x.name === props.value.propname);
      this.integrationservice.getDBLookup(this.propsList[this.propsIndex].dblookup).subscribe(res => this.assignDblookupvalue(res, this.propsIndex));
    }
    this.addFormProps.reset();
    this.addFormProps.patchValue({ proptype: 'TEXT', propreadonly: 'FALSE', proprequired: 'FALSE' });
    this.addProps = false;
    this.selectedIndex = -1;
  }
  assignDblookupvalue(res, k) {
    const value = JSON.parse(res._body);
    for (let val = 0; val < value.length; val++) {
      if (val === 0) {
        // const elementval = {
        //   'id': value[val].value,
        //   'itemName': value[val].label
        // };
      }
      const elementval = {
        'id': value[val].value,
        'itemName': value[val].label
      };
      this.propsList[k].dbvalue.push(elementval);
    }
  }
  clickNo() {
    this.selectedIndex = -1;
    this.addProps = false;
    this.editProp = false;
  }
  clickedOndatatable(datatable, index) {
    this.addProps = false;
    this.selectedIndex = -1;
    this.datatableIndex = index;
    this.editDatatableItems = true;
    this.adddatatableitems = false;
    for (let i = 0; i < this.datatableList.length; i++) {
      if (i === this.datatableIndex) {
        this.addDatatableForm.patchValue({ key: datatable.key, value: datatable.value });
      }
    }
  }
  saveDatatable(datatable) {
    const data = {
      'key': datatable.value.key,
      'value': datatable.value.value
    };
    this.datatableList.push(data);
    this.addDatatableForm.reset();
    this.datatableIndex = -1;
    this.selectedIndex = -1;
  }
  editDatatable(datatable, editValue) {
    this.selectedIndex = -1;
    for (let i = 0; i < this.datatableList.length; i++) {
      if (i === this.datatableIndex) {
        if (editValue === 'EDIT') {
          this.datatableList[i].key = datatable.value.key;
          this.datatableList[i].value = datatable.value.value;
          break;
        } else {
          this.datatableList.splice(i, 1);
          this.datatableIndex = -1;
          this.addDatatableForm.reset();
          this.adddatatableitems = true;
          this.editDatatableItems = false;
          break;
        }
      }


    }
  }
  deleteListProps(i) {
    this.propsList.splice(i, 1);
  }

  editListProps(k, editProps) {
    this.activeIndexNumber = 1;
    setTimeout(() => {
      this.activeIndexNumber = 0;
    }, 0);
    this.selectedProp = this.propsList[k];
    this.editProp = true;
    this.editPropname = this.selectedProp.label;
    const prop = this.selectedProp;
    this.selectedIndex = k;
    this.selectedType = prop.type;
    this.selectedPropType = prop.uitype.uitype;
    let lookupvalue;
    this.proplookupArray = [];
    this.adddatatableitems = false;
    this.editDatatableItems = false;
    // if (this.editIs) {
    //   for (let i = 0; i < this.propTemplatesDrop.length; i++) {
    //     if (this.propTemplatesDrop[i].id === prop.propTemplate) {

    //     }
    //   }
    // } else {
    //   this.addFormProps.patchValue({ propTemplate: this.propTemplatesDrop[0].id, proplabel: this.propTemplatesDrop[0].name, propname: this.propTemplatesDrop[0].symName })
    // }
    if (this.selectedPropType === 'LOOKUP' || this.selectedPropType === 'LOOKUPCONDITION') {
      lookupvalue = '';
      for (let i = 0; i < prop.uitype.lookups.length; i++) {
        if (i === 0) {
          lookupvalue = prop.uitype.lookups[i].name;
        } else {
          lookupvalue = lookupvalue + ',' + prop.uitype.lookups[i].name;
        }
      }
      prop.uitype.lookups.forEach(element => {
        this.proplookupArray.push(element.name);
      });
    }
    this.addFormProps.patchValue({
      propTemplate: prop.prop.id, uitype: prop.uitype.uitype, actiondrop: prop.action,
      actions: prop.actions, propTemplateSelected: prop.prop, propname: prop.name, proplabel: prop.label, proptype: prop.type,
      proplength: prop.length, propwidth: prop.width, propreadonly: prop.rOnly, propvisible: prop.visible, proprequired: prop.req,
      proplookup: lookupvalue
    });
    if (this.selectedPropType === 'LOOKUPCONDITION') {
      this.addFormProps.patchValue({ condition: prop.uitype.condition });
    } else if (prop.uitype.uitype === 'DBLOOKUP' || prop.uitype.uitype === 'STEXTTA' || prop.uitype.uitype === 'MTEXTTA') {
      this.addFormProps.patchValue({
        propdburl: prop.uitype.dblookup.dburl, propdbtype: prop.uitype.dblookup.dbtype,
        propuser: prop.uitype.dblookup.user, proppassword: prop.uitype.dblookup.password, propsql: prop.uitype.dblookup.sql,
        propnamecol: prop.uitype.dblookup.namecol, propvaluecol: prop.uitype.dblookup.valuecol, isDefaultDB: prop.isDefaultDB
      });
      if (prop.uitype.dblookup.dburl === 'DEFAULT') {
        if (prop.uitype.dblookup.dbtype) {
          this.addFormProps.patchValue({ propdbtype: prop.uitype.dblookup.dbtype });
        } else {
          this.addFormProps.patchValue({ propdbtype: 'SQLSERVER' });
        } if (prop.uitype.dblookup.user) {
          this.addFormProps.patchValue({ propuser: prop.uitype.dblookup.user });
        } else {
          this.addFormProps.patchValue({ propuser: 'DEFAULT' });
        } if (prop.uitype.dblookup.password) {
          this.addFormProps.patchValue({ proppassword: prop.uitype.dblookup.password });
        } else {
          this.addFormProps.patchValue({ proppassword: 'DEFAULT' });
        }
      }
    } else if (prop.uitype.uitype === 'TEXTAF') {
      this.addFormProps.patchValue({
        propdburl: prop.uitype.autofill.dburl, propdbtype: prop.uitype.autofill.dbtype,
        propuser: prop.uitype.autofill.user, proppassword: prop.uitype.autofill.password, propsql: prop.uitype.autofill.sql,
        fillprops: prop.uitype.autofill.fillprops, isDefaultDB: prop.isDefaultDB
      });
      if (prop.uitype.autofill.dburl === 'DEFAULT') {
        if (prop.uitype.autofill.dbtype) {
          this.addFormProps.patchValue({ propdbtype: prop.uitype.autofill.dbtype });
        } else {
          this.addFormProps.patchValue({ propdbtype: 'SQLSERVER' });
        } if (prop.uitype.autofill.user) {
          this.addFormProps.patchValue({ propuser: prop.uitype.autofill.user });
        } else {
          this.addFormProps.patchValue({ propuser: 'DEFAULT' });
        } if (prop.uitype.autofill.password) {
          this.addFormProps.patchValue({ proppassword: prop.uitype.autofill.password });
        } else {
          this.addFormProps.patchValue({ proppassword: 'DEFAULT' });
        }
      }
    } else if (this.selectedPropType === 'MULTIPLICATION' || this.selectedPropType === 'AUTONUMBER' || this.selectedPropType === 'ROWSUB' || this.selectedPropType === 'ROWSUM' || this.selectedPropType === 'ROWDIV' || this.selectedPropType === 'ROWAVG') {
      this.addFormProps.controls.propreadonly.disable();
      this.addFormProps.patchValue({ calc: prop.uitype.calc, propreadonly: 'TRUE' });
      if (this.selectedPropType !== 'AUTONUMBER') {
        this.pushRowCalced();
      }
    } else if (this.selectedPropType === 'ROWDATEDIFF') {
      this.addFormProps.patchValue({ calc: prop.uitype.calc, propreadonly: 'TRUE' });

      this.addFormProps.controls.propreadonly.disable();
      this.pushDateDiffValue();
    } else {
      this.addFormProps.controls.propreadonly.enable();
    }
    // if (prop.uitype.uitype === 'TEXTAF') {
    //   this.textAutoFilled();
    // }
    this.addProps = true;
  }
  lookupEntered(value) {
    this.proplookupArray.push(value);
    this.addFormProps.patchValue({ proplookup: this.proplookupArray.toString(), proplookupTemp: '' });
  }
  removeLookupValue(value, i) {
    this.proplookupArray.splice(i, 1);
    this.addFormProps.patchValue({ proplookup: this.proplookupArray.toString() });
  }
}
