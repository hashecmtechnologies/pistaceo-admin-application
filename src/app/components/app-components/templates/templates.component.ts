import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormGroupDirective, FormBuilder, Validators } from '@angular/forms';
import { FileUploader } from 'ng2-file-upload';
import { DocumentService } from '../../../service/document.service';
import { SchemaService } from '../../../service/schema.service';
import { ToastrService } from 'ngx-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-templates',
  templateUrl: './templates.component.html',
  styleUrls: ['./templates.component.css']
})
export class TemplatesComponent implements OnInit {
  @ViewChild(FormGroupDirective) formDirective: FormGroupDirective;
  public templateList = [];
  public templateform: FormGroup;
  public templateUploder = new FileUploader({});
  public selectedTempelate;
  public isAdd = false;
  public isView = true;
  public isDownloadClicked = false;
  constructor(private ds: DocumentService, private schemaService: SchemaService, private fb: FormBuilder, private tr: ToastrService,
    private spinner: Ng4LoadingSpinnerService, private ngxSmartModalService: NgxSmartModalService) { }

  ngOnInit() {
    this.spinner.show();
    this.templateform = this.fb.group({
      name: [null, Validators.compose([Validators.required])],
      docid: [null]
    });
    this.schemaService.getTemplates().subscribe(data => { this.getAllTemplates(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
    this.isDownloadClicked = false;
  }
  getAllTemplates(data) {
    this.templateList = JSON.parse(data._body);
  }
  clickedOnAdd() {
    this.isAdd = true;
    this.isView = false;
    this.templateform.reset();
    this.tempObject();
  }
  clickedOnView() {
    this.isView = true;
    this.isAdd = false;
  }
  editTemplate(event) {
    this.templateUploder = new FileUploader({});
    this.selectedTempelate = event.data;
    setTimeout(() => {
      if (!this.isDownloadClicked) {
        this.isView = false;
        this.isAdd = false;
      } else {
        this.isView = true;
        this.isAdd = false;
        this.ds.downloadDocument(this.selectedTempelate.docId)
          .subscribe(data => { this.downloadCurrentDocument(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
      }
    }, 100);
    this.templateform.patchValue({
      name: this.selectedTempelate.name,
      docid: this.selectedTempelate.docId
    });
  }
  download() {
    this.spinner.show();
    this.isDownloadClicked = true;
  }

  downloadCurrentDocument(data) {
    let filename = '';
    const disposition = data
      .headers
      .get('Content-Disposition');
    const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
    const matches = filenameRegex.exec(disposition);
    if (matches != null && matches[1]) {
      filename = matches[1].replace(/['"]/g, '');
    }
    saveAs(data._body, filename);
    this.tr.success('', 'Download Success');
    this.isDownloadClicked = false;
  }
  templateSubmitted(event) {
    const docInfo = {
      docclass: 'ProductivitiTemplates',
      docTitle: this.templateform.controls.name.value,
      props: [{
        'name': 'Document Title',
        'symName': 'DocumentTitle',
        'dtype': 'STRING',
        'mvalues': [this.templateform.controls.name.value],
        'mtype': 'N',
        'len': 255,
        'rOnly': 'false',
        'hidden': 'false',
        'req': 'false'
      }],
    };
    const fileReader = new FileReader();
    const formData = new FormData();
    formData.append('DocInfo', JSON.stringify(docInfo));
    if (this.templateUploder.queue.length > 0) {
      formData.append('file', this.templateUploder.queue['0']._file);
    }
    if (!this.isAdd) {
      this.spinner.show();
      if (this.templateUploder.queue.length > 0) {
        this.ds.checkOut(this.selectedTempelate.docId).subscribe(data => { this.templateCheckout(data); }, error => { this.spinner.hide(); });
      } else {
        const saveName = {
          id: this.selectedTempelate.id,
          name: this.templateform.value.name,
          docId: this.selectedTempelate.docId
        };
        this.schemaService.saveConfigDocument(saveName).subscribe(nameData => { this.tr.success('Template name updated'); this.documentAdded(nameData); this.spinner.hide(); }, error => { });
      }
    } else {
      this.spinner.show();
      this.schemaService.addTemplate(formData).subscribe(data => { this.tr.success('Added new template'); this.documentAdded(data); this.spinner.hide(); }, error => { this.tempObject(); this.spinner.hide(); });

    }
  }

  templateCheckout(data) {
    const docInfo = {
      id: data._body,
      docclass: 'ProductivitiTemplates',
      docTitle: this.templateform.controls.name.value,
      props: [{
        'name': 'Document Title',
        'symName': 'DocumentTitle',
        'dtype': 'STRING',
        'mvalues': [this.templateform.controls.name.value],
        'mtype': 'N',
        'len': 255,
        'rOnly': 'false',
        'hidden': 'false',
        'req': 'false'
      }],
    };
    const fileReader = new FileReader();
    const formData = new FormData();
    formData.append('DocInfo', JSON.stringify(docInfo));
    formData.append('file', this.templateUploder.queue['0']._file);
    this.ds.checkIn(formData).subscribe(datas => {
      this.tr.success('Template updated'); this.documentAdded(datas);
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }
  documentAdded(data) {
    this.isAdd = false;
    this.isView = true;
    this.ngOnInit();
  }
  inputfileChanged(event) {
  }
  tempObject() {
    this.templateUploder = new FileUploader({});
  }
  cancelAdd() {
    this.templateform.reset();
    this.formDirective.resetForm();
    this.templateUploder = new FileUploader({});
  }
}
