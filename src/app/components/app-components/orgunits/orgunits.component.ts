import { Component, OnInit, ViewChild, EventEmitter, Output, Input } from '@angular/core';
import { FormGroupDirective, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AdministrationService } from '../../../service/Administration.service';
import { UserService } from '../../../service/user.service';
import { ToastrService } from 'ngx-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-orgunits',
  templateUrl: './orgunits.component.html',
  styleUrls: ['./orgunits.component.css']
})
export class OrgunitsComponent implements OnInit {
  @ViewChild(FormGroupDirective) formDirective: FormGroupDirective;
  @Output() showUserSpinner = new EventEmitter();
  @Output() userResult = new EventEmitter();
  @Output() RepositoryValue = new EventEmitter();
  @Input() selectedTree: any;
  @Input() addingNewNode: any;
  public isAdd = false;
  public isView = true;
  public repository: any;
  public orgForm: FormGroup;
  public getrolecategory;
  public rolesList;
  public roleMembers;
  public rolesSelect;
  public getRolesResult;
  constructor(private as: AdministrationService, private fb: FormBuilder, private us: UserService, private tr: ToastrService, private loading: Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.loading.show();
    this.rolesSelect = {};
    this.as.getTopLevelOrgUnit().subscribe(data => { this.getOrgUnit(data); this.loading.hide(); }, error => { this.loading.hide(); });
    this.us.getRoles().subscribe(data => { this.getRoles(data); this.getrolesforcategory(data); }, error => { });
    this.orgForm = this.fb.group({
      name: [null, Validators.compose([Validators.required, Validators.minLength(3)])],  // , Validators.pattern('[a-zA-z_0-9]*')
      orgId: [null],
      parent: [null],
      parentName: [null],
      orgName: [null, Validators.compose([Validators.required])],
      headEmpNo: [null],
      headEmpName: [null],
      headRoleId: [null],
      headRoleName: [null],
      headRoleIdName: [null],
      type: [null],
      orgCode: [null],
      id: [null]
    });
  }
  getRoles(data) {
    this.rolesList = [];
    const value = JSON.parse(data._body);
    this.getRolesResult = [];
    this.getRolesResult = JSON.parse(data._body);
    if (value && value.length > 0) {
      value.forEach(element => {
        const pushed = {
          value: element.id,
          label: element.name
        };
        this.rolesList.push(pushed);
      });
    }
  }
  getrolesforcategory(data) {
    if (data._body !== '') {
      this.getrolecategory = JSON.parse(data._body);
    }
  }
  roleValueChanged(event) {
    let value;
    this.getRolesResult.forEach(element => {
      if (element.id === event.value) {
        value = element;
        return false;
      }
    });
    if (event.value !== undefined) {
      this.orgForm.patchValue({
        headRoleId: value.id,
        headRoleIdName: value.name,
        headRoleName: value
      });
      this.us.getRoleMembers(event.value).subscribe(data => this.getRoleMembers(data));
    }
  }
  getRoleMembers(data) {
    this.roleMembers = [];
    const value = JSON.parse(data._body);
    if (value && value.length > 0) {
      value.forEach(element => {
        const pushed = {
          value: element.EmpNo,
          label: element.fulName
        };
        this.roleMembers.push(pushed);
      });
    }
    if (this.roleMembers.length > 0) {
      this.orgForm.patchValue({
        headEmpName: this.roleMembers[0].label,
        headEmpNo: this.roleMembers[0].value,
      });
    }

  }
  clickedOnAdd() {
    this.isAdd = true;
    this.isView = false;
    this.orgForm.reset();
    this.orgForm.enable();
    this.selectedTree = undefined;
    this.as.getTopLevelOrgUnit().subscribe(data => { this.getOrgUnit(data); this.loading.hide(); }, error => { this.loading.hide(); });
  }
  clickedOnView() {
    this.isView = true;
    this.isAdd = false;
    this.ngOnInit();
  }
  selectOrg() {
    if (this.orgForm.controls['parentName'].enabled) {
      this.tr.info('', 'Please select parent organization from organization tree');
    }
  }
  orgFormSubmitted(value) {
    if (!this.isAdd) {
      console.log("SUBMIT IFFF");
      const orgEdit = {
        parent: this.orgForm.controls.parent.value,
        name: this.orgForm.controls.name.value,
        hid: this.orgForm.value.hid,
        type: this.orgForm.controls.type.value,
        headEmpNo: this.orgForm.controls.headEmpNo.value,
        headRoleId: this.orgForm.controls.headRoleId.value,
        offset: this.orgForm.value.offset,
        count: this.orgForm.value.count,
        orgCode: this.orgForm.controls.orgCode.value,
        id: this.orgForm.controls.id.value
      };
      this.as.getSubLevelOrgUnits(this.repository.id).subscribe(resdata => this.getTopLevelOrgUnit(resdata, orgEdit));
    } else {
      console.log("SUBMIT ELSEE");
      if (this.selectedTree.id !== undefined) {
        console.log("SUBMIT ELSEEE IFFF selectedTree: " + (this.selectedTree.id) + "name" + (this.selectedTree.name) + "parentName: " + (this.selectedTree.parentName));
        const org = {
          parent: this.selectedTree.id,
          name: this.orgForm.controls.name.value,
          hid: this.selectedTree.hid,
          type: this.selectedTree.type,
          headEmpNo: this.orgForm.controls.headEmpNo.value,
          headRoleId: this.orgForm.controls.headRoleId.value,
          offset: this.selectedTree.offset,
          count: this.selectedTree.count,
          orgCode: this.orgForm.controls.orgCode.value,
        };
        this.as.getSubLevelOrgUnits(this.repository.id).subscribe(resdata => this.getTopLevelOrgUnit(resdata, org));
      }

      if (this.selectedTree.id === undefined) { //This is just for the top level directory
        console.log("SUBMIT ELSEEE IFFF selectedTree: " + (this.selectedTree.id) + "name" + (this.selectedTree.name) + "parentName: " + (this.selectedTree.parentName));
        const org = {
          parent: this.repository.id,
          name: this.orgForm.controls.name.value,
          hid: this.repository.hid,
          // type: this.selectedTree.type,
          headEmpNo: this.orgForm.controls.headEmpNo.value,
          headRoleId: this.orgForm.controls.headRoleId.value
          // offset: this.selectedTree.offset,
          // count: this.selectedTree.count,
          // orgCode: this.orgForm.controls.orgCode.value,
        };
        this.as.getSubLevelOrgUnits(this.repository.id).subscribe(resdata => this.getTopLevelOrgUnit(resdata, org));
      }
      
    }

  }
  getTopLevelOrgUnit(resdata, org) {
    const value = JSON.parse(resdata._body);
    let flag = 0;
    for (let a = 0; a < value.length; a++) {
      if ((org.name === value[a].name || (org.name).toUpperCase() === value[a].name || this.capitalString(org.name) === value[a].name) && this.isAdd) {
        flag = 1;
        break;
      } else {
        flag = 0;
      }
    }
    if (flag === 1) {
      this.tr.warning('', 'This organization is already exist');
    } else {
      if (!this.isAdd) {
        this.as.saveOrgUnit(org).subscribe(data => { this.orgadded(data, 'edit'); this.tr.success('', 'Edited Successfully'); });
      } else {
        this.as.saveOrgUnit(org).subscribe(data => { this.orgadded(data, 'add'); this.tr.success('', 'Added Successfully'); });
      }
    }
  }
  orgadded(datas, isadd) {
    this.orgForm.reset();
    this.isView = true;
    this.isAdd = false;
    // if (isadd === 'add') {
    //   this.isAdd = true;
    // } else {
    //   this.isAdd = false;
    // }
    this.as.getTopLevelOrgUnit().subscribe(data => this.getOrgUnit(data));
  }
  capitalString(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  getOrgUnit(data) {
    if (data._body !== '') {
      this.repository = JSON.parse(data._body);
      console.log("ORGUNITS JSON REPOSITORIES : " + JSON.stringify(this.repository));
      this.RepositoryValue.emit(this.repository);

    }
  }
  clickedOnTop(event) {
    // this.clickedOnView();
    this.edit(event);
  }
  edit(event) {
    console.log("ORGEVENT: " + event.label);
    this.isAdd = false;
    this.isView = false;
    this.selectedTree = null;
    this.selectedTree = event;
    // console.log("SELECTEDTREEEDIT: " + JSON.stringify(this.selectedTree));
    // console.log("PARENTEVENT: " + event.parent.label);
    // console.log("EVENT JSON: " + JSON.stringify(event.label));
    this.orgForm.reset();
    if (event.parent === undefined) {
      console.log("IF edit");
      this.orgForm.patchValue({
        name: event.label,
        headRoleName: event.headRoleName,
        headRoleId: event.headRoleId,
        id: event.id,
        type: event.type,
        hid: event.hid,
        offset: event.offset,
        count: event.count,
        orgCode: event.orgCode
      });
      this.orgForm.controls.parentName.disable();
    } else {
      console.log("ELSE edit");
      this.orgForm.patchValue({
        name: event.label,
        headRoleName: event.headRoleName,
        headRoleId: event.headRoleId,
        parent: event.parent.id,
        id: event.id,
        type: event.type,
        hid: event.hid,
        offset: event.offset,
        count: event.count,
        parentName: event.parent.label,
        orgCode: event.orgCode
      });
    }
    this.rolesSelect = {
      value: event.headRoleId,
      label: event.headRoleName
    };
    this.us.getRoleMembers(event.headRoleId).subscribe(data => this.getRoleMembers(data));
    this.orgForm.enable();
    // this.orgForm.controls.name.enable();
  }
  patchValue(event) {
    this.selectedTree = event;
    console.log("SELECTEDTREECHILD: " + this.selectedTree);
    if (this.selectedTree !== null) {
      console.log("patchValue: " + event.parent.label);
      this.orgForm.patchValue({
        orgName: this.selectedTree.label,
        orgId: this.selectedTree.id,
        id: this.selectedTree.id,
        hid: this.selectedTree.hid,
        parentName: this.selectedTree.parent.label,
        name: this.selectedTree.label,

        headRoleName: event.headRoleName,
        headRoleId: event.headRoleId,
        parent: event.parent.id,
        type: event.type,
        offset: event.offset,
        count: event.count,
        orgCode: event.orgCode
      });
      this.rolesSelect = {
        value: this.selectedTree.headRoleId,
        label: this.selectedTree.headRoleName
      };
      this.us.getRoleMembers(event.headRoleId).subscribe(data => this.getRoleMembers(data));
    }
    else {
      console.log("ELSE patchValue: " + event.parent.label);
      }
  }
  patchParentValue(event) {
    this.selectedTree = event;
    console.log("SELECTEDTREEParent: " + this.selectedTree);
    if (this.selectedTree !== null) {
      console.log("patchParentValue: " + event.parent.label);
      this.orgForm.patchValue({
        parentName: this.selectedTree.label,
        parent: this.selectedTree.id
      });
    }
    else {
    console.log("ELSEEE patchParentValue: ");
    }
  }

  topClicked(event) {
    console.log("topClicked: " + this.repository.name);
    this.selectedTree = event;
    this.orgForm.patchValue({
      parentName: this.repository.name,
      parent: this.repository.id,
      orgName: this.repository.name,
      orgId: this.repository.id,
      // orgCode: this.repository.orgCode
    })
  }

  patchMain(event) {
    console.log("FOR MAIN");
  }

  cancelAdd() {
    this.orgForm.reset();
    this.orgForm.enable();
    this.formDirective.resetForm();
  }
}
