import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroupDirective, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AdministrationService } from '../../../service/Administration.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../../service/user.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-roles-list',
  templateUrl: './roles-list.component.html',
  styleUrls: ['./roles-list.component.css']
})
export class RolesListComponent implements OnInit {
  @ViewChild(FormGroupDirective) formDirective: FormGroupDirective;
  public formRolesList: FormGroup;
  public addUserToRoleForm: FormGroup;
  public repository: any;
  public rolesValue = [];
  public roleList = [];
  public rolesList = [];
  public isAdd = false;
  public isView = true;
  public source = [];
  public rolename: '';
  public listId;
  public showRolesFromListed = [];
  public availabaleRoles = false;
  public filteredRoles = [];
  public getRoleToAdd = [];
  public deleteValue: any;
  constructor(private fb: FormBuilder, private as: AdministrationService, private tr: ToastrService, private us: UserService,
    public ngxSmartModalService: NgxSmartModalService, private loading: Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.loading.show();
    this.getRoleList();
    this.formRolesList = this.fb.group({
      id: [null],
      name: [null, Validators.compose([Validators.required])],
      type: [null],
      roleid: [null],
      roleName: ['']
    });
    this.us.getRoles().subscribe(data => { this.getRoles(data); this.loading.hide(); }, error => { this.loading.hide(); });
  }
  getRoles(data) {
    this.getRoleToAdd = [];
    this.filteredRoles = [];
    if (data) {
      this.getRoleToAdd = JSON.parse(data._body);
      this.getRoleToAdd.forEach(element => {
        const val = {
          label: element.name,
          value: element.id
        };
        this.filteredRoles.push(val);
      });
    }
  }
  getRoleList() {
    this.us.getRoleLists().subscribe(data => { this.roleListApi(data); this.loading.hide(); }, error => { this.loading.hide(); });
  }
  roleListApi(data) {
    this.source = [];
    this.source = JSON.parse(data._body);
  }
  clickedOnAdd() {
    this.isAdd = true;
    this.isView = false;
    this.availabaleRoles = false;
    this.formRolesList.reset();
  }
  clickedOnView() {
    this.isAdd = false;
    this.isView = true;
  }
  editRolesList(event) {
    this.isAdd = false;
    this.isView = false;
    this.formRolesList.reset();
    this.formRolesList.patchValue({ id: event.id, name: event.name, type: event.type });
    this.rolename = '';
    this.listId = event.id;
    this.us.getRoleListRoles(event.id).subscribe(data => { this.showRoles(data); });
  }
  rolesListFormSubmitted(event) {
    if (this.isAdd === false) {
      this.loading.show();
      const value = {
        id: this.formRolesList.value.id,
        name: this.formRolesList.value.name,
        type: this.formRolesList.value.type
      };
      this.us.saveRoleList(value).subscribe(data => {
        this.getRoleList(); this.loading.hide();
        this.tr.success('', 'Edit Success');
      }, error => { this.loading.hide(); });
      if (this.formRolesList.value.roleid) {
        this.us.addRoleToRoleList(this.formRolesList.value.id, this.formRolesList.value.roleid.value).subscribe(data => {
          this.formRolesList.patchValue({ rolename: '' });
          this.editRolesList(event.value);
          this.us.getRoleListRoles(this.listId).subscribe(result => this.showRoles(result));
        }, error => { this.formRolesList.patchValue({ rolename: '' }); });
      }
    } else {
      if (this.formRolesList.value.name) {
        this.loading.show();
        const value = {
          name: this.formRolesList.value.name
        };
        this.us.saveRoleList(value).subscribe(data => {
          this.getRoleList();
          this.loading.hide();
        }, error => { this.loading.hide(); });
        this.tr.success('Save Success', 'success');
        this.formRolesList.reset();
      }
    }
  }
  showRoles(data) {
    this.showRolesFromListed = [];
    this.availabaleRoles = false;
    if (data !== undefined || data !== null) {
      if (JSON.parse(data._body).length > 0) {
        this.showRolesFromListed = JSON.parse(data._body);
        this.availabaleRoles = true;
      }
    } else {
      this.availabaleRoles = false;
    }
  }
  cancelAdd() {
    this.formRolesList.reset();
    this.formRolesList.enable();
    this.formDirective.resetForm();
  }
  roleNamechanged(event) {
    this.formRolesList.patchValue({ roleid: event.value });
  }
  deleteRoleModel(value) {
    this.ngxSmartModalService.getModal('deleteRoleUsers').open();
    this.deleteValue = value;
  }
  deleteConfirmed() {
    this.loading.show();
    this.us.removeRoleFromRoleList(this.listId, this.deleteValue.id).subscribe(data => { this.us.getRoleListRoles(this.listId).subscribe(result => { this.showRoles(result); this.tr.success('Role Removed'); this.loading.hide(); }); },
      error => {
        this.loading.hide();
      });
  }
}
