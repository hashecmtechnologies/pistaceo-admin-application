
import { Component, Input, OnInit, AfterViewInit, OnDestroy, ElementRef, Renderer, ViewChild, Output, EventEmitter, OnChanges } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { MenuItem, Message } from 'primeng/primeng';
import { TranslateService } from '../../../../../../node_modules/@ngx-translate/core';
import { AdminLayoutComponent } from '../admin-layout/admin-layout.component';
import { SchemaService } from '../../../../service/schema.service';
import { ContentService } from '../../../../service/content.service';
import { WorkService } from '../../../../service/work.service';
import { UserService } from '../../../../service/user.service';
import { Observable } from 'rxjs/Observable';
import { ConfigurationService } from '../../../../service/Configuration.service';
import { DocumentService } from '../../../../service/document.service';
import { DataService } from '../../../../service/data.service';
declare var jQuery: any;

@Component({
    selector: 'app-menu-items',
    templateUrl: './menu-items.component.html',
    styleUrls: ['./menu-items.component.css']
})
export class MenuItemsComponent implements OnInit, AfterViewInit, OnDestroy, OnChanges {

    @Input() reset: boolean;
    model = [];
    msgs: Message[];
    @Input() type: any;
    layoutMenuScroller: HTMLDivElement;
    @Input() inboxBadgeCounted: any;
    @Input() draftsBageCounted: any;

    @ViewChild('layoutMenuScroller') layoutMenuScrollerViewChild: ElementRef;
    public showDrafts = false;
    public showArchive = false;
    public showRegister = false;
    public inboxCount = 0;
    public draftsCount = 0;
    public previewLogo;
    constructor(public app: AdminLayoutComponent, public translate: TranslateService, public cs: ContentService, private confiService: ConfigurationService,
        public ss: SchemaService, public router: Router, private ws: WorkService, private us: UserService, private ds: DocumentService, private dataService: DataService) {
        this.ss.getWorkTypes().subscribe(data => { this.dynamicItems(data); });
        const browserLang: string = translate.getBrowserLang();
        translate.use('en');
        translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
        this.ws.getInboxItems(this.us.getCurrentUser().EmpNo, 1).subscribe(data => this.getInboxResult(data));

        Observable.interval(1000 * 60).subscribe(x => {
            if (window.location.href.includes('inbox')) {
                this.ws.getInboxItems(this.us.getCurrentUser().EmpNo, 1).subscribe(data => this.getInboxResult(data));
            }
        });
        this.ws.getDraftItems(this.us.getCurrentUser().EmpNo, 1).subscribe(data => this.getDraftResult(data));
        this.confiService.getConfiguration('BROWSEDOCS').subscribe(data => this.browseDocs(data));
        this.dataService.getDataSchemaList().subscribe(data => { this.DataItems(data); }, error => { this.DataItems(error); });
    }
    ngOnInit() {
        this.confiService.getLogo().subscribe(data => this.getPreviewImage(data), error => { this.previewLogo = ''; });
        this.model = [
            {
                label: this.translate.instant('Organization'), icon: 'group_work', items: [
                    { label: this.translate.instant('Org Uints'), icon: 'ac_unit', routerLink: ['/orgunits'] },
                    { label: this.translate.instant('Roles'), icon: 'contacts', routerLink: ['/roles'] },
                    { label: this.translate.instant('Role Lists'), icon: 'list', routerLink: ['/rolesList'] },
                    { label: this.translate.instant('Users'), icon: 'person', routerLink: ['/users'] },
                    { label: this.translate.instant('Delegations'), icon: 'person_add', routerLink: ['/delegate'] },

                ]
            },
            {
                label: this.translate.instant('Workflow'), icon: 'dashboard', items: [
                    { label: this.translate.instant('Documents'), icon: 'dock', routerLink: ['/docTypes'] },
                    { label: this.translate.instant('Forms'), icon: 'book', routerLink: ['/forms'] },
                    { label: this.translate.instant('Respose Types'), icon: 'reply', routerLink: ['/responseTypes'] },
                    { label: this.translate.instant('Work types'), icon: 'merge_type', routerLink: ['/workTypes'] },
                    { label: this.translate.instant('Templates'), icon: 'print', routerLink: ['/templates'] },
                    { label: this.translate.instant('Response Forms'), icon: 'filter_list', routerLink: ['/filterForms'] },
                    { label: this.translate.instant('Properties'), icon: 'info', routerLink: ['/propTemplate'] }

                ]
            },
            {
                label: this.translate.instant('Documents'), icon: 'domain', items: [
                    { label: this.translate.instant('Repository'), icon: 'collections', routerLink: ['/repository'] },
                    { label: this.translate.instant('Properties'), icon: 'info', routerLink: ['/propTemplate'] },
                    { label: this.translate.instant('Classes'), icon: 'class', routerLink: ['/classes'] },
                    { label: this.translate.instant('Root Folders'), icon: 'folder', routerLink: ['/rootFolders'] },
                    { label: this.translate.instant('Recyle Bin'), icon: 'delete', routerLink: ['/recycleBin'] },

                ]
            },
            {
                label: this.translate.instant('Settings'), icon: 'settings', items: [
                    { label: this.translate.instant('Configuaration'), icon: 'confirmation_number', routerLink: ['/configuarations'] },
                    { label: this.translate.instant('Translations'), icon: 'translate', routerLink: ['/translations'] },
                    { label: this.translate.instant('License'), icon: 'lock', routerLink: ['/license'] },

                ]
            },
            {
                label: this.translate.instant('Reports'), icon: 'report', items: [
                    { label: this.translate.instant('Reports'), icon: 'report', routerLink: ['/reports'] },
                    { label: this.translate.instant('Filter Forms'), icon: 'filter_list', routerLink: ['/filterForms'] },

                ]
            },


        ];

        if (this.model !== undefined) {
            for (let i = 0; i < this.model.length; i++) {
                if (this.model[i].label === 'Inbox') {
                    this.model[i].badge = this.inboxBadgeCounted;
                }
            }
            for (let a = 0; a < this.model.length; a++) {
                if (this.model[a].label === 'Drafts') {
                    this.model[a].badge = this.inboxBadgeCounted;
                }
            }
        } else {

        }
    }

    getPreviewImage(data) {
        if (data._body.image !== undefined) {
            const previewImage = JSON.parse(data._body);
            this.previewLogo = 'data:image/png;base64,' + previewImage.image;
        } else {
            this.previewLogo = '';
        }
    }

    dynamicItems(data) {
        const datares = JSON.parse(data._body);
        for (let i = 0; i < this.model.length; i++) {
            if (this.model[i].label === 'Create') {
                if (datares.length <= 5) {
                    for (let k = 0; k < datares.length; k++) {
                        const items = {
                            label: datares[k].name,
                            icon: 'edit',
                            id: datares[k].id,
                            multiFormSupport: datares[k].multiFormSupport,
                            subjectProps: datares[k].subjectProps,
                            initActivityType: datares[k].initActivityType,
                            command: (event) => { this.CreatePage(datares[k].id, datares[k].initActivityType, datares[k].multiFormSupport, datares[k].subjectProps); }
                        };
                        this.model[i].items.push(items);
                        if (datares[k].draftSupport === 1) {
                            this.showDrafts = true;
                        }
                        if (datares[k].archiveSupport === 1) {
                            this.showArchive = true;
                        }
                        if (datares[k].registerSupport === 1) {
                            this.showRegister = true;
                        }
                    }
                } else {
                    for (let k = 0; k < datares.length; k++) {
                        if (datares[k].draftSupport === 1) {
                            this.showDrafts = true;
                        }
                        if (datares[k].archiveSupport === 1) {
                            this.showArchive = true;
                        }
                        if (datares[k].registerSupport === 1) {
                            this.showRegister = true;
                        }
                    }
                    this.model[i].items = null;
                    this.model[i].routerLink = ['/createFormNames'];
                }
            }
        }
        for (let reg = 0; reg < this.model.length; reg++) {
            if (this.model[reg].label === 'Drafts' && this.showDrafts === false) {
                this.model.splice(reg, 1);
                break;
            }
        }
        if (this.showRegister === true) {
            this.ss.getRegisterWorkTypes().subscribe(dataresp => this.workCategories(dataresp));
        } else {
            for (let reg = 0; reg < this.model.length; reg++) {
                if (this.model[reg].label === 'Registers') {
                    this.model.splice(reg, 1);
                    break;
                }
            }
        }
    }
    DataItems(data) {
        const datares = JSON.parse(data._body);
        for (let i = 0; i < this.model.length; i++) {
            if (this.model[i].label === 'Data') {
                if (datares && datares.length > 0) {
                    for (let k = 0; k < datares.length; k++) {
                        const items = {
                            label: datares[k].label,
                            icon: 'room_service',
                            name: datares[k].name,
                            formId: datares[k].formId,
                            id: datares[k].id,
                            status: datares[k].status,
                            table: datares[k].table,
                            command: (event) => { this.dataPage(datares[k].id, datares[k].formId, datares[k].table, datares[k].name, datares[k].label, datares[k].status, datares[k].keyName); }
                        };
                        this.model[i].items.push(items);
                    }
                } else {
                    this.model.splice(i, 1);
                }
            }
        }
    }
    dataPage(id, formId, table, name, label, status, keyName) {
        this.router.navigate(['/data'], { queryParams: { 'id': id, 'formId': formId, 'table': table, 'name': name, 'label': label, 'status': status, 'keyName': keyName } });
    }

    workCategories(data) {
        let datares = [];
        datares = JSON.parse(data._body);
        for (let rek = 0; rek < this.model.length; rek++) {
            if (this.model[rek].label === 'Registers') {
                for (let reg = 0; reg < datares.length; reg++) {
                    const value = {
                        label: this.translate.instant(datares[reg].name),
                        icon: 'edit',
                        id: datares[reg].id,
                        command: (event) => { this.registerPage(datares[reg].id, datares[reg].name); }
                    };
                    this.model[rek].items.push(value);
                }
            }
        }
    }
    registerPage(id, name) {
        this.router.navigate(['/register'], { queryParams: { id: id, name: name } });
    }
    CreatePage(id, initActivityType, multiFormSupport, subjectProps) {

        this.router.navigate(['/create'], { queryParams: { 'id': initActivityType, 'typeId': id, 'multtiFromSupport': multiFormSupport, 'subjectProps': subjectProps } });
    }

    updateRepositoryValue(data) {
        const resp = JSON.parse(data._body);
        for (let index = 0; index < this.model.length; index++) {
            if (this.model[index].icon === 'folder') {
                this.model[index].items = [];
            }
        }
        for (let index = 0; index < this.model.length; index++) {
            if (this.model[index].icon === 'folder') {
                this.model[index].items.push({
                    label: resp.name, icon: 'folder',
                    command: (event) => { this.folderClicked(resp); }
                });
            }
        }
    }

    folderClicked(repo) {

        this.router.navigate(['/folderview'], { queryParams: { 'repoName': repo.name, 'repoId': repo.id } });
    }

    browseDocs(data) {
        if (data._body !== '') {
            const browseDocs = JSON.parse(data._body);
            if (browseDocs.value === 'YES') {
                this.cs.getAppRepository().subscribe(res => this.updateRepositoryValue(res));
            } else {
                for (let reg = 0; reg < this.model.length; reg++) {
                    if (this.model[reg].label === 'Documents') {
                        this.model.splice(reg, 1);
                        break;
                    }
                }
            }
        }
    }

    ngAfterViewInit() {
        this.layoutMenuScroller = <HTMLDivElement>this.layoutMenuScrollerViewChild.nativeElement;
        setTimeout(() => {
            jQuery(this.layoutMenuScroller).nanoScroller({ flash: true });
        }, 10);
    }

    changeTheme(theme) {
        const themeLink: HTMLLinkElement = <HTMLLinkElement>document.getElementById('theme-css');
        themeLink.href = 'assets/theme/theme-' + theme + '.css';
    }

    changeLayout(theme) {
        const layoutLink: HTMLLinkElement = <HTMLLinkElement>document.getElementById('layout-css');
        layoutLink.href = 'assets/layout/css/layout-' + theme + '.css';
    }

    updateNanoScroll() {
        setTimeout(() => {
            jQuery(this.layoutMenuScroller).nanoScroller();
        }, 500);
    }
    itemColor(model) {
        // console.log(model);
    }
    ngOnDestroy() {
        jQuery(this.layoutMenuScroller).nanoScroller({ flash: true });
    }

    getMenuItems() {
    }

    getInboxResult(data) {
        const res = JSON.parse(data._body);
        let count = 0;
        for (let index = 0; index < res.activities.length; index++) {
            if (res.activities[index].status === 'NEW') {
                count++;
            }
        }
        for (let i = 0; i < this.model.length; i++) {
            if (this.model[i].label === 'Inbox') {
                this.model[i].badge = count;
                this.inboxBadgeCounted = count;
            }
        }
    }

    getDraftResult(data) {
        const res = JSON.parse(data._body);
        const value = res.activities;
        const count = value.length;
        for (let i = 0; i < this.model.length; i++) {
            if (this.model[i].label === 'Drafts') {
                this.model[i].badge = count;
                this.draftsBageCounted = count;
            }
        }
    }
    update(item) {

    }
    updateInboxBadge(event) {

    }
    ngOnChanges() {
        if (this.model !== undefined) {
            for (let i = 0; i < this.model.length; i++) {
                if (this.model[i].label === 'Inbox') {
                    this.model[i].badge = this.inboxBadgeCounted;
                }
            }
            for (let a = 0; a < this.model.length; a++) {
                if (this.model[a].label === 'Drafts') {
                    this.model[a].badge = this.draftsBageCounted;
                }
            }
        }
    }

    officialWebsite() {
        window.open('http://www.hashecm.com/', 'mywindow', 'status=1,toolbar=1');
    }
}

@Component({
    /* tslint:disable:component-selector */
    selector: '[app-submenu]',
    /* tslint:enable:component-selector */
    template: `
        <ng-template ngFor let-child let-i="index" [ngForOf]="(root ? item : item.items)">
            <li [ngClass]="{'active-menuitem': isActive(i)}" [class]="child.badgeStyleClass">
                <a [href]="child.url||'#'" (click)="itemClick($event,child,i);" *ngIf="!child.routerLink"
                   [attr.tabindex]="!visible ? '-1' : null" [attr.target]="child.target"
                    (mouseenter)="hover=true" (mouseleave)="hover=false" class="ripplelink">
                    <i class="material-icons">{{child.icon}}</i>
                    <span class="menuitem-text">{{ child.label | translate }}</span>
                    <i class="material-icons layout-submenu-toggler" *ngIf="child.items">keyboard_arrow_down</i>
                    <span class="menuitem-badge" *ngIf="child.badge">{{child.badge}}</span>
                </a>

                <a (click)="itemClick($event,child,i);" *ngIf="child.routerLink"
                    [routerLink]="child.routerLink" routerLinkActive="active-menuitem-routerlink"
                   [routerLinkActiveOptions]="{exact: true}" [attr.tabindex]="!visible ? '-1' : null" [attr.target]="child.target"
                    (mouseenter)="hover=true" (mouseleave)="hover=false" class="ripplelink">
                    <i class="material-icons">{{child.icon}}</i>
                    <span class="menuitem-text">{{ child.label | translate }}</span>
                    <i class="material-icons layout-submenu-toggler" *ngIf="child.items">>keyboard_arrow_down</i>
                    <span class="menuitem-badge" *ngIf="child.badge">{{child.badge}}</span>
                </a>
                <ul app-submenu [item]="child" *ngIf="child.items" [visible]="isActive(i)" [reset]="reset"
                    [@children]="isActive(i) ? 'visible' : 'hidden'"></ul>
            </li>
        </ng-template>
    `,
    animations: [
        trigger('children', [
            state('visible', style({
                height: '*'
            })),
            state('hidden', style({
                height: '0px'
            })),
            transition('visible => hidden', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
            transition('hidden => visible', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
        ])
    ]
})
export class SubMenuMenuItemsComponent {

    @Input() item: MenuItem;

    @Input() root: boolean;

    @Input() visible: boolean;

    _reset: boolean;

    activeIndex: number;

    hover: boolean;

    constructor(public app: AdminLayoutComponent, public router: Router, public location: Location) { }

    itemClick(event: Event, item: MenuItem, index: number) {
        // avoid processing disabled items
        if (item.disabled) {
            event.preventDefault();
            return true;
        }

        // activate current item and deactivate active sibling if any
        if (item.routerLink || item.items || item.command || item.url) {
            this.activeIndex = (this.activeIndex === index) ? null : index;
        }

        // execute command
        if (item.command) {
            item.command({ originalEvent: event, item: item });
        }

        // prevent hash change
        if (item.items || (!item.url && !item.routerLink)) {
            event.preventDefault();
        }

        // hide menu
        if (!item.items) {
            if (this.app.isMobile()) {
                this.app.sidebarActive = false;
                this.app.mobileMenuActive = false;
            }
        }

    }

    isActive(index: number): boolean {
        return this.activeIndex === index;
    }

    @Input() get reset(): boolean {
        return this._reset;
    }

    set reset(val: boolean) {
        this._reset = val;
    }


}
