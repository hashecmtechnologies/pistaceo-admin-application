import { Component, OnInit, Renderer } from '@angular/core';
import { TranslateService } from '../../../../../../node_modules/@ngx-translate/core';
import { SchemaService } from '../../../../service/schema.service';
import * as operators from '../../../../operators.variables';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
@Component({
    selector: 'app-admin-layout',
    templateUrl: './admin-layout.component.html',
    styleUrls: ['./admin-layout.component.css']
})
export class AdminLayoutComponent {

    menuClick: boolean;

    menuButtonClick: boolean;

    topbarMenuButtonClick: boolean;

    topbarMenuClick: boolean;

    topbarMenuActive: boolean;

    activeTopbarItem: Element;

    layoutStatic = true;

    sidebarActive: boolean;

    mobileMenuActive: boolean;

    darkMenu: boolean;

    isRTL: boolean;
    public dirType;
    public options = [];
    public inboxBadgeCounted = 0;
    public draftsBadgeCounted = 0;
    public show = false;
    constructor(public renderer: Renderer, translate: TranslateService, location: Location, router: Router) {
        if (sessionStorage.getItem('Default Language') === 'ar') {
            this.dirType = 'rtl';
        } else {
            this.dirType = 'ltr';
        }
        if (sessionStorage.getItem('Default Language') === 'ar') {
            this.isRTL = true;
        } else {
            this.isRTL = false;
        }
        const value = sessionStorage.getItem('Default Language');
        translate.setDefaultLang(value);
        translate.use(sessionStorage.getItem('Default Language'));
        // }

        if (sessionStorage.getItem('Default Theme') === undefined || sessionStorage.getItem('Default Theme') === null) {
            const themeLink: HTMLLinkElement = <HTMLLinkElement>document.getElementById('theme-css');
            themeLink.href = 'assets/theme/theme-' + 'cyan' + '.css';
            this.changedLayout('flatiron');
        } else {
            const theme = sessionStorage.getItem('Default Theme');
            const themeLink: HTMLLinkElement = <HTMLLinkElement>document.getElementById('theme-css');
            themeLink.href = 'assets/theme/theme-' + 'cyan' + '.css';
            this.changedLayout('flatiron');
        }
        router.events.subscribe((val) => {
            if (location.isCurrentPathEqualTo('/home')) {
                this.show = true;
            } else {
                this.show = true;
            }
        });
    }
    changedLayout(layout) {
        const layoutLink: HTMLLinkElement = <HTMLLinkElement>document.getElementById('layout-css');
        layoutLink.href = 'assets/layout/css/layout-' + layout + '.css';
    }

    onWrapperClick() {
        if (!this.menuClick && !this.menuButtonClick) {
            this.mobileMenuActive = false;
        }

        if (!this.topbarMenuClick && !this.topbarMenuButtonClick) {
            this.topbarMenuActive = false;
            this.activeTopbarItem = null;
        }

        this.menuClick = false;
        this.menuButtonClick = false;
        this.topbarMenuClick = false;
        this.topbarMenuButtonClick = false;
    }

    onMenuButtonClick(event: Event) {
        this.menuButtonClick = true;

        if (this.isMobile()) {
            this.mobileMenuActive = !this.mobileMenuActive;
        }

        event.preventDefault();
    }

    onTopbarMobileMenuButtonClick(event: Event) {
        this.topbarMenuButtonClick = true;
        this.topbarMenuActive = !this.topbarMenuActive;
        event.preventDefault();
    }

    onTopbarRootItemClick(event: Event, item: Element) {
        if (this.activeTopbarItem === item) {
            this.activeTopbarItem = null;
        } else {
            this.activeTopbarItem = item;
        }

        event.preventDefault();
    }

    onTopbarMenuClick(event: Event) {
        this.topbarMenuClick = true;
    }

    onSidebarClick(event: Event) {
        this.menuClick = true;
    }

    onToggleMenuClick(event: Event) {
        this.layoutStatic = !this.layoutStatic;
    }

    isMobile() {
        return window.innerWidth <= 1024;
    }
    rtlChange(event) {
        if (event === 'ar') {
            this.dirType = 'rtl';
        } else {
            this.dirType = 'ltr';
        }
    }
    updateValue(type, count) {
        if (type === 'Inbox') {
            this.inboxBadgeCounted = count;
        } else if (type === 'Drafts') {
            this.draftsBadgeCounted = count;
        }
    }
}
