import { Component, OnInit, ViewChild, Output, Input, OnChanges } from '@angular/core';
import { FormGroupDirective, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EventEmitter } from 'events';
import { AdministrationService } from '../../../service/Administration.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../../service/user.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ConfigurationService } from '../../../service/Configuration.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, OnChanges {
  @ViewChild(FormGroupDirective) formDirective: FormGroupDirective;
  @Output() showUserSpinner = new EventEmitter();
  @Output() userResult = new EventEmitter();
  @Output() RepositoryValue = new EventEmitter();
  @Input() selectedTree: any;
  @Input() addingNewNode: any;
  public userForm: FormGroup;
  public repository: any;
  public rolesValue = [];
  public roleList = [];
  public userList = [];
  public isAdd = false;
  public isView = true;
  public roleNgModel: any;
  public selectedUser;
  public getUserRole: any;
  public websocketNotOpen = false;
  public dbDummyValue = [];
  public userTypeOptions: any;
  public deploymentType = 'NotCloud';
  public dbvalue = [];
  public existingSupervisor: any;
  public roles = [];
  public rolesToChoose = [];
  public rolesSelect: any;

  public emailvalue = null;

  constructor(private fb: FormBuilder, private as: AdministrationService, private tr: ToastrService, private us: UserService,
    public ngxSmartModalService: NgxSmartModalService, private spinner: Ng4LoadingSpinnerService,
    private config: ConfigurationService) { }

  ngOnInit() {
    this.userTypeOptions = [
      { label: 'LDAP User', value: 'AD' },
      { label: 'Pistaceo User', value: 'HASH' }
    ];
    this.userForm = this.fb.group({
      fulName: [null, Validators.required],
      orgId: [null],
      mail: [null, Validators.compose([Validators.required])],
      roles: [null, Validators.compose([Validators.required])],
      EmpNo: [null],
      userLogin: [null],
      orgName: [null, Validators.required],
      title: [null, Validators.compose([Validators.required])],
      userType: ['HASH', Validators.required]
    });
    this.config.getDeploymentType().subscribe(data => {
      this.checkDeploymentType(data);
    }, error => {
      if (error.status === 404) {
        this.deploymentType = 'NotCloud';
        this.userForm.controls.userType.enable();
        this.userForm.controls['userLogin'].setValidators([Validators.required]);

      }
    });
    this.isView = true;
    this.isAdd = false;
    this.as.getTopLevelOrgUnit().subscribe(data => this.getOrgUnit(data));
    this.us.getRoles().subscribe(data => { this.getRolesEvent(data); }, error => { });
    this.spinner.show();
    this.us.getAllUsers().subscribe(data => { this.usersResult(data); this.spinner.hide(); }, error => { this.spinner.hide(); this.spinner.hide(); });
  }
  checkDeploymentType(data) {
    if (data._body === 'CLOUD') {
      this.deploymentType = 'Cloud';
      this.userForm.controls.userType.disable();
      this.userForm.patchValue({ userLogin: 'login' });
      this.userForm.controls['userLogin'].setValidators([Validators.pattern('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$'), Validators.required]);
    } else {
      this.userForm.controls.userType.enable();
      this.userForm.controls['userLogin'].setValidators([Validators.required]);
    }
  }
  ngOnChanges() {
    if (this.selectedTree) {
      this.userForm.patchValue({ orgID: this.selectedTree.id, orgName: this.selectedTree.label });
    }
  }
  getRolesEvent(data) {
    this.spinner.hide();
    if (data !== null || data !== '') {
      const roles = JSON.parse(data._body);
      this.roleList = [];
      for (let role = 0; role < roles.length; role++) {
        const objct = {
          value: roles[role].id,
          label: roles[role].name
        };
        this.roleList.push(objct);
      }
    }
  }
  usersResult(data) {
    if (data._body) {
      this.userList = [];
      this.userList = JSON.parse(data._body);
    }
  }
  getOrgUnit(data) {
    if (data._body !== '') {
      this.repository = JSON.parse(data._body);
      this.RepositoryValue.emit(this.repository);
    }
  }
  userFormSubmitted(userForm) {
    console.log("SUBMIT CLICKED");
    console.log("SelectedTree: " + this.selectedTree);
    console.log("userform.valid: " + this.userForm.valid + "  value  " + JSON.stringify(this.userForm.value));
    console.log("ROLES: " + this.userForm.value.roles);
    // console.log("LENGTH: " + this.userForm.value.roles.length);
    if (this.selectedTree) {
      console.log("SUBMIT CLICKED 1");
      this.userForm.patchValue({ orgId: this.selectedTree.id, orgName: this.selectedTree.label });
    }
    if ((this.userForm.value.orgId !== null && this.userForm.value.orgId !== undefined)) {
      console.log("SUBMIT CLICKED 2");
      const rolesArray = [];
      // for (let index = 0; index < this.userForm.value.roles.length; index++) {
      //   const rolesValue = {
      //     id: this.userForm.value.roles[index].value,
      //     name: this.userForm.value.roles[index].label
      //   };
      //   rolesArray.push(rolesValue);
      // }
      setTimeout(() => {
        const org = {
          orgID: this.userForm.value.orgId,
          fulName: this.userForm.value.fulName,
          roles: rolesArray,
          mail: this.userForm.value.mail,
          EmpNo: this.userForm.value.EmpNo,
          userLogin: this.userForm.value.userLogin,
          orgName: this.userForm.value.orgName,
          title: this.userForm.value.title,
          userType: this.userForm.value.userType
        };
        this.spinner.show();
        if (this.isAdd) {
          this.us.saveUser(org).subscribe(data => { this.useradded(data); this.tr.success('', 'Added Successfully'); }, error => { this.spinner.hide(); });
        } else {
          this.us.saveUser(org).subscribe(data => { this.useradded(data); this.tr.success('', 'Updated Successfully'); }, error => { this.spinner.hide(); });
        }
      }, 0);
    } else if ((this.userForm.valid) && (this.userForm.value.orgId === null || this.userForm.value.orgId === undefined)) {
      this.tr.warning('', 'Please select any one organization');
    }
  }
  useradded(data) {
    this.spinner.hide();
    this.userResult.emit('Success');
    this.userForm.reset();
    this.formDirective.resetForm();
    this.ngOnInit();
    this.roleNgModel = [];

  }
  cancelAdd() {
    this.userForm.reset();
    this.userForm.enable();
    this.formDirective.resetForm();
    if (this.deploymentType === 'Cloud') {
      this.userForm.controls.userType.disable();
      this.userForm.patchValue({ userType: 'HASH' });
      this.userForm.controls['userLogin'].setValidators([Validators.pattern('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$'), Validators.required]);
    } else {
      this.userForm.controls.userType.enable();
      this.userForm.controls['userLogin'].setValidators([Validators.required]);
    }
    this.as.getTopLevelOrgUnit().subscribe(data => this.getOrgUnit(data));
  }
  selectedRoles(event) {
    let flag = 0;
    if (!this.isAdd && (this.getUserRole.roles.length > this.roleNgModel.length)) {
      for (let a = 0; a < this.getUserRole.roles.length; a++) {
        for (let b = 0; b < this.roleNgModel.length; b++) {
          if (this.getUserRole.roles[a].id === this.roleNgModel[b].value) {
            flag = 1;
            break;
          } else {
            flag = 0;
          }
        }
        if (flag === 0) {
          this.us.removeRoleFromUser(this.userForm.value.EmpNo, this.getUserRole.roles[a].id).subscribe(data => { this.removed(data); }, error => { });
          console.log(this.getUserRole.roles[a]);
          this.getUserRole.roles.splice(a, 1);
          break;
        }
      }
    } else if (!this.isAdd && (this.roleNgModel.length > this.getUserRole.roles.length)) {
      let flaga = 0;
      for (let amt = 0; amt < this.roleNgModel.length; amt++) {
        for (let aka = 0; aka < this.getUserRole.roles.length; aka++) {
          if (this.roleNgModel[amt].value === this.getUserRole.roles[aka].id) {
            flaga = 1;
            break;
          } else {
            flaga = 0;
          }
        }
        if (flaga === 0) {
          this.us.addUserToRole(this.userForm.value.EmpNo, this.roleNgModel[amt].value).subscribe(data => { this.removed(data); }, error => { });
          console.log(this.roleNgModel[amt]);
          const value = {
            name: this.roleNgModel[amt].label,
            id: this.roleNgModel[amt].value
          };
          this.getUserRole.roles.push(value);
          break;
        }
      }
    }
  }
  removed(data) {
    console.log(data);
  }
  getUnique(array) {
    const l = array.length;
    let i;
    let j;
    const unique = [];

    for (i = 0; i < l; i++) {
      for (j = 0; j < l; j++) {
        if (i === j) {
          continue;
        }
        if (array[i].id === array[j].id) {
          break;
        }
      }
      if (j === l) {
        unique.push(array[i]);
      }
    }
  }
  comparer(otherArray) {
    return function (current) {
      return otherArray.filter(function (other) {
        return other.id === current.id && other.name === current.name;
      }).length === 0;
    };
  }
  remove(user) {
    this.selectedUser = user;
    this.ngxSmartModalService.open('confirmUserDelete');
  }
  removeUser() {
    this.spinner.show();
    if (this.selectedUser.status === 'ACTIVE') {
      this.us.inactivateUser(this.selectedUser.EmpNo).subscribe(data => { this.activateUser(data); this.tr.success('', this.selectedUser.fulName + ' ' + ' is inactivated'); }, error => { this.spinner.hide(); });
    } else {
      this.us.activateUser(this.selectedUser.EmpNo).subscribe(data => { this.activateUser(data); this.tr.success('', this.selectedUser.fulName + ' ' + ' is activated'); }, error => { this.spinner.hide(); });
    }
  }
  activateUser(data) {
    this.spinner.hide();
    this.ngOnInit();
    this.formDirective.resetForm();
    this.tr.success(this.selectedUser.fulName + ' is Active Now');
  }
  inActivateUser(data) {
    this.spinner.hide();
    this.ngOnInit();
    this.formDirective.resetForm();
    this.tr.success(this.selectedUser.fulName + ' is Inactive Now');
  }
  editUser(user) {
    this.isAdd = false;
    this.isView = false;
    this.spinner.show();
    this.selectedUser = user.data;
    this.us.getUserDetailsOf(user.data.EmpNo).subscribe(data => {
      this.editUserDetails(data, user.data);
      this.spinner.hide();
    }, error => { this.spinner.hide(); });
  }
  editUserDetails(data, user) {
    if (data._body) {
      this.getUserRole = [];
      this.getUserRole = JSON.parse(data._body);
      this.roleNgModel = [];
      for (let role = 0; role < this.getUserRole.roles.length; role++) {
        const objct = {
          value: this.getUserRole.roles[role].id,
          label: this.getUserRole.roles[role].name
        };
        this.roleNgModel.push(objct);
      }
    }
    this.dbDummyValue.push({
      'id': user.userLogin,
      'itemName': user.userLogin
    });
    if (user.type === 'AD' || user.type === undefined) {
      this.userForm.patchValue({ userLogin: this.dbDummyValue, userType: user.type });
    } else {
      this.userForm.patchValue({ userLogin: user.userLogin, userType: user.type });
    }
    this.userForm.patchValue({
      EmpNo: user.EmpNo,
      fulName: user.fulName,
      orgId: user.orgID,
      mail: user.mail,
      roles: this.roleNgModel,
      userLogin: user.userLogin,
      orgName: user.orgName,
      title: user.title,
      userType: user.type
    });
    window.scroll(0, 0);
  }
  selectOrg() {
    this.tr.info('', 'Please select organization from organization tree');
  }
  lookupRowStyleClass(rowData) {
  }
  clickedOnAdd() {
    this.isAdd = true;
    this.isView = false;
    this.userForm.reset();
    if (this.deploymentType === 'Cloud') {
      this.userForm.controls.userType.disable();
      this.userForm.patchValue({ userType: 'HASH' });
      this.userForm.controls['userLogin'].setValidators([Validators.pattern('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$'), Validators.required]);
    } else {
      this.userForm.controls.userType.enable();
      this.userForm.controls['userLogin'].setValidators([Validators.required]);
    }
  }
  clickedOnView() {
    // this.isView = true;
    // this.isAdd = false;
  }

  topClicked(event) {
    console.log("topClicked: " + this.repository.name);
    this.selectedTree = event;
    this.userForm.patchValue({
      parentName: this.repository.name,
      parent: this.repository.id,
      orgName: this.repository.name,
      orgId: this.repository.id,
      // orgCode: this.repository.orgCode
    })
  }

  edit(event) {
    this.userForm.patchValue({ orgId: event.id, orgName: event.label });
  }
  userTypeChanged(event) {
    const userloginValue = this.userForm.value.userLogin;
    if (this.userForm.value.userLogin !== undefined && this.userForm.value.userLogin !== null) {
      if (this.userForm.value.userLogin.length > 0) {
        if (event.value === 'HASH') {
          this.userForm.patchValue({ userLogin: userloginValue[0].id });
        } else {
          this.dbDummyValue = [];
          this.dbDummyValue.push({
            'id': userloginValue,
            'itemName': userloginValue
          });
          this.userForm.patchValue({ userLogin: this.dbDummyValue });
        }
      } else {
        if (event.value === 'AD') {
          this.dbDummyValue = [];
          this.userForm.patchValue({ userLogin: this.dbDummyValue });
        } else {
          this.userForm.patchValue({ userLogin: null });
        }
      }
    } else {
      if (event.value === 'AD') {
        this.dbDummyValue = [];
        this.userForm.patchValue({ userLogin: this.dbDummyValue });
      } else {
        this.userForm.patchValue({ userLogin: null });
      }
    }
  }
  onSearch(event) {
    if (event.query.length > 2) {
      this.as.searchLDAPUsers(event.query).subscribe(data => {
        this.userNameResult(data);
      }, error => { });
    }
  }
  userNameResult(data) {
    this.dbvalue = [];
    this.dbDummyValue = [];
    const loginNames = JSON.parse(data._body);
    for (let index = 0; index < loginNames.length; index++) {
      this.dbvalue.push({
        'id': loginNames[index].login,
        'itemName': loginNames[index].login
      });
    }
  }
  selectSingle(event) {

  }
  makeUserAdmin() {
    this.ngxSmartModalService.getModal('assignAdmin').open();
  }
  makeUserInactivate() {
    this.ngxSmartModalService.getModal('inactivateUser').open();

  }
  resetPassword() {
    this.ngxSmartModalService.getModal('resetPasswords').open();
  }
  adminUserClick() {
    this.spinner.show();
    if (this.selectedUser.isAdmin === 'N') {
      this.us.setUserAsAdmin(this.selectedUser.EmpNo, 1).subscribe(data => { this.adminIsSet(data); }, error => { this.spinner.hide(); });
    } else {
      this.us.setUserAsAdmin(this.selectedUser.EmpNo, 0).subscribe(data => { this.adminIsRemove(data); }, error => { this.spinner.hide(); });
    }
  }
  adminIsSet(data) {
    this.spinner.hide();
    this.tr.success('User Set As Admin');
    this.activateUser(data);
  }
  adminIsRemove(data) {
    this.spinner.hide();
    this.tr.success('User is removed form admin');
    this.activateUser(data);
  }
  inactivateClick() {
    this.spinner.show();
    if (this.selectedUser.status === 'ACTIVE') {
      this.us.inactivateUser(this.userForm.controls.EmpNo.value).subscribe(data => this.inActivateUser(data), error => { this.spinner.hide(); });
    } else {
      this.us.activateUser(this.userForm.controls.EmpNo.value).subscribe(data => this.activateUser(data), error => { this.spinner.hide(); });
    }
  }
  resetOk() {
    this.spinner.show();
    this.us.resetPasswordForUser(this.selectedUser.EmpNo).subscribe(data => {
      this.tr.success('Reset password success');
      this.spinner.hide();
    }, error => { this.spinner.hide(); });
  }

  assignSupervisor() {
    this.ngxSmartModalService.getModal('assignedSupervisor').open();
    setTimeout(() => {
      this.spinner.show();
      this.us.getRoles().subscribe(data => { this.getRolesEvents(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
      this.us.getSupervisor(this.selectedUser.EmpNo).subscribe(supervisor => { this.exisistingSupervisorAssigned(supervisor), this.spinner.hide(); }, err => { this.spinner.hide(); });
    }, 0);
  }

  exisistingSupervisorAssigned(supervisor) {
    this.existingSupervisor = JSON.parse(supervisor._body);
  }

  getRolesEvents(data) {
    if (data !== null || data !== '') {
      this.rolesSelect = {};
      this.rolesToChoose = [];
      this.rolesToChoose = JSON.parse(data._body);
      this.roles = [];
      const value = {
        value: 0,
        label: 'Select'
      };
      this.roles.push(value);
      for (let role = 0; role < this.rolesToChoose.length; role++) {
        const objct = {
          value: this.rolesToChoose[role].id,
          label: this.rolesToChoose[role].name
        };
        this.roles.push(objct);
      }
    }
  }
  saveSupervisor() {
    this.spinner.show();
    this.us.setSupervisor(this.selectedUser.EmpNo, this.existingSupervisor.id).subscribe(data => {
      this.spinner.hide();
      this.tr.success('', 'Supervisor is set successfully');
      this.ngOnInit();
      this.formDirective.resetForm();
    }, error => { this.spinner.hide(); });
  }
  supervisorSelected(event) {
    for (let i = 0; i < this.rolesToChoose.length; i++) {
      if (this.rolesToChoose[i].id === (+event.value.value)) {
        this.existingSupervisor = this.rolesToChoose[i];
        break;
      }
      if (event.value.label === 'Select' && event.value.value === 0) {
        this.existingSupervisor.name = 'Unassigned';
        this.existingSupervisor.id = event.value.value;
        break;
      }
    }
  }

  type(event) {
    // console.log("TYPE: " + event.target.value);
    this.emailvalue = event.target.value;
  }

}


