import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { AdministrationService } from '../../../service/Administration.service';
import { TreeDragDropService } from 'primeng/primeng';

@Component({
  selector: 'app-orgtree',
  templateUrl: './orgtree.component.html',
  styleUrls: ['./orgtree.component.css']
})
export class OrgtreeComponent implements OnInit, OnChanges {
  public fillTree: any;
  @Input() repository;
  @Output() selectedOrg = new EventEmitter();
  @Output() topClick = new EventEmitter();
  @Input() addingNode;
  @Input() callingFrom;
  public asyncChildren;
  public topLevel;
  public selectedValues: any;
  constructor(private as: AdministrationService) {
  }

  ngOnInit() {

    this.addingNode = undefined;
  }
  expandAll(toggle: boolean) {
    this.fillTree.map(node => {
      node.expanded = toggle;
    });
    setTimeout(() => {
      this.fillTree[0].children.map(node1 => {
        node1.expanded = toggle;
      });
    }, 100);
  }
  ngOnChanges() {
    if (sessionStorage.getItem('index') !== '1') {
      this.addingNode = undefined;
    }
    if (this.repository !== undefined && this.addingNode === undefined) {
      this.as.getSubLevelOrgUnits(this.repository.id).subscribe(resdata => this.getTopLevelOrgUnits(resdata));
    } else if (this.addingNode !== undefined && (this.selectedValues.parent.id !== 0)) {

      this.pushNewNode(this.fillTree[0].children);
    }
  }
  pushNewNode(obj) {
    for (let k = 0; k < obj.length; k++) {
      obj[k].children.expanded = true;
      if (typeof obj[k] === 'object' && obj[k] !== null) {
        if (obj[k].id === this.addingNode.parent) {
          obj[k].expanded = true;
          obj[k].children.expanded = true;
          const childNode = {
            label: this.addingNode.name,
            parent: this.selectedValues,
            expanded: true,
            parentName: this.selectedValues.label,
            expandedIcon: 'ui-icon-person',
            collapsedIcon: 'ui-icon-person',
            children: []
          };
          obj[k].children.push(childNode);

          break;
        } else {
        }
        this.pushNewNode(obj[k].children);

      }
    }
  }
  getTopLevelOrgUnits(data) {
    const childrenNode = [];
    for (const orgData of JSON.parse(data._body)) {
      const childNode = {
        id: orgData.id,
        parent: orgData.parent,
        label: orgData.name,
        expandedIcon: 'ui-icon-person',
        collapsedIcon: 'ui-icon-person',
        hid: orgData.hid,
        orgCode: orgData.orgCode,
        headEmpNo: orgData.headRoleId,
        headRoleId: orgData.headRoleId,
        headRoleName: orgData.headRoleName,
        children: []
      };
      childrenNode.push(childNode);
    }
    this.fillTree = [];
    this.fillTree.push({
      id: this.repository.id,
      label: this.repository.name,
      parent: this.repository.parent,
      hid: this.repository.hid,
      expanded: true,
      expandedIcon: 'ui-icon-people',
      collapsedIcon: 'ui-icon-people',
      orgCode: this.repository.orgCode,
      headEmpNo: this.repository.headRoleId,
      headRoleId: this.repository.headRoleId,
      headRoleName: this.repository.headRoleName,
      parentName: this.repository.parentName,
      children: childrenNode
    });
    this.expandAll(true);
  }
  getOrgs(selected) {
    if (selected.node.parent === undefined && this.callingFrom !== 'roles') {
      this.topClick.emit(selected);
    }
    if (selected.node.id) {
      this.as.getSubLevelOrgUnits(selected.node.id).subscribe(resdata => this.addChildNodes(resdata, selected.node));
      this.selectedValues = selected.node;
      if (this.callingFrom === 'roles') {
        // const value = {
        //   name: selected.node.label
        // };
        // this.addingNode = value;
        // this.as.getSubLevelOrgUnits(selected.node.id).subscribe(resdata => this.newAddedNode(resdata), error => { });
        this.selectedOrg.emit(selected.node);
      }
    } else {
      const value = {
        parent: selected.node.parent.id,
        name: selected.node.label
      };
      this.addingNode = value;
      this.as.getSubLevelOrgUnits(this.addingNode.parent).subscribe(resdata => this.newAddedNode(resdata), error => { });
    }
  }
  newAddedNode(data) {
    const value = JSON.parse(data._body);
    for (let index = 0; index < value.length; index++) {
      if (value[index].name === this.addingNode.name) {
        this.selectedOrg.emit(value[index]);
        break;
      }
    }
  }

  addChildNodes(resdata, selected) {
    this.eachRecursive(this.fillTree[0].children, selected, resdata._body);
  }
  onUpdateData(event) {
  }

  eachRecursive(obj, selected, data) {
    for (let k = 0; k < obj.length; k++) {
      obj[k].children.expanded = true;
      if (typeof obj[k] === 'object' && obj[k] !== null) {
        if (obj[k].id === selected.id) {
          obj[k].children = [];
          obj[k].expanded = true;
          obj[k].children.expanded = true;
          for (const childNodeDetails of JSON.parse(data)) {
            const childNode = {
              id: childNodeDetails.id,
              label: childNodeDetails.name,
              parent: childNodeDetails.parent,
              hid: childNodeDetails.hid,
              expanded: true,
              orgCode: childNodeDetails.orgCode,
              headEmpNo: childNodeDetails.headRoleId,
              headRoleId: childNodeDetails.headRoleId,
              headRoleName: childNodeDetails.headRoleName,
              parentName: childNodeDetails.parentName,
              expandedIcon: 'ui-icon-person',
              collapsedIcon: 'ui-icon-person',
              children: []
            };
            obj[k].children.push(childNode);
          }
          this.selectedOrg.emit(selected);
          break;
        } else {
        }
        this.eachRecursive(obj[k].children, selected, data);

      }
    }
  }

}

