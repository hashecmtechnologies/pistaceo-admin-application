import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroupDirective, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../../service/user.service';
import { ToastrService } from 'ngx-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { SchemaService } from '../../service/schema.service';
import { DocumentType } from '../../models/documetType.model';

@Component({
  selector: 'app-document-types',
  templateUrl: './document-types.component.html',
  styleUrls: ['./document-types.component.css']
})
export class DocumentTypesComponent implements OnInit {
  @ViewChild(FormGroupDirective) formDirective: FormGroupDirective;
  public listOfDocument = [];
  public typeList = [];
  public isAdd = false;
  public isView = true;
  public documentType: FormGroup;
  constructor(private schemaService: SchemaService, private fb: FormBuilder, private tr: ToastrService, private cdr: ChangeDetectorRef,
    private spinner: Ng4LoadingSpinnerService, private ngxSmartModalService: NgxSmartModalService) { }

  ngOnInit() {
    this.typeList = [
      { label: 'Document', value: 'DOCUMENT' },
      { label: 'Form', value: 'FORM' }
    ];
    this.documentType = this.fb.group({
      id: [null],
      name: [null, Validators.compose([Validators.required])],
      primaryFlag: [null],
      req: [null],
      signEnabled: [null],
      type: ['DOCUMENT']
    });
    this.spinner.show();
    this.schemaService.getDocumentTypes().subscribe(data => { this.getDocumentTypes(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
  getDocumentTypes(data) {
    if (data._body) {
      this.listOfDocument = JSON.parse(data._body);
    }
  }
  clickedOnAdd() {
    this.isAdd = true;
    this.isView = false;
    this.documentType.reset();
  }
  clickedOnView() {
    this.isView = true;
    this.isAdd = false;
  }
  editDocumentType(event) {
    this.isAdd = false;
    this.isView = false;
    this.documentType.patchValue({
      id: event.data.id,
      name: event.data.name,
      primaryFlag: event.data.primaryFlag,
      req: event.data.req,
      signEnabled: event.data.signEnabled,
      type: event.data.type
    });
    if (event.data.id === 1) {
      this.documentType.disable();
    } else {
      this.documentType.enable();
    }
  }
  documentTypeSubmitted(event) {
    // if (this.isAdd) {
    //   const newDocument = new DocumentType();
    //   newDocument.name = this.documentType.controls.name.value;
    //   newDocument.type = 'DOCUMENT';
    //   this.spinner.show();
    //   this.schemaService.saveDocumentType(newDocument).subscribe(data => {
    //     this.refreshDocumentList(data); this.spinner.hide();
    //     this.tr.success('', 'New Document Type Added');
    //   }, error => { this.spinner.hide(); });
    // } else {
    const newDocument = new DocumentType();
    newDocument.id = this.documentType.controls.id.value;
    newDocument.name = this.documentType.controls.name.value;
    newDocument.type = this.documentType.controls.type.value;
    if (this.documentType.controls.primaryFlag.value === true || this.documentType.controls.primaryFlag.value === 1) {
      newDocument.primaryFlag = 1;
    } else {
      newDocument.primaryFlag = 0;
    }
    if (this.documentType.controls.signEnabled.value === true || this.documentType.controls.signEnabled.value === 1) {
      newDocument.signEnabled = 1;
    } else {
      newDocument.signEnabled = 0;
    }
    if (this.documentType.controls.req.value === true || this.documentType.controls.req.value === 1) {
      newDocument.req = 1;
    } else {
      newDocument.req = 0;
    }
    this.spinner.show();
    this.schemaService.saveDocumentType(newDocument).subscribe(data => {
      this.refreshDocumentList(data); this.spinner.hide();
      // this.tr.success('', 'Updated Document Type');
    }, error => { this.spinner.hide(); });
    // }


  }
  documentTypeSelected(event) {

  }
  refreshDocumentList(data) {
    if (this.isAdd) {
      this.tr.success('', 'New Document Type Added');
    } else {
      this.tr.success('', 'Updated Document Type');
    }
    this.isAdd = false;
    this.isView = true;
    this.ngOnInit();
  }
  cancelAdd() {
    this.documentType.reset();
    this.documentType.enable();
    this.formDirective.resetForm();
    this.documentType.patchValue({
      type: 'DOCUMENT'
    });
  }
}
