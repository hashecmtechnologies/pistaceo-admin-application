import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormGroupDirective, FormBuilder, Validators } from '@angular/forms';
import { ContentService } from '../../service/content.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../service/user.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-classes',
  templateUrl: './classes.component.html',
  styleUrls: ['./classes.component.css']
})
export class ClassesComponent implements OnInit {
  @ViewChild(FormGroupDirective) formDirective: FormGroupDirective;
  public showClassDropDown = false;
  classFrom: FormGroup;
  public documentResult = [];
  public isAdd = false;
  public isView = true;
  public propTemplatesDrop = [];
  public accessLevelDrop = [];
  public singleSelectDropdown: any;
  public dbvalue = [];
  public dbDummyValue = [];
  public isActive = true;
  public editId: any;
  public propList = true;
  public permissionList = true;
  public propform: FormGroup;
  public permissionForm: FormGroup;
  public alertType: any;
  public result: any;
  public closeResult: any;
  public selectedTab1 = true;
  public selectedTab2 = false;
  public reIndexClicked = false;
  public accessTypeOptions: any;
  public accessLevelSmall: any;
  public deletepermissionsValue: any;
  public selectedTabProperty1 = true;
  public selectedTabProperty2 = false;
  constructor(private cs: ContentService, private fb: FormBuilder, private tr: ToastrService,
    private us: UserService, private spinner: Ng4LoadingSpinnerService, public ngxSmartModalService: NgxSmartModalService) { }

  ngOnInit() {
    this.spinner.show();
    this.callDocClasses();
    this.classFrom = this.fb.group({
      name: [null, Validators.compose([Validators.required])],
      symName: [null, Validators.compose([Validators.required, Validators.pattern('[a-z]*')])],
      proptemplate: [null],
      accesslevel: [3],
      roleid: [null],
      roleName: [null],
      accesstype: ['This Class Only']
    });
    this.getPropTemplated();
    this.accessLevelDrop = [
      { label: 'Full Control (Delete)', value: 127 },
      { label: 'Owner (Permissions)', value: 63 },
      { label: 'Author (Versioning)', value: 31 },
      { label: 'Author (Modify)', value: 51 },
      // { label: 'Author (Download)', value: 7 },
      // { label: 'Author (Annotate)', value: 3 },
      { label: 'Viewer', value: 1 }
    ];
    this.accessTypeOptions = [
      { label: 'Class', value: 0 },
      { label: 'Class Documents', value: 1 }
    ];
    this.accessLevelSmall = [
      { label: 'Owner', value: 3 },
      { label: 'Create document', value: 1 }
    ];
    this.propform = this.fb.group({
      propid: [null, Validators.compose([Validators.required])],
      required: [1, Validators.compose([Validators.required])]
    });
    this.permissionForm = this.fb.group({
      proproleid: [null, Validators.compose([Validators.required])],
      propaccesslevel: [3],
      propaccesstype: ['0']

    });
  }
  radioChanged(event) {

  }
  getPropTemplated() {
    this.cs.getPropertyTemplates().subscribe(data => { this.proptemdrop(data); });
  }
  callDocClasses() {
    this.cs.getDocumentClasses().subscribe(data => { this.documentClass(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
  proptemdrop(data) {
    this.propTemplatesDrop = [];
    const value = JSON.parse(data._body);
    value.forEach(element => {
      const forValue = {
        label: element.name,
        value: element.id
      };
      this.propTemplatesDrop.push(forValue);
    });

  }
  addClicked() {
    this.isAdd = true;
    this.isView = false;
    this.classFrom.reset();
    this.classFrom.enable();
    this.classFrom.patchValue({ accesslevel: 3, accesstype: 'This Class Only', proptemplate: this.propTemplatesDrop[0].id });
  }
  documentClass(data) {
    this.documentResult = [];
    this.documentResult = JSON.parse(data._body);
  }
  clickedOnView() {
    this.isView = true;
    this.isAdd = false;
    this.reIndexClicked = false;
  }
  classFormSubmitted(event) {

    this.spinner.show();
    if (this.isAdd) {
      const value = {
        name: this.classFrom.value.name,
        symName: this.classFrom.value.symName,
        permissions: [{
          roleId: -10000,
        }]
      };
      this.cs.saveDocumentClass(value).subscribe(data => { this.added(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
    } else {
      const value = {
        name: this.classFrom.value.name,
        id: this.editId.id
      };
      this.cs.saveDocumentClass(value).subscribe(data => { this.edited(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
    }

  }
  edited(data) {
    this.spinner.hide();
    this.tr.success('', 'Existing class type updated');
    this.callDocClasses();
  }
  added(data) {
    this.spinner.hide();
    this.tr.success('', 'New class type added');
    this.callDocClasses();
    this.classFrom.reset();
    this.classFrom.patchValue({ accesslevel: 3, accesstype: 'This Class Only', proptemplate: this.propTemplatesDrop[0].id });
    this.isActive = true;
  }
  selectSingle(event) {

  }
  deletePropertyAlert(event) {
    this.result = event;
    this.ngxSmartModalService.getModal('deletePropertyConfirmationAlert').open();
  }
  deletePropertyConfirmed() {
    this.spinner.show();
    this.cs.deletePropertyFromClass(this.editId.id, this.result.id).subscribe(data => { this.propSubmitted(data); this.tr.success('', 'Deleted property from class'); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
  propSubmitted(data) {
    this.isAdd = false;
    this.isView = false;
    this.classFrom.reset();
    this.classFrom.patchValue({ name: this.editId.name, symName: this.editId.symName });
    this.classFrom.controls.symName.disable();
    this.propform.reset();
    this.propform.patchValue({ required: 1 });
    setTimeout(() => {
      this.propList = true;
      this.callDocClasses();
    }, 0);
  }
  deletePermissionAlert(event) {
    this.deletepermissionsValue = event;
    this.ngxSmartModalService.getModal('deleteConfirmationAlert').open();
  }
  deletePermissionConfirmed() {
    this.spinner.show();
    const value = {
      name: this.classFrom.value.name,
      symName: this.classFrom.value.symName,
      id: this.editId.id,
      permissions: [{
        roleId: this.deletepermissionsValue.roleId,
        accessMask: 0,
        accessType: this.deletepermissionsValue.accessType,
        aclId: this.deletepermissionsValue.aclId
      }]
    };
    this.cs.updateClassSecurity(value).subscribe(data => { this.permissionupdated(data); this.tr.success('', 'Deleted permissions'); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
  reindexConfirmed() {
    this.spinner.show();
    this.reIndexClicked = false;
    this.cs.indexClassDocument(this.editId.symName).subscribe(data => { this.tr.success('Reindex is done'); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
  cancelAdd() {
    this.classFrom.reset();
    this.classFrom.patchValue({ accesslevel: 3 });
    if (!this.isAdd) {
      this.classFrom.patchValue({ name: this.editId.name, symName: this.editId.symName });
    }
  }
  cancelAddProp() {
    this.propform.reset();
    this.propform.patchValue({ required: 1 });
  }
  cancelAddPermission() {
    this.permissionForm.reset();
    this.permissionForm.patchValue({ propaccesslevel: 3, propaccesstype: '0' });
  }
  clickedOnAdd() {
    this.isAdd = true;
    this.isView = false;
    this.classFrom.reset();
    this.classFrom.controls.symName.enable();
    this.classFrom.patchValue({ accesslevel: 3 });
    this.reIndexClicked = false;
  }
  onSearchAutoStextTA(event) {
    let stringTyped = '';
    stringTyped = event.query;
    if (stringTyped.length > 2) {
      this.us.searchRoles(stringTyped).subscribe(data => this.getAllRoles(data));
    }
  }
  getAllRoles(data) {
    this.dbvalue = [];
    this.dbDummyValue = [];
    const getrolecategory = JSON.parse(data._body);
    for (let index = 0; index < getrolecategory.length; index++) {
      const dbValues = {
        'value': getrolecategory[index].id,
        'name': getrolecategory[index].name
      };
      this.dbvalue.push(dbValues);
    }
    this.spinner.hide();
  }
  accessLevelChanged(event) {

  }
  editClass(event) {
    setTimeout(() => {
      if (!this.reIndexClicked) {
        this.isAdd = false;
        this.isView = false;
        this.classFrom.reset();
        this.classFrom.patchValue({ name: event.data.name, symName: event.data.symName });
        this.classFrom.controls.symName.disable();
        this.editId = event.data;
      } else {
        this.editId = event.data;
        this.ngxSmartModalService.open('reindexAlertModel');

      }
    }, 100);
  }
  onTabChange(event) {

  }
  reindex() {
    this.reIndexClicked = true;
  }
  addSymname() {
    if (this.isAdd) {
      let myString = '';
      myString = this.classFrom.controls['name'].value.replace(/[^A-Za-z]+/g, '');
      const resString = myString.toLowerCase();
      this.classFrom.patchValue({ symName: resString });
    }
  }
  permissionFormSubmitted(event) {
    this.selectedTab1 = false;
    this.selectedTab2 = true;
    this.spinner.show();
    const value = {
      name: this.classFrom.value.name,
      symName: this.classFrom.value.symName,
      id: this.editId.id,
      permissions: [{
        roleId: this.permissionForm.value.proproleid.value,
        accessMask: this.permissionForm.value.propaccesslevel,
        inheritDepth: this.permissionForm.value.propaccesstype
      }]
    };
    this.cs.updateClassSecurity(value).subscribe(data => { this.permissionupdated(data); this.spinner.hide(); this.selectedTab2 = false; this.selectedTab1 = true; }, error => { this.spinner.hide(); });
    this.tr.success('', 'Updated permissions');
  }
  permissionupdated(data) {
    this.permissionList = true;
    this.isAdd = false;
    this.isView = false;
    this.classFrom.reset();
    this.permissionForm.reset();
    this.permissionForm.patchValue({ propaccesslevel: 3, propaccesstype: '0' });
    this.classFrom.patchValue({ name: this.editId.name, symName: this.editId.symName });
    this.classFrom.controls.symName.disable();
    this.callDocClasses();
  }
  accessTypeChanged(event) {
    if (event.value === 1) {
      this.showClassDropDown = true;
      this.permissionForm.patchValue({ propaccesslevel: 127 });
    } else {
      this.showClassDropDown = false;
      this.permissionForm.patchValue({ propaccesslevel: 3 });
    }

  }
  propFormSubmitted(event) {
    this.selectedTabProperty1 = false;
    this.selectedTabProperty2 = true;
    this.spinner.show();
    this.cs.addPropertyToClass(this.editId.id, this.propform.value.propid, this.propform.value.required).subscribe(data => {
      this.propSubmitted(data);
      this.selectedTabProperty1 = true; this.selectedTabProperty2 = false; this.tr.success('', 'Added property to class'); this.spinner.hide();
    }, error => { this.spinner.hide(); });
  }
}

