import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FileUploader } from 'ng2-file-upload';
import { ReportService } from '../../service/report.service';
import { DocumentService } from '../../service/document.service';
import { SchemaService } from '../../service/schema.service';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';
import { UserService } from '../../service/user.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {
  public reportForm: FormGroup;
  public showreportTab = false;
  public addeditReportToggle = false;
  public editReportToggle = false;
  public addReportToggle = false;
  public selectedReport;
  public templateList;
  public defid = new FileUploader({});
  public documentFileName = 'Please upload a jasper file';
  public getRoleToAdd = [];
  public rolename: any;
  public addRoleForm: FormGroup;
  public showRoles = [];
  public deleteRole: any;
  public isAdd = false;
  public isView = true;
  public source: any;
  constructor(private reportService: ReportService, private ds: DocumentService, private fb: FormBuilder, private schemaService: SchemaService,
    private tr: ToastrService, private _sanitizer: DomSanitizer, private us: UserService, private spinner: Ng4LoadingSpinnerService, private smartModal: NgxSmartModalService) { }

  ngOnInit() {
    this.reportForm = this.fb.group({
      name: [null, Validators.compose([Validators.required])],
      defid: [null, Validators.compose([Validators.required])],
      formName: [null],
      formDef: [null],
      description: [null]
    });
    this.addRoleForm = this.fb.group({
      roleName: [null, Validators.required],
      roleid: [null]
    });
    this.spinner.show();
    this.schemaService.getSimpleForms().subscribe(data => this.getAllTemplate(data));
    this.reportService.getReports().subscribe(data => { this.getReports(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
    this.us.getRoles().subscribe(data => { this.getRoles(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
  getRoles(data) {
    this.getRoleToAdd = [];
    const value = JSON.parse(data._body);
    value.forEach(element => {
      const role = {
        label: element.name,
        value: element.id
      };
      this.getRoleToAdd.push(role);
    });
  }
  getAllTemplate(data) {
    const value = JSON.parse(data._body);
    this.templateList = [];
    value.forEach(element => {
      const value1 = {
        label: element.name,
        value: element.docId
      };
      this.templateList.push(value1);
    });
  }
  getReports(data) {
    this.source = JSON.parse(data._body);
  }

  addNewReportForm() {
    this.documentFileName = 'Please upload a jasper file';
    this.addeditReportToggle = true;
    this.addReportToggle = true;
    this.editReportToggle = false;
    this.selectedReport = null;
    this.reportForm.reset();
    this.defid.clearQueue();
  }

  onReportSubmit(formReport) {
    this.spinner.show();
    if (this.selectedReport !== undefined && this.selectedReport !== null) {
      const report = {
        'id': this.selectedReport.id,
        'name': this.reportForm.controls.name.value,
        'defid': this.reportForm.controls.defid.value,
        'formName': this.reportForm.controls.formName.value,
        'formDef': this.reportForm.controls.formDef.value,
        'desc': this.reportForm.controls.description.value,
        'defName': this.documentFileName,
      };
      this.reportService.updateReport(report).subscribe(data => { this.refreshReports(data); this.tr.success('Report Edited'); }, error => { this.spinner.hide(); });
    } else {
      const report = {
        'name': this.reportForm.controls.name.value,
        'defid': this.reportForm.controls.defid.value,
        'formName': this.reportForm.controls.formName.value,
        'formDef': this.reportForm.controls.formDef.value,
        'desc': this.reportForm.controls.description.value,
        'defName': this.documentFileName
      };
      this.reportService.addReport(report).subscribe(data => { this.refreshReports(data); this.tr.success('Added New Report'); }, error => { this.spinner.hide(); });
    }
  }

  refreshReports(data) {
    this.editReportToggle = false;
    this.addReportToggle = false;
    this.addeditReportToggle = false;
    this.isAdd = false;
    this.isView = true;
    this.reportService.getReports().subscribe(dataRes => { this.getReports(dataRes); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }

  documentAdded(data) {
    if (data._body !== '') {
      this.reportForm.patchValue({
        defid: data._body
      });
    }
  }

  fileChanged(defid) {
    if (this.defid.queue.length > 0) {
      const defLen = this.defid.queue.length - 1;
      const string = this.defid.queue[defLen].file.name.split('.')[1];
      if (string === 'jasper') {
        if (this.reportForm.controls.defid.value !== null) {
          this.spinner.show();
          this.ds.checkOut(this.reportForm.controls.defid.value).subscribe(data => { this.checkinfile(data); }, error => { this.spinner.hide(); });
        } else {
          this.spinner.show();
          const docInfo = {
            docclass: '',
            docTitle: this.defid.queue[0].file.name,
            props: [{
              'name': 'Document Title',
              'symName': 'DocumentTitle',
              'dtype': 'STRING',
              'mvalues': [this.defid.queue[0].file.name],
              'mtype': 'N',
              'len': 255,
              'rOnly': 'false',
              'hidden': 'false',
              'req': 'false'
            }],
          };
          const formData = new FormData();
          formData.append('DocInfo', JSON.stringify(docInfo));
          formData.append('file', this.defid.queue[0]._file);
          this.documentFileName = this.defid.queue[0].file.name;
          this.defid.clearQueue();
          this.ds.addDocument(formData).subscribe(data => { this.documentAdded(data); this.spinner.hide(); }, error => { this.defid.clearQueue(); });
        }
      } else {
        this.tr.error('', 'Select Only .jasper File');
      }

    }
  }

  checkinfile(data) {
    const docInfo = {
      id: data._body,
      docclass: 'ProductivitiConfig',
      docTitle: this.defid.queue[0].file.name,
      props: [{
        'name': 'Document Title',
        'symName': 'DocumentTitle',
        'dtype': 'STRING',
        'mvalues': [this.defid.queue[0].file.name],
        'mtype': 'N',
        'len': 255,
        'rOnly': 'false',
        'hidden': 'false',
        'req': 'false'
      }],
      accessPolicies: []
    };
    const name = this.defid.queue[0].file.name;
    const formData = new FormData();
    formData.append('DocInfo', JSON.stringify(docInfo));
    formData.append('file', this.defid.queue[0]._file);
    this.ds.checkIn(formData).subscribe(dataRes => { this.documentAdded(dataRes); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
  onUserRowSelect(event) {
    this.isAdd = false;
    this.isView = false;
    this.selectedReport = event.data;
    this.reportService.getReportRoles(this.selectedReport.id).subscribe(data => {
      this.showRolesResult(data);
    }, error => {
    });
    this.editReportToggle = false;
    this.editReportToggle = true;
    this.addReportToggle = false;
    this.addeditReportToggle = true;
    this.documentFileName = event.data.fileName;
    this.documentFileName = event.data.defName;
    this.reportForm.patchValue({ name: event.data.name, defid: event.data.defid, formName: event.data.formName, description: event.data.desc, formDef: event.data.formDef });
  }
  roleSelected(rolename) {
    let flag = false;
    for (let index = 0; index < this.showRoles.length; index++) {
      if (this.showRoles[index].id === rolename.value) {
        flag = false;
        this.tr.warning('', 'This Role Is Already Available');
        this.addRoleForm.patchValue({ roleName: '' });
        break;
      } else {
        flag = true;
      }
    }
    if (flag || this.showRoles.length === 0) {
      this.addRoleForm.patchValue({ roleid: rolename.value });
    }
  }
  userClear() {
    this.addRoleForm.reset();
  }
  addRole(event) {
    this.spinner.show();
    this.reportService.addRoleToReport(this.selectedReport.id, this.addRoleForm.value.roleid).subscribe(data => {
      this.addRoleForm.reset(); this.addRoleForm.patchValue({ roleName: '' }); this.roleAdded(data); this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }
  roleAdded(data) {
    this.spinner.show();
    this.reportService.getReportRoles(this.selectedReport.id).subscribe(dataRes => {
      this.showRolesResult(dataRes); this.spinner.hide();
    }, error => { this.spinner.hide(); });
  }
  showRolesResult(data) {
    this.showRoles = [];
    this.showRoles = JSON.parse(data._body);
  }
  removeRole() {
    this.spinner.show();
    this.reportService.removeRoleFromReport(this.selectedReport.id, this.deleteRole.id).subscribe(data => {
      this.roleAdded(data); this.spinner.hide();
    }, error => { this.spinner.hide(); });
  }
  selectedFormName(event) {
    for (let index = 0; index < this.templateList.length; index++) {
      if (event.value === this.templateList[index].docId) {
        this.reportForm.patchValue({ formName: this.templateList[index].name });
      }
    }
  }
  deleteRoleModel(value) {
    this.deleteRole = value;
    this.smartModal.getModal('deleteModel').open();
  }
  clickedOnAdd() {
    this.isAdd = true;
    this.reportForm.reset();
    this.isView = false;
    this.documentFileName = '';
    this.editReportToggle = false;
  }
  clickedOnView() {
    this.isAdd = false;
    this.isView = true;
  }
  cancelAdd() {
    this.reportForm.reset();
    this.defid = new FileUploader({});
    this.documentFileName = '';
  }
}
