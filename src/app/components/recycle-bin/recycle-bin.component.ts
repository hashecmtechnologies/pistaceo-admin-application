import { Component, OnInit } from '@angular/core';
import { UserService } from '../../service/user.service';
import { ContentService } from '../../service/content.service';
import { ToastrService } from 'ngx-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-recycle-bin',
  templateUrl: './recycle-bin.component.html',
  styleUrls: ['./recycle-bin.component.css']
})
export class RecycleBinComponent implements OnInit {

  public source = [];
  constructor(private us: UserService, private cs: ContentService, private tr: ToastrService, private spinner: Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.spinner.show();
    this.cs.getRecycleBin().subscribe(data => { this.getRoles(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
  restore(event) {
    this.spinner.show();
    this.cs.restoreDocument(event.data.id).subscribe(data => {
      this.tr.success('Restored Document Successfully'); this.ngOnInit(); this.spinner.hide();
    }, error => { this.spinner.hide(); });
  }
  getRoles(data) {
    this.source = [];
    this.source = JSON.parse(data._body);
  }

}
