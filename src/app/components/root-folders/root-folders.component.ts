import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormGroupDirective } from '@angular/forms';
import { ContentService } from '../../service/content.service';
import { UserService } from '../../service/user.service';
import { ToastrService } from 'ngx-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-root-folders',
  templateUrl: './root-folders.component.html',
  styleUrls: ['./root-folders.component.css']
})
export class RootFoldersComponent implements OnInit {
  @ViewChild(FormGroupDirective) formDirective: FormGroupDirective;
  public rootFolderForm: FormGroup;
  public folderid: any;
  public permissionForm: FormGroup;
  public permissionList = true;
  public singleSelectDropdown: any;
  public dbvalue = [];
  public dbDummyValue = [];
  public accessLevelDrop = [];
  public permissionsResult = [];
  public closeResult: any;
  public editId;
  public deleteValue: any;
  public isAdd = false;
  public isView = true;
  public rootFolderResult = [];
  public selectedTab1 = true;
  public selectedTab2 = false;
  public appliesToDrop = [];
  constructor(private cs: ContentService, private fb: FormBuilder, private us: UserService, private tr: ToastrService,
    private spinner: Ng4LoadingSpinnerService, private ngxSmartModalService: NgxSmartModalService) { }

  ngOnInit() {
    this.accessLevelDrop = [
      { label: 'Full Control (Delete)', value: 127 },
      { label: 'Owner (Permissions)', value: 63 },
      { label: 'Unfile', value: 31 },
      { label: 'File', value: 51 },
      { label: 'Create', value: 7 },
      { label: 'Modify', value: 3 },
      { label: 'View', value: 1 }
    ];
    this.appliesToDrop = [
      { label: 'Folder', value: 0 },
      { label: 'Folder Document', value: 1 }
    ];
    this.spinner.show();
    this.cs.getTopFolders().subscribe(data => { this.rootFolders(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
    this.rootFolderForm = this.fb.group({
      name: [null, Validators.compose([Validators.required])]
    });
    this.permissionForm = this.fb.group({
      proproleid: [null, Validators.compose([Validators.required])],
      propaccesslevel: [127],
      propaccesstype: ['0']
    });
  }
  rootFolders(data) {
    this.rootFolderResult = [];
    this.rootFolderResult = JSON.parse(data._body);

  }
  clickedOnAdd() {
    this.isAdd = true;
    this.isView = false;
    this.rootFolderForm.reset();
  }
  editRootFolder(event) {
    this.isAdd = false;
    this.isView = false;
    this.rootFolderForm.reset();
    this.rootFolderForm.patchValue({ name: event.data.name });
    this.folderid = event.data;
    this.cs.getFolder(this.folderid.id).subscribe(data => { this.folderResult(data); });
  }
  folderResult(data) {
    this.permissionsResult = [];
    const value = JSON.parse(data._body);
    this.permissionsResult = value.permissions;
  }
  clickedOnView() {
    this.isView = true;
    this.isAdd = false;
  }
  rootFolderSubmitted(event) {
    this.spinner.show();
    if (this.isAdd) {
      this.cs.createSubfolder(encodeURIComponent(this.rootFolderForm.value.name), 0).subscribe(data => { this.added(data); }, error => { this.spinner.hide(); });
    } else {
      this.cs.renameFolder(encodeURIComponent(this.rootFolderForm.value.name), this.folderid.id).subscribe(data => this.renamed(data), error => { this.spinner.hide(); });
    }

  }
  added(data) {
    this.rootFolderForm.reset();
    this.isAdd = true;
    this.isView = false;
    this.tr.success('New root folder added');
    this.cs.getTopFolders().subscribe(result => { this.rootFolders(result); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
  renamed(data) {
    this.rootFolderForm.reset();
    this.isAdd = false;
    this.isView = false;
    this.tr.success('Updated root folder name');
    this.cs.getTopFolders().subscribe(result => { this.rootFolders(result); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
  selectSingle(event) {

  }
  cancelAdd() {
    this.rootFolderForm.reset();
    this.formDirective.resetForm();
  }
  permissionFormSubmitted(event) {
    this.selectedTab1 = false;
    this.selectedTab2 = true;
    const value = {
      name: this.rootFolderForm.value.name,
      id: this.folderid.id,
      permissions: [{
        roleId: this.permissionForm.value.proproleid.value,
        accessMask: this.permissionForm.value.propaccesslevel,
        inheritDepth: this.permissionForm.value.propaccesstype
      }]
    };
    this.spinner.show();
    this.cs.updateFolderSecurity(value).subscribe(data => { this.permissionupdated(data, 'edit'); this.selectedTab2 = false; this.selectedTab1 = true; }, error => { this.spinner.hide(); });
  }
  onSearchAutoStextTA(event) {
    let stringTyped = '';
    stringTyped = event.query;
    if (stringTyped.length > 2) {
      this.us.searchRoles(stringTyped).subscribe(data => this.getAllRoles(data));
    }
  }
  getAllRoles(data) {
    this.dbvalue = [];
    this.dbDummyValue = [];
    const getrolecategory = JSON.parse(data._body);
    for (let index = 0; index < getrolecategory.length; index++) {
      this.dbvalue.push({
        'value': getrolecategory[index].id,
        'name': getrolecategory[index].name
      });
    }
  }
  deleteAlert(value) {
    this.deleteValue = value;
    this.ngxSmartModalService.getModal('deleteConfirmationAlert').open();
  }
  accessLevelChanged(event) {

  }
  deleteConfirmed() {
    const value = {
      name: this.rootFolderForm.value.name,
      id: this.folderid.id,
      permissions: [{
        roleId: this.deleteValue.roleId,
        accessMask: 0,
        accessType: this.deleteValue.accessType,
        inheritDepth: this.deleteValue.inheritDepth,
        aclId: this.deleteValue.aclId
      }]
    };
    this.spinner.show();
    this.cs.updateFolderSecurity(value).subscribe(data => { this.permissionupdated(data, 'delete'); this.tr.success('Delete success'); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
  permissionupdated(data, editis) {
    this.permissionList = true;
    this.isAdd = false;
    this.isView = false;
    this.rootFolderForm.reset();
    this.permissionForm.reset();
    this.permissionForm.patchValue({ propaccesslevel: 127, propaccesstype: '0' });
    this.rootFolderForm.patchValue({ name: this.folderid.name });
    this.cs.getFolder(this.folderid.id).subscribe(datas => {
      this.folderResult(datas);
      if (editis !== 'delete') {
        this.tr.success('Updated permission');
      }
      this.spinner.hide();
    }, error => { this.spinner.hide(); });
  }
  cancelAddPer() {
    this.permissionForm.reset();
    this.permissionForm.patchValue({ propaccesslevel: 127, propaccesstype: '0' });
  }
}
