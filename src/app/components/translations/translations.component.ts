import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormGroupDirective } from '@angular/forms';
import { AdministrationService } from '../../service/Administration.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-translations',
  templateUrl: './translations.component.html',
  styleUrls: ['./translations.component.css']
})
export class TranslationsComponent implements OnInit {
  @ViewChild(FormGroupDirective) formDirective: FormGroupDirective;
  public isAdd = false;
  public isView = true;
  public translationResult = [];
  public translationsForm: FormGroup;
  public languageDrop = [];
  public editId: any;
  public isDelete = false;
  constructor(private as: AdministrationService, private fb: FormBuilder, private tr: ToastrService, private smartModal: NgxSmartModalService,
    private spinner: Ng4LoadingSpinnerService) {
  }

  ngOnInit() {
    this.spinner.show();
    this.as.getTranslations().subscribe(data => { this.getTanslationsResult(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
    this.translationsForm = this.fb.group({
      language: ['en', Validators.compose([Validators.required])],
      enValue: [null, Validators.compose([Validators.required])],
      langValue: [null, Validators.compose([Validators.required])]
    });
    this.languageDrop = [
      { label: 'English', value: 'en' },
      { label: 'Kannada', value: 'ka' },
      { label: 'Arabic', value: 'ar' }
    ];
    this.isDelete = false;
  }
  getTanslationsResult(data) {
    this.translationResult = [];
    this.translationResult = JSON.parse(data._body);
  }
  clickedOnAdd() {
    this.isAdd = true;
    this.isView = false;
    this.translationsForm.reset();
    this.translationsForm.patchValue({ language: 'en' });
  }
  clickedOnView() {
    this.isAdd = false;
    this.isView = true;
  }
  editTranslation(event) {
    this.editId = event.data;
    setTimeout(() => {
      if (this.isDelete) {
        this.isAdd = false;
        this.isView = true;
      } else {
        this.isView = false;
        this.isAdd = false;
        this.translationsForm.reset();
        this.translationsForm.patchValue({ language: event.data.lang, enValue: event.data.enString, langValue: event.data.langString });
      }
    }, 100);
  }
  languageChanged(event) {

  }
  cancelAdd() {
    this.translationsForm.reset();
    this.translationsForm.enable();
    this.formDirective.resetForm();
    this.translationsForm.reset();
    this.translationsForm.patchValue({ language: 'en' });
  }
  translationsFormSubmitted(event) {
    this.spinner.show();
    if (!this.isAdd) {
      const value = {
        id: this.editId.id,
        lang: this.translationsForm.value.language,
        enString: this.translationsForm.value.enValue,
        langString: this.translationsForm.value.langValue
      };
      this.as.saveTranslation(value).subscribe(data => { this.saved(data); }, error => { this.spinner.hide(); });
    } else {
      const value = {
        lang: this.translationsForm.value.language,
        enString: this.translationsForm.value.enValue,
        langString: this.translationsForm.value.langValue
      };
      this.as.saveTranslation(value).subscribe(data => { this.saved(data); }, error => { this.spinner.hide(); });
    }
  }
  saved(data) {
    if (this.isAdd) {
      this.tr.success('', 'New word added');
    }
    if (!this.isAdd) {
      this.tr.success('', 'Existing word updated');
    }
    this.isAdd = false;
    this.isView = true;

    this.as.getTranslations().subscribe(result => { this.getTanslationsResult(result); this.spinner.hide(); }, error => { this.spinner.hide(); });
    this.translationsForm.reset();
  }
  deleteAlert(event) {
    this.isDelete = true;
    this.smartModal.getModal('deleteAlertModel').open();
  }
  deleteConfirmed() {
    this.spinner.show();
    this.as.deleteTranslation(this.editId.id).subscribe(data => { this.deleted(data); }, error => { this.spinner.hide(); });
  }
  deleted(data) {
    this.tr.success('', 'Word deleted');
    this.as.getTranslations().subscribe(result => { this.getTanslationsResult(result); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
}
