import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { FormProps } from '../../models/formProps.model';
import { Fform } from '../../models/Fform.model';
import { FileUploader } from 'ng2-file-upload';
import { IntegrationService } from '../../service/integration.service';
import { ContentService } from '../../service/content.service';
import { SchemaService } from '../../service/schema.service';
import { DocumentService } from '../../service/document.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { ToastrService } from 'ngx-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit {
  public addForm: FormGroup;
  public showSections = false;
  public property1 = [];
  public addProp = new FormProps();
  public isAddTab = false;
  public isListTab = true;
  public showAddTab = false;
  public showAddFormName = false;
  public formsName: FormGroup;
  public formsArray = new Fform();
  public showFormButton = true;
  public showAddSectionforms = false;
  public sectionForms: FormGroup;
  public showCSSns = false;
  public addColumn: FormGroup;
  public selectedSection: any;
  public selectedColumn: any;
  public showAddPropArea = false;
  public addFormProps: FormGroup;
  public showAddSectionsBut = false;
  public showAddColumnsBut = false;
  public disableAddColumnbut = false;
  public showAddPropsBut = false;
  public selectedSectionId = null;
  public arrayToShow = [];
  public editSectionItems = false;
  public editColumns = false;
  public editProperty = false;
  public secIndex: any;
  public colIndex: any;
  public propsIndex: any;
  public tempIndex: any;
  public selectedPropType = '';
  public date = new Date();
  public dbValue = [];
  public typeAV = [];
  public singleSelectDropdown: any;
  public multipleSelectDropdown: any;
  public showSaveFormBut = false;
  public source = [];
  public showView = [];
  public readDate = true;
  public sectionFont: any;
  public colFont: any;
  public propFont: any;
  public formFont = false;
  public emptyArray = [];
  public singleSelectDropdownDisabled: any;
  public multipleSelectDropdownDisabled: any;
  public showEditFormName = false;
  public templateList = [];
  public isForm = true;
  public propNames = [];
  public checkIn = new FileUploader({});
  public adddatatableitems = false;
  public editDatatableItems = false;
  public addDatatableBtn = false;
  public datatableIndex = -1;
  public actionSelected: any;
  public visibilityShow = [];
  public visibilityHide = [];
  public visiblityRonly = [];
  public visibilityHidSec = [];
  public visibilityRolySec = [];
  public addDatatableForm: FormGroup;
  public docId: any;
  public editIs = false;
  public disableFormType = false;
  public importForm;
  public defid = new FileUploader({});
  public time: any;
  public calcArray = [];
  public autoFillArray = [];
  public dateDifferenceProps = [];
  public dateExpiryProps = [];
  public propTemplatesDrop = [];
  public selectedType = 'TEXT';
  public selectedActionDrop = 'NONE';
  public copyToArray = [];
  public date1: Date = new Date();
  public formName: any;
  public formTypeDropDown = [];
  public addRowsDropDown = [];
  public columnWidthDropDown = [];
  public typeDropDown = [];
  public uiTypeDropDown = [];
  public dbTypeDropDown = [];
  public actionsDropDwon = [];
  public templateDropDown = [];
  public colCalcDropDown = [];
  public copyToArryDropDown = [];
  public proplookupArray = [];
  constructor(private fb: FormBuilder, private integrationservice: IntegrationService, private cs: ContentService,
    private ss: SchemaService, private ds: DocumentService, private modalService: NgxSmartModalService,
    private tr: ToastrService, private spinner: Ng4LoadingSpinnerService) { }

  view(event) {
    this.docId = event.data.docId;
    this.editIs = true;
    this.spinner.show();
    this.callPropertyTemplates();
    this.refreshDefId();
    this.cs.getJSONFromDocument(event.data.docId).subscribe(data => { this.viewForms(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
  callPropertyTemplates() {
    this.cs.getPropertyTemplates().subscribe(data => { this.proptemdrop(data); });
  }
  proptemdrop(data) {
    this.propTemplatesDrop = [];
    this.propTemplatesDrop = JSON.parse(data._body);
    this.templateDropDown = [];
    const value = JSON.parse(data._body);
    value.forEach(element => {
      const template = {
        label: element.name,
        value: element.id
      };
      this.templateDropDown.push(template);
    });
  }
  ngOnInit() {
    this.formTypeDropDown = [
      { label: 'Form', value: 'FORM' },
      { label: 'Table', value: 'TABLE' }
    ];
    this.addRowsDropDown = [
      { label: 'True', value: 'TRUE' },
      { label: 'False', value: 'FALSE' }
    ];
    this.columnWidthDropDown = [
      { label: '100%', value: '100%' },
      { label: '80%', value: '80%' },
      { label: '60%', value: '60%' },
      { label: '50%', value: '50%' }
    ];
    this.typeDropDown = [
      { label: 'Text', value: 'TEXT' },
      { label: 'Number', value: 'NUMBER' },
      { label: 'Date', value: 'DATE' }
    ];
    this.dbTypeDropDown = [
      { label: 'Sql Server', value: 'SQLSERVER' },
      { label: 'Oracle', value: 'ORACLE' },
      { label: 'Mysql', value: 'MYSQL' }
    ];
    this.actionsDropDwon = [
      { label: 'None', value: 'NONE' },
      { label: 'Auto Fill', value: 'autoFill' },
      { label: 'Set Visibility', value: 'setVisibility' }
    ];
    this.dbTypeDropDown = [
      { label: 'Sql Server', value: 'SQLSERVER' },
      { label: 'Oracle', value: 'ORACLE' },
      { label: 'Mysql', value: 'MYSQL' }
    ];
    this.uiTypeDropDown = [
      { value: 'TEXT', label: 'Text Box' },
      { value: 'NUMBER', label: 'Number Box' },
      { value: 'DATE', label: 'Date' },
      { value: 'DATERANGE', label: 'Date Range' },
      { value: 'TIME', label: 'Time' },
      { value: 'LOOKUP', label: 'Lookup' },
      { value: 'LOOKUPCONDITION', label: 'Conditional Lookup' },
      { value: 'DBLOOKUP', label: 'DbLookup' },
      { value: 'TEXTAREA', label: 'Textarea' },
      { value: 'CHECKBOX', label: 'Checkbox' },
      { value: 'STEXTTA', label: 'Single Text Type Ahead' },
      { value: 'MTEXTTA', label: 'Multi Text Type Ahead' },
      { value: 'ROWSUM', label: 'Sum' },
      { value: 'MULTIPLICATION', label: 'Multiplication' },
      { value: 'ROWSUB', label: 'Subtraction' },
      { value: 'ROWDIV', label: 'Division' },
      { value: 'ROWAVG', label: 'Average' },
      { value: 'ROWDATEDIFF', label: 'Date Difference' }
    ];
    this.colCalcDropDown = [
      { label: 'None', value: 'NONE' },
      { label: 'Sum', value: 'SUM' },
      { label: 'Average', value: 'AVERAGE' }
    ];
    this.refreshDefId();
    this.callPropertyTemplates();
    this.source = [];
    this.spinner.show();
    const date = new Date();
    this.time = {
      'hour': date.getHours(),
      'minute': date.getMinutes(),
      'second': 0
    };
    this.ss.getForms().subscribe(data => { this.gotForms(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
    this.ss.getTemplates().subscribe(data => this.getAllTemplates(data));
    this.showAddFormName = true;
    this.showEditFormName = false;
    this.formsName = this.fb.group({
      formId: [null],
      formName: [null, Validators.compose([Validators.maxLength(2000), Validators.required, Validators.pattern('[a-z_]*')])],
      formLabel: [null, Validators.required],
      formTemplate: ['']
    });
    this.formsName.reset();
    this.sectionForms = this.fb.group({
      sectionHeading: [null, Validators.compose([Validators.required, Validators.maxLength(2000)])],
      sectionName: [null, Validators.compose([Validators.maxLength(2000), Validators.required, Validators.pattern('[a-z_]*')])],
      type: [null, Validators.required],
      showHeader: ['TRUE'],
      addRows: ['TRUE'],
      rows: [null],
      visible: ['TRUE'],
      rOnly: ['FALSE']
    });
    this.addColumn = this.fb.group({
      columnName: [null, (Validators.maxLength(2000), Validators.required)],
      columnWidth: [null, (Validators.required)]
    });
    this.addFormProps = this.fb.group({
      propname: [null, (Validators.maxLength(2000), Validators.required)],
      proplabel: [null, (Validators.maxLength(2000), Validators.required)],
      proptype: ['TEXT', Validators.required],
      proplength: [null],
      propreadonly: ['FALSE'],
      proprequired: ['FALSE'],
      propvisible: ['TRUE'],
      proplookup: [null],
      proplookupTemp: [null],
      propdburl: [null],
      propdbtype: ['SQLSERVER'],
      propuser: [null],
      proppassword: [null],
      propsql: [null],
      propnamecol: [null],
      propvaluecol: [null],
      propdbvalue: [null],
      propwidth: [null],
      condition: [[]],
      calc: [[]],
      colcalc: [{
        type: 'NONE',
        label: 'None',
        copyto: ''
      }],
      colcalcType: ['NONE'],
      copytoProp: '',
      fillprops: [[]],
      isDefaultDB: [true],
      propTemplate: [null, Validators.required],
      actiondrop: ['NONE'],
      uitype: ['TEXT', Validators.required],
      actions: [[]],
      propTemplateSelected: [{}],
      setVisible: [{}],
    });

    this.addDatatableForm = this.fb.group({
      key: [null, Validators.required],
      value: [null, Validators.required]
    });
  }
  AddFormSubmit(Event) {

    for (const inputField of [].slice.call(event.target)) {
    }
  }
  lists() {
  }
  refreshDefId() {
    this.defid = new FileUploader({});
  }
  addSections() {
    this.showSections = !this.showSections;
  }
  clickedOnView() {
    this.isListTab = true;
    this.isAddTab = false;
    this.refreshDefId();
  }
  getAllTemplates(data) {
    this.templateList = [];
    const value = JSON.parse(data._body);
    value.forEach(element => {
      const template = {
        label: element.name,
        value: element.docId
      };
      this.templateList.push(template);
    });
  }
  changingTab(event) {
    if (event.tabTitle === 'Forms') {
      this.showAddTab = false;
      this.isAddTab = false;
      this.isListTab = true;
    } else {
      this.isListTab = false;
    }
  }
  addNewForm() {
    this.editIs = false;
    this.assignFalse();
    this.formsArray = null;
    this.isAddTab = true;
    this.isListTab = false;
    this.showAddTab = true;
    this.formsArray = {
      id: 0,
      name: '',
      formTemplate: '',
      label: '',
      sections: [],
      actions: ['SUBMIT'],
      datatable: [
      ]
    };
    this.arrayToShow.push(this.formsArray);
    this.formsName.reset();
  }
  addSimpleForm() {

  }
  saveFormName(formname, isEdit) {
    this.showFormButton = false;
    for (let g = 0; g < this.arrayToShow.length; g++) {
      this.arrayToShow[g].id = this.formsName.controls.formId.value;
      this.arrayToShow[g].name = this.formsName.controls.formName.value;
      this.arrayToShow[g].formTemplate = this.formsName.controls.formTemplate.value;
      this.arrayToShow[g].label = this.formsName.controls.formLabel.value;
      break;
    }
    if (isEdit === 'edit') {
      this.showAddFormName = false;
    } else {
      this.propNames = [];
      this.formsName.reset();
      this.showAddFormName = false;
    }

  }
  addSection() {
    this.showEditFormName = false;
    this.showAddFormName = false;
    this.showAddSectionforms = true;
    this.showAddPropArea = false;
    this.editProperty = false;
    this.showCSSns = false;
    this.editColumns = false;
    this.adddatatableitems = false;
    this.sectionForms.patchValue({ sectionName: '', sectionHeading: '', type: null, showHeader: 'TRUE', addRows: 'TRUE', visible: 'TRUE', rOnly: 'FALSE' });
  }
  addDatatable(data) {
    this.showEditFormName = false;
    this.showAddFormName = false;
    this.adddatatableitems = true;
    this.editDatatableItems = false;
    this.addDatatableForm.reset();
    this.showAddSectionforms = false;
    this.showAddPropArea = false;
    this.editProperty = false;
    this.showCSSns = false;
    this.editColumns = false;
  }
  saveSections(sec) {
    const time = new Date();
    let flag = false;
    let value = {};
    if (this.isForm) {
      value = {};
      value = {
        heading: sec.value.sectionHeading,
        name: sec.value.sectionName,
        type: sec.value.type,
        showHeader: sec.value.showHeader,
        rOnly: sec.value.rOnly,
        visible: sec.value.visible,
        columns: []
      };
    } else {
      value = {};
      value = {
        heading: sec.value.sectionHeading,
        name: sec.value.sectionName,
        type: sec.value.type,
        showHeader: sec.value.showHeader,
        columns: null,
        rowheader: [],
        rOnly: sec.value.rOnly,
        visible: sec.value.visible,
        addRows: sec.value.addRows,
        rows: [{
          row: 1,
          items: []
        }
        ]
      };
    }
    for (let g = 0; g < this.arrayToShow.length; g++) {
      if (this.arrayToShow[g].sections.length > 0) {
        for (let i = 0; i < this.arrayToShow[g].sections.length; i++) {
          if (this.arrayToShow[g].sections[i].heading === sec.value.sectionHeading) {
            flag = false;
            break;
          } else {
            flag = true;

          }
        }
        if (flag === true) {
          this.arrayToShow[g].sections.push(value);
          this.sectionForms.reset();
          this.showAddSectionforms = false;
        } else {
          alert('Section name should be unique');
        }
      } else {
        this.arrayToShow[g].sections.push(value);
        this.sectionForms.reset();
        this.showAddSectionforms = false;
      }

    }
  }
  showEachSection() {
    this.showCSSns = true;
    this.showAddSectionforms = false;
    this.editSectionItems = false;
    this.addColumn.reset();
    this.addColumn.patchValue({ columnWidth: '100%' });
    for (let g = 0; g < this.arrayToShow.length; g++) {
      for (let i = 0; i < this.arrayToShow[g].sections.length; i++) {
        if (i === this.secIndex) {
          if (this.arrayToShow[g].sections[i].columns.length < 3) {
            this.addColumn.patchValue({ columnName: 'column' + (this.arrayToShow[g].sections[i].columns.length + 1) });
          }
        }
      }
    }
  }
  saveDatatable(datatable) {
    const data = {
      'key': datatable.value.key,
      'value': datatable.value.value
    };
    this.arrayToShow[0].datatable.push(data);
    this.addDatatableForm.reset();
    this.adddatatableitems = false;
    this.datatableIndex = -1;
  }

  editDatatable(datatable, editValue) {

    for (let i = 0; i < this.arrayToShow[0].datatable.length; i++) {
      if (i === this.datatableIndex) {
        if (editValue === 'EDIT') {
          this.arrayToShow[0].datatable[i].key = datatable.value.key;
          this.arrayToShow[0].datatable[i].value = datatable.value.value;
          break;
        } else {
          this.arrayToShow[0].datatable.splice(i, 1);
          this.datatableIndex = -1;
          this.adddatatableitems = false;
          this.editDatatableItems = false;
          break;
        }
      }


    }
  }
  clickedOndatatable(datatable, index) {
    this.showAddPropsBut = false;
    this.showAddSectionsBut = false;
    this.addDatatableBtn = false;
    this.showAddColumnsBut = false;
    this.showAddSectionforms = false;
    this.editSectionItems = false;
    this.showAddPropArea = false;
    this.editProperty = false;
    this.showCSSns = false;
    this.editColumns = false;
    this.colIndex = -1;
    this.sectionFont = -1;
    this.colFont = -1;
    this.propFont = -1;
    this.secIndex = -1;
    this.propsIndex = -1;
    this.formFont = false;
    this.showAddFormName = false;
    this.showEditFormName = false;
    this.datatableIndex = index;
    this.editDatatableItems = true;
    this.adddatatableitems = false;
    for (let i = 0; i < this.arrayToShow[0].datatable.length; i++) {
      if (i === this.datatableIndex) {
        this.addDatatableForm.patchValue({ key: datatable.key, value: datatable.value });
      }
    }

  }
  addFormTitle(event) {
    let myString = '';
    myString = this.formsName.controls['formLabel'].value.replace(/[^A-Za-z]+/g, '');
    const resString = myString.toLowerCase();
    this.formsName.patchValue({ formName: resString });
  }
  addSectionName(event) {
    let myString = '';
    myString = this.sectionForms.controls['sectionHeading'].value.replace(/[^A-Za-z]+/g, '');
    const resString = myString.toLowerCase();
    this.sectionForms.patchValue({ sectionName: resString });
  }
  addColumns(columnvalue) {
    const time = new Date();
    const value = {
      heading: columnvalue.value.columnName,
      width: columnvalue.value.columnWidth,
      properties: []
    };
    for (let g = 0; g < this.arrayToShow.length; g++) {
      for (let i = 0; i < this.arrayToShow[g].sections.length; i++) {
        if (i === this.secIndex) {
          if (this.arrayToShow[g].sections[i].columns.length < 3) {
            this.arrayToShow[g].sections[i].columns.push(value);
            if (this.arrayToShow[g].sections[i].columns.length === 3) {
              this.disableAddColumnbut = true;
            }
            break;
          }
        }
      }
    }
    this.addColumn.reset();
    this.showCSSns = false;
  }
  showAddProp() {
    this.showCSSns = false;
    this.editColumns = false;
    this.showAddPropArea = true;
    this.editProperty = false;
    this.addFormProps.reset();
    this.selectedType = 'TEXT';
    this.selectedPropType = 'TEXT';
    this.showAddSectionforms = false;
    this.editSectionItems = false;
    this.addFormProps.patchValue({
      proptype: 'TEXT', uitype: 'TEXT', propTemplate: this.propTemplatesDrop[0].id,
      propTemplateSelected: this.propTemplatesDrop[0], propname: this.propTemplatesDrop[0].symName,
      proplabel: this.propTemplatesDrop[0].name, propreadonly: 'FALSE', proprequired: 'FALSE', propvisible: 'TRUE', condition: [],
      actions: [], calc: [], colcalc: { type: 'NONE', label: 'None', copyto: '' }, copytoProp: '', actiondrop: 'NONE'
    });
    if (this.propTemplatesDrop[0].dtype === 'INTEGER') {
      this.selectedType = 'NUMBER';
      this.selectedPropType = 'NUMBER';
      this.selectedTypeIsNumber();
      this.addFormProps.patchValue({ proptype: 'NUMBER', uitype: 'NUMBER' });
    } else if (this.propTemplatesDrop[0].dtype === 'STRING') {
      this.selectedType = 'TEXT';
      this.selectedPropType = 'TEXT';
      this.selectedTypeIsText();
      this.addFormProps.patchValue({ proptype: 'TEXT', uitype: 'TEXT' });
    } else if (this.propTemplatesDrop[0].dtype === 'DATE' || this.propTemplatesDrop[0].dtype === 'DATETIME') {
      this.selectedType = 'DATE';
      this.selectedPropType = 'DATE';
      this.selectedTypeIsDate();
      this.addFormProps.patchValue({ proptype: 'DATE', uitype: 'DATE' });
    }
  }
  addProperties(props) {
    this.propNames.push(props.value.propname);
    const time = new Date();
    const lookup = [];
    let number = 1;
    let splitproplookup = [];

    if (props.value.proplookup !== null) {
      if (props.value.proplookup.includes(',')) {
        splitproplookup = props.value.proplookup.split(',');
      } else {
        splitproplookup.push(props.value.proplookup);
      }

      for (let u = 0; u < splitproplookup.length; u++) {
        const propss = {
          name: splitproplookup[u],
          value: splitproplookup[u]
        };
        lookup.push(propss);
        number++;
      }
    }
    const propts = new FormProps();
    this.propsIndex = undefined;
    const isAddTrue = this.isExist(props.value);
    if (isAddTrue) {
      this.tr.warning('', 'This property template is already present in this form please select another template');
      return;
    } else {
      if (this.isForm) {
        for (let g = 0; g < this.arrayToShow.length; g++) {
          for (let j = 0; j < this.arrayToShow[g].sections.length; j++) {
            if (j === this.secIndex) {
              for (let k = 0; k < this.arrayToShow[g].sections[j].columns.length; k++) {
                if (k === this.colIndex) {
                  propts.name = props.value.propname;
                  propts.label = props.value.proplabel;
                  propts.value = [];
                  propts.type = props.value.proptype;
                  propts.length = props.value.proplength;
                  propts.rOnly = props.value.propreadonly;
                  propts.req = props.value.proprequired;
                  propts.visible = props.value.propvisible;
                  propts.uitype.uitype = props.value.uitype;
                  propts.prop = props.value.propTemplateSelected;
                  propts.action = props.value.actiondrop;
                  propts.actions = props.value.actions;
                  propts.setVisible = props.value.setVisible;
                  if (props.value.uitype === 'LOOKUP' || props.value.uitype === 'LOOKUPCONDITION') {
                    propts.uitype.lookups = lookup;
                    delete propts.uitype.dblookup;
                    delete propts.dbvalue;
                    delete propts.length;
                    if (props.value.uitype === 'LOOKUPCONDITION') {
                      propts.uitype.condition = props.value.condition;
                    }
                  } if (props.value.uitype === 'DBLOOKUP' || props.value.uitype === 'MTEXTTA' || props.value.uitype === 'STEXTTA') {
                    propts.uitype.dblookup.dburl = props.value.propdburl;
                    propts.uitype.dblookup.dbtype = props.value.propdbtype;
                    propts.uitype.dblookup.sql = props.value.propsql;
                    propts.uitype.dblookup.user = props.value.propuser;
                    propts.uitype.dblookup.password = props.value.proppassword;
                    propts.uitype.dblookup.filter = null;
                    propts.uitype.dblookup.namecol = props.value.propnamecol;
                    propts.uitype.dblookup.valuecol = props.value.propvaluecol;
                    propts.isDefaultDB = props.value.isDefaultDB;
                    propts.dbvalue = [];
                    delete propts.uitype.lookups;
                    delete propts.length;
                  } if (props.value.uitype === 'TEXTAF') {
                    propts.uitype.autofill.dburl = props.value.propdburl;
                    propts.uitype.autofill.dbtype = props.value.propdbtype;
                    propts.uitype.autofill.sql = props.value.propsql;
                    propts.uitype.autofill.user = props.value.propuser;
                    propts.uitype.autofill.password = props.value.proppassword;
                    propts.uitype.autofill.fillprops = props.value.fillprops;
                    propts.isDefaultDB = props.value.isDefaultDB;
                  } if (props.value.uitype !== 'DBLOOKUP' && props.value.uitype !== 'MTEXTTA' && props.value.uitype !== 'STEXTTA' && props.value.uitype !== 'LOOKUP' && props.value.uitype !== 'LOOKUPCONDITION') {
                    delete propts.uitype.dblookup;
                    delete propts.dbvalue;
                    delete propts.uitype.lookups;
                    delete propts.uitype.condition;
                    //  delete propts.isDefaultDB;
                  } if (props.value.uitype !== 'TEXTAF') {
                    //  delete propts.uitype.autofill;
                    //  delete propts.isDefaultDB;

                  } if (props.value.uitype === 'MULTIPLICATION' || props.value.uitype === 'ROWSUB' || props.value.uitype === 'ROWSUM' || props.value.uitype === 'ROWDIV' || props.value.uitype === 'ROWAVG' || props.value.uitype === 'ROWDATEDIFF') {
                    propts.uitype.calc = props.value.calc;
                    propts.uitype.colcalc = props.value.colcalc;
                    propts.rOnly = 'TRUE';
                    delete propts.colcalcType;
                  }
                  if (props.value.uitype === 'ROWEXPIRYDATE') {
                    propts.uitype.calc = props.value.calc;
                    delete props.colcalc;
                    delete propts.colcalcType;
                    propts.rOnly = props.value.propreadonly;
                  }
                  if (props.value.uitype !== 'MULTIPLICATION' && props.value.uitype !== 'ROWSUB' && props.value.uitype !== 'ROWSUM' && props.value.uitype !== 'ROWDIV' && props.value.uitype !== 'ROWAVG' && props.value.uitype !== 'ROWDATEDIFF' && props.value.uitype !== 'ROWEXPIRYDATE') {
                    delete propts.uitype.calc;
                    delete propts.uitype.colcalc;
                    delete propts.colcalcType;
                  }
                  this.arrayToShow[g].sections[j].columns[k].properties.push(propts);
                  this.showSaveFormBut = true;
                  if (props.value.uitype === 'DBLOOKUP') {
                    this.propsIndex = this.arrayToShow[g].sections[j].columns[k].properties.findIndex(x => x.name === props.value.propname);
                    this.integrationservice.getDBLookup(this.arrayToShow[g].sections[j].columns[k].properties[this.propsIndex].dblookup).subscribe(res => this.assignDblookupvalue(res, g, j, k, this.propsIndex));
                  }
                  if (props.value.uitpye === 'TEXTAF') {
                    this.propsIndex = this.arrayToShow[g].sections[j].columns[k].properties.findIndex(x => x.name === props.value.propname);
                    this.integrationservice.getDBLookup(this.arrayToShow[g].sections[j].columns[k].properties[this.propsIndex].autofill).subscribe(res => this.assignDblookupvalue(res, g, j, k, this.propsIndex));
                  }
                  break;
                }
              }
            }
          }
        }
      } else {
        for (let g = 0; g < this.arrayToShow.length; g++) {
          for (let j = 0; j < this.arrayToShow[g].sections.length; j++) {
            if (j === this.secIndex) {
              propts.name = props.value.propname;
              propts.label = props.value.proplabel;
              propts.value = [];
              propts.type = props.value.proptype;
              propts.length = props.value.proplength;
              propts.uitype.uitype = props.value.uitype;
              propts.prop = props.value.propTemplateSelected;
              propts.action = props.value.actiondrop;
              propts.actions = props.value.actions;
              propts.setVisible = props.value.setVisible;
              if (props.value.uitype === 'AUTONUMBER' || props.value.uitype === 'MULTIPLICATION' || props.value.uitype === 'ROWSUB' || props.value.uitype === 'ROWSUM' || props.value.uitype === 'ROWDIV' || props.value.uitype === 'ROWAVG' || props.value.uitype === 'ROWDATEDIFF') {
                propts.rOnly = 'TRUE';
              } else {
                propts.rOnly = props.value.propreadonly;
              }
              propts.req = props.value.proprequired;
              propts.width = props.value.propwidth;
              propts.visible = props.value.propvisible;
              if (props.value.uitype === 'LOOKUP' || props.value.uitype === 'LOOKUPCONDITION') {
                propts.uitype.lookups = lookup;
                delete propts.uitype.dblookup;
                delete propts.dbvalue;
                delete propts.length;
                if (props.value.uitype === 'LOOKUPCONDITION') {
                  propts.uitype.condition = props.value.condition;
                }
              } else if (props.value.uitype === 'DBLOOKUP' || props.value.uitype === 'MTEXTTA' || props.value.uitype === 'STEXTTA') {
                propts.uitype.dblookup.dburl = props.value.propdburl;
                propts.uitype.dblookup.dbtype = props.value.propdbtype;
                propts.uitype.dblookup.sql = props.value.propsql;
                propts.uitype.dblookup.user = props.value.propuser;
                propts.uitype.dblookup.password = props.value.proppassword;
                propts.uitype.dblookup.filter = null;
                propts.uitype.dblookup.namecol = props.value.propnamecol;
                propts.uitype.dblookup.valuecol = props.value.propvaluecol;
                propts.isDefaultDB = props.value.isDefaultDB;
                propts.dbvalue = [];
                delete propts.uitype.lookups;
                delete propts.length;
              } else if (props.value.uitpye === 'TEXTAF') {
                propts.uitype.autofill.dburl = props.value.propdburl;
                propts.uitype.autofill.dbtype = props.value.propdbtype;
                propts.uitype.autofill.sql = props.value.propsql;
                propts.uitype.autofill.user = props.value.propuser;
                propts.uitype.autofill.password = props.value.proppassword;
                propts.isDefaultDB = props.value.isDefaultDB;
                propts.dbvalue = [];
                propts.uitype.autofill.fillprops = props.value.fillprops;

              } else if (props.value.uitype === 'MULTIPLICATION' || props.value.uitype === 'ROWSUB' || props.value.uitype === 'ROWSUM' || props.value.uitype === 'ROWDIV' || props.value.uitype === 'ROWAVG' || props.value.uitype === 'ROWDATEDIFF') {
                propts.uitype.calc = props.value.calc;
                propts.uitype.colcalc = props.value.colcalc;
                delete propts.colcalcType;
              } else if (props.value.uitype === 'ROWEXPIRYDATE') {
                propts.uitype.calc = props.value.calc;
                delete props.colcalc;
                delete propts.colcalcType;
                propts.rOnly = props.value.propreadonly;
              } else if (props.value.uitype === 'NUMBER' || props.value.uitype === 'AUTONUMBER') {
                propts.uitype.colcalc = props.value.colcalc;
                delete propts.colcalcType;
                delete propts.uitype.calc;
              } else if (props.value.uitype !== 'DBLOOKUP' && props.value.uitype !== 'MTEXTTA' && props.value.uitype !== 'STEXTTA' && props.value.uitype !== 'LOOKUP' && props.value.uitype !== 'LOOKUPCONDITION') {
                delete propts.uitype.dblookup;
                delete propts.dbvalue;
                delete propts.uitype.lookups;
                delete propts.uitype.condition;
              } else if (props.value.uitype !== 'TEXTAF') {


              } else if (props.value.uitype !== 'MULTIPLICATION' && props.value.uitype !== 'NUMBER' &&
                props.value.uitype !== 'AUTONUMBER' && props.value.uitype !== 'ROWSUB' && props.value.uitype !== 'ROWSUM' &&
                props.value.uitype !== 'ROWDIV' && props.value.uitype !== 'ROWAVG' && props.value.uitype !== 'ROWDATEDIFF' &&
                props.value.uitype !== 'ROWEXPIRYDATE') {
                delete propts.uitype.calc;
                delete propts.uitype.colcalc;
                delete propts.colcalcType;
              }

              this.arrayToShow[g].sections[j].rowheader.push(propts);
              this.arrayToShow[g].sections[j].rows[0].items.push(propts);
              this.showSaveFormBut = true;
              if (props.value.uitype === 'DBLOOKUP') {
                this.propsIndex = this.arrayToShow[g].sections[j].rowheader.findIndex(x => x.name === props.value.propname);
                this.integrationservice.getDBLookup(this.arrayToShow[g].sections[j].rowheader[this.propsIndex].dblookup).subscribe(res => this.assignDblookupvalueForTable(res, g, j, this.propsIndex));
              }
              if (props.value.uitype === 'TEXTAF') {
                this.propsIndex = this.arrayToShow[g].sections[j].rowheader.findIndex(x => x.name === props.value.propname);
                this.integrationservice.getDBLookup(this.arrayToShow[g].sections[j].rowheader[this.propsIndex].autofill).subscribe(res => this.assignDblookupvalueForTable(res, g, j, this.propsIndex));
              }
              break;
            }
          }
        }
      }

      this.addFormProps.reset();
      this.showAddPropArea = false;
    }
  }
  isExist(prop) {
    for (let g = 0; g < this.arrayToShow.length; g++) {
      if (this.arrayToShow[g].sections) {
        for (let j = 0; j < this.arrayToShow[g].sections.length; j++) {
          if (this.arrayToShow[g].sections[j].columns) {
            for (let k = 0; k < this.arrayToShow[g].sections[j].columns.length; k++) {
              if (this.arrayToShow[g].sections[j].columns[k].properties) {
                for (let p = 0; p < this.arrayToShow[g].sections[j].columns[k].properties.length; p++) {
                  if (this.arrayToShow[g].sections[j].columns[k].properties[p].prop.id === prop.propTemplateSelected.id && this.colIndex !== k && this.secIndex !== j) {
                    return true;
                  }
                }
              }
            }
          }
          if (this.arrayToShow[g].sections[j].rowheader) {
            for (let r = 0; r < this.arrayToShow[g].sections[j].rowheader.length; r++) {
              if (this.arrayToShow[g].sections[j].rowheader[r].prop.id === prop.propTemplateSelected.id && this.secIndex !== j) {
                return true;
              }
            }
          }
        }
      }
    }
  }
  showAddSecBut(title) {
    this.showAddSectionsBut = true;
    this.addDatatableBtn = true;
    this.showAddColumnsBut = false;
    this.showAddPropsBut = false;
    this.showAddSectionforms = false;
    this.showAddPropArea = false;
    this.editProperty = false;
    this.showCSSns = false;
    this.editColumns = false;
    this.editSectionItems = false;
    this.formFont = true;
    this.datatableIndex = -1;
    this.adddatatableitems = false;
    this.editDatatableItems = false;
    this.sectionFont = -1;
    this.colFont = -1;
    this.propFont = -1;
    this.showEditFormName = false;
    this.showAddFormName = false;
    this.adddatatableitems = false;
    this.editDatatableItems = false;
    this.formsName.patchValue({ formId: title.id, formName: title.name, formTemplate: title.formTemplate, formLabel: title.label });
    this.disableFormType = false;
    this.sectionForms.controls.type.enable(); //  changed
    this.isForm = true;

  }
  editFormTitle(title) {
    this.showEditFormName = true;
    this.showAddFormName = false;
    this.adddatatableitems = false;
    this.editDatatableItems = false;
    this.formsName.patchValue({ formId: title.id, formName: title.name, formTemplate: title.formTemplate, formLabel: title.label });
    this.disableFormType = false;
    this.sectionForms.controls.type.enable(); //  changed
    this.isForm = true;
  }
  showAddColBut(value, secIndex) {
    this.showAddColumnsBut = true;
    this.showAddSectionsBut = false;
    this.addDatatableBtn = false;
    this.showAddSectionforms = false;
    this.showAddPropArea = false;
    this.editProperty = false;
    this.showCSSns = false;
    this.editColumns = false;
    this.selectedSection = value;
    this.secIndex = secIndex;
    this.datatableIndex = -1;
    this.adddatatableitems = false;
    this.editDatatableItems = false;
    this.showAddPropsBut = false;
    this.showEditFormName = false;
    this.propsIndex = -1;
    // edit
    this.editSectionItems = false;
    this.sectionForms.patchValue({ sectionName: value.name, sectionHeading: value.heading, type: value.type, showHeader: value.showHeader, visible: value.visible, rOnly: value.rOnly, addRows: value.addRows });
    this.disableFormType = true;

    for (let g = 0; g < this.arrayToShow[0].sections.length; g++) {
      if (g === secIndex) {
        if (this.arrayToShow[0].sections[g].type === 'FORM') {
          this.isForm = true;
          break;
        } else {
          this.isForm = false;
        }
      }
    }
    if (this.isForm) {
      for (let a = 0; a < this.arrayToShow[0].sections.length; a++) {
        if (a === this.secIndex) {
          if (this.arrayToShow[0].sections[a].columns.length < 3) {
            this.disableAddColumnbut = false;
            break;
          } else {
            this.disableAddColumnbut = true;
            break;
          }
        }
      }
    } else {
      this.showAddColumnsBut = false;
      this.showAddPropsBut = true;
    }
  }
  editSectionName(value, secIndex) {
    this.showEditFormName = false;
    this.showAddPropArea = false;
    this.editProperty = false;
    this.editSectionItems = true;
    this.sectionForms.patchValue({ sectionName: value.name, sectionHeading: value.heading, type: value.type, showHeader: value.showHeader, visible: value.visible, rOnly: value.rOnly, addRows: value.addRows });
    this.disableFormType = true;
    this.showCSSns = false;
  }
  // }
  showAddPropBut(col, value, colIndex, secIndex) {
    this.showAddPropsBut = true;
    this.showAddSectionsBut = false;
    this.addDatatableBtn = false;
    this.showAddColumnsBut = false;
    this.selectedColumn = col;
    this.colIndex = colIndex;
    this.secIndex = secIndex;
    this.propsIndex = -1;
    this.datatableIndex = -1;
    this.adddatatableitems = false;
    this.editDatatableItems = false;
    this.showAddSectionforms = false;
    this.editSectionItems = false;
    this.showAddPropArea = false;
    this.editProperty = false;
    this.showCSSns = false;
    this.editColumns = false;
    this.selectedSection = value;
    this.showEditFormName = false;
    this.addColumn.patchValue({ columnName: col.heading });
  }
  editColumnName(col, value, colIndex, secIndex) {
    this.showCSSns = false;
    this.editColumns = true;
    this.selectedSection = value;
    this.showEditFormName = false;
    this.showAddPropArea = false;
    this.editProperty = false;
    if (col.width) {
      this.addColumn.patchValue({ columnName: col.heading, columnWidth: col.width });
    } else {
      this.addColumn.patchValue({ columnName: col.heading, columnWidth: '100%' });
    }
  }
  clickedOnProperty(prop, col, value, propIdex, colIndex, secIndex) {
    this.showAddPropsBut = false;
    this.showAddSectionsBut = false;
    this.addDatatableBtn = false;
    this.showAddColumnsBut = false;
    this.showAddSectionforms = false;
    this.editSectionItems = false;
    this.showAddPropArea = false;
    this.editProperty = true;
    this.showCSSns = false;
    this.editColumns = false;
    this.selectedSection = value;
    this.selectedColumn = col;
    this.propsIndex = propIdex;
    this.colIndex = colIndex;
    this.secIndex = secIndex;
    this.datatableIndex = -1;
    this.adddatatableitems = false;
    this.editDatatableItems = false;
    this.selectedType = prop.type;
    this.selectedPropType = prop.uitype.uitype;
    this.proplookupArray = [];
    if (this.selectedType === 'TEXT') {
      this.selectedTypeIsText();
    } else if (this.selectedType === 'NUMBER') {
      this.selectedTypeIsNumber();
    } else if (this.selectedType === 'DATE') {
      this.selectedTypeIsDate();
    } else {

    }
    let lookupvalue;

    if (this.selectedPropType === 'LOOKUP' || this.selectedPropType === 'LOOKUPCONDITION') {
      lookupvalue = '';
      for (let i = 0; i < prop.uitype.lookups.length; i++) {
        if (i === 0) {
          lookupvalue = prop.uitype.lookups[i].name;
        } else {
          lookupvalue = lookupvalue + ',' + prop.uitype.lookups[i].name;
        }
      }
      prop.uitype.lookups.forEach(element => {
        this.proplookupArray.push(element.name);
      });
    }
    this.addFormProps.patchValue({
      propTemplate: prop.prop.id, uitype: prop.uitype.uitype, actions: prop.actions,
      setVisible: prop.setVisible, propTemplateSelected: prop.prop, propname: prop.name, proplabel: prop.label,
      proptype: prop.type, proplength: prop.length, propwidth: prop.width, propreadonly: prop.rOnly, propvisible: prop.visible,
      proprequired: prop.req, proplookup: lookupvalue
    });
    if (!prop.actions || prop.actions.length === 0) {
      this.addFormProps.patchValue({ actions: [], actiondrop: 'NONE' });
    } else if (prop.actions.length > 0) {
      if (prop.actions[0].action === 'autoFill' || prop.actions[0].action === 'TEXTAF') { // chnaged
        this.addFormProps.patchValue({ actiondrop: 'autoFill' });
        this.textAutoFilled();
      }
    } else {
      if (!prop.setVisible) {
        this.addFormProps.patchValue({ setVisible: {}, actiondrop: 'NONE' });
      } else {
        this.addFormProps.patchValue({ actiondrop: 'setVisibility' });
        this.setVisibleConditions();
      }
    }

    if (this.selectedPropType === 'LOOKUPCONDITION') {
      this.addFormProps.patchValue({ condition: prop.uitype.condition });
    } else if (prop.uitype.uitype === 'DBLOOKUP' || prop.uitype.uitype === 'STEXTTA' || prop.uitype.uitype === 'MTEXTTA') {
      this.addFormProps.patchValue({
        propdburl: prop.uitype.dblookup.dburl, propdbtype: prop.uitype.dblookup.dbtype,
        propuser: prop.uitype.dblookup.user, proppassword: prop.uitype.dblookup.password, propsql: prop.uitype.dblookup.sql,
        propnamecol: prop.uitype.dblookup.namecol, propvaluecol: prop.uitype.dblookup.valuecol, isDefaultDB: prop.isDefaultDB
      });
      if (prop.uitype.dblookup.dburl === 'DEFAULT') {
        if (prop.uitype.dblookup.dbtype) {
          this.addFormProps.patchValue({ propdbtype: prop.uitype.dblookup.dbtype });
        } else {
          this.addFormProps.patchValue({ propdbtype: 'SQLSERVER' });
        } if (prop.uitype.dblookup.user) {
          this.addFormProps.patchValue({ propuser: prop.uitype.dblookup.user });
        } else {
          this.addFormProps.patchValue({ propuser: 'DEFAULT' });
        } if (prop.uitype.dblookup.password) {
          this.addFormProps.patchValue({ proppassword: prop.uitype.dblookup.password });
        } else {
          this.addFormProps.patchValue({ proppassword: 'DEFAULT' });
        }
      }
    } else if (prop.uitype.uitype === 'TEXTAF') {
      this.addFormProps.patchValue({
        propdburl: prop.uitype.autofill.dburl, propdbtype: prop.uitype.autofill.dbtype,
        propuser: prop.uitype.autofill.user, proppassword: prop.uitype.autofill.password, propsql: prop.uitype.autofill.sql,
        fillprops: prop.uitype.autofill.fillprops, isDefaultDB: prop.isDefaultDB
      });
      if (prop.uitype.autofill.dburl === 'DEFAULT') {
        if (prop.uitype.autofill.dbtype) {
          this.addFormProps.patchValue({ propdbtype: prop.uitype.autofill.dbtype });
        } else {
          this.addFormProps.patchValue({ propdbtype: 'SQLSERVER' });
        } if (prop.uitype.autofill.user) {
          this.addFormProps.patchValue({ propuser: prop.uitype.autofill.user });
        } else {
          this.addFormProps.patchValue({ propuser: 'DEFAULT' });
        } if (prop.uitype.autofill.password) {
          this.addFormProps.patchValue({ proppassword: prop.uitype.autofill.password });
        } else {
          this.addFormProps.patchValue({ proppassword: 'DEFAULT' });
        }
      }
    } else if (this.selectedPropType === 'MULTIPLICATION' || this.selectedPropType === 'AUTONUMBER' || this.selectedPropType === 'ROWSUB' || this.selectedPropType === 'ROWSUM' || this.selectedPropType === 'ROWDIV' || this.selectedPropType === 'ROWAVG') {
      this.addFormProps.controls.propreadonly.disable();
      if (this.isForm) {
        this.addFormProps.patchValue({ calc: prop.uitype.calc, propreadonly: 'TRUE' });
      } else {
        this.addFormProps.patchValue({ calc: prop.uitype.calc, colcalc: prop.uitype.colcalc, copytoProp: prop.uitype.colcalc.copyto, colcalcType: prop.uitype.colcalc.type, propreadonly: 'TRUE' });
      }
      if (this.selectedPropType !== 'AUTONUMBER') {
        this.pushRowCalced();
      }
    } else if (this.selectedPropType === 'ROWDATEDIFF') {
      if (this.isForm) {
        this.addFormProps.patchValue({ calc: prop.uitype.calc, propreadonly: 'TRUE' });
      } else {
        this.addFormProps.patchValue({ calc: prop.uitype.calc, colcalc: prop.uitype.colcalc, copytoProp: prop.uitype.colcalc.copyto, colcalcType: prop.uitype.colcalc.type, propreadonly: 'TRUE' });
      }
      this.addFormProps.controls.propreadonly.disable();
      this.pushDateDiffValue();
    } else {
      this.addFormProps.controls.propreadonly.enable();
    }
    if ((this.selectedPropType === 'NUMBER' || this.selectedPropType === 'AUTONUMBER') && !this.isForm) {
      this.addFormProps.patchValue({ colcalcType: prop.uitype.colcalc.type, copytoProp: prop.uitype.colcalc.copyto });
      if (prop.uitype.colcalc.type === 'SUM') {
        this.addFormProps.controls.colcalc.patchValue({ type: 'SUM', label: 'Total', copyto: this.addFormProps.value.copytoProp });

      } else if (prop.uitype.colcalc.type === 'AVERAGE') {
        this.addFormProps.controls.colcalc.patchValue({ type: 'AVERAGE', label: 'Average', copyto: this.addFormProps.value.copytoProp });

      } else {
        this.addFormProps.controls.colcalc.patchValue({ type: 'NONE', label: 'None', copyto: this.addFormProps.value.copytoProp });
      }
    }
    if (prop.uitype.uitype === 'TEXTAF') {
      this.textAutoFilled();
    }
    if (!this.isForm) {
      this.pushCopyToArray();
    }
  }
  isActive(id) {
    return this.selectedSectionId === id;
  }

  getSub(id) {
    // TODO//
    this.selectedSectionId = (this.selectedSectionId === id ? id : null);
  }
  editSections(sec, isEdit) {
    let flag = true;
    for (let i = 0; i < this.arrayToShow[0].sections.length; i++) {
      if (i === this.secIndex && i !== undefined) {
        if (isEdit === 'EDIT') {
          for (let g = 0; g < this.arrayToShow.length; g++) {
            if (this.arrayToShow[g].sections.length > 0) {
              for (let ind = 0; ind < this.arrayToShow[g].sections.length; ind++) {
                if (this.arrayToShow[g].sections[ind].heading === sec.value.sectionHeading && ind !== this.secIndex) {
                  flag = false;
                  break;
                } else {
                  flag = true;

                }
              }
              if (flag === true && i === this.secIndex) {
                this.arrayToShow[0].sections[this.secIndex].heading = sec.value.sectionHeading;
                this.arrayToShow[0].sections[this.secIndex].name = sec.value.sectionName;
                this.arrayToShow[0].sections[this.secIndex].type = sec.value.type;
                this.arrayToShow[0].sections[this.secIndex].showHeader = sec.value.showHeader;
                this.arrayToShow[0].sections[this.secIndex].rOnly = sec.value.rOnly;
                this.arrayToShow[0].sections[this.secIndex].visible = sec.value.visible;
                this.arrayToShow[0].sections[this.secIndex].addRows = sec.value.addRows;
                this.selectedSection = sec;

              } else if (flag === false) {
                alert('Section name should be unique');
              }
            } else {
              this.arrayToShow[0].sections[this.secIndex].heading = sec.value.sectionHeading;
              this.arrayToShow[0].sections[this.secIndex].name = sec.value.sectionName;
              this.arrayToShow[0].sections[this.secIndex].type = sec.value.type;
              this.arrayToShow[0].sections[this.secIndex].showHeader = sec.value.showHeader;
              this.arrayToShow[0].sections[this.secIndex].rOnly = sec.value.rOnly;
              this.arrayToShow[0].sections[this.secIndex].visible = sec.value.visible;
              this.arrayToShow[0].sections[this.secIndex].addRows = sec.value.addRows;
              this.selectedSection = sec;

            }

          }

          break;
        } else {
          this.arrayToShow[0].sections.splice(i, 1);
          this.selectedColumn = '';
          this.selectedSection = '';
          this.secIndex = -1;
          this.datatableIndex = -1;
          this.adddatatableitems = false;
          this.editDatatableItems = false;
          this.colIndex = -1;
          this.propsIndex = -1;
          this.editSectionItems = false;
          this.showAddColumnsBut = false;
          this.showAddPropsBut = false;
          this.isForm = true;
          break;
        }
      }
    }
  }
  saveEditColumns(col, isEDit) {
    for (let i = 0; i < this.arrayToShow[0].sections.length; i++) {
      if (i === this.secIndex) {
        for (let k = 0; k < this.arrayToShow[0].sections[i].columns.length; k++) {
          if (k === this.colIndex) {
            if (isEDit === 'EDIT') {
              this.arrayToShow[0].sections[i].columns[k].heading = col.value.columnName;
              this.arrayToShow[0].sections[i].columns[k].width = col.value.columnWidth;
              this.selectedColumn = col;
              break;
            } else {
              this.arrayToShow[0].sections[i].columns.splice(k, 1);
              this.editColumns = false;
              this.showCSSns = false;
              this.selectedColumn = '';
              this.colIndex = -1;
              this.showAddColumnsBut = false;
              this.showAddPropsBut = false;
              break;
            }
          }
        }
      }
    }
  }
  saveEditProperty(prop, isEdit) {
    const isAddTrue = this.isExist(prop.value);
    if (isAddTrue) {
      this.tr.warning('', 'This property template is already present in this form please select another template');
      return;
    } else {
      if (this.isForm) {
        for (let i = 0; i < this.arrayToShow[0].sections.length; i++) {
          if (i === this.secIndex) {
            for (let k = 0; k < this.arrayToShow[0].sections[i].columns.length; k++) {
              if (k === this.colIndex) {
                for (let u = 0; u < this.arrayToShow[0].sections[i].columns[k].properties.length; u++) {
                  if (u === this.propsIndex) {
                    if (isEdit === 'EDIT') {
                      const lookup = [];
                      let number = 1;
                      let splitproplookup = [];
                      if (prop.value.proplookup === undefined) {
                        prop.value.proplookup = null;
                      }
                      if (prop.value.proplookup !== null) {
                        if (prop.value.proplookup.includes(',')) {
                          splitproplookup = prop.value.proplookup.split(',');
                        } else {
                          splitproplookup.push(prop.value.proplookup);
                        }

                        for (let a = 0; a < splitproplookup.length; a++) {
                          const propss = {
                            name: splitproplookup[a],
                            value: splitproplookup[a]
                          };
                          lookup.push(propss);
                          number++;
                        }
                      }
                      const propts = new FormProps();
                      this.arrayToShow[0].sections[i].columns[k].properties[u].name = prop.value.propname;
                      this.arrayToShow[0].sections[i].columns[k].properties[u].label = prop.value.proplabel;
                      this.arrayToShow[0].sections[i].columns[k].properties[u].type = prop.value.proptype;
                      this.arrayToShow[0].sections[i].columns[k].properties[u].length = prop.value.proplength;
                      this.arrayToShow[0].sections[i].columns[k].properties[u].rOnly = prop.value.propreadonly;
                      this.arrayToShow[0].sections[i].columns[k].properties[u].visible = prop.value.propvisible;
                      this.arrayToShow[0].sections[i].columns[k].properties[u].req = prop.value.proprequired;
                      this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.uitype = prop.value.uitype;
                      this.arrayToShow[0].sections[i].columns[k].properties[u].prop = prop.value.propTemplateSelected;
                      this.arrayToShow[0].sections[i].columns[k].properties[u].action = prop.value.actiondrop;
                      this.arrayToShow[0].sections[i].columns[k].properties[u].actions = prop.value.actions;
                      this.arrayToShow[0].sections[i].columns[k].properties[u].setVisible = prop.value.setVisible;
                      // break;
                      if (prop.value.uitype === 'LOOKUP' || prop.value.uitype === 'LOOKUPCONDITION') {
                        this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.lookups = lookup;
                        delete propts.uitype.dblookup;
                        delete propts.dbvalue;
                        delete propts.length;
                        if (prop.value.uitype === 'LOOKUPCONDITION') {
                          this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.condition = prop.value.condition;
                        }
                      } if (prop.value.uitype === 'DBLOOKUP' || prop.value.uitype === 'MTEXTTA' || prop.value.uitype === 'STEXTTA') {

                        this.arrayToShow[0].sections[i].columns[k].properties[u].dbvalue = [];

                        this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.dblookup = {};
                        this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.dblookup.dburl = prop.value.propdburl;
                        this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.dblookup.dbtype = prop.value.propdbtype;
                        this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.dblookup.sql = prop.value.propsql;
                        this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.dblookup.user = prop.value.propuser;
                        this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.dblookup.password = prop.value.proppassword;
                        this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.dblookup.filter = null;
                        this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.dblookup.namecol = prop.value.propnamecol;
                        this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.dblookup.valuecol = prop.value.propvaluecol;
                        this.arrayToShow[0].sections[i].columns[k].properties[u].isDefaultDB = prop.value.isDefaultDB;
                        delete propts.uitype.lookups;
                        delete propts.length;
                        if (prop.value.uitype === 'DBLOOKUP') {
                          this.integrationservice.getDBLookup(this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.dblookup).subscribe(res => this.assignDblookupvalue(res, 0, i, k, u));
                        }

                      } if (prop.value.uitype === 'ROWDATEDIFF' || prop.value.uitype === 'ROWSUM' || prop.value.uitype === 'ROWDIV' || prop.value.uitype === 'ROWAVG' || prop.value.uitype === 'ROWSUB') {
                        this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.calc = [];
                        this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.calc = prop.value.calc;
                        this.arrayToShow[0].sections[i].columns[k].properties[u].rOnly = 'TRUE';
                        delete this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.colcalc;
                        delete this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.colcalcType;
                      } if (prop.value.uitype === 'ROWEXPIRYDATE') {
                        this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.calc = [];
                        this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.calc = prop.value.calc;
                        delete this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.colcalc;
                        delete this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.colcalcType;
                        this.arrayToShow[0].sections[i].columns[k].properties[u].rOnly = prop.value.propreadonly;
                      }
                      if (prop.value.uitype === 'TEXTAF') {
                        this.arrayToShow[0].sections[i].columns[k].properties[u].dbvalue = [];
                        this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.autofill = {};
                        this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.autofill.dburl = prop.value.propdburl;
                        this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.autofill.dbtype = prop.value.propdbtype;
                        this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.autofill.sql = prop.value.propsql;
                        this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.autofill.user = prop.value.propuser;
                        this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.autofill.password = prop.value.proppassword;

                        this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.autofill.fillprops = [];
                        this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.autofill.fillprops = prop.value.fillprops;
                        this.arrayToShow[0].sections[i].columns[k].properties[u].isDefaultDB = prop.value.isDefaultDB;
                        this.integrationservice.getDBLookup(this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.autofill).subscribe(res => this.assignDblookupvalue(res, 0, i, k, u));

                      } if (prop.value.uitype !== 'DBLOOKUP' && prop.value.uitype !== 'MTEXTTA' && prop.value.uitype !== 'STEXTTA' && prop.value.uitype !== 'LOOKUP' && prop.value.uitype !== 'LOOKUPCONDITION') {
                        delete propts.uitype.dblookup;
                        delete propts.dbvalue;
                        delete propts.uitype.lookups;
                      } if (prop.value.uitype !== 'ROWDATEDIFF' && prop.value.uitype !== 'ROWSUM' && prop.value.uitype !== 'ROWDIV' && prop.value.uitype !== 'ROWAVG' && prop.value.uitype !== 'ROWSUB' && prop.value.uitype !== 'ROWEXPIRYDATE') {
                        delete this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.calc;
                        delete this.arrayToShow[0].sections[i].columns[k].properties[u].uitype.colcalc;
                        delete this.arrayToShow[0].sections[i].columns[k].properties[u].colcalcType;

                      } if (prop.value.uitype !== 'TEXTAF') {
                      }
                      delete propts.uitype.calc;
                      delete propts.uitype.colcalc;
                      delete propts.colcalcType;
                      break;
                    } else {
                      this.arrayToShow[0].sections[i].columns[k].properties.splice(u, 1);
                      this.editProperty = false;
                      this.showAddPropArea = false;
                      this.propsIndex = -1;
                      break;

                    }
                  }
                }
              }
            }
          }
        }
      } else {
        for (let i = 0; i < this.arrayToShow[0].sections.length; i++) {
          if (i === this.secIndex) {
            for (let u = 0; u < this.arrayToShow[0].sections[i].rowheader.length; u++) {
              if (u === this.propsIndex) {
                if (isEdit === 'EDIT') {
                  const lookup = [];
                  let number = 1;
                  let splitproplookup = [];
                  if (prop.value.proplookup === undefined) {
                    prop.value.proplookup = null;
                  }
                  if (prop.value.proplookup !== null) {
                    if (prop.value.proplookup.includes(',')) {
                      splitproplookup = prop.value.proplookup.split(',');
                    } else {
                      splitproplookup.push(prop.value.proplookup);
                    }

                    for (let a = 0; a < splitproplookup.length; a++) {
                      const propss = {
                        name: splitproplookup[a],
                        value: splitproplookup[a]
                      };
                      lookup.push(propss);
                      number++;
                    }
                  }
                  const propts = new FormProps();
                  this.arrayToShow[0].sections[i].rowheader[u].name = prop.value.propname;
                  this.arrayToShow[0].sections[i].rowheader[u].label = prop.value.proplabel;
                  this.arrayToShow[0].sections[i].rowheader[u].type = prop.value.proptype;
                  this.arrayToShow[0].sections[i].rowheader[u].length = prop.value.proplength;
                  this.arrayToShow[0].sections[i].rowheader[u].uitype.uitype = prop.value.uitype;
                  this.arrayToShow[0].sections[i].rowheader[u].prop = prop.value.propTemplateSelected;
                  this.arrayToShow[0].sections[i].rowheader[u].action = prop.value.actiondrop;
                  this.arrayToShow[0].sections[i].rowheader[u].actions = prop.value.actions;
                  this.arrayToShow[0].sections[i].rowheader[u].setVisible = prop.value.setVisible;
                  if (prop.value.uitype === 'AUTONUMBER' || prop.value.uitype === 'MULTIPLICATION' || prop.value.uitype === 'ROWSUB' || prop.value.uitype === 'ROWSUM' || prop.value.uitype === 'ROWDIV' || prop.value.uitype === 'ROWAVG' || prop.value.uitype === 'ROWDATEDIFF') {
                    this.arrayToShow[0].sections[i].rowheader[u].rOnly = 'TRUE';
                  } else {
                    this.arrayToShow[0].sections[i].rowheader[u].rOnly = prop.value.propreadonly;
                  }
                  this.arrayToShow[0].sections[i].rowheader[u].visible = prop.value.propvisible;
                  this.arrayToShow[0].sections[i].rowheader[u].req = prop.value.proprequired;
                  this.arrayToShow[0].sections[i].rowheader[u].width = prop.value.propwidth;
                  if (prop.value.uitype === 'ROWEXPIRYDATE') {
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.calc = [];
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.calc = prop.value.calc;
                    delete this.arrayToShow[0].sections[i].rowheader[u].uitype.colcalc;
                    this.arrayToShow[0].sections[i].rowheader[u].rOnly = prop.value.propreadonly;
                  }
                  if (prop.value.uitype === 'LOOKUP' || prop.value.uitype === 'LOOKUPCONDITION') {
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.lookups = lookup;
                    this.arrayToShow[0].sections[i].rows[0].items[u].uitype.lookups = lookup;
                    delete propts.uitype.dblookup;
                    delete propts.dbvalue;
                    delete propts.length;
                    if (this.arrayToShow[0].sections[i].rowheader[u].dblookup) {
                      delete this.arrayToShow[0].sections[i].rowheader[u].uitypedblookup;
                    } else if (this.arrayToShow[0].sections[i].rowheader[u].dbvalue) {
                      delete this.arrayToShow[0].sections[i].rowheader[u].dbvalue;
                    } else if (this.arrayToShow[0].sections[i].rowheader[u].length) {
                      delete this.arrayToShow[0].sections[i].rowheader[u].length;
                    } else { }
                    if (prop.value.uitype === 'LOOKUPCONDITION') {
                      this.arrayToShow[0].sections[i].rowheader.uitypecondition = prop.value.condition;
                    }
                  } else if (prop.value.uitype === 'DBLOOKUP' || prop.value.uitype === 'MTEXTTA' || prop.value.uitype === 'STEXTTA') {

                    this.arrayToShow[0].sections[i].rowheader[u].dbvalue = [];

                    this.arrayToShow[0].sections[i].rowheader[u].uitypedblookup = {};
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.dblookup.dburl = prop.value.propdburl;
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.dblookup.dbtype = prop.value.propdbtype;
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.dblookup.sql = prop.value.propsql;
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.dblookup.user = prop.value.propuser;
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.dblookup.password = prop.value.proppassword;
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.dblookup.filter = null;
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.dblookup.namecol = prop.value.propnamecol;
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.dblookup.valuecol = prop.value.propvaluecol;
                    this.arrayToShow[0].sections[i].rowheader[u].isDefaultDB = prop.value.isDefaultDB;

                    delete propts.uitype.lookups;
                    delete propts.length;
                    if (this.arrayToShow[0].sections[i].rowheader[u].lookups) {
                      delete this.arrayToShow[0].sections[i].rowheader[u].uitype.lookups;
                    } else if (this.arrayToShow[0].sections[i].rowheader[u].length) {
                      delete this.arrayToShow[0].sections[i].rowheader[u].length;
                    } else { }

                    if (prop.value.uitype === 'DBLOOKUP') {
                      this.integrationservice.getDBLookup(this.arrayToShow[0].sections[i].rowheader[u].uitype.dblookup).subscribe(res => this.assignDblookupvalueForTable(res, 0, i, u));
                    }
                  } else if (prop.value.uitpye === 'TEXTAF') {
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.fillprops = [];
                    this.arrayToShow[0].sections[i].rowheader[u].dbvalue = [];
                    this.arrayToShow[0].sections[i].rowheader[u].uitypeautofill = {};
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.autofill.dburl = prop.value.propdburl;
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.autofill.dbtype = prop.value.propdbtype;
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.autofill.sql = prop.value.propsql;
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.autofill.user = prop.value.propuser;
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.autofill.password = prop.value.proppassword;
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.autofill.isDefaultDB = prop.value.isDefaultDB;
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.autofill.fillprops = prop.value.fillprops;

                    this.integrationservice.getDBLookup(this.arrayToShow[0].sections[i].rowheader[u].uitypeautofill).subscribe(res => this.assignDblookupvalueForTable(res, 0, i, u));


                  } else if (prop.value.uitype === 'MULTIPLICATION' || prop.value.uitype === 'ROWSUB' || prop.value.uitype === 'ROWSUM' || prop.value.uitype === 'ROWDIV' || prop.value.uitype === 'ROWAVG' || prop.value.uitype === 'ROWDATEDIFF') {
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.calc = [];
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.colcalc = {};
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.calc = prop.value.calc;
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.colcalc = prop.value.colcalc;
                    this.arrayToShow[0].sections[i].rowheader[u].rOnly = 'TRUE';

                    delete propts.colcalcType;
                    if (this.arrayToShow[0].sections[i].rowheader[u].colcalcType) {
                      delete this.arrayToShow[0].sections[i].rowheader[u].colcalcType;
                    } else { }
                  } else if (prop.value.uitype === 'AUTONUMBER' || prop.value.uitype === 'NUMBER') {
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.colcalc = {};
                    this.arrayToShow[0].sections[i].rowheader[u].uitype.colcalc = prop.value.colcalc;

                    delete propts.uitype.calc;
                    delete propts.colcalcType;
                    delete this.arrayToShow[0].sections[i].rowheader[u].uitype.dblookup;
                    delete this.arrayToShow[0].sections[i].rowheader[u].uitype.autofill;
                    if (this.arrayToShow[0].sections[i].rowheader[u].uitype.calc !== undefined) {
                      delete this.arrayToShow[0].sections[i].rowheader[u].uitype.calc;
                    } else if (this.arrayToShow[0].sections[i].rowheader[u].colcalcType !== undefined) {
                      delete this.arrayToShow[0].sections[i].rowheader[u].colcalcType;
                    }

                  } else { }
                  if (prop.value.uitype !== 'DBLOOKUP' && prop.value.uitype !== 'MTEXTTA' && prop.value.uitype !== 'STEXTTA' && prop.value.uitype !== 'LOOKUP' && prop.value.uitype !== 'LOOKUPCONDITION') {
                    delete propts.uitype.dblookup;
                    delete propts.dbvalue;
                    delete propts.uitype.lookups;
                    delete this.arrayToShow[0].sections[i].rowheader[u].uitype.dblookup;
                    delete this.arrayToShow[0].sections[i].rowheader[u].uitype.dbvalue;
                    delete this.arrayToShow[0].sections[i].rowheader[u].uitype.lookups;

                  } if (prop.value.uitype !== 'TEXTAF') {

                  } if (prop.value.uitype !== 'MULTIPLICATION' && prop.value.uitype !== 'NUMBER' &&
                    prop.value.uitype !== 'AUTONUMBER' && prop.value.uitype !== 'ROWSUB' && prop.value.uitype !== 'ROWSUM' &&
                    prop.value.uitype !== 'ROWDIV' && prop.value.uitype !== 'ROWAVG' && prop.value.uitype !== 'ROWDATEDIFF' &&
                    prop.value.uitype !== 'ROWEXPIRYDATE') {
                    delete propts.uitype.calc;
                    delete propts.uitype.colcalc;
                    delete propts.colcalcType;
                    delete this.arrayToShow[0].sections[i].rowheader[u].uitype.calc;
                    delete this.arrayToShow[0].sections[i].rowheader[u].uitype.colcalc;
                    delete this.arrayToShow[0].sections[i].rowheader[u].colcalcType;

                  } if (prop.value.uitype === 'DATE') {
                    delete propts.uitype.colcalc;
                    delete propts.colcalcType;
                    delete propts.uitype.autofill;
                    delete propts.uitype.calc;
                    delete this.arrayToShow[0].scetions[i].rowheader[u].uitype.calc;
                    delete this.arrayToShow[0].sections[i].rowheader[u].uitype.colcalc;
                    delete this.arrayToShow[0].sections[i].rowheader[u].colcalType;
                    delete this.arrayToShow[0].sections[i].rowheader[u].uitype.autofill;
                  }

                  this.arrayToShow[0].sections[i].rows[0].items = [];
                  this.arrayToShow[0].sections[i].rows[0].items = this.arrayToShow[0].sections[i].rowheader;
                  break;
                } else {
                  this.arrayToShow[0].sections[i].rowheader.splice(u, 1);
                  this.arrayToShow[0].sections[i].rows[0].items.splice(u, 1);
                  this.editProperty = false;
                  this.showAddPropArea = false;
                  this.propsIndex = -1;
                  this.arrayToShow[0].sections[i].rows[0].items = [];
                  this.arrayToShow[0].sections[i].rows[0].items.push(this.arrayToShow[0].sections[i].rowheader);
                  break;

                }
              }
            }

          }
        }
      }

    }
  }
  moveUpSections(selectedSection) {
    if (this.secIndex > 0) {
      this.tempIndex = this.arrayToShow[0].sections[this.secIndex];
      this.arrayToShow[0].sections[this.secIndex] = this.arrayToShow[0].sections[this.secIndex - 1];
      this.arrayToShow[0].sections[this.secIndex - 1] = this.tempIndex;
      this.selectedSection = selectedSection;
      this.secIndex = this.secIndex - 1;
      this.sectionFont = this.secIndex;
      this.colFont = -1;
      this.propFont = -1;
    }
  }
  moveDownSections(selectedSection) {
    if (this.secIndex < this.arrayToShow[0].sections.length - 1) {
      this.tempIndex = this.arrayToShow[0].sections[this.secIndex];
      this.arrayToShow[0].sections[this.secIndex] = this.arrayToShow[0].sections[this.secIndex + 1];
      this.arrayToShow[0].sections[this.secIndex + 1] = this.tempIndex;
      this.selectedSection = selectedSection;
      this.secIndex = this.secIndex + 1;
      this.sectionFont = this.secIndex;
      this.colFont = -1;
      this.propFont = -1;
    }
  }
  moveUpColumns(selectedColumn) {
    if (this.colIndex > 0) {
      this.tempIndex = this.arrayToShow[0].sections[this.secIndex].columns[this.colIndex];
      this.arrayToShow[0].sections[this.secIndex].columns[this.colIndex] = this.arrayToShow[0].sections[this.secIndex].columns[this.colIndex - 1];
      this.arrayToShow[0].sections[this.secIndex].columns[this.colIndex - 1] = this.tempIndex;
      this.colIndex = this.colIndex - 1;
      this.selectedColumn = selectedColumn;
      this.sectionFont = -1;
      this.colFont = this.colIndex;
      this.propFont = -1;
    }
  }
  moveDowncolumns(selectedColumn) {
    if (this.colIndex < this.arrayToShow[0].sections[this.secIndex].columns.length - 1) {
      this.tempIndex = this.arrayToShow[0].sections[this.secIndex].columns[this.colIndex];
      this.arrayToShow[0].sections[this.secIndex].columns[this.colIndex] = this.arrayToShow[0].sections[this.secIndex].columns[this.colIndex + 1];
      this.arrayToShow[0].sections[this.secIndex].columns[this.colIndex + 1] = this.tempIndex;
      this.colIndex = this.colIndex + 1;
      this.selectedColumn = selectedColumn;
      this.sectionFont = -1;
      this.colFont = this.colIndex;
      this.propFont = -1;
    }
  }
  moveUpProps() {
    if (this.propsIndex > 0) {
      this.tempIndex = this.arrayToShow[0].sections[this.secIndex].columns[this.colIndex].properties[this.propsIndex];
      this.arrayToShow[0].sections[this.secIndex].columns[this.colIndex].properties[this.propsIndex] = this.arrayToShow[0].sections[this.secIndex].columns[this.colIndex].properties[this.propsIndex - 1];
      this.arrayToShow[0].sections[this.secIndex].columns[this.colIndex].properties[this.propsIndex - 1] = this.tempIndex;
      this.propsIndex = this.propsIndex - 1;
      this.sectionFont = -1;
      this.colFont = -1;
      this.propFont = this.propsIndex;
    }
  }
  moveDownProps() {
    if (this.propsIndex < this.arrayToShow[0].sections[this.secIndex].columns[this.colIndex].properties.length - 1) {
      this.tempIndex = this.arrayToShow[0].sections[this.secIndex].columns[this.colIndex].properties[this.propsIndex];
      this.arrayToShow[0].sections[this.secIndex].columns[this.colIndex].properties[this.propsIndex] = this.arrayToShow[0].sections[this.secIndex].columns[this.colIndex].properties[this.propsIndex + 1];
      this.arrayToShow[0].sections[this.secIndex].columns[this.colIndex].properties[this.propsIndex + 1] = this.tempIndex;
      this.propsIndex = this.propsIndex + 1;
      this.sectionFont = -1;
      this.colFont = -1;
      this.propFont = this.propsIndex;
    }
  }
  moveUpRowHeader() {
    if (this.propsIndex > 0) {
      this.tempIndex = this.arrayToShow[0].sections[this.secIndex].rowheader[this.propsIndex];
      this.arrayToShow[0].sections[this.secIndex].rowheader[this.propsIndex] = this.arrayToShow[0].sections[this.secIndex].rowheader[this.propsIndex - 1];
      this.arrayToShow[0].sections[this.secIndex].rowheader[this.propsIndex - 1] = this.tempIndex;
      this.tempIndex = this.arrayToShow[0].sections[this.secIndex].rows[0].items[this.propsIndex];
      this.arrayToShow[0].sections[this.secIndex].rows[0].items[this.propsIndex] = this.arrayToShow[0].sections[this.secIndex].rows[0].items[this.propsIndex - 1];
      this.arrayToShow[0].sections[this.secIndex].rows[0].items[this.propsIndex - 1] = this.tempIndex;
      this.propsIndex = this.propsIndex - 1;
      this.sectionFont = -1;
      this.colFont = -1;
      this.propFont = this.propsIndex;


    }
  }
  moveDownRowHeader() {
    if (this.propsIndex < this.arrayToShow[0].sections[this.secIndex].rowheader.length - 1) {
      this.tempIndex = this.arrayToShow[0].sections[this.secIndex].rowheader[this.propsIndex];
      this.arrayToShow[0].sections[this.secIndex].rowheader[this.propsIndex] = this.arrayToShow[0].sections[this.secIndex].rowheader[this.propsIndex + 1];
      this.arrayToShow[0].sections[this.secIndex].rowheader[this.propsIndex + 1] = this.tempIndex;
      this.tempIndex = this.arrayToShow[0].sections[this.secIndex].rows[0].items[this.propsIndex];
      this.arrayToShow[0].sections[this.secIndex].rows[0].items[this.propsIndex] = this.arrayToShow[0].sections[this.secIndex].rows[0].items[this.propsIndex + 1];
      this.arrayToShow[0].sections[this.secIndex].rows[0].items[this.propsIndex + 1] = this.tempIndex;
      this.propsIndex = this.propsIndex + 1;
      this.sectionFont = -1;
      this.colFont = -1;
      this.propFont = this.propsIndex;
    }
  }
  onTypeChange(event) {
    if (this.selectedType === 'TEXT') {
      this.selectedPropType = 'TEXT';
      this.addFormProps.patchValue({ uitype: 'TEXT' });
    } else if (this.selectedType === 'NUMBER') {
      this.selectedPropType = 'NUMBER';
      this.addFormProps.patchValue({ uitype: 'NUMBER' });
      if (!this.isForm) {
        this.addFormProps.controls.colcalcType.patchValue('NONE');
        this.addFormProps.controls.copytoProp.patchValue('');
        this.addFormProps.controls.colcalc.patchValue({ type: 'NONE', label: 'None', copyto: '' });
      }
    } else {
      this.selectedPropType = 'DATE';
      this.addFormProps.patchValue({ uitype: 'DATE' });
    }

  }
  actionTypeChanged(event) {
    this.selectedActionDrop = event.value;
    if (event.value === 'autoFill') {
      this.textAutoFilled();
      this.addFormProps.patchValue({ actions: [] });
      const actionOne = {
        action: 'autoFill',
        isDefaultDB: true,
        autofill: {
          dburl: 'DEFAULT',
          dbtype: 'SQLSERVER',
          user: 'DEFAULT',
          password: 'DEFAULT',
          sql: '',
          fillprops: [],
          fillparams: [],
        }
      };
      this.addFormProps.controls.actions.value.push(actionOne);
    } else {
      this.addFormProps.patchValue({ actions: [] });
    }
    if (event.value === 'setVisibility') {
      this.addFormProps.patchValue({ setVisible: {} });
      const value1 = {
        val: '',
        show: [],
        hide: [],
        ronly: [],
        hidesec: [],
        ronlysec: []
      };
      this.addFormProps.patchValue({ setVisible: value1 });
      this.setVisibleConditions();

    }
  }
  valAdded(event) {
    if (event.value) {
      const showValue = this.addFormProps.controls.setVisible.value.show;
      const hideValue = this.addFormProps.controls.setVisible.value.hide;
      const ronlyValue = this.addFormProps.controls.setVisible.value.ronly;
      const hideSec = this.addFormProps.controls.setVisible.value.hidesec;
      const ronlySec = this.addFormProps.controls.setVisible.value.ronlysec;
      this.addFormProps.controls.setVisible.patchValue({ val: event.value, show: showValue, hide: hideValue, ronly: ronlyValue, hidesec: hideSec, ronlysec: ronlySec });
    }
  }
  addAction() {
    const actionOne = {
      action: 'autoFill',
      isDefaultDB: true,
      autofill: {
        dburl: 'DEFAULT',
        dbtype: 'SQLSERVER',
        user: 'DEFAULT',
        password: 'DEFAULT',
        sql: '',
        fillprops: [],
        fillparams: [],
      }
    };
    this.addFormProps.controls.actions.value.push(actionOne);
  }
  actionUp(act, index) {
    const temp = this.addFormProps.controls.actions.value[index - 1];
    this.addFormProps.controls.actions.value[index - 1] = this.addFormProps.controls.actions.value[index];
    this.addFormProps.controls.actions.value[index] = temp;
  }
  actionDown(act, index) {
    const temp = this.addFormProps.controls.actions.value[index + 1];
    this.addFormProps.controls.actions.value[index + 1] = this.addFormProps.controls.actions.value[index];
    this.addFormProps.controls.actions.value[index] = temp;
  }
  actionRemove(id, act, index) {
    this.actionSelected = {
      action: act,
      index: index
    };
    this.modalService.getModal('').open();

  }
  confirmToactionRemove() {
    this.addFormProps.controls.actions.value.splice(this.actionSelected.index, 1);
    console.log(this.addFormProps.controls.actions.value);
  }
  deafultActionDbChanged(event, act, index) {
    this.addFormProps.controls.actions.value[index].isDefaultDB = event;
    if (event === true) {
      this.addFormProps.controls.actions.value[index].autofill.dburl = 'DEFAULT';
      this.addFormProps.controls.actions.value[index].autofill.dbtype = 'SQLSERVER';
      this.addFormProps.controls.actions.value[index].autofill.user = 'DEFAULT';
      this.addFormProps.controls.actions.value[index].autofill.password = 'DEFAULT';
    }
  }
  onUITypeChange(event) {
    this.selectedPropType = event.value;
    this.addFormProps.controls.calc.patchValue([]);
    this.addFormProps.controls.fillprops.patchValue([]);
    this.pushCopyToArray();
    this.addFormProps.patchValue({ propdburl: 'DEFAULT', propdbtype: 'SQLSERVER', propuser: 'DEFAULT', proppassword: 'DEFAULT', isDefaultDB: true });
    if (event.value === 'MULTIPLICATION' || event.value === 'AUTONUMBER' || event.value === 'ROWSUB' || event.value === 'ROWSUM' || event.value === 'ROWDIV' || event.value === 'ROWAVG') {
      this.pushRowCalced();
      this.pushCopyToArray();
      this.addFormProps.controls.colcalcType.patchValue('NONE');
      this.addFormProps.controls.copytoProp.patchValue('');
      this.addFormProps.controls.colcalc.patchValue({ type: 'NONE', label: 'None', copyto: '' });
      this.addFormProps.controls.propreadonly.disable();
      this.addFormProps.controls.propreadonly.patchValue('TRUE');
    } else {
      this.addFormProps.controls.propreadonly.enable();
    }
    if (event.value === 'NUMBER') {
      this.addFormProps.controls.colcalcType.patchValue('NONE');
      this.addFormProps.controls.copytoProp.patchValue('');
      this.addFormProps.controls.colcalc.patchValue({ type: 'NONE', label: 'None', copyto: '' });
    } if (event.value === 'TEXTAF') {
      this.textAutoFilled();
    }
    if (event.value === 'ROWDATEDIFF') {
      this.addFormProps.controls.propreadonly.disable();
      this.addFormProps.controls.propreadonly.patchValue('TRUE');
      this.addFormProps.controls.colcalcType.patchValue('NONE');
      this.addFormProps.controls.copytoProp.patchValue('');
      this.addFormProps.controls.colcalc.patchValue({ type: 'NONE', label: 'None', copyto: '' });
      this.pushDateDiffValue();
    }
    if (event.value === 'ROWEXPIRYDATE') {
      this.addFormProps.controls.propreadonly.enable();
      this.addFormProps.controls.propreadonly.patchValue('FALSE');
      this.addFormProps.controls.colcalcType.patchValue('NONE');
      this.addFormProps.controls.copytoProp.patchValue('');
      this.addFormProps.controls.colcalc.patchValue({ type: 'NONE', label: 'None', copyto: '' });
      this.pushDateExpiryValue();
    }
  }
  pushCopyToArray() {
    this.copyToArray = [];
    if (!this.isForm) {
      const arrayValue = this.arrayToShow[0].sections;
      for (let a = 0; a < arrayValue.length; a++) {
        if (arrayValue[a].type === 'FORM') {
          for (let b = 0; b < arrayValue[a].columns.length; b++) {
            for (let d = 0; d < arrayValue[a].columns[b].properties.length; d++) {
              if (arrayValue[a].columns[b].properties[d].uitype.uitype === 'NUMBER') {
                this.copyToArray.push(arrayValue[a].columns[b].properties[d].name);
                this.copyToArray.forEach(element => {
                  const value = {
                    label: arrayValue[a].columns[b].properties[d].name,
                    value: arrayValue[a].columns[b].properties[d].name
                  };
                  this.copyToArryDropDown.push(value);
                });
              }
            }
          }
        }
      }
    }
  }
  pushDateDiffValue() {
    this.dateDifferenceProps = [];
    if (this.isForm) {
      const arrayValue = this.arrayToShow[0].sections;
      for (let a = 0; a < arrayValue.length; a++) {
        if (arrayValue[a].type === 'FORM') {
          for (let b = 0; b < arrayValue[a].columns.length; b++) {
            for (let d = 0; d < arrayValue[a].columns[b].properties.length; d++) {
              if (arrayValue[a].columns[b].properties[d].type === 'DATE') {
                this.dateDifferenceProps.push(arrayValue[a].columns[b].properties[d].name);
              }
            }
          }
        }
      }
    } else {
      const arrayVlaue = this.arrayToShow[0].sections[this.secIndex].rowheader;
      for (let g = 0; g < arrayVlaue.length; g++) {
        if (arrayVlaue[g].type === 'DATE') {
          this.dateDifferenceProps.push(arrayVlaue[g].name);
        }
      }
    }
  }
  pushDateExpiryValue() {
    this.dateExpiryProps = [];
    if (this.isForm) {
      const arrayValue = this.arrayToShow[0].sections;
      for (let a = 0; a < arrayValue.length; a++) {
        if (arrayValue[a].type === 'FORM') {
          for (let b = 0; b < arrayValue[a].columns.length; b++) {
            for (let d = 0; d < arrayValue[a].columns[b].properties.length; d++) {
              if (arrayValue[a].columns[b].properties[d].type === 'DATE' || arrayValue[a].columns[b].properties[d].type === 'NUMBER') {
                const expDate = {
                  name: arrayValue[a].columns[b].properties[d].name,
                  type: arrayValue[a].columns[b].properties[d].type
                };
                this.dateExpiryProps.push(expDate);
              }
            }
          }
        }
      }
    } else {
      const arrayVlaue = this.arrayToShow[0].sections[this.secIndex].rowheader;
      for (let g = 0; g < arrayVlaue.length; g++) {
        if (arrayVlaue[g].type === 'DATE' || arrayVlaue[g].type === 'NUMBER') {
          const expDate = {
            name: arrayVlaue[g].name,
            type: arrayVlaue[g].type
          };
          this.dateExpiryProps.push(arrayVlaue[g].name);
        }
      }
    }
  }
  textAutoFilled() {
    const arrayLeng = this.arrayToShow[0].sections;
    this.autoFillArray = [];
    for (let f = 0; f < arrayLeng.length; f++) {
      if (arrayLeng[f].type === 'FORM') {
        for (let c = 0; c < arrayLeng[f].columns.length; c++) {
          for (let p = 0; p < arrayLeng[f].columns[c].properties.length; p++) {
            if (p !== this.propsIndex || c !== this.colIndex || f !== this.secIndex) {
              this.autoFillArray.push(arrayLeng[f].columns[c].properties[p].name);
            }
          }
        }
      }
    }
  }
  setVisibleConditions() {
    const arrayLeng = this.arrayToShow[0].sections;
    this.visibilityShow = [];
    this.visibilityHide = [];
    this.visiblityRonly = [];
    this.visibilityHidSec = [];
    this.visibilityRolySec = [];
    for (let f = 0; f < arrayLeng.length; f++) {
      if (arrayLeng[f].type === 'FORM') {
        if (f !== this.secIndex) {
          this.visibilityHidSec.push(arrayLeng[f].heading);
          this.visibilityRolySec.push(arrayLeng[f].heading);

        }
        for (let c = 0; c < arrayLeng[f].columns.length; c++) {
          for (let p = 0; p < arrayLeng[f].columns[c].properties.length; p++) {
            if (p !== this.propsIndex || c !== this.colIndex || f !== this.secIndex) {
              this.visibilityShow.push(arrayLeng[f].columns[c].properties[p].name);
              this.visibilityHide.push(arrayLeng[f].columns[c].properties[p].name);
              this.visiblityRonly.push(arrayLeng[f].columns[c].properties[p].name);
            }
          }
        }
      }
    }
  }
  pushRowCalced() {
    this.calcArray = [];
    if (this.isForm) {
      const arrayValue = this.arrayToShow[0].sections;
      for (let a = 0; a < arrayValue.length; a++) {
        if (arrayValue[a].type === 'FORM') {
          for (let b = 0; b < arrayValue[a].columns.length; b++) {
            for (let d = 0; d < arrayValue[a].columns[b].properties.length; d++) {
              if (arrayValue[a].columns[b].properties[d].type === 'NUMBER') {
                this.calcArray.push(arrayValue[a].columns[b].properties[d].name);
              }
            }
          }
        }
      }
    } else {
      const arrayVlaue = this.arrayToShow[0].sections[this.secIndex].rowheader;
      for (let g = 0; g < arrayVlaue.length; g++) {
        if ((arrayVlaue[g].type === 'NUMBER' || arrayVlaue[g].type === 'AUTONUMBER' ||
          arrayVlaue[g].type === 'MULTIPLICATION' || arrayVlaue[g].type === 'ROWSUB' ||
          arrayVlaue[g].type === 'ROWSUM' || arrayVlaue[g].type === 'ROWDIV' || arrayVlaue[g].type === 'ROWAVG') &&
          g !== this.propsIndex && arrayVlaue.type !== 'ROWDATEDIFF') {
          this.calcArray.push(arrayVlaue[g].name);
        }
      }
    }
  }
  onSearch(evt, dBlookUp, sectionValue, columnvalue, propvalue) {
    dBlookUp.filter = evt.target.value;
    const keyUp: String = evt.target.value;
    this.arrayToShow[0].sections[sectionValue].columns[columnvalue].properties[propvalue].dbvalue = [];
    if (keyUp.length > 3) {
      const newRole = {
        'id': '',
        'itemName': evt.target.value
      };
      this.arrayToShow[0].sections[sectionValue].columns[columnvalue].properties[propvalue].dbvalue.push(newRole);
      this.integrationservice.getDBTypeAhead(dBlookUp).subscribe(res => this.assinDBTypeAhead(res, sectionValue, columnvalue, propvalue));
    }
  }


  assinDBTypeAhead(res, sectionValue, columnvalue, propvalue) {
    for (let index = 0; index < this.arrayToShow.length; index++) {
      for (let i = 0; i < this.arrayToShow[index].sections.length; i++) {
        for (let j = 0; j < this.arrayToShow[index].sections[i].columns.length; j++) {
          for (let k = 0; k < this.arrayToShow[index].sections[i].columns[j].properties.length; k++) {
            if (sectionValue === i && columnvalue === j && propvalue === k) {
              const value = JSON.parse(res._body);
              for (let val = 0; val < value.length; val++) {
                const element = {
                  'id': value[val].value,
                  'itemName': value[val].label
                };
                this.arrayToShow[index].sections[i].columns[j].properties[k].dbvalue.push(element);
              }
            }
          }
        }
      }
    }
  }
  assignDblookupvalue(res, index, i, j, k) {
    const value = JSON.parse(res._body);
    for (let val = 0; val < value.length; val++) {
      if (val === 0) {
      }
      const elementval = {
        'id': value[val].value,
        'itemName': value[val].label
      };
      this.arrayToShow[index].sections[i].columns[j].properties[k].dbvalue.push(elementval);
    }
  }
  assignDblookupvalueForTable(res, index, i, k) {
    const value = JSON.parse(res._body);
    for (let val = 0; val < value.length; val++) {
      if (val === 0) {

      }
      const elementval = {
        'id': value[val].value,
        'itemName': value[val].label
      };
      this.arrayToShow[index].sections[i].rowheader[k].dbvalue.push(elementval);
      this.arrayToShow[index].sections[i].rows[k].dbvalue.push(elementval);

    }
  }
  saveForm() {
    for (let i = 0; i < this.arrayToShow[0].sections.length; i++) {
      if (this.arrayToShow[0].sections[i].columns) {
        for (let k = 0; k < this.arrayToShow[0].sections[i].columns.length; k++) {
          for (let u = 0; u < this.arrayToShow[0].sections[i].columns[k].properties.length; u++) {
            if (this.arrayToShow[0].sections[i].columns[k].properties[u].type === 'DBLOOKUP' || this.arrayToShow[0].sections[i].columns[k].properties[u].type === 'STEXTTA' || this.arrayToShow[0].sections[i].columns[k].properties[u].type === 'MTEXTTA') {
              delete this.arrayToShow[0].sections[i].columns[k].properties[u].dbvalue;
              this.arrayToShow[0].sections[i].columns[k].properties[u].value = [];
            }
          }
        }
      } else {

        for (let u = 0; u < this.arrayToShow[0].sections[i].rowheader.length; u++) {
          if (this.arrayToShow[0].sections[i].rowheader[u].type === 'DBLOOKUP' || this.arrayToShow[0].sections[i].rowheader[u].type === 'STEXTTA' || this.arrayToShow[0].sections[i].rowheader[u].type === 'MTEXTTA') {
            delete this.arrayToShow[0].sections[i].rowheader[u].dbvalue;
            this.arrayToShow[0].sections[i].rowheader[u].value = [];
          }
        }
      }

    }
    this.spinner.show();
    if (this.editIs === false) {

      const value = this.arrayToShow.pop();
      this.ss.addform(value).subscribe(data => { this.formAdded(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
    } else {
      this.ds.checkOut(this.docId).subscribe(data => { this.checkinfile(data); }, error => { this.spinner.hide(); });
    }
  }


  checkinfile(data) {
    const docInfo = {
      id: data._body,
      docclass: 'ProductivitiConfig',
      docTitle: this.arrayToShow[0].name,
      props: [{
        'name': 'Document Title',
        'symName': 'DocumentTitle',
        'dtype': 'STRING',
        'mvalues': [this.arrayToShow[0].name],
        'mtype': 'N',
        'len': 255,
        'rOnly': 'false',
        'hidden': 'false',
        'req': 'false'
      }],
      accessPolicies: []
    };
    const name = this.arrayToShow[0].name;
    const formData = new FormData();
    formData.append('DocInfo', JSON.stringify(docInfo));
    formData.append('file', new Blob([JSON.stringify(this.arrayToShow.pop())]), name);
    this.ds.checkIn(formData).subscribe(dataRes => { this.formAdded(dataRes); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
  formAdded(data) {
    this.isAddTab = false;
    this.isListTab = true;
    this.showAddTab = false;
    this.assignFalse();
    this.ngOnInit();
  }
  gotForms(data) {
    this.source = JSON.parse(data._body);
  }
  assignFalse() {
    this.arrayToShow = [];
    this.showAddSectionsBut = false;
    this.addDatatableBtn = false;
    this.showAddColumnsBut = false;
    this.showAddPropsBut = false;
    this.showAddSectionforms = false;
    this.showAddPropArea = false;
    this.editProperty = false;
    this.showCSSns = false;
    this.editColumns = false;
    this.editSectionItems = false;
    this.showSaveFormBut = false;
    this.showFormButton = true;
    this.showAddFormName = true;
    this.showEditFormName = false;

  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
  toggleDivClicked(secindex) {
    for (let index = 0; index < this.arrayToShow[0].sections.length; index++) {
      const x = document.getElementById(this.arrayToShow[0].sections[index].heading);
      if (secindex === this.arrayToShow[0].sections[index].heading) {
        x.className = x.className.replace('preToggle', 'postToggle ');
      } else {
        x.className = x.className.replace('postToggle ', 'preToggle ');
      }
    }
  }
  viewForms(data) {
    this.formName = (JSON.parse(data._body)).name;
    this.showView = [];
    this.showView.push(JSON.parse(data._body));

    this.assignFalse();
    this.showFormButton = false;
    this.formsArray = null;
    this.isAddTab = true;
    this.isListTab = false;
    this.showAddTab = true;
    this.formsArray = JSON.parse(data._body);
    this.arrayToShow.push(this.formsArray);
    this.propNames = [];
    if (this.arrayToShow[0].datatable === undefined) {
      this.arrayToShow[0].datatable = [];
    }
    for (let i = 0; i < this.arrayToShow[0].sections.length; i++) {
      if (this.arrayToShow[0].sections[i].type === 'FORM') {
        for (let a = 0; a < this.arrayToShow[0].sections[i].columns.length; a++) {
          if (this.arrayToShow[0].sections[i].columns[a].heading === undefined) {
            this.arrayToShow[0].sections[i].columns[a].heading = 'col' + a + i;
          }
          for (let t = 0; t < this.arrayToShow[0].sections[i].columns[a].properties.length; t++) {
            this.propNames.push(this.arrayToShow[0].sections[i].columns[a].properties[t].name);
          }
        }
      } else {
        for (let b = 0; b < this.arrayToShow[0].sections[i].rowheader.length; b++) {
          this.propNames.push(this.arrayToShow[0].sections[i].rowheader[b].name);
        }
      }
    }
    this.showAddSectionsBut = false;
    this.addDatatableBtn = false;
    this.showAddColumnsBut = false;
    this.showAddPropsBut = false;
    this.showAddSectionforms = false;
    this.showAddPropArea = false;
    this.editProperty = false;
    this.showCSSns = false;
    this.editColumns = false;
    this.editSectionItems = false;
    this.showSaveFormBut = true;
    this.showAddFormName = false;
    const title = {
      id: this.formsArray.id,
      name: this.formsArray.name,
      formTemplate: this.formsArray.formTemplate,
      label: this.formsArray.label
    };
    this.showAddSecBut(title);
    if (this.arrayToShow[0].sections[0] && this.arrayToShow[0].sections[0].type === 'TABLE') {
      this.isForm = false;
    } else {
      this.isForm = true;
    }
  }

  changeSectionFont(secindex) {
    this.sectionFont = secindex;
    this.colFont = -1;
    this.propFont = -1;
    this.formFont = false;
  }
  changeColumnFont(colinex, heading) {
    this.colFont = colinex;
    this.sectionFont = -1;
    this.propFont = -1;
    this.formFont = false;
    for (let index = 0; index < this.arrayToShow[0].sections.length; index++) {
      if (this.arrayToShow[0].sections[index].columns) {
        for (let i = 0; i < this.arrayToShow[0].sections[index].columns.length; i++) {
          const colvalue = this.arrayToShow[0].sections[index].columns[i].heading;
          const secvalue = this.secIndex;
          const x = document.getElementById(secvalue + colvalue);
          if ((heading) === (this.arrayToShow[0].sections[index].columns[i].heading)) {
            x.className = x.className.replace('preColToggle', 'postColToggle');
          } else {
            if (x) {
              x.className = x.className.replace('postColToggle', 'preColToggle');
            }
          }
        }
      }
    }
  }
  changePropFont(propindex, colindex) {
    this.propFont = -1;
    this.propFont = propindex;
    this.propFont = propindex;
    this.sectionFont = -1;
    this.colFont = -1;
    this.formFont = false;
  }
  onDbTypeChanged(event) {
    this.addFormProps.patchValue({ propdbtype: event.value });
  }
  onActionDbTypeChanged(event, index) {
    this.addFormProps.controls.actions.value[index].autofill.dbtype = event.target.value;
  }
  dbUrl(event, index) {
    this.addFormProps.controls.actions.value[index].autofill.dburl = event.arget.tvalue;
  }
  dbUser(event, index) {
    this.addFormProps.controls.actions.value[index].autofill.user = event.target.value;
  }
  dbPassword(event, index) {
    this.addFormProps.controls.actions.value[index].autofill.password = event.target.value;
  }
  dbSql(event, index) {
    this.addFormProps.controls.actions.value[index].autofill.sql = event.target.value;
  }
  templateChanged(event) {
    this.formsName.patchValue({ 'formTemplate': event.value });
  }
  propertyTemplateChanged(event) {
    this.pushCopyToArray();
    for (let i = 0; i < this.propTemplatesDrop.length; i++) {
      if ((+this.propTemplatesDrop[i].id) === (+event.value)) {
        this.addFormProps.patchValue({ propTemplate: event.value, propname: this.propTemplatesDrop[i].symName, proplabel: this.propTemplatesDrop[i].name, propTemplateSelected: this.propTemplatesDrop[i] });
        if (this.propTemplatesDrop[i].dtype === 'INTEGER') {
          this.selectedType = 'NUMBER';
          this.selectedPropType = 'NUMBER';
          this.selectedTypeIsNumber();
          this.addFormProps.patchValue({ proptype: 'NUMBER', uitype: 'NUMBER' });
        } else if (this.propTemplatesDrop[i].dtype === 'STRING') {
          this.selectedType = 'TEXT';
          this.selectedPropType = 'TEXT';
          this.selectedTypeIsText();
          this.addFormProps.patchValue({ proptype: 'TEXT', uitype: 'TEXT' });
        } else if (this.propTemplatesDrop[i].dtype === 'DATE' || this.propTemplatesDrop[i].dtype === 'DATETIME') {
          this.selectedType = 'DATE';
          this.selectedPropType = 'DATE';
          this.selectedTypeIsDate();
          this.addFormProps.patchValue({ proptype: 'DATE', uitype: 'DATE' });
        }
        this.onTypeChange(event);
        break;
      }
    }
  }
  selectedTypeIsNumber() {
    this.uiTypeDropDown = [];
    this.uiTypeDropDown = [
      { value: 'NUMBER', label: 'Number Box' },
      { value: 'CHECKBOX', label: 'Checkbox' },
      { value: 'ROWSUM', label: 'Sum' },
      { value: 'MULTIPLICATION', label: 'Multiplication' },
      { value: 'ROWSUB', label: 'Subtraction' },
      { value: 'ROWDIV', label: 'Division' },
      { value: 'ROWAVG', label: 'Average' },
    ];
    this.uiTypeDropDown.forEach((element, i) => {
      if (element.value === 'AUTONUMBER') {
        this.uiTypeDropDown.splice(i, 1);
        return true;
      }
    });
    if (!this.isForm) {
      const value = {
        label: 'Auto Number', value: 'AUTONUMBER'
      };
      this.uiTypeDropDown.push(value);
    }
  }
  selectedTypeIsText() {
    this.uiTypeDropDown = [];
    this.uiTypeDropDown = [
      { value: 'TEXT', label: 'Text Box' },
      { value: 'LOOKUP', label: 'Lookup' },
      { value: 'LOOKUPCONDITION', label: 'Conditional Lookup' },
      { value: 'DBLOOKUP', label: 'DbLookup' },
      { value: 'TEXTAREA', label: 'Textarea' },
      { value: 'STEXTTA', label: 'Single Text Type Ahead' },
      { value: 'MTEXTTA', label: 'Multi Text Type Ahead' },
    ];
  }
  selectedTypeIsDate() {
    this.uiTypeDropDown = [];
    this.uiTypeDropDown = [
      { value: 'DATE', label: 'Date' },
      { value: 'DATERANGE', label: 'Date Range' },
      { value: 'TIME', label: 'Time' },
      { value: 'ROWDATEDIFF', label: 'Date Difference' },
    ];
    this.uiTypeDropDown.forEach((element, i) => {
      if (element.value === 'ROWEXPIRYDATE' || element.value === 'DATETIME') {
        this.uiTypeDropDown.splice(i, 1);
      }
    });
    if (this.isForm) {
      const value1 = { label: 'Expiry Date', value: 'ROWEXPIRYDATE' };
      const value2 = { label: 'Date Time', value: 'DATETIME' };
      this.uiTypeDropDown.push(value1);
      this.uiTypeDropDown.push(value2);
    }
  }
  sectionType(event) {
    if (event.value === 'FORM') {
      this.isForm = true;
    } else {
      this.isForm = false;
    }
  }
  addNewTableRow(row) {
    for (let index = 0; index < this.arrayToShow.length; index++) {
      for (let i = 0; i < this.arrayToShow[index].sections.length; i++) {
        if (this.arrayToShow[index].sections[i].type === 'TABLE' && row.id === this.arrayToShow[index].sections[i].id) {
          let rowheader: any;
          const rowItem = {
            row: this.arrayToShow[index].sections[i].rows.length + 1,
            items: []
          };
          for (let j = 0; j < this.arrayToShow[index].sections[i].rowheader.length; j++) {
            rowheader = this.arrayToShow[index].sections[i].rowheader[j];
            const itemFormName = rowheader.name + rowItem.row;
            let sel: any;
            if (this.arrayToShow[index].sections[i].rowheader[j].type === 'LOOKUP') {
              rowheader.lookup = this.arrayToShow[index].sections[i].rowheader[j].lookups;
              rowheader.lookupOptions = [];
              rowheader.value = [];
              const select = {
                name: rowheader.lookups[0].name,
                value: rowheader.lookups[0].value
              };
              sel = select;
              rowheader.value.push(select);
              for (const options of rowheader.lookups) {
                const lookupValue = {
                  label: options.name,
                  value: options.value
                };
                rowheader.lookupOptions.push(lookupValue);
              }
            }

            rowheader.value = [];
            rowItem.items.push(rowheader);
          }
          this.arrayToShow[index].sections[i].rows.push(rowItem);
        }
      }
    }
  }

  importNewForm() {
    this.modalService.getModal('JSONImporter').open();

  }
  addCondition() {
    const pushvalue = {
      val: '',
      show: [],
      hide: []
    };
    if (this.addFormProps.controls.condition.value === undefined) {
      this.addFormProps.patchValue({ condition: [] });
    }
    this.addFormProps.controls.condition.value.push(pushvalue);

  }
  addValue(event, index) {
    const pushValue = this.addFormProps.controls.condition.value;
    for (let i = 0; i < pushValue.length; i++) {
      if (i === index) {
        pushValue[i].val = event.value;
      }

    }
  }
  showToggle(i) {
    document.getElementById(i).classList.toggle('show');
  }
  hideToggle(i, id) {
    document.getElementById(id).classList.toggle('hide');
  }
  toggleCalced(id) {
    document.getElementById(id).classList.toggle('hide');
  }
  toggleCalcedExp(id) {
    document.getElementById(id).classList.toggle('hide');
  }
  toggleAutoFilled(id) {
    document.getElementById(id).classList.toggle('hide');
  }
  toggleActionAutoFilled(id, r) {
    document.getElementById(id).classList.toggle('hide');
  }
  toggleVisiHideFilled(id) {
    document.getElementById(id).classList.toggle('hide');
  }
  filterFunction(index, event) {
    let input, filter, a, i;
    input = document.getElementById(index + 'id');
    filter = input.value.toUpperCase();
    const div = document.getElementById(index);
    a = div.getElementsByTagName('a');
    for (i = 0; i < a.length; i++) {
      const txtValue = a[i].textContent || a[i].innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = '';
      } else {
        a[i].style.display = 'none';
      }
    }
  }
  filterFunctionHide(index, event) {
    let input, filter, a, i;
    input = document.getElementById(index + 'idHide');
    filter = input.value.toUpperCase();
    const div = document.getElementById(index + 'hide');
    a = div.getElementsByTagName('a');
    for (i = 0; i < a.length; i++) {
      const txtValue = a[i].textContent || a[i].innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = '';
      } else {
        a[i].style.display = 'none';
      }
    }
  }
  filterFunctionCalced(calcId, event) {
    let input, filter, a, i;
    input = document.getElementById(calcId);
    filter = input.children['0'].value.toUpperCase();
    const div = document.getElementById(calcId);
    a = div.getElementsByTagName('a');
    for (i = 0; i < a.length; i++) {
      const txtValue = a[i].textContent || a[i].innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = '';
      } else {
        a[i].style.display = 'none';
      }
    }
  }
  filterFunctionCalcedExp(calcId, event) {
    let input, filter, a, i;
    input = document.getElementById(calcId);
    filter = input.children['0'].value.toUpperCase();
    const div = document.getElementById(calcId);
    a = div.getElementsByTagName('a');
    for (i = 0; i < a.length; i++) {
      const txtValue = a[i].textContent || a[i].innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = '';
      } else {
        a[i].style.display = 'none';
      }
    }
  }
  filterFunctionAutoFilled(id, event) {
    let input, filter, a, i;
    input = document.getElementById(id);
    filter = input.children['0'].value.toUpperCase();
    const div = document.getElementById(id);
    a = div.getElementsByTagName('a');
    for (i = 0; i < a.length; i++) {
      const txtValue = a[i].textContent || a[i].innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = '';
      } else {
        a[i].style.display = 'none';
      }
    }
  }
  filterFunctionVisiHide(id, event) {
    let input, filter, a, i;
    input = document.getElementById(id);
    filter = input.children['0'].value.toUpperCase();
    const div = document.getElementById(id);
    a = div.getElementsByTagName('a');
    for (i = 0; i < a.length; i++) {
      const txtValue = a[i].textContent || a[i].innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = '';
      } else {
        a[i].style.display = 'none';
      }
    }
  }
  filterFunctionActionAutoFilled(id, event, index) {
    let input, filter, a, i;
    input = document.getElementById(id);
    filter = input.children['0'].value.toUpperCase();
    const div = document.getElementById(id);
    a = div.getElementsByTagName('a');
    for (i = 0; i < a.length; i++) {
      const txtValue = a[i].textContent || a[i].innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = '';
      } else {
        a[i].style.display = 'none';
      }
    }
  }
  selectedOptions(options, index) {
    const pushValue = this.addFormProps.controls.condition.value;
    for (let i = 0; i < pushValue.length; i++) {
      if (i === index) {
        for (let a = 0; a < pushValue[i].hide.length; a++) {
          if (pushValue[i].hide.length > 0 && options === pushValue[i].hide[a]) {
            pushValue[i].hide.splice(a, 1);
          }
        }
        let flag = true;
        for (let t = 0; t < pushValue[i].show.length; t++) {
          if (options !== pushValue[i].show[t]) {
            flag = true;
          } else {
            flag = false;
            break;
          }
        }
        if (flag) {
          pushValue[i].show.push(options);
        }
      }
    }
  }
  selectedOptionsCalced(options, index) {
    const pushValue = this.addFormProps.value.calc;
    let calcFlag = 0;
    if (pushValue.length > 0) {
      for (let a = 0; a < pushValue.length; a++) {
        if (pushValue[a] === options) {
          calcFlag = 0;
          break;
        } else {
          calcFlag = 1;
        }
      }
      if (calcFlag === 1 && (this.selectedPropType === 'ROWSUB' || this.selectedPropType === 'ROWDIV') && this.addFormProps.value.calc.length < 2) {
        this.addFormProps.value.calc.push(options);
      } else if (calcFlag === 1 && this.selectedPropType !== 'ROWSUB' && this.selectedPropType !== 'ROWDIV') {
        this.addFormProps.value.calc.push(options);
      } else {
        this.tr.warning('', 'Only two properties you can select');
      }
    } else {
      this.addFormProps.value.calc.push(options);
    }
  }
  selectedOptionsCalcedExp(options, index) {
    const pushValue = this.addFormProps.value.calc;
    let calcFlag = 0;
    if (pushValue.length > 0) {
      for (let a = 0; a < pushValue.length; a++) {
        if (pushValue[a].name === options.name) {
          calcFlag = 0;
          break;
        } else {
          calcFlag = 1;
        }
      }
      if (calcFlag === 1 && this.selectedPropType === 'ROWEXPIRYDATE') {
        let flag = 0;
        for (let a = 0; a < this.dateExpiryProps.length; a++) {
          for (let i = 0; i < this.addFormProps.value.calc.length; i++) {
            if (this.addFormProps.value.calc[i] === this.dateExpiryProps[a].name) {
              if (this.addFormProps.value.calc.length === 1) {
                if (this.dateExpiryProps[a].type !== options.type) {
                  this.addFormProps.value.calc.push(options.name);
                  flag = 1;
                  break;
                } else {
                  this.addFormProps.value.calc.splice(i, 1);
                  this.addFormProps.value.calc.push(options.name);
                  flag = 1;
                  break;
                }

              } else {
                if (this.dateExpiryProps[a].type === options.type) {
                  this.addFormProps.value.calc.splice(i, 1);
                  this.addFormProps.value.calc.push(options.name);
                  flag = 1;
                  break;
                }
              }
            }
          }
          if (flag === 1) {
            break;
          }
        }

      }
    } else {
      this.addFormProps.value.calc.push(options.name);
    }
  }
  dateDiffCalced(options, index) {
    const pushValue = this.addFormProps.value.calc;
    let calcFlag = 0;
    if (pushValue.length > 0) {
      for (let a = 0; a < pushValue.length; a++) {
        if (pushValue[a] === options) {
          calcFlag = 0;
          break;
        } else {
          calcFlag = 1;
        }
      }
      if (calcFlag === 1 && this.addFormProps.value.calc.length < 2) {
        this.addFormProps.value.calc.push(options);
      } else {
        this.tr.warning('', 'Only two properties you can select');
      }
    } else {
      this.addFormProps.value.calc.push(options);
    }
  }
  selectedOptionsAutoFilled(options, index) {
    const pushValue = this.addFormProps.value.fillprops;
    let AfFlag = 0;
    if (pushValue.length > 0) {
      for (let a = 0; a < pushValue.length; a++) {
        if (pushValue[a] === options) {
          AfFlag = 0;
          break;
        } else {
          AfFlag = 1;
        }
      }
      if (AfFlag === 1 && this.addFormProps.value.fillprops.length < 4) {
        this.addFormProps.value.fillprops.push(options);
      } else if (AfFlag === 1 && this.addFormProps.value.fillprops.length === 4) {
        this.tr.warning('', 'Only 4 parameters you can have');
      }
    } else {
      this.addFormProps.value.fillprops.push(options);
    }
  }
  selectedOptionsActionAutoFilled(options, index, r) {
    const pushValue = this.addFormProps.controls.actions.value[r].autofill.fillprops;
    let AfFlag = 0;
    if (pushValue.length > 0) {
      for (let a = 0; a < pushValue.length; a++) {
        if (pushValue[a] === options) {
          AfFlag = 0;
          break;
        } else {
          AfFlag = 1;
        }
      }
      if (AfFlag === 1 && this.addFormProps.controls.actions.value[r].autofill.fillprops.length < 4) {
        this.addFormProps.controls.actions.value[r].autofill.fillprops.push(options);
      } else if (AfFlag === 1 && this.addFormProps.controls.actions.value[r].autofill.fillprops.length === 4) {
        this.tr.warning('', 'Only 4 parameters you can have');
      }
    } else {
      this.addFormProps.controls.actions.value[r].autofill.fillprops.push(options);
    }
  }
  selectedOptionsVisiHide(options, index) {
    const hideValue = this.addFormProps.controls.setVisible.value.hide;
    const showValue = this.addFormProps.controls.setVisible.value.show;
    const ronlyValue = this.addFormProps.controls.setVisible.value.ronly;
    let hideFlag = 0;
    for (let s = 0; s < showValue.length; s++) {
      if (showValue[s] === options) {
        this.addFormProps.controls.setVisible.value.show.splice(s, 1);
        break;
      } else {
      }
    }
    for (let r = 0; r < ronlyValue.length; r++) {
      if (ronlyValue[r] === options) {
        this.addFormProps.controls.setVisible.value.ronly.splice(r, 1);
        break;
      } else {
      }
    }
    if (hideValue.length > 0) {
      for (let h = 0; h < hideValue.length; h++) {
        if (hideValue[h] === options) {
          hideFlag = 1;
          break;
        } else {
          hideFlag = 0;
        }
      }
      if (hideFlag === 0) {
        this.addFormProps.controls.setVisible.value.hide.push(options);
      }
    } else {
      this.addFormProps.controls.setVisible.value.hide.push(options);
    }
  }
  selectedOptionsVisiShow(options, index) {
    const hideValue = this.addFormProps.controls.setVisible.value.hide;
    const showValue = this.addFormProps.controls.setVisible.value.show;
    let showFlag = 0;
    for (let h = 0; h < hideValue.length; h++) {
      if (hideValue[h] === options) {
        this.addFormProps.controls.setVisible.value.hide.splice(h, 1);
        break;
      } else {
      }

    }
    if (showValue.length > 0) {
      for (let s = 0; s < showValue.length; s++) {
        if (showValue[s] === options) {
          showFlag = 1;
          break;
        } else {
          showFlag = 0;
        }
      }
      if (showFlag === 0) {
        this.addFormProps.controls.setVisible.value.show.push(options);
      }
    } else {
      this.addFormProps.controls.setVisible.value.show.push(options);
    }
  }
  selectedOptionsVisiRonly(options, index) {
    const hideValue = this.addFormProps.controls.setVisible.value.hide;
    const ronlyValue = this.addFormProps.controls.setVisible.value.ronly;
    let ronlyFlag = 0;
    for (let h = 0; h < hideValue.length; h++) {
      if (hideValue[h] === options) {
        this.addFormProps.controls.setVisible.value.hide.splice(h, 1);
        break;
      } else {
      }
    }
    if (ronlyValue.length > 0) {
      for (let r = 0; r < ronlyValue.length; r++) {
        if (ronlyValue[r] === options) {
          ronlyFlag = 1;
          break;
        } else {
          ronlyFlag = 0;
        }
      }
      if (ronlyFlag === 0) {
        this.addFormProps.controls.setVisible.value.ronly.push(options);
      }
    } else {
      this.addFormProps.controls.setVisible.value.ronly.push(options);
    }
  }
  selectedOptionsVisiSecHide(options, index) {
    const hideSecValue = this.addFormProps.controls.setVisible.value.hidesec;
    const ronlySecValue = this.addFormProps.controls.setVisible.value.ronlysec;
    let hideSecFlag = 0;
    for (let r = 0; r < ronlySecValue.length; r++) {
      if (ronlySecValue[r] === options) {
        this.addFormProps.controls.setVisible.value.ronlysec.splice(r, 1);
        break;
      } else {
      }
    }
    if (hideSecValue.length > 0) {
      for (let h = 0; h < hideSecValue.length; h++) {
        if (hideSecValue[h] === options) {
          hideSecFlag = 1;
          break;
        } else {
          hideSecFlag = 0;
        }
      }
      if (hideSecFlag === 0) {
        this.addFormProps.controls.setVisible.value.hidesec.push(options);
      }
    } else {
      this.addFormProps.controls.setVisible.value.hidesec.push(options);
    }
  }
  selectedOptionsVisiRonlySec(options, index) {
    const hideSecValue = this.addFormProps.controls.setVisible.value.hidesec;
    const ronlySecValue = this.addFormProps.controls.setVisible.value.ronlysec;
    let ronlySecFlag = 0;
    for (let h = 0; h < hideSecValue.length; h++) {
      if (hideSecValue[h] === options) {
        this.addFormProps.controls.setVisible.value.hidesec.splice(h, 1);
        break;
      } else {
      }
    }
    if (ronlySecValue.length > 0) {
      for (let r = 0; r < ronlySecValue.length; r++) {
        if (ronlySecValue[r] === options) {
          ronlySecFlag = 1;
          break;
        } else {
          ronlySecFlag = 0;
        }
      }
      if (ronlySecFlag === 0) {
        this.addFormProps.controls.setVisible.value.ronlysec.push(options);
      }
    } else {
      this.addFormProps.controls.setVisible.value.ronlysec.push(options);
    }
  }
  removeShowValue(value, index) {
    const pushValue = this.addFormProps.controls.condition.value;
    for (let i = 0; i < pushValue.length; i++) {
      if (i === index) {
        for (let a = 0; a < pushValue[i].show.length; a++) {
          if (value === pushValue[i].show[a]) {
            pushValue[i].show.splice(a, 1);
          }
        }
      }
    }
  }
  removeCalcValue(value) {
    const pushValue = this.addFormProps.value.calc;
    for (let d = 0; d < pushValue.length; d++) {
      if (pushValue[d] === value) {
        this.addFormProps.value.calc.splice(d, 1);
      }
    }
  }
  removeAutoFilledValue(value) {
    const autoFilled = this.addFormProps.value.fillprops;
    for (let af = 0; af < autoFilled.length; af++) {
      if (autoFilled[af] === value) {
        this.addFormProps.value.fillprops.splice(af, 1);
      }
    }
  }
  removeActionAutoFilledValue(value, r) {
    const autoFilled = this.addFormProps.controls.actions.value[r].autofill.fillprops;
    for (let af = 0; af < autoFilled.length; af++) {
      if (autoFilled[af] === value) {
        this.addFormProps.controls.actions.value[r].autofill.fillprops.splice(af, 1);
      }
    }
  }
  removeSetvisibleHide(value) {
    const val = this.addFormProps.controls.setVisible.value.hide;
    for (let a = 0; a < val.length; a++) {
      if (val[a] === value) {
        this.addFormProps.controls.setVisible.value.hide.splice(a, 1);
        break;
      }
    }
  }
  removeSetvisibleRonly(value) {
    const val = this.addFormProps.controls.setVisible.value.ronly;
    for (let a = 0; a < val.length; a++) {
      if (val[a] === value) {
        this.addFormProps.controls.setVisible.value.ronly.splice(a, 1);
        break;
      }
    }
  }
  removeSetvisibleHideSec(value) {
    const val = this.addFormProps.controls.setVisible.value.hidesec;
    for (let a = 0; a < val.length; a++) {
      if (val[a] === value) {
        this.addFormProps.controls.setVisible.value.hidesec.splice(a, 1);
        break;
      }
    }
  }
  removeSetvisibleRonlySec(value) {
    const val = this.addFormProps.controls.setVisible.value.ronlysec;
    for (let a = 0; a < val.length; a++) {
      if (val[a] === value) {
        this.addFormProps.controls.setVisible.value.ronlysec.splice(a, 1);
        break;
      }
    }
  }
  removeSetvisibleShow(value) {
    const val = this.addFormProps.controls.setVisible.value.show;
    for (let a = 0; a < val.length; a++) {
      if (val[a] === value) {
        this.addFormProps.controls.setVisible.value.show.splice(a, 1);
        break;
      }
    }
  }
  removeHideValue(value, index) {
    const pushValue = this.addFormProps.controls.condition.value;
    for (let i = 0; i < pushValue.length; i++) {
      if (i === index) {
        for (let a = 0; a < pushValue[i].hide.length; a++) {
          if (value === pushValue[i].hide[a]) {
            pushValue[i].hide.splice(a, 1);
          }
        }
      }
    }
  }
  selectedOptionsHide(options, index) {
    const pushValue = this.addFormProps.controls.condition.value;
    for (let i = 0; i < pushValue.length; i++) {
      if (i === index) {
        for (let a = 0; a < pushValue[i].show.length; a++) {
          if (pushValue[i].show.length > 0 && options === pushValue[i].show[a]) {
            pushValue[i].show.splice(a, 1);
          }
        }
        let flag = true;
        for (let t = 0; t < pushValue[i].hide.length; t++) {
          if (options !== pushValue[i].hide[t]) {
            flag = true;
          } else {
            flag = false;
            break;
          }
        }
        if (flag) {
          pushValue[i].hide.push(options);
        }
      }
    }
  }
  download() {
    this
      .ds
      .downloadDocument(this.docId)
      .subscribe(data => this.downloadCurrentDocument(data));

  }
  update(event, checkinWorktype) {
    this.docId = event.data.docId;
    this.modalService.getModal('checkinWorktype').open();
  }
  checkedOut(data, checkinWorktype) {
    this.spinner.show();
    this.ds.checkOut(this.docId).subscribe(dataRes => { this.updateDocument(dataRes); }, error => { this.spinner.hide(); });
  }

  updateDocument(data) {
    if (this.checkIn.queue.length > 0) {
      const docInfo = {
        docclass: '',
        docTitle: this.checkIn.queue[0].file.name,
        id: this.docId,
        props: [{
          'name': 'Document Title',
          'symName': 'DocumentTitle',
          'dtype': 'STRING',
          'mvalues': [this.checkIn.queue[0].file.name],
          'mtype': 'N',
          'len': 255,
          'rOnly': 'false',
          'hidden': 'false',
          'req': 'false'
        }],
      };
      const formData = new FormData();
      formData.append('DocInfo', JSON.stringify(docInfo));
      formData.append('file', this.checkIn.queue[0]._file);
      this.checkIn.clearQueue();
      this.spinner.show();
      this.ds.checkIn(formData).subscribe(dataRes => { this.formUpdated(dataRes); this.spinner.hide(); }, error => {
        this.tr.error('Incorrect form selected');
        this.checkIn.clearQueue(); this.spinner.hide(); this.ds.cancelCheckOut(this.docId).subscribe();
      });
    } else {
      this.tr.error('Please select form to update');
    }
  }

  formUpdated(data) {
    this.tr.success('', 'form updated');
    this.ngOnInit();
  }

  downloadCurrentDocument(data) {
    let filename = '';
    const disposition = data
      .headers
      .get('Content-Disposition');
    const filenameRegex = /filename[^;=\n]*=((['']).*?\2|[^;\n]*)/;
    const matches = filenameRegex.exec(disposition);
    if (matches != null && matches[1]) {
      filename = matches[1].replace(/['']/g, '');
    }
    saveAs(data._body, filename);
  }
  fileChanged(event) {

  }
  submitImportDoc() {
    if (this.defid.queue.length > 0) {
      const docInfo = {
        docclass: '',
        docTitle: this.defid.queue[0].file.name,
        props: [{
          'name': 'Document Title',
          'symName': 'DocumentTitle',
          'dtype': 'STRING',
          'mvalues': [this.defid.queue[0].file.name],
          'mtype': 'N',
          'len': 255,
          'rOnly': 'false',
          'hidden': 'false',
          'req': 'false'
        }],
      };
      const fileReader = new FileReader();
      const formData = new FormData();
      formData.append('DocInfo', JSON.stringify(docInfo));
      formData.append('file', this.defid.queue[0]._file);
      this.defid.clearQueue();
      this.spinner.show();
      this.ss.importForm(formData).subscribe(data => { this.imported(data); this.spinner.hide(); }, error => {
        this.defid.clearQueue(); this.spinner.hide();
      });
    } else {
      this.tr.error('Please select form to import');
    }
  }
  imported(data) {
    this.tr.success('Import Success');
    this.ngOnInit();
  }
  dbLookupDropdownCondition(event, ind, i, j, k, prop) {
    if (prop.condition !== undefined) {
      for (let index = 0; index < prop.condition.length; index++) {
        if (prop.condition[index].value === event.value && event.value !== undefined) {
          if (prop.condition[index].show !== undefined) {
            for (let show = 0; show < prop.condition[index].show.length; show++) {
              this.showPropsOnScreen(prop.condition[index].show[show], ind, i, j, k);
            }
          }
          if (prop.condition[index].hide !== undefined) {
            for (let hide = 0; hide < prop.condition[index].hide.length; hide++) {
              this.hidePropsOnScreen(prop.condition[index].hide[hide]);
            }
          }

        }
      }
    }
  }

  showPropsOnScreen(showProp, ind, a, b, c) {
    for (let index = 0; index < this.arrayToShow.length; index++) {
      for (let i = 0; i < this.arrayToShow[index].sections.length; i++) {
        if (this.arrayToShow[index].sections[i].type === 'FORM') {
          for (let j = 0; j < this.arrayToShow[index].sections[i].columns.length; j++) {
            for (let k = 0; k < this.arrayToShow[index].sections[i].columns[j].properties.length; k++) {
              if (this.arrayToShow[index].sections[i].columns[j].properties[k].name === showProp) {
                this.arrayToShow[index].sections[i].columns[j].properties[k].visible = 'TRUE';
                this.arrayToShow[index].sections[i].columns[j].properties[k].visible = 'TRUE';
                this.arrayToShow[index].sections[i].columns[j].properties[k].value = [];
              }
            }
          }
        }
      }
    }
  }

  hidePropsOnScreen(showProp) {
    for (let index = 0; index < this.arrayToShow.length; index++) {
      for (let i = 0; i < this.arrayToShow[index].sections.length; i++) {
        if (this.arrayToShow[index].sections[i].type === 'FORM') {
          for (let j = 0; j < this.arrayToShow[index].sections[i].columns.length; j++) {
            for (let k = 0; k < this.arrayToShow[index].sections[i].columns[j].properties.length; k++) {
              if (this.arrayToShow[index].sections[i].columns[j].properties[k].name === showProp) {
                this.arrayToShow[index].sections[i].columns[j].properties[k].visible = 'FALSE';
                this.arrayToShow[index].sections[i].columns[j].properties[k].visible = 'FALSE';
                this.arrayToShow[index].sections[i].columns[j].properties[k].value = [];
              }
            }
          }
        }
      }
    }
  }
  focusElement(event) {
    window.setTimeout(function () {
      document.getElementById(event.target.id).focus();
    }, 0);
  }
  focusOutFunction(event, prop) {
    if (event.value.length > 0) {
      const autoFillComplete = prop.autofill;
      autoFillComplete.fillparams = [];
      autoFillComplete.fillparams.push(event.value);
      this.integrationservice.getDBAutoFill(autoFillComplete).subscribe(data => this.getDBAutoFill(data, prop), error => this.resetDBAutoFill(prop));
    }

  }
  removeExistingTableRow(i, row, pos) {
    this.arrayToShow[0].sections[i].rows.splice(pos, 1);
  }
  getDBAutoFill(data, prop) {
    const keyValuePair = JSON.parse(data._body);
    if (keyValuePair.length > 0) {
      for (let keyindex = 0; keyindex < keyValuePair.length; keyindex++) {
        for (let index = 0; index < this.arrayToShow.length; index++) {
          for (let i = 0; i < this.arrayToShow[index].sections.length; i++) {
            if (this.arrayToShow[index].sections[i].type === 'FORM') {
              for (let j = 0; j < this.arrayToShow[index].sections[i].columns.length; j++) {
                for (let k = 0; k < this.arrayToShow[index].sections[i].columns[j].properties.length; k++) {
                  if (this.converToUppercase(this.arrayToShow[index].sections[i].columns[j].properties[k].name) === this.converToUppercase(keyValuePair[keyindex].key)) {
                    this.arrayToShow[index].sections[i].columns[j].properties[k].value = [];
                    this.arrayToShow[index].sections[i].columns[j].properties[k].value.push({ 'value': keyValuePair[keyindex].value });
                  }
                }
              }
            }
          }
        }
      }
    } else {
      this.resetDBAutoFill(prop);
    }
  }
  resetDBAutoFill(props) {
    for (let keyindex = 0; keyindex < props.autofill.fillprops.length; keyindex++) {
      for (let index = 0; index < this.arrayToShow.length; index++) {
        for (let i = 0; i < this.arrayToShow[index].sections.length; i++) {
          if (this.arrayToShow[index].sections[i].type === 'FORM') {
            for (let j = 0; j < this.arrayToShow[index].sections[i].columns.length; j++) {
              for (let k = 0; k < this.arrayToShow[index].sections[i].columns[j].properties.length; k++) {
                if (this.arrayToShow[index].sections[i].columns[j].properties[k].name === props.autofill.fillprops[keyindex]) {
                  this.arrayToShow[index].sections[i].columns[j].properties[k].value = [];
                }
              }
            }
          }
        }
      }
    }
  }
  converToUppercase(string) {
    if (string) {
      return string.toUpperCase();
    }
  }
  onColCalcChanged(event) {
    if (event.value === 'SUM') {
      this.addFormProps.controls.colcalc.patchValue({ type: 'SUM', label: 'Total', copyto: this.addFormProps.value.copytoProp });
    } else if (event.value === 'AVERAGE') {
      this.addFormProps.controls.colcalc.patchValue({ type: 'AVERAGE', label: 'Average', copyto: this.addFormProps.value.copytoProp });
    } else {
      this.addFormProps.controls.colcalc.patchValue({ type: 'NONE', label: 'None', copyto: this.addFormProps.value.copytoProp });
    }
  }
  onColCalcCopytoChanged(event) {
    if (this.addFormProps.value.colcalcType === 'SUM') {
      this.addFormProps.controls.colcalc.patchValue({ type: 'SUM', label: 'Total', copyto: event.value });
    } else if (this.addFormProps.value.colcalcType === 'AVERAGE') {
      this.addFormProps.controls.colcalc.patchValue({ type: 'AVERAGE', label: 'Average', copyto: event.value });
    } else {
      this.addFormProps.controls.colcalc.patchValue({ type: 'NONE', label: 'None', copyto: event.value });
    }
  }
  deafultDbChanged(event) {
    this.addFormProps.controls.isDefaultDB.patchValue(event);
    if (event === true) {
      this.addFormProps.patchValue({ propdburl: 'DEFAULT', propdbtype: 'SQLSERVER', propuser: 'DEFAULT', proppassword: 'DEFAULT', isDefaultDB: true });
    }
  }
  lookupEntered(value) {
    this.proplookupArray.push(value);
    this.addFormProps.patchValue({ proplookup: this.proplookupArray.toString(), proplookupTemp: '' });
  }
  removeLookupValue(value, i) {
    this.proplookupArray.splice(i, 1);
    this.addFormProps.patchValue({ proplookup: this.proplookupArray.toString() });
  }
}

