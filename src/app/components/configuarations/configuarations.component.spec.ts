import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfiguarationsComponent } from './configuarations.component';

describe('ConfiguarationsComponent', () => {
  let component: ConfiguarationsComponent;
  let fixture: ComponentFixture<ConfiguarationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfiguarationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfiguarationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
