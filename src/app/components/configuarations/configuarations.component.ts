import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { ConfigurationService } from '../../service/Configuration.service';
import { ContentService } from '../../service/content.service';
import { ToastrService } from 'ngx-toastr';
import { SchemaService } from '../../service/schema.service';
import { AdministrationService } from '../../service/Administration.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { FileUploader } from 'ng2-file-upload';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-configuarations',
  templateUrl: './configuarations.component.html',
  styleUrls: ['./configuarations.component.css']
})
export class ConfiguarationsComponent implements OnInit {
  public configurationForm: FormGroup;
  public listOfConfig;
  public closeResult;
  public imageRender;
  public editConfig = false;
  public updateIndex = null;
  constructor(private configurationService: ConfigurationService, private contentService: ContentService, private tr: ToastrService,
    private as: AdministrationService, private smarModel: NgxSmartModalService, private schemaService: SchemaService,
    private fb: FormBuilder, private spinner: Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.configurationForm = this.fb.group({});
    this.spinner.show();
    this.updateIndex = null;
    this.configurationService.getAllConfigurations().subscribe(data => { this.getAllConfigurations(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
  getAllConfigurations(data) {
    this.listOfConfig = JSON.parse(data._body);
    if (data._body !== '') {
      this.dynamicConfigurationForm();
    }
  }
  viewDocument(docId) {
    this.imageRender = '';
    this.spinner.show();
    if (docId !== undefined) {
      this.contentService.getPreviewPage(docId, 1).subscribe(data => { this.getPreviewPage(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
    } else {
      this.spinner.hide();

    }
  }

  getPreviewPage(data) {
    const imageDetails = JSON.parse(data._body);
    this.imageRender = 'data:image/png;base64,' + imageDetails.image;
    this.smarModel.getModal('imageViewModel').open();
  }

  downloadDocument(docId) {
    this.spinner.show();
    if (docId !== undefined) {
      this.contentService.downloadDocument(docId).subscribe(data => { this.downloadCurrentDocument(data); this.spinner.hide(); }, error => { this.spinner.hide(); });

    } else {
      this.spinner.hide();
    }
  }

  downloadCurrentDocument(data) {
    let filename = '';
    const disposition = data.headers.get('Content-Disposition');
    const filenameRegex = /filename[^;=\n]*=((['']).*?\2|[^;\n]*)/;
    const matches = filenameRegex.exec(disposition);
    if (matches != null && matches[1]) {
      filename = matches[1].replace(/['']/g, '');
    }
    saveAs(data._body, filename);
  }

  editConfigForm() {
    this.configurationForm.enable();
    this.editConfig = true;
  }

  fileChanged(event, uiType, index) {
    const docInfo = {
      docclass: '',
      docTitle: event.queue['0']._file.name,
      props: [{
        'name': 'Document Title',
        'symName': 'DocumentTitle',
        'dtype': 'STRING',
        'mvalues': [event.queue['0']._file.name],
        'mtype': 'N',
        'len': 255,
        'rOnly': 'false',
        'hidden': 'false',
        'req': 'false'
      }],
    };
    const fileReader = new FileReader();
    const formData = new FormData();
    formData.append('DocInfo', JSON.stringify(docInfo));
    formData.append('file', event.queue['0']._file);
    this.spinner.show();
    if (uiType === 'IMAGE') {
      this.schemaService.addImage(formData).subscribe(data => this.documentAdded(data, event, index), error => { event.clearQueue(); this.spinner.hide(); });
    } else if (uiType === 'TEMPLATE') {
      this.schemaService.addTemplate(formData).subscribe(data => this.documentAdded(data, event, index), error => { event.clearQueue(); this.spinner.hide(); });
    } else {
      this.schemaService.addSignature(formData).subscribe(data => this.documentAdded(data, event, index), error => { event.clearQueue(); this.spinner.hide(); });
    }
  }

  documentAdded(data, filename, ind) {
    this.spinner.hide();
    if (data._body !== this.listOfConfig[ind].value) {
      if (typeof this.updateIndex !== 'number') {
        this.updateIndex = ind;
        this.listOfConfig[ind].imageNameUpdated = filename.queue['0']._file.name;
        this.configurationForm.controls[this.listOfConfig[ind].name].patchValue(data._body);
        for (let index = 0; index < this.listOfConfig.length; index++) {
          if (index !== ind) {
            this.configurationForm.get(this.listOfConfig[index].name).disable();
          }
        }
      }
    } else {
      this.updateIndex = null;
      for (let index = 0; index < this.listOfConfig.length; index++) {
        this.configurationForm.get(this.listOfConfig[index].name).enable();
      }
    }
  }

  dynamicConfigurationForm() {
    for (let index = 0; index < this.listOfConfig.length; index++) {
      const control: FormControl = new FormControl(this.listOfConfig[index].value);
      this.configurationForm.addControl(this.listOfConfig[index].name, control);
      if (this.listOfConfig[index].uiType === 'LOOKUP') {
        let uiDetailsList = [];
        this.listOfConfig[index].lookUpValue = [];
        this.listOfConfig[index].tempValue = '';
        this.listOfConfig[index].tempValue = this.listOfConfig[index].value;
        uiDetailsList = this.listOfConfig[index].uiDetails.split(';');
        uiDetailsList.forEach(element => {
          const value = {
            label: element,
            value: element
          };
          this.listOfConfig[index].lookUpValue.push(value);
        });
        const control1: FormControl = new FormControl(this.listOfConfig[index].value);
        this.configurationForm.addControl(this.listOfConfig[index].name, control1);
      } else if (this.listOfConfig[index].uiType === 'IMAGE' || this.listOfConfig[index].uiType === 'TEMPLATE' || this.listOfConfig[index].uiType === 'SIGNATURE') {
        this.listOfConfig[index].fileObject = new FileUploader({});
        this.listOfConfig[index].imageName = '';
        this.listOfConfig[index].imageNameUpdated = '';
        if (this.listOfConfig[index].value !== undefined && this.listOfConfig[index].value !== '') {
          this.contentService.getThisDocument(this.listOfConfig[index].value).subscribe(data => this.getThisDocument(data, index));
        }
      } else {

      }
    }
  }

  textChanged(event, i) {
    if (event.value !== this.listOfConfig[i].value) {
      if (typeof this.updateIndex !== 'number') {
        this.updateIndex = i;
        for (let index = 0; index < this.listOfConfig.length; index++) {
          if (index !== i) {
            this.configurationForm.get(this.listOfConfig[index].name).disable();
          }
        }
      }
    } else {
      this.updateIndex = null;
      for (let index = 0; index < this.listOfConfig.length; index++) {
        this.configurationForm.get(this.listOfConfig[index].name).enable();
      }
    }
  }

  textChangedLookup(event, i) {
    if (event.value !== this.listOfConfig[i].tempValue) {
      if (typeof this.updateIndex !== 'number') {
        this.updateIndex = i;
        for (let index = 0; index < this.listOfConfig.length; index++) {
          if (index !== i) {
            this.configurationForm.get(this.listOfConfig[index].name).disable();
          }
        }
      }
    } else {
      this.updateIndex = null;
      for (let index = 0; index < this.listOfConfig.length; index++) {
        this.configurationForm.get(this.listOfConfig[index].name).enable();
      }
    }
  }
  dropDownChanged(event, i) {
    if (event.target.selected !== this.listOfConfig[i].value) {
      if (typeof this.updateIndex !== 'number') {
        this.updateIndex = i;
        for (let index = 0; index < this.listOfConfig.length; index++) {
          if (index !== i) {
            this.configurationForm.get(this.listOfConfig[index].name).disable();
          }
        }
      }
    } else {
      this.updateIndex = null;
      for (let index = 0; index < this.listOfConfig.length; index++) {
        this.configurationForm.get(this.listOfConfig[index].name).enable();
      }
    }
  }

  updateConfigForm(i) {
    const config = [];
    let selectedConfig;
    for (let index = 0; index < this.listOfConfig.length; index++) {
      if (i === index) {
        config.push(this.listOfConfig[index]);
        selectedConfig = this.listOfConfig[index];
        break;
      }
    }
    if (selectedConfig.uiType === 'PASSWORD' || selectedConfig.uiType === 'ENCTEXT') {
      this.as.encryptConfig(this.configurationForm.controls[this.listOfConfig[i].name].value).subscribe(data => this.encryptConfig(data, selectedConfig));
    } else {
      config[0].value = this.configurationForm.controls[this.listOfConfig[i].name].value;
      delete config[0].fileObject;
      delete config[0].imageName;
      delete config[0].lookUpValue;
      delete config[0].imageNameUpdated;
      this.configurationService.updateConfigurations(config).subscribe(data => { this.tr.success('', 'Configuration updated'); this.ngOnInit(); }, error => { this.spinner.hide(); });
    }
  }

  encryptConfig(data, selectedConfig) {
    const config = [selectedConfig];
    config[0].value = data._body;
    delete config[0].fileObject;
    delete config[0].imageName;
    delete config[0].lookUpValue;
    delete config[0].imageNameUpdated;
    this.configurationService.updateConfigurations(config).subscribe(datares => { this.tr.success('', 'Configuration updated'); this.ngOnInit(); }, error => { this.spinner.hide(); });
  }



  resetConfigForm(i) {
    for (let index = 0; index < this.listOfConfig.length; index++) {
      if (i === index) {
        if (this.listOfConfig[index].uiType === 'IMAGE' || this.listOfConfig[index].uiType === 'SIGNATURE' || this.listOfConfig[index].uiType === 'TEMPLATE') {
          this.listOfConfig[index].imageNameUpdated = '';
        }
        this.configurationForm.get(this.listOfConfig[index].name).patchValue(this.listOfConfig[index].value);
      }
    }
    this.updateIndex = null;
    for (let index = 0; index < this.listOfConfig.length; index++) {
      this.configurationForm.get(this.listOfConfig[index].name).enable();
    }
  }
  getThisDocument(data, index) {
    const documentName = JSON.parse(data._body);
    if (documentName !== '') {
      this.listOfConfig[index].imageName = documentName.docTitle;
    }
  }
  configurationFormSubmit(event) {
    for (const inputField of [].slice.call(event.target)) {
      for (const docClass of this.listOfConfig) {
        if (docClass.uiType === 'IMAGE' || docClass.uiType === 'TEMPLATE' || docClass.uiType === 'SIGNATURE') {
          docClass.value = docClass.value;
        } else {
          if (inputField.id === docClass.name) {
            docClass.value = inputField.value;
          }
        }
        delete docClass.fileObject;
        delete docClass.imageName;
        delete docClass.lookUpValue;
      }
    }
    this.spinner.show();
    this.configurationService.updateConfigurations(this.listOfConfig).subscribe(data => this.updateConfigurations(data), error => { this.spinner.hide(); });
  }

  updateConfigurations(data) {
    this.configurationService.getAllConfigurations().subscribe(datares => { this.getAllConfigurations(datares); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
}
