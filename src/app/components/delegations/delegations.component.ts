import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { UserService } from '../../service/user.service';
import { FormBuilder, FormGroup, Validators, FormGroupDirective } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-delegations',
  templateUrl: './delegations.component.html',
  styleUrls: ['./delegations.component.css']
})
export class DelegationsComponent implements OnInit {
  @ViewChild(FormGroupDirective) formDirective: FormGroupDirective;
  public source = [];
  public usersList = [];
  public isAdd = false;
  public isView = true;
  public delegationForm: FormGroup;
  public fromMinDate: any;
  public toMinDate: any;
  public date: any;
  public isDiableFromDelegate = false;

  constructor(private us: UserService,
    private fb: FormBuilder, private tr: ToastrService, private cdr: ChangeDetectorRef, private spinner: Ng4LoadingSpinnerService,
    private ngxSmartModalService: NgxSmartModalService) {

  }

  ngOnInit() {
    this.delegationForm = this.fb.group({
      id: [null],
      delegatedBy: [null, Validators.compose([Validators.required])],
      delegateId: [null, Validators.compose([Validators.required])],
      fromDate: [null, Validators.compose([Validators.required])],
      toDate: [null, Validators.compose([Validators.required])],
      status: ['ACTIVE'],
      delegatedByName: [null],
      delegateIdName: [null],
      fromValue: [null],
      toValue: [null]
    });
    this.date = new Date(); // (this.date.getDate() - 1) + '/' + (this.date.getMonth() + 1) + '/' + (this.date.getFullYear());
    this.fromMinDate = this.date;
    this.toMinDate = this.date;
    this.spinner.show();
    this.us.getAllDelegations().subscribe(data => { this.getAllDelegations(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
    this.us.getUsers().subscribe(data => { this.getUsers(data); }, error => { });
    this.isDiableFromDelegate = false;
  }

  getAllDelegations(data) {
    if (data._body !== '') {
      this.source = JSON.parse(data._body);
    }
  }
  getUsers(data) {
    const value = JSON.parse(data._body);
    this.usersList = [];
    value.forEach(element => {
      const drop = {
        label: element.fulName,
        value: element.EmpNo
      };
      this.usersList.push(drop);
    });
  }
  clickedOnAdd() {
    this.isAdd = true;
    this.isView = false;
    this.delegationForm.reset();
    this.formDirective.resetForm();
    this.isDiableFromDelegate = false;
  }
  clickedOnView() {
    this.isView = true;
    this.isAdd = false;
  }
  editDelegations(event) {
    this.isAdd = false;
    this.isView = false;
    this.delegationForm.reset();
    this.patchDate(event.data.fromDate, 'fromDate');
    this.patchDate(event.data.toDate, 'toDate');

    const fromValue = {
      label: event.data.delegatedByName,
      value: event.data.delegatedBy
    };
    const toValue = {
      label: event.data.delName,
      value: event.data.delegateId
    };
    this.delegationForm.patchValue({ 'id': event.data.id, 'delegatedBy': event.data.delegatedBy, 'delegatedByName': event.data.delegatedByName, 'delegateId': event.data.delegateId, 'delegatedIdName': event.data.delName, 'fromValue': fromValue, 'toValue': toValue, 'status': event.data.status });
    this.isDiableFromDelegate = true;
  }
  patchDate(thisvalue, name) {
    if (thisvalue) {
      const dateFormat = thisvalue.split('/')[1] + '/' + thisvalue.split('/')[0] + '/' + thisvalue.split('/')[2];
      const date = new Date(dateFormat);
      let date1;
      let date2;
      if ((date.getDate()).toString().length < 2) {
        date1 = '0' + (date.getDate());
      } else {
        date1 = (date.getDate());
      }
      if ((date.getMonth() + 1).toString().length < 2) {
        date2 = '0' + (date.getMonth() + 1);
      } else {
        date2 = (date.getMonth() + 1);
      }
      const dateFormated = date1 + '/' + date2 + '/' + date.getFullYear();
      this.delegationForm.controls[name].patchValue(dateFormated);
    }
  }
  delagationFormSubmitted(event) {
    if (this.delegationForm.controls.delegatedBy.value !== this.delegationForm.controls.delegateId.value) {
      this.spinner.show();
      const del = {
        id: this.delegationForm.controls.id.value,
        empNo: this.delegationForm.controls.delegatedBy.value,
        delegateId: this.delegationForm.controls.delegateId.value,
        fromDate: this.delegationForm.controls.fromDate.value,
        toDate: this.delegationForm.controls.toDate.value,
        delegatedBy: this.delegationForm.controls.delegatedBy.value,
        status: this.delegationForm.controls.status.value,
      };
      this.us.saveDelegation(del).subscribe(data => {
        this.saveDeligationUser(data);
        this.tr.success('', 'Saved Successfully');
        this.spinner.hide();
      }, error => {
        this.spinner.hide();
      });
    } else {
      this.tr.warning('', 'Cannot Have Same From and To Delegation');
    }
  }
  saveDeligationUser(data) {
    this.isView = true;
    this.isAdd = false;
    this.delegationForm.reset();
    this.formDirective.resetForm();
    this.delegationForm.patchValue({
      status: 'ACTIVE'
    });
    this.us.getAllDelegations().subscribe(dataResult => this.getAllDelegations(dataResult));
    this.isDiableFromDelegate = false;
  }
  delegateFromSelected(event) {
    this.delegationForm.patchValue({ 'delegatedBy': event.value.value, 'delegatedByName': event.value.label });
  }
  delegatedToSelected(event) {
    this.delegationForm.patchValue({ 'delegateId': event.value.value, 'delegatedIdName': event.value.label });
  }
  FromDateChanged(event) {
    this.actuallFormat(event, 'fromDate');
    this.toMinDate = event;
  }
  toDateChanged(event) {
    this.actuallFormat(event, 'toDate');
  }
  cancelAdd() {
    this.delegationForm.reset();
    this.delegationForm.enable();
    this.formDirective.resetForm();
    this.delegationForm.patchValue({
      status: 'ACTIVE'
    });
    this.isDiableFromDelegate = false;
  }
  openRevokeModal() {
    this.ngxSmartModalService.open('confirmDelegateRovoke');
  }
  revokeDelegation() {
    this.spinner.show();
    this.us.revokeDelegation(this.delegationForm.controls.id.value).subscribe(data => { this.saveDeligationUser(data); this.spinner.hide(); this.tr.success('', 'Revoked Successfully'); }, error => { this.spinner.hide(); });
  }
  actuallFormat(event, valued) {
    const d = new Date(Date.parse(event));
    let newDate;
    let date1;
    let date2;
    if (((d.getDate()).toString()).length < 2) {
      date1 = '0' + (d.getDate());
    } else {
      date1 = (d.getDate());
    }
    if (((d.getMonth() + 1).toString()).length < 2) {
      date2 = '0' + (d.getMonth() + 1);
    } else {
      date2 = (d.getMonth() + 1);
    }
    newDate = `${date1}/${date2}/${d.getFullYear()}`;
    this.delegationForm.controls[valued].patchValue(newDate);
  }
}
