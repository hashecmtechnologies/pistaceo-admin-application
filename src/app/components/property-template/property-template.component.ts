import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormGroupDirective } from '@angular/forms';
import { ContentService } from '../../service/content.service';
import { ToastrService } from 'ngx-toastr';
import { Ng4LoadingSpinnerComponent, Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-property-template',
  templateUrl: './property-template.component.html',
  styleUrls: ['./property-template.component.css']
})
export class PropertyTemplateComponent implements OnInit {
  @ViewChild(FormGroupDirective) formDirective: FormGroupDirective;
  propForm: FormGroup;
  public showlookups = false;
  public editid: any;
  public lookupArray = [];
  public selectedIndex: any;
  public propTemplateResult: any;
  public isAdd = false;
  public isView = true;
  public dataTypes = [];
  constructor(private cs: ContentService, private fb: FormBuilder, private tr: ToastrService, private spinner: Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.spinner.show();
    this.dataTypes = [
      { label: 'Integer', value: 'INTEGER' },
      { label: 'String', value: 'STRING' },
      { label: 'DateTime', value: 'DATETIME' },
      { label: 'Boolean', value: 'BOOLEAN' }
    ];
    this.callTemplates();
    this.propForm = this.fb.group({
      name: [null, Validators.compose([Validators.required])],
      lookups: [null],
      descriptions: [null],
      dataType: [null],
      length: [10, Validators.compose([Validators.required])],
      symName: [null, Validators.compose([Validators.required, Validators.pattern('[a-z_]*')])]
    });
    this.propForm.patchValue({ dataType: 'INTEGER' });
  }
  callTemplates() {
    this.cs.getPropertyTemplates().subscribe(data => { this.propTemplate(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
  propTemplate(data) {
    this.propTemplateResult = [];
    this.propTemplateResult = JSON.parse(data._body);
  }
  clickedOnAdd() {
    this.isAdd = true;
    this.isView = false;
    this.showlookups = false;
    this.propForm.reset();
    this.propForm.patchValue({ dataType: 'INTEGER', length: 50 });
    this.propForm.enable();
    this.lookupArray = [];
    this.selectedIndex = undefined;
  }
  clickedOnView() {
    this.isAdd = false;
    this.isView = true;
  }
  editPropsTemplate(event) {
    for (let i = 0; i < this.propTemplateResult.length; i++) {
      if (this.propTemplateResult[i].name === event.data.name) {
        this.selectedIndex = i;
        break;
      }
    }
    this.editid = event.data;
    this.isAdd = false;
    this.isView = false;
    const value = '';
    if (event.data.dtype === 'STRING') {
      this.showlookups = true;
      if (event.data.lookups !== undefined) {
        this.lookupArray = event.data.lookups;
      } else {
        this.lookupArray = [];
      }

    } else {
      this.showlookups = false;
    }
    this.propForm.reset();
    this.propForm.disable();
    this.propForm.controls.name.enable();
    this.propForm.controls.lookups.enable();
    this.propForm.controls.descriptions.enable();
    this.propForm.patchValue({ name: event.data.name, dataType: event.data.dtype, length: event.data.len, symName: event.data.symName, lookups: value, descriptions: event.data.desc });
  }
  addto(lookups) {
    const value = lookups.replace(/\s/g, '');
    const element = {
      label: value,
      value: value
    };
    this.lookupArray.push(element);
    this.propForm.controls['lookups'].reset();
  }
  propsFormSubmitted(event) {
    let isOkTOAdd = 0;
    for (const { index, element } of this.propTemplateResult.map((element, index) => ({ index, element }))) {
      if (((this.converToUppercase(element.name) === this.converToUppercase(this.propForm.value.name)) || ((this.converToUppercase(element.symName) === this.converToUppercase(this.propForm.value.symName)))) && index !== this.selectedIndex) {
        this.tr.warning('', 'Property template with this name or symbolic name already exist please change that');
        isOkTOAdd = 0;
        break;
      } else {
        isOkTOAdd = 1;
      }
    }
    if (isOkTOAdd === 1) {
      this.spinner.show();
      if (this.isAdd) {
        const value = {
          name: this.propForm.value.name,
          dtype: this.propForm.value.dataType,
          lookups: [],
          len: this.propForm.value.length,
          symName: this.propForm.value.symName
        };
        if (this.showlookups === true) {
          value.lookups = this.lookupArray;
        }
        this.cs.addPropertyTemplate(value).subscribe(data => { this.added(data); this.spinner.hide(); this.tr.success('', 'New property tempelate added'); }, error => { this.spinner.hide(); });
      } else {
        const value = {
          id: this.editid.id,
          name: this.propForm.value.name,
          desc: this.propForm.value.descriptions,
          lookups: this.lookupArray,
          len: this.propForm.value.length
        };
        this.cs.addPropertyTemplate(value).subscribe(data => { this.added(data); this.spinner.hide(); this.tr.success('', 'Edit property tempelate is success'); }, error => { this.spinner.hide(); });
      }
    }
  }
  added(data) {
    this.propForm.reset();
    this.propForm.enable();
    this.propForm.patchValue({ length: 50 });
    this.isAdd = false;
    this.isView = true;
    this.callTemplates();
  }
  cancelAdd() {
    if (!this.isAdd) {
      this.propForm.reset();
      this.propForm.patchValue({ name: this.editid.name, symName: this.editid.symName, dataType: this.editid.dtype, length: this.editid.len, descriptions: this.editid.desc });
    } else {
      this.propForm.reset();
      this.propForm.enable();
      this.formDirective.resetForm();
    }

  }
  dataTypechanged(event) {
    if (event.value === 'STRING') {
      this.showlookups = true;
    } else {
      this.showlookups = false;
    }
  }
  removeLookupValue(value) {
    for (let aks = 0; aks < this.lookupArray.length; aks++) {
      if (this.lookupArray[aks].label === value.label) {
        this.lookupArray.splice(aks, 1);
      }
    }
  }
  converToUppercase(string) {
    if (string) {
      return string.toUpperCase();
    }
  }
  addSymname() {
    if (this.isAdd) {
      let myString = '';
      myString = this.propForm.controls['name'].value.replace(/[^A-Za-z]+/g, '');
      const resString = myString.toLowerCase();
      this.propForm.patchValue({ symName: resString });
    }
  }
}
