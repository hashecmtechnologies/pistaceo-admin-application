import { Component, OnInit } from '@angular/core';
import { AdministrationService } from '../../service/Administration.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { ToastrService } from 'ngx-toastr';
import { ConfigurationService } from '../../service/Configuration.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-license',
  templateUrl: './license.component.html',
  styleUrls: ['./license.component.css']
})
export class LicenseComponent implements OnInit {
  public licenseDetails: any;
  public licenseKey: any;
  public licesnseInfo: any;
  constructor(private as: AdministrationService, private smartModal: NgxSmartModalService, private tr: ToastrService,
    private configurationService: ConfigurationService, private spinner: Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.spinner.show();
    this.as.getLicenseInfo().subscribe(data => { this.spinner.hide(); this.getLicenseInfo(data); }, error => { this.spinner.hide(); });
    this.configurationService.getConfiguration('LICENSE').subscribe(data => this.getConfigLicense(data), error => { this.spinner.hide(); });
  }

  getConfigLicense(data) {
    this.licesnseInfo = JSON.parse(data._body);
  }

  getLicenseInfo(data) {
    this.licenseDetails = JSON.parse(data._body);
  }
  updateAsk() {
    this.smartModal.getModal('updateAlert').open();
  }
  updateConfirmed() {
    this.licesnseInfo.value = this.licenseKey;
    const info = [
      this.licesnseInfo
    ];
    this.configurationService.updateConfigurations(info).subscribe(data => this.updateConfigurations(data), error => { this.tr.error('', 'Couldn\'t update license key'); });
  }

  updateConfigurations(data) {
    this.licenseKey = undefined;
    this.as.getLicenseInfo().subscribe(datares => this.getLicenseInfo(data));
    this.configurationService.getConfiguration('LICENSE').subscribe(datares => this.getConfigLicense(data));
  }
}
