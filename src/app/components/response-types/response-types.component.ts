import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroupDirective, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SchemaService } from '../../service/schema.service';
import { ToastrService } from 'ngx-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-response-types',
  templateUrl: './response-types.component.html',
  styleUrls: ['./response-types.component.css']
})
export class ResponseTypesComponent implements OnInit {
  @ViewChild(FormGroupDirective) formDirective: FormGroupDirective;
  public listOfResponses = [];
  public purposeList = [];
  public isAdd = false;
  public isView = true;
  public responseType: FormGroup;
  public SimpleFormsResult = [];
  public editId: any;
  public showForms = false;
  constructor(private schemaService: SchemaService, private fb: FormBuilder, private tr: ToastrService, private cdr: ChangeDetectorRef,
    private spinner: Ng4LoadingSpinnerService, private ngxSmartModalService: NgxSmartModalService) { }

  ngOnInit() {
    this.purposeList = [
      { label: 'None', value: 'None' },
      { label: 'Email', value: 'Email' },
      { label: 'ESIGN', value: 'ESIGN' },
      { label: 'Document', value: 'Document' }
    ];
    this.responseType = this.fb.group({
      name: [null, Validators.compose([Validators.required])],
      purpose: ['None'],
      form: [null]
    });
    this.showForms = false;
    this.spinner.show();
    this.schemaService.getResponseTypes().subscribe(data => { this.getResponseTypes(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
    this.getSimpleFormsApi();
  }
  getSimpleFormsApi() {
    this.schemaService.getSimpleForms().subscribe(data => { this.gotSimpleForms(data); }, error => { });
  }
  getResponseTypes(data) {
    if (data._body !== '') {

      this.listOfResponses = JSON.parse(data._body);
    }
  }
  gotSimpleForms(data) {
    this.SimpleFormsResult = [];
    const value = JSON.parse(data._body);
    value.forEach(element => {
      const formsVal = {
        label: element.name,
        value: element.docId
      };
      this.SimpleFormsResult.push(formsVal);
    });
  }
  clickedOnAdd() {
    this.isAdd = true;
    this.isView = false;
    this.responseType.reset();
    this.showForms = false;
  }
  clickedOnView() {
    this.isView = true;
    this.isAdd = false;
    this.showForms = false;
  }
  editResponseType(event) {
    this.isView = false;
    this.isAdd = false;
    this.editId = event.data;
    this.responseType.reset();
    this.responseType.patchValue({ name: event.data.name });
    if (event.data.purpose !== undefined) {
      this.responseType.patchValue({ purpose: event.data.purpose });
      this.showForms = true;
    } else if (event.data.purpose === undefined) {
      this.responseType.patchValue({ purpose: 'None' });
    } if (event.data.finishForm !== undefined) {
      this.responseType.patchValue({ form: event.data.finishForm });
    }
  }
  responseTypeSubmitted(event) {
    if (this.isAdd) {
      const newResponse = {
        name: this.responseType.controls.name.value
      };
      this.spinner.show();
      this.schemaService.saveResponseType(newResponse).subscribe(data => { this.refreshResponse(data); this.tr.success('', 'New response added'); this.isAdd = false; this.spinner.hide(); }, error => { this.spinner.hide(); });
    } else {
      const newResponse = {
        id: this.editId.id,
        name: this.responseType.controls.name.value,
        purpose: this.responseType.controls.purpose.value,
        finishForm: this.responseType.controls.form.value
      };
      this.spinner.show();
      this.schemaService.saveResponseType(newResponse).subscribe(data => { this.refreshResponse(data); this.tr.success('', 'Response type is edited'); this.isAdd = false; this.spinner.hide(); }, error => { this.spinner.hide(); });
    }
  }
  refreshResponse(data) {
    this.isAdd = false;
    this.isView = true;
    this.ngOnInit();
  }
  responseTypeSelected(event) {
    if (event.value === 'Email') {
      this.showForms = true;
    } else {
      this.showForms = false;
    }

  }
  cancelAdd() {
    this.responseType.reset();
    this.responseType.enable();
    this.formDirective.resetForm();
    this.responseType.patchValue({
      purpose: 'None'
    });
    this.showForms = false;
  }
  simpleFormSelected(event) {

  }
}
