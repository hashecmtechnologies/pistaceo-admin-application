import { Component, OnInit, DoCheck, Input, ViewChild } from '@angular/core';
import { SchemaService } from '../../service/schema.service';
import { FormBuilder, FormGroup, Validators, FormArray, FormGroupDirective, NgForm } from '@angular/forms';
import { ContentService } from '../../service/content.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { DocumentService } from '../../service/document.service';
import { UserService } from '../../service/user.service';
import { ToastrService } from 'ngx-toastr';
import { AdministrationService } from '../../service/Administration.service';
import { DomSanitizer } from '@angular/platform-browser';
import { FileUploader } from 'ng2-file-upload';
import { WorkType } from '../../models/workType.model';
import { WorkActivityType } from '../../models/workActivityType.model';
import { Roles } from '../../models/roles.model';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import * as operators from '../../operators.variables';
import { ActivityType } from '../../models/ActivityType.model';
import { Response } from '../../models/Response.model';
import { saveAs } from 'file-saver';
import { DocumentType } from '../../models/documentType.model';

@Component({
  selector: 'app-work-types',
  templateUrl: './work-types.component.html',
  styleUrls: ['./work-types.component.css']
})
export class WorkTypesComponent implements OnInit, DoCheck {
  @Input() addingNewNode: any;
  @ViewChild('formDirective') private formDirective: NgForm;
  public workTypeList;
  public imageRender;
  public listOfWorkTypes;
  public selectedWorkType;
  public workTypeForm: FormGroup;
  public editWorkTypeToggle = false;
  public isWorktypeEdit = false;
  public workTypeActivity: WorkType;
  public selectedActivity: any;
  public listOfActivity = [];
  public closeResult;
  public activityName: FormGroup;
  public activityTypeForm: FormGroup;
  public workTypeName: FormGroup;
  public documentAddForm: FormGroup;
  public documentAddFormModel: FormGroup;
  public routeToForm: FormGroup;
  public newWorkType: WorkType;
  public selectedRouteTo: Response[];
  public selectedDocument = [];
  public editActivityToggle = false;
  public selected;
  public responseTypeList: any[];
  public documentsFileUploder = new FileUploader({});
  public negtiveId = 0;
  public compareObject: WorkType;
  public listOfFormtemplate = [];
  public toggleDocSelected;
  public listOfRoles: Roles[];
  public listOfRolesList;
  public recipientList;
  public template: FileUploader = new FileUploader({});
  public repository;
  // public selectedNode: TreeNode;
  public getRoleToAdd;
  public getRoleListToAdd;
  public selectedRoleList = [];
  public rolenameSelected;
  public initActivityTypeId;
  public initRoleId;
  public initRoleIdToggle = false;
  public editActivityTabToggle = false;
  public listOfBegineActivity = [];
  public listOfInitalRoute = [];
  public roleListNameSelected;
  public isActivityActive = true;
  public isRouteActive = false;
  public isDocumentsActive = false;
  public listOftemplates = [];
  public listOfDocumentTypes = [];
  public documentAddFormModelName: any;
  public deleteType: any;
  public deleteIndex: any;
  public showDocClass = false;
  public getDocClassResult = [];
  // validation regx
  public numberValidation = '/^-?(0|[1-9]\d*)?$/)';
  public defid = new FileUploader({});
  public checkIn = new FileUploader({});
  public checkOutFile;
  public selectedwList;
  public responsedPermissionRoles = [];
  public permissionAccessType = 0;
  public value = 0;
  public showAlertToDeleteRole = false;
  public okToDelete = false;
  public roleObjct: any;
  public isAdd = true;
  public isAddActivitySymName = true;
  public updateRoute = false;
  public updateDocumentType = false;
  public updateSelectedDocument;
  public editedMappingValue = [];
  public selectedNode: any;
  public addPatternDropDown = [];
  public addedActivityList = true;
  public selectedActivityIndex = -1;
  public nextScreenDropDown = [];
  public activityTypeDropDown = [];
  public salTypeDropDown = [];
  public activityInfoEdit: any;
  public activeIndexForInfo = 0;
  public showDocList = true;
  public selectedDocTypeIndex = -1;
  public selectedDocType: any;
  public documentTypeForm: FormGroup;
  public dropDowndocumentList = [];
  public formAssociatedDropDown = [];
  public documentClassResultValue = [];
  public documetClassDropDown = [];
  public displayTypeDropDown = [];
  public showRouteList = true;
  public selectedRoutesIndex = -1;
  public showRouteRulesList = true;
  public selectedRouteRulesIndex = -1;
  public selectedRoute: any;
  public selectedRouteRules: any;
  public routeRulesForm: FormGroup;
  public responseDropDown = [];
  public rolesList = [];
  public rolesDropDown = [];
  public routeToActivityDropDown = [];
  public routeToRecipientList = [];
  public routeToRecipientDropDown = [];
  public recipientGroupDropDown = [];
  public showActionsList = true;
  public selectedActionsIndex = -1;
  public selectedAction: any;
  public actionsForm: FormGroup;
  public actionDropDown = [];
  public sourceFromDropDown = [];
  public targetFromDropDown = [];
  public showPermissionList = true;
  public selectedPermissionIndex = -1;
  public selectedPermission: any;
  public permissionForm: FormGroup;
  public permissionDropDown = [];
  public routesForm: FormGroup;
  public propertiesArray = [];
  public selectedRouteConditions: any;
  // public addSubProps: any;
  public addSupPropesForm: FormGroup;
  public addRouteConditionsForm: FormGroup;
  public keyDropDown = [];
  public dataTypeDropDown = [];
  public operatorsDropDown = [];
  public selectedRouteConditionOption = null;
  public selectedRouteRuleConditionIndex = true;
  public docTypeFormControlDropDown = [];
  public docTypeFormControlForm: FormGroup;
  public docTypeFormControlList = true;
  public docTypeFormControlSelectedIndex = -1;
  public selectedDocTypeFormControl = null;
  public formControlDummy = [];
  public mapForm: FormGroup;
  public setAsPrimDocDropDown = [];

  constructor(private schemaService: SchemaService, private fb: FormBuilder, private contentService: ContentService,
    private ds: DocumentService, private us: UserService, public tr: ToastrService, private administrationService: AdministrationService,
    private modalService: NgxSmartModalService, private _sanitizer: DomSanitizer, private spinner: Ng4LoadingSpinnerService,
  ) { }

  ngOnInit() {
    this.workTypeForm = this.fb.group({
      id: [null],
      name: [null, Validators.compose([Validators.required])],
      symName: [null, Validators.required],
      draftSupport: [null],
      archiveSupport: [null],
      multiFormSupport: [0],
      registerSupport: [0],
      subjectProps: [null],
      template: [null],
      templateId: [null],
      refNoPrefix: [null, Validators.compose([Validators.required])],
      orgUnit: [null],
      orgName: [null],
      initRoute: [null],
      initActivityType: [null],
      registerOnCreate: [0],
      mapping: [null],
      filePattern: [null],
      filePaternDropDown: [null],
      isDraft: [null]
    });
    this.mapForm = this.fb.group({
      mapProp1: [],
      mapProp2: [],
      mapProp3: [],
      mapProp4: []
    });
    this.activityName = this.fb.group({
      name: [null, Validators.compose([Validators.required])],
      symName: [null, Validators.required]
    });
    this.workTypeName = this.fb.group({
      name: [null, Validators.compose([Validators.required])],
      symName: [null, Validators.compose([Validators.required])]
    });
    this.activityTypeForm = this.fb.group({
      id: [null],
      name: [null, Validators.compose([Validators.required])],
      symName: [null, Validators.required],
      instruction: [null],
      nextscreen: ['INBOX', Validators.compose([Validators.required])],
      activitytype: ['ACTIVITY', Validators.compose([Validators.required])],
      availForExtUser: [null],
      canrecall: [null],
      slaNumber: [null, Validators.compose([Validators.pattern(/^-?(0|[1-9]\d*)?$/)])],
      slatype: [1000],
      updatePrimary: [null]
    });

    this.documentAddForm = this.fb.group({
      name: [null, Validators.compose([Validators.required])],
      type: ['DOCUMENT'],
      signEnabled: [null],
      primaryflag: [false],
      req: [false],
      template: [null],
      docClass: [null],
      showType: [null, Validators.compose([Validators.required])],
      showClass: [false]
    });
    this.routesForm = this.fb.group({
      id: [null, Validators.compose([Validators.required])],
      name: [null, Validators.compose([Validators.required])],
      validate: [1],
      activityType: [null],
      keyId: [null],
      purpose: [null, Validators.compose([Validators.required])],
      routeToId: [null, Validators.compose([Validators.required])],
      routeToName: [null, Validators.compose([Validators.required])],
      roleId: [null, Validators.compose([Validators.required])],
      roleName: [null, Validators.compose([Validators.required])],
      signEnabled: [null, Validators.compose([Validators.required])],
      comment: [null, Validators.compose([Validators.required])],
      roleSelectedId: [null],
      roleSelectedName: [null],
      roleSelectedGrpId: [null],
      roleSelectedGrpName: [null]

    });
    this.routeRulesForm = this.fb.group({


      id: [null, Validators.compose([Validators.required])],
      activityType: [null, Validators.compose([Validators.required])],
      roleId: [null, Validators.compose([Validators.required])],
      routeToId: [null, Validators.compose([Validators.required])],
      routeToName: [null, Validators.compose([Validators.required])],
      routeRole: [null, Validators.compose([Validators.required])],
      routeRoleName: [null, Validators.compose([Validators.required])],
      roleName: [null, Validators.compose([Validators.required])],
      condition: [null, Validators.compose([Validators.required])],
      responseId: [null, Validators.compose([Validators.required])],
      responseName: [null, Validators.compose([Validators.required])],
      responseValidity: [null, Validators.compose([Validators.required])],
      routeConditions: [null, Validators.compose([Validators.required])],
      roleSelectedId: [null],
      roleSelectedName: [null],
      roleSelectedGrpId: [null],
      roleSelectedGrpName: [null],
      responseValidate: [null]
    });
    this.actionsForm = this.fb.group({
      symName: [null],
      action: ['FORMCOPY'],
      param1: [null],
      param2: [null],
      param3: [null],
      param4: [null],
      responseId: [''],
      responseName: [null],
      responseValidate: [1],
      id: [null]
    });
    this.permissionForm = this.fb.group({
      id: [null],
      accessType: [null],
      accessMask: [null],
      granteeName: [null],
      granteeType: [null],
      permissionSource: [null],
      inheritDepth: [null],
      accessLevel: [null],
      action: [null],
      depthName: [null],
      roleId: [null],
      aclId: [null],
      objectId: [null],
    });
    this.addSupPropesForm = this.fb.group({
      subProps: [null, Validators.compose([Validators.required])]
    });
    this.addRouteConditionsForm = this.fb.group({
      key: [null, Validators.compose([Validators.required])],
      value: [null, Validators.compose([Validators.required])],
      oper: [null, Validators.compose([Validators.required])],
      dtype: [null, Validators.compose([Validators.required])],
      id: [null],
      property: [null],
      ruleId: [null],
    });
    this.docTypeFormControlForm = this.fb.group({
      key: [null],
      value: [null],
    });
    this.keyDropDown = [
      { label: 1, value: 1 },
      { label: 2, value: 2 },
      { label: 3, value: 3 },
      { label: 4, value: 4 }
    ];
    this.actionDropDown = [
      { label: 'FORMCOPY', value: 'FORMCOPY' },
      { label: 'FORMDBTBLFILL', value: 'FORMDBTBLFILL' },
      { label: 'PRIMARYATTACH', value: 'PRIMARYATTACH' },
      { label: 'ATTACHMENTPROPSET', value: 'ATTACHMENTPROPSET' },
      { label: 'INVOKEWS', value: 'INVOKEWS' },
      { label: 'COLLECTDOCS', value: 'COLLECTDOCS' },
      { label: 'COLLECTWFDOCS', value: 'COLLECTWFDOCS' },
    ];
    this.dataTypeDropDown = [
      { label: 'INT', value: 'INT' },
      { label: 'STRING', value: 'STRING' },
      { label: 'DATE', value: 'DATE' }
    ];
    this.operatorsDropDown = [
      { label: '=', value: '=' },
      { label: '<', value: '<' },
      { label: '>', value: '>' },
      { label: '>=', value: '>=' },
      { label: '<=', value: '<=' },
      { label: '!=', value: '!=' },
      { label: 'Contains', value: 'Contains' },
      { label: 'Starts With', value: 'Starts With' }
    ];
    this.permissionDropDown = [
      { label: 'Create', value: 0 },
      { label: 'Search', value: 1 },
      { label: 'Both', value: 2 },
    ];
    this.addPatternDropDown = [
      { label: '{ORG}', value: '{ORG}' },
      { label: '{TYPE}', value: '{TYPE}' },
      { label: '{YEAR}', value: '{YEAR}' },
      { label: '{MONTH}', value: '{MONTH}' },
      { label: '{UD$1}', value: '{UD$1}' },
      { label: '{UD$2}', value: '{UD$2}' },
      { label: '{UD$3}', value: '{UD$3}' },
      { label: '{UD$4}', value: '{UD$4}' },
      { label: '{ID}', value: '{ID}' },
      { label: '{STATUS}', value: '{STATUS}' },
      { label: '{REFNO}', value: '{REFNO}' },
      { label: '{SUBJECT}', value: '{SUBJECT}' }
    ];
    this.nextScreenDropDown = [
      { label: 'Inbox', value: 'INBOX' },
      { label: 'Sent', value: 'SENT' },
      { label: 'Draft', value: 'DRAFT' }
    ];
    this.activityTypeDropDown = [
      { label: 'Begin', value: 'BEGIN' },
      { label: 'Activity', value: 'ACTIVITY' },
      { label: 'Archive', value: 'ARCHIVE' },
      { label: 'Invoke', value: 'INVOKE' },
      { label: 'Dispacth', value: 'DISPATCH' },
      { label: 'End', value: 'END' },
      { label: 'Return', value: 'RETURN' }
    ];
    this.salTypeDropDown = [
      { label: 'Hours', value: '2000' },
      { label: 'Days', value: '3000' },
      { label: 'Business Days', value: '4000' },
      { label: 'Weeks', value: '5000' },
      { label: 'Months', value: '6000' },
      { label: 'Years', value: '7000' }
    ];
    this.recipientGroupDropDown = [
      { label: '#All_Users', value: -9000001 },
      { label: 'All HR', value: -9000002 },
      { label: 'All Managers', value: -9000003 },
      { label: 'Top Management', value: -9000004 },
    ];
    this.setAsPrimDocDropDown = [
      { label: 'None', value: 0 },
      { label: 'As Is', value: 1 },
      { label: 'PDF', value: 2 },
      { label: 'Word', value: 3 }
    ];
    this.documentTypeForm = this.fb.group({
      activityType: [null],
      id: [null, Validators.compose([Validators.required])],
      name: [null, Validators.compose([Validators.required])],
      type: ['DOCUMENT'],
      signEnabled: [null],
      primaryFlag: ['As Is'],
      req: [false],
      template: [null],
      docClass: [null],
      showType: [null, Validators.compose([Validators.required])],
      showClass: [false],
      formControl: ['']
    });
    this.displayTypeDropDown = [
      { label: 'Add & Show In Documents', value: '0' },
      { label: 'Add Only', value: '1' },
      { label: 'Show In Documents Only', value: '2' },
      { label: 'Hide Everywhere', value: '3' }
    ];
    this.routeToRecipientList = [
      { label: 'To Specific Role', value: -100 },
      { label: 'Work Flow Initiator', value: -2 },
      { label: 'Manager Of the User', value: -1 },
      { label: 'Loop To Self', value: -4 },
      { label: 'User Selects Manually', value: -5 },
      { label: 'To a System Activity', value: -3 },
      { label: 'Return To Sender', value: -7 },
      { label: 'To a Group', value: -101 },
      { label: 'None', value: -8 },
      { label: 'Undefined', value: -6 },
    ];
    this.docTypeFormControlDropDown = [
      { label: 'Read/Write', value: 'SHOW' },
      { label: 'Readonly', value: 'READONLY' },
      { label: 'Hidden', value: 'HIDE' }
    ];
    this.spinner.show();
    this.schemaService.getWorkTypesForEdit().subscribe(data => { this.getWorkType(data); this.spinner.hide(); }, error => { console.log(error); this.spinner.hide(); });
    this.listOfActivity = [];
    this.selectedRoleList = [];
    this.recipientList = operators.recipientList;
    this.schemaService.getForms().subscribe(data => this.getForms(data), error => console.log(error));
    this.schemaService.GetDocumentTypes().subscribe(data => { this.getDocumentTypesList(data); }, error => { });
    this.contentService.getDocumentClasses().subscribe(data => { this.documentClassResult(data); }, error => { });
    this.schemaService.getResponseTypes().subscribe(data => this.getResponseTypeData(data));
    this.us.getRoles().subscribe(data => { this.getRolesResult(data); });
  }
  converToUppercase(string) {
    if (string) {
      return string.toUpperCase();
    }
  }
  getRolesResult(data) {
    this.rolesList = JSON.parse(data._body);
    this.rolesList.forEach(element => {
      const val = {
        label: element.name,
        value: element.id
      };
      this.rolesDropDown.push(val);
    });
  }
  getResponseTypeList(data) {
    this.responseTypeList = JSON.parse(data._body);
  }
  getDocumentClass(data) {
    this.getDocClassResult = JSON.parse(data._body);
    this.documentAddFormModel.patchValue({ docClass: this.allToLowerCase(this.getDocClassResult[0].symName) });
  }
  ngDoCheck() {
    if (this.editWorkTypeToggle === true && this.listOfActivity.length > 0) {
      const listOfBegineActivity = [];
      for (let index = 0; index < this.listOfActivity.length; index++) {
        if (this.listOfActivity[index].type.seqType === 'BEGIN') {
          listOfBegineActivity.push(this.listOfActivity[index]);
        }
      }
      this.listOfBegineActivity = listOfBegineActivity;
    }
  }

  getWorkType(data) {
    if (data._body !== '') {
      this.listOfWorkTypes = JSON.parse(data._body);
    }
  }

  editWorkType(workType) {
    this.selectedWorkType = workType.data;
    setTimeout(() => {
      this.administrationService.getTopLevelOrgUnit().subscribe(data => this.getOrgUnit(data), error => { });
      this.schemaService.getTemplates().subscribe(data => this.getTemplates(data));
      this.spinner.show();
      this.schemaService.getWorkTypeFromID(this.selectedWorkType.id).subscribe(data => { this.spinner.hide(); this.getWorkTypeJSON(data); }, error => { this.spinner.hide(); });
      this.selectedActivity = null;
      this.selectedActivityIndex = -1;
      this.schemaService.getWorkTypeInitiatingForm(this.selectedWorkType.id).subscribe(data => { this.spinner.hide(); this.props(data); }, error => { this.spinner.hide(); });
    }, 0);
  }
  props(data) {
    if (data._body !== '') {
      const value = JSON.parse(data._body);
      for (let i = 0; i < value.sections.length; i++) {
        if (value.sections[i].type === 'FORM') {
          for (let j = 0; j < value.sections[i].columns.length; j++) {
            for (let k = 0; k < value.sections[i].columns[j].properties.length; k++) {
              const val = {
                label: value.sections[i].columns[j].properties[k].prop.symName,
                value: value.sections[i].columns[j].properties[k].prop.id
              };
              this.propertiesArray.push(val);
            }
          }
        }
      }
    }
  }
  activityInfoResult(data) {
    this.activityInfoEdit = JSON.parse(data._body);
    if (this.activityInfoEdit.docTypes.length > 0) {
      this.sourceFromDropDown = [];
      this.activityInfoEdit.docTypes.forEach(element => {
        const value = {
          label: element.name,
          value: element.id
        };
        this.sourceFromDropDown.push(value);
      });
    }
    if (this.activityInfoEdit.docTypes.length > 0) {
      this.sourceFromDropDown = [];
      this.activityInfoEdit.docTypes.forEach(element => {
        const value = {
          label: element.name,
          value: element.id
        };
        this.sourceFromDropDown.push(value);
      });
    }
    console.log(this.sourceFromDropDown);
  }
  getActivityTypes(data) {
    console.log(data);
  }
  getOrgUnit(data) {
    this.repository = JSON.parse(data._body);
  }

  getRoleLists(data) {
    this.getRoleListToAdd = JSON.parse(data._body);
  }

  getRoles(data) {
    this.getRoleToAdd = JSON.parse(data._body);
  }

  workTypeNameSubmit() {
    const newWorkType = new WorkType();
    newWorkType.name = this.workTypeName.controls.name.value;
    newWorkType.symName = this.workTypeName.controls.symName.value;
    newWorkType.activityTypes = [];
    newWorkType.archiveSupport = 0;
    newWorkType.draftSupport = 0;
    newWorkType.multiFormSupport = 0;
    newWorkType.mapping = null;
    newWorkType.filePattern = null; // changed workTypeDefinition
    this.spinner.show();
    this.schemaService.addWorkType(newWorkType).subscribe(data => { this.saveWorktype(data); this.tr.success('', 'New worktype added'); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
  enteredWorkTypeName(event) {
    this.workTypeName.patchValue({ symName: event.target.value });
  }
  addNewWorkType() {
    this.modalService.getModal('workTypeModel').open();
    this.showDocClass = false;
    this.selectedActivity = null;
    this.selectedRouteTo = [];
    this.selectedDocument = [];
    this.editActivityToggle = false;
    this.editActivityTabToggle = false;
  }
  deletedAlert(deleteAlert, type, index) {
    this.deleteType = type;
    this.deleteIndex = index;
    this.modalService.getModal('deleteAlert').open();
  }
  getWorkTypeJSON(data) {
    this.editWorkTypeToggle = true;
    this.workTypeActivity = JSON.parse(data._body);
    this.compareObject = JSON.parse(data._body);
    this.workTypeForm.patchValue({
      id: this.workTypeActivity.id,
      name: this.workTypeActivity.name,
      symName: this.workTypeActivity.symName,
      archiveSupport: this.workTypeActivity.archiveSupport,
      multiFormSupport: this.workTypeActivity.multiFormSupport,
      subjectProps: this.workTypeActivity.subjectProps,
      registerSupport: this.workTypeActivity.registerSupport,
      registerOnCreate: this.workTypeActivity.registerOnCreate,
      refNoPrefix: this.workTypeActivity.refNoPrefix,
      orgName: this.workTypeActivity.orgName,
      orgUnit: this.workTypeActivity.orgUnit,
      template: this.workTypeActivity.template,
      filePattern: this.workTypeActivity.filePattern,
      mapping: this.workTypeActivity.mapping
    });
    this.listOfActivity = [];
    this.routeToActivityDropDown = [];
    this.listOfActivity = this.workTypeActivity.activityTypes;
    this.listOfActivity.forEach(element => {
      const val = {
        label: element.type.name,
        value: element.type.id
      };
      this.routeToActivityDropDown.push(val);
    });
    if (this.workTypeActivity.draftSupport === 1) {
      this.workTypeForm.patchValue({ isDraft: true });
    } else {
      this.workTypeForm.patchValue({ isDraft: false });
    }
    console.log(this.workTypeForm.controls['isDraft']);
  }
  getWorkTypeJSONActivityEdit(data) {
    this.activityInfoEdit = JSON.parse(data._body);
    if (this.activityInfoEdit.docTypes.length > 0) {
      this.sourceFromDropDown = [];
      this.activityInfoEdit.docTypes.forEach(element => {
        const value = {
          label: element.name,
          value: element.id
        };
        this.sourceFromDropDown.push(value);
      });
    }
    console.log(this.sourceFromDropDown);
  }
  patch(value) {

    const control = <FormArray>this.workTypeForm.controls.mapping;

  }
  listTabClicked(event, warningModel) {
    this.editWorkTypeToggle = false;

  }

  tabChangeWithoutSave() {
    this.editWorkTypeToggle = false;
    this.selectedWorkType = '';
    this.editActivityToggle = false;
    this.selectedRoleList = [];
    this.listOfBegineActivity = [];
    this.listOfInitalRoute = [];
  }

  tabChangeClickedNo() {
    this.editWorkTypeToggle = false;
    setTimeout(() => {
      this.editWorkTypeToggle = true;
    }, 0);
  }

  addNewRoute(addNewRouteModel) {
    this.schemaService.getResponseTypes().subscribe(data => this.getResponseType(data, addNewRouteModel));
  }

  getListOfRolelist(data) {
    this.listOfRolesList = JSON.parse(data._body);
  }

  getAllRoles(data) {
    this.listOfRoles = JSON.parse(data._body);
  }
  getResponseType(data, addNewRouteModel) {
    this.responseTypeList = JSON.parse(data._body);
    if (this.responseTypeList[0].id !== undefined) {
      this.routeToForm.reset();
      this.routeToForm.patchValue({
        respoId: this.responseTypeList[0].id,
        recipientId: -1,
        activityId: this.listOfActivity[0].type.id,
        purpose: null,
        comment: null,
        signEnabled: 0
      });
    }
    this.modalService.getModal('addNewRouteModel').open();
  }



  assignDocNameToDocuments(data, index) {
    const fileName = JSON.parse(data._body);
    this.selectedDocument[index].filename = fileName.props[0].mvalues[0];
  }

  deleteActivity(i) {
    for (let index = 0; index < this.selectedRouteTo.length; index++) {
      if (i === index) {
        this.selectedRouteTo.splice(index, 1);
        this.deleteType = undefined;
        this.deleteIndex = -1000000;
      }
    }
  }

  deleteDocumentActivity(i) {
    for (let index = 0; index < this.selectedDocument.length; index++) {
      if (i === index) {
        this.selectedDocument.splice(index, 1);
        this.deleteType = undefined;
        this.deleteIndex = -1000000;
      }
    }
  }
  delete() {
    if (this.deleteType === 'listOfActivity') {
      this.deleteListActivity(this.deleteIndex);
    } else if (this.deleteType === 'routeTo') {
      this.deleteActivity(this.deleteIndex);
    } else if (this.deleteType === 'documentsDelete') {
      this.deleteDocumentActivity(this.deleteIndex);
    }

  }
  toggleDivClicked(docName, documentValue) {
    this.toggleDocSelected = documentValue;
    this.documentAddForm.patchValue({
      name: documentValue.name,
      type: documentValue.type,
      signEnabled: documentValue.signEnabled,
      primaryflag: documentValue.primaryFlag,
      req: documentValue.req,
    });

    for (let index = 0; index < this.selectedDocument.length; index++) {
      const x = document.getElementById(this.selectedDocument[index].name);
      if (docName === this.selectedDocument[index].name) {
        x.className = x.className.replace('preToggle', 'postToggle');
      } else {
        x.className = x.className.replace('postToggle', 'preToggle');
      }
    }

  }

  changeDocumentsOrderDown(position) {
    let temp;
    for (let swap = 0; swap < this.selectedDocument.length; swap++) {
      if (swap === position) {
        temp = this.selectedDocument[swap];
        this.selectedDocument[swap] = this.selectedDocument[swap + 1];
        this.selectedDocument[swap + 1] = temp;
      }
    }
  }
  changeDocumentsOrderUp(position) {
    let temp;
    for (let swap = 0; swap < this.selectedDocument.length; swap++) {
      if (swap === position) {
        temp = this.selectedDocument[swap];
        this.selectedDocument[swap] = this.selectedDocument[swap - 1];
        this.selectedDocument[swap - 1] = temp;
      }
    }
  }
  changerouteOrderUp(index) {
    let temp;
    for (let swap = 0; swap < this.selectedRouteTo.length; swap++) {
      if (swap === index) {
        temp = this.selectedRouteTo[swap];
        this.selectedRouteTo[swap] = this.selectedRouteTo[swap - 1];
        this.selectedRouteTo[swap - 1] = temp;
      }
    }
  }
  changerouteOrderDown(index) {
    let temp;
    for (let swap = 0; swap < this.selectedRouteTo.length; swap++) {
      if (swap === index) {
        temp = this.selectedRouteTo[swap];
        this.selectedRouteTo[swap] = this.selectedRouteTo[swap + 1];
        this.selectedRouteTo[swap + 1] = temp;
      }
    }
  }
  clickedOnActivityType(value, index) {
    this.activeIndexForInfo = 0;
    setTimeout(() => {
      this.activeIndexForInfo = 1;
    }, 0);
    this.selectedActivityIndex = index;
    this.selectedActivity = value;
    this.showDocList = true;
    this.activeIndexForInfo = 1;
    this.schemaService.getActivityTypeFromID(this.selectedActivity.type.id).subscribe(dataRes => this.getActivityTypesInfo(dataRes), error => { });
  }
  clickedOnDocType(value, index) {
    this.selectedDocTypeIndex = index;
    this.selectedDocType = value;
    this.showDocList = false;
    if (this.selectedDocType.formControl) {
      const valuu = JSON.parse(this.selectedDocType.formControl);
      this.formControlDummy = [];
      valuu.forEach(element => {
        if (element.value === 'SHOW') {
          const value1 = {
            key: element.key,
            value: element.value,
            valueName: 'Read/Write'
          };
          this.formControlDummy.push(value1);
        }
        if (element.value === 'HIDE') {
          const value1 = {
            key: element.key,
            value: element.value,
            valueName: 'HIDDEN'
          };
          this.formControlDummy.push(value1);
        }
        if (element.value === 'READONLY') {
          const value1 = {
            key: element.key,
            value: element.value,
            valueName: 'Read Only'
          };
          this.formControlDummy.push(value1);
        }
      });
    }
    this.documentTypeForm.patchValue({
      activityType: value.activityType,
      id: value.id,
      name: value.name,
      type: value.type,
      signEnabled: value.signEnabled,
      template: value.template,
      templateName: value.templateName,
      docClass: value.docClass,
      showType: value.showType,
      formControl: value.formControl,
      keyId: value.keyId,
      primaryFlag: value.primaryFlag
    });
    if (value.req === 1) {
      this.documentTypeForm.patchValue({ req: true });
    } else {
      this.documentTypeForm.patchValue({ req: false });
    }
    // if (value.primaryFlag === 1) {
    //   this.documentTypeForm.patchValue({ primaryFlag: true });
    // } else {
    //   this.documentTypeForm.patchValue({ primaryFlag: false });
    // }
    if (value.showClass === 1) {
      this.documentTypeForm.patchValue({ showClass: true });
    } else {
      this.documentTypeForm.patchValue({ showClass: false });
    }
  }
  clickedOnRoutes(value, index) {
    this.selectedRoutesIndex = index;
    this.showRouteList = false;
    this.selectedRoute = value;
    this.routesForm.controls['roleSelectedId'].disable();
    this.routesForm.controls['roleSelectedGrpId'].disable();

    this.routesForm.patchValue({
      id: value.id,
      name: value.name,
      activityType: this.activityInfoEdit.id,
      keyId: value.keyId,
      routeToId: value.routeToId,
      routeToName: value.routeName,
      signEnabled: value.signEnabled,
      comment: value.comment,
      purpose: value.purpose,
    });
    if (value.signEnabled === 1) {
      this.documentTypeForm.patchValue({ signEnabled: true });
    } else {
      this.documentTypeForm.patchValue({ signEnabled: false });
    }
    const val1 = (value.roleId).toString();
    if ((val1.length) > 6) {
      this.routesForm.controls['roleSelectedGrpId'].enable();
      this.routesForm.patchValue({ roleId: -101, roleName: 'To a Group', roleSelectedGrpId: value.roleId, roleSelectedGrpName: value.roleName });
    } else {
      if (value.roleId > 0) {
        this.routesForm.controls['roleSelectedId'].enable();
        this.routesForm.patchValue({ roleId: -100, roleName: 'To Specific Role', roleSelectedId: value.roleId, roleSelectedName: value.roleName });
      } else {
        this.routesForm.patchValue({ roleId: value.roleId, roleName: value.roleName });
      }
    }
  }
  clickToAddNewRoute() {
    this.selectedRoute = null;
    this.routesForm.controls['roleSelectedId'].disable();
    this.routesForm.controls['roleSelectedGrpId'].disable();
  }
  clickedOnRouteRules(value, index) {
    this.selectedRouteRulesIndex = index;
    this.selectedRouteRules = value;
    this.showRouteRulesList = false;
    this.routeRulesForm.controls['roleSelectedId'].disable();
    this.routeRulesForm.controls['roleSelectedGrpId'].disable();
    this.selectedRouteConditions = value.routeConditions;

    this.routeRulesForm.patchValue({
      id: value.id,
      name: value.name,
      responseId: value.response.id,
      responseName: value.response.name,
      responseValidate: value.response.validate,
      activityType: value.activityType,
      keyId: value.keyId,
      roleId: value.roleId,
      routeToId: value.routeToId,
      routeToName: value.routeName,
      signEnabled: value.signEnabled,
      condition: value.condition,
    });

    const val1 = (value.routeRole).toString();
    if ((val1.length) > 6) {
      this.routeRulesForm.controls['roleSelectedGrpId'].enable();
      this.routeRulesForm.patchValue({ routeRole: -101, routeRoleName: 'To a Group', roleSelectedGrpId: value.routeRole, roleSelectedGrpName: value.routeRoleName });
    } else {
      if (value.roleId > 0) {
        this.routeRulesForm.controls['roleSelectedId'].enable();
        this.routeRulesForm.patchValue({ routeRole: -100, routeRoleName: 'To Specific Role', roleSelectedId: value.routeRole, roleSelectedName: value.routeRoleName });
      } else {
        this.routeRulesForm.patchValue({ routeRole: value.routeRole, routeRoleName: value.routeRoleName });
      }
    }

  }
  clickToAddNewRouteRule() {
    this.selectedRouteRules = null;
    this.showRouteRulesList = false;
    this.routeRulesForm.controls['roleSelectedId'].disable();
    this.routeRulesForm.controls['roleSelectedGrpId'].disable();
  }
  getActivityTypesInfo(data) {
    this.activityInfoEdit = {};
    this.activityInfoEdit = JSON.parse(data._body);
    if (this.activityInfoEdit.docTypes.length > 0) {
      this.sourceFromDropDown = [];
      this.activityInfoEdit.docTypes.forEach(element => {
        const value = {
          label: element.name,
          value: element.id
        };
        this.sourceFromDropDown.push(value);
      });
    }
    console.log(this.sourceFromDropDown);
    this.activeIndexForInfo = 1;
    setTimeout(() => {
      this.activityTypeForm.patchValue({
        id: this.workTypeActivity.id,
        name: this.activityInfoEdit.name,
        symName: this.activityInfoEdit.symName,
        activitytype: this.activityInfoEdit.seqType,
        instruction: this.activityInfoEdit.instructions,
        nextscreen: this.activityInfoEdit.nextScreen,
        slaNumber: this.activityInfoEdit.sla
      });
      if (this.activityInfoEdit.canRecall === 1) {
        this.activityTypeForm.patchValue({ canrecall: true });
      } else {
        this.activityTypeForm.patchValue({ canrecall: false });
      }
    }, 0);
    console.log(this.activityTypeForm.controls['canrecall']);
  }
  activityNameSubmit() {
    const typeName = new ActivityType();
    typeName.name = this.activityName.controls.name.value;
    typeName.symName = 'AT_' + this.activityName.controls.symName.value;
    typeName.workType = this.selectedWorkType.id;
    let flag = 0;
    for (let index = 0; index < this.listOfActivity.length; index++) {
      if (this.listOfActivity[index].type.name === typeName.name) {
        flag = 1;
        break;
      }
    }
    if (flag === 0) {
      this.spinner.show();
      this.schemaService.addActivityType(typeName).subscribe(data => { this.spinner.hide(); this.tr.success('Added New Work Type'); this.callWorkTypesFormId(); this.addedActivityList = true; }, error => { this.spinner.hide(); });
    } else {
      this.tr.warning('', 'Activity Name Already Exists');
    }
    this.activityName.reset();
  }
  callWorkTypesFormId() {
    this.spinner.show();
    this.schemaService.getWorkTypeFromID(this.selectedWorkType.id).subscribe(data => { this.spinner.hide(); this.getWorkTypeJSON(data); }, error => { this.spinner.hide(); });
  }

  fileChanged(event) {
    const docInfo = {
      docclass: '',
      docTitle: this.template.queue['0']._file.name,
      props: [{
        'name': 'Document Title',
        'symName': 'DocumentTitle',
        'dtype': 'STRING',
        'mvalues': [this.template.queue['0']._file.name],
        'mtype': 'N',
        'len': 255,
        'rOnly': 'false',
        'hidden': 'false',
        'req': 'false'
      }],
    };
    const fileReader = new FileReader();
    const formData = new FormData();
    formData.append('DocInfo', JSON.stringify(docInfo));
    formData.append('file', this.template.queue['0']._file);
    this.schemaService.addTemplate(formData).subscribe(data => this.documentAdded(data, this.template.queue['0']._file.name), error => console.log(error));
  }

  documentAdded(data, filename) {
    this.workTypeForm.patchValue({
      template: filename,
      templateId: data
    });
    this.template.clearQueue();
  }


  viewDocument(imageViewModel, docId) {
    this.imageRender = '';
    this.contentService.getPreviewPage(docId._body, 1).subscribe(data => this.getPreviewPage(data, imageViewModel), error => console.log(error));

  }

  getPreviewPage(data, imageViewModel) {
    const imageDetails = JSON.parse(data._body);
    this.imageRender = 'data:image/png;base64,' + imageDetails.image;
    this.modalService.getModal('imageViewModel').open();
  }


  downloadDocument(docId) {
    this.contentService.downloadDocument(docId._body).subscribe(data => this.downloadCurrentDocument(data), error => console.log(error));
  }

  downloadCurrentDocument(data) {
    let filename = '';
    const disposition = data.headers.get('Content-Disposition');
    const filenameRegex = /filename[^;=\n]*=((['']).*?\2|[^;\n]*)/;
    const matches = filenameRegex.exec(disposition);
    if (matches != null && matches[1]) {
      filename = matches[1].replace(/['']/g, '');
    }
    const blob = new Blob([data._body], { type: data.headers.get('Content-Type') });
    saveAs(blob, filename);
  }

  addNewDocument(addNewDocumentModel) {
    this.showDocClass = false;
    this.updateDocumentType = false;
    // this.documentAddFormModel.reset();
    this.documentAddFormModel.patchValue({
      id: this.listOfDocumentTypes[0].id,
      name: this.listOfDocumentTypes[0].name,
      primaryflag: 0,
      docClass: this.getDocClassResult[0].symName
    });
    this.documentAddFormModel.controls['id'].enable();
    this.documentAddFormModel.controls['name'].enable();
    this.documentAddFormModelName = this.listOfDocumentTypes[0];
    this.schemaService.getForms().subscribe(data => this.getForms(data), error => console.log(error));
    this.modalService.getModal('addNewDocumentModel').open();

  }

  editDocumentModel(addNewDocumentModel, document, i) {
    this.showDocClass = false;
    this.updateDocumentType = true;
    this.updateSelectedDocument = i;
    if (document.template !== null) {
      for (let index = 0; index < this.listOfFormtemplate.length; index++) {
        if (this.listOfFormtemplate[index].docId === this.documentAddFormModel.controls.template.value) {
          this.documentAddFormModel.patchValue({
            template: this.listOfFormtemplate[index].docId
          });
        }
      }
    } else {
      this.documentAddFormModel.patchValue({
        template: 'Null',
      });
    }
    const docClass = this.allToLowerCase(document.docClass);
    this.documentAddFormModel.patchValue({
      id: document.id,
      name: document.name,
      type: document.type,
      signEnabled: document.signEnabled,
      primaryflag: document.primaryFlag,
      req: document.req,
      showClass: document.showClass,
      docClass: docClass,
      formControl: document.formControl,
      template: document.template
    });
    for (let index = 0; index < this.listOfDocumentTypes.length; index++) {
      if (this.listOfDocumentTypes[index].name === document.name) {
        this.documentAddFormModelName = this.listOfDocumentTypes[index];
      }
    }
    this.documentAddFormModel.controls['id'].disable();
    this.documentAddFormModel.controls['name'].disable();
    this.schemaService.getForms().subscribe(data => this.getForms(data), error => console.log(error));
    this.modalService.getModal('addNewDocumentModel').open();
  }

  getDocumentTypesList(data) {
    this.listOfDocumentTypes = JSON.parse(data._body);
    this.listOfDocumentTypes.forEach(element => {
      const val = {
        label: element.name,
        value: element.id
      };
      this.dropDowndocumentList.push(val);
    });
  }
  clickToAddNewDocType() {
    this.selectedDocType = null;
    this.documentTypeForm.reset();
    this.documentTypeForm.patchValue({ templateName: this.dropDowndocumentList[0].label });
    this.documentTypeForm.patchValue({ docClassName: this.documetClassDropDown[0].label });

  }

  getForms(data) {
    this.listOfFormtemplate = JSON.parse(data._body);
    this.listOfFormtemplate.forEach(element => {
      const val = {
        label: element.name,
        value: element.docId
      };
      this.formAssociatedDropDown.push(val);
    });
  }

  fileChangedForDocument(index) {
  }

  documentTypeFileUploadModel(event) {
    const docInfo = {
      docclass: '',
      docTitle: event.queue['0']._file.name,
      props: [{
        'name': 'Document Title',
        'symName': 'DocumentTitle',
        'dtype': 'STRING',
        'mvalues': [event.queue['0']._file.name],
        'mtype': 'N',
        'len': 255,
        'rOnly': 'false',
        'hidden': 'false',
        'req': 'false'
      }],
    };
    const fileReader = new FileReader();
    const formData = new FormData();
    formData.append('DocInfo', JSON.stringify(docInfo));
    formData.append('file', event.queue['0']._file);
    this.schemaService.addTemplate(formData).subscribe(data => this.documentNewAdded(data, event.target.files['0'].name), error => console.log(error));
  }

  documentNewAdded(data, name) {

  }

  documentAddFormSubmit() {
    const newDocument = new DocumentType();
    newDocument.primaryFlag = this.documentAddFormModel.controls.primaryflag.value;
    newDocument.docClass = this.documentAddFormModel.controls.docClass.value;
    if (this.documentAddFormModel.controls.req.value) {
      newDocument.req = 1;
    } else {
      newDocument.req = 0;
    }
    if (this.documentAddFormModel.controls.showClass.value) {
      newDocument.showClass = 1;
    } else {
      newDocument.showClass = 0;
    }
    if (this.documentAddFormModel.controls.signEnabled.value) {
      newDocument.signEnabled = 1;
    } else {
      newDocument.signEnabled = 0;
    }
    newDocument.name = this.documentAddFormModelName.name;
    newDocument.id = this.documentAddFormModelName.id;
    newDocument.type = this.documentAddFormModelName.type;
    newDocument.formControl = this.documentAddFormModel.controls.formcontrol.value;
    if (this.documentAddFormModel.controls.template.value !== null) {
      for (let index = 0; index < this.listOfFormtemplate.length; index++) {
        if (this.listOfFormtemplate[index].docId === this.documentAddFormModel.controls.template.value) {
          newDocument.template = this.listOfFormtemplate[index].docId,
            newDocument.templateName = this.listOfFormtemplate[index].name;
        }
      }
    } else {
      newDocument.template = null;
      newDocument.templateName = null;
    }

    let flag = 0;
    for (let index = 0; index < this.selectedDocument.length; index++) {
      if (this.selectedDocument[index].name === newDocument.name) {
        flag = 1;
        break;
      }
    }
    if (flag === 0) {
      this.selectedDocument.push(newDocument);
    } else {
      this.tr.warning('', 'Document Type already present');
    }
    this.selectedActivity.type.docTypes = this.selectedDocument;
    // this.documentAddFormModel.reset();
    this.documentAddFormModel.patchValue({
      id: this.listOfDocumentTypes[0].id,
      name: this.listOfDocumentTypes[0].name,
      primaryflag: 0,
      docClass: this.getDocClassResult[0].symName
    });
  }

  documentUpdateFormSubmit() {
    const newDocument = new DocumentType();
    newDocument.primaryFlag = this.documentAddFormModel.controls.primaryflag.value;
    newDocument.docClass = this.documentAddFormModel.controls.docClass.value;
    if (this.documentAddFormModel.controls.req.value) {
      newDocument.req = 1;
    } else {
      newDocument.req = 0;
    }
    if (this.documentAddFormModel.controls.showClass.value) {
      newDocument.showClass = 1;
    } else {
      newDocument.showClass = 0;
    }
    if (this.documentAddFormModel.controls.signEnabled.value) {
      newDocument.signEnabled = 1;
    } else {
      newDocument.signEnabled = 0;
    }
    newDocument.id = this.documentAddFormModelName.id;
    newDocument.type = this.documentAddFormModelName.name;
    newDocument.name = this.documentAddFormModelName.name;
    newDocument.formControl = this.documentAddFormModel.controls.formcontrol.value;
    if (this.documentAddFormModel.controls.template.value !== null) {
      for (let index = 0; index < this.listOfFormtemplate.length; index++) {
        if (this.listOfFormtemplate[index].docId === this.documentAddFormModel.controls.template.value) {
          newDocument.template = this.listOfFormtemplate[index].docId,
            newDocument.templateName = this.listOfFormtemplate[index].name;
        }
      }
    } else {
      newDocument.template = null;
      newDocument.templateName = null;
    }
    for (let index = 0; index < this.selectedDocument.length; index++) {
      if (index === this.updateSelectedDocument) {
        this.selectedDocument[index] = newDocument;
        break;
      }
    }
    this.selectedActivity.type.docTypes = [];
    this.selectedActivity.type.docTypes = this.selectedDocument;
    // this.documentAddFormModel.reset();
    this.documentAddFormModel.patchValue({
      id: this.listOfDocumentTypes[0].id,
      primaryflag: 0,
      docClass: this.getDocClassResult[0].symName
    });
  }

  documentAddFormModelChanged(event) {
    for (let index = 0; index < this.listOfDocumentTypes.length; index++) {
      if (this.listOfDocumentTypes[index].name === event.target.value) {
        this.documentAddFormModelName = this.listOfDocumentTypes[index];
      }
    }
  }


  checkOutDocument(data) {
    const docInfo = {
      id: data._body,
      docclass: 'ProductivitiConfig',
      docTitle: this.selectedWorkType.name,
      props: [{
        'name': 'Document Title',
        'symName': 'DocumentTitle',
        'dtype': 'STRING',
        'mvalues': [this.selectedWorkType.name],
        'mtype': 'N',
        'len': 255,
        'rOnly': 'false',
        'hidden': 'false',
        'req': 'false'
      }],
      accessPolicies: []
    };
    const formData = new FormData();
    formData.append('DocInfo', JSON.stringify(docInfo));
    formData.append('file', new Blob([JSON.stringify(this.workTypeActivity)]));
    this.contentService.checkIn(formData).subscribe(datares => { this.saveWorktype(datares); this.tr.success('', 'Worktype Updated'); });
  }

  saveWorktype(data) {
    this.workTypeName.reset();
    // this.documentAddFormModel.reset();
    this.workTypeName.reset();
    this.activityName.reset();
    if (this.selectedWorkType) {
      if (this.selectedWorkType.status === 'ACTIVE') {
        this.schemaService.setConfigDocumentAsDraft(this.selectedWorkType.id).subscribe();
      }
    }
    this.schemaService.getWorkTypesForEdit().subscribe(dataRes => this.getWorkType(dataRes), error => console.log(error));
    this.editWorkTypeToggle = false;
    this.editActivityToggle = false;
  }

  addNewRouetModel() {
    const addnewRoute = new Response();
    addnewRoute.id = this.routeToForm.controls.respoId.value;
    addnewRoute.comment = this.routeToForm.controls.comment.value;
    if (this.routeToForm.controls.signEnabled.value) {
      addnewRoute.signEnabled = 1;
    } else {
      addnewRoute.signEnabled = 0;
    }
    for (let index = 0; index < this.responseTypeList.length; index++) {
      if (this.responseTypeList[index].id === +addnewRoute.id) {
        addnewRoute.name = this.responseTypeList[index].name;
      }
    }
    let flag = 0;
    for (let index = 0; index < this.selectedRouteTo.length; index++) {
      if (this.selectedRouteTo[index].name === addnewRoute.name) {
        flag = 0;
        break;
      }
    }
    if (flag === 1) {
      this.tr.warning('', 'Route name already exists in this activity');
    } else {
      addnewRoute.routeToId = this.routeToForm.controls.activityId.value;
      for (let index = 0; index < this.listOfActivity.length; index++) {
        if (this.listOfActivity[index].type.id === +this.routeToForm.controls.activityId.value) {
          addnewRoute.routeToName = this.listOfActivity[index].type.name;
        }
      }
      if (this.routeToForm.controls.recipientId.value === '-1' || this.routeToForm.controls.recipientId.value === -1) {
        addnewRoute.roleId = +this.routeToForm.controls.recipientId.value;
        addnewRoute.roleName = 'Manager';
      } else if (this.routeToForm.controls.recipientId.value === '-2' || this.routeToForm.controls.recipientId.value === -2) {
        addnewRoute.roleId = +this.routeToForm.controls.recipientId.value;
        addnewRoute.roleName = 'Initator';
      } else if (this.routeToForm.controls.recipientId.value === '-3' || this.routeToForm.controls.recipientId.value === -3) {
        addnewRoute.roleId = +this.routeToForm.controls.recipientId.value;
        addnewRoute.roleName = 'System';
      } else if (this.routeToForm.controls.recipientId.value === '-4' || this.routeToForm.controls.recipientId.value === -4) {
        addnewRoute.roleId = +this.routeToForm.controls.recipientId.value;
        addnewRoute.roleName = 'Self';
      } else if (this.routeToForm.controls.recipientId.value === '-5' || this.routeToForm.controls.recipientId.value === -5) {
        addnewRoute.roleId = +this.routeToForm.controls.recipientId.value;
        addnewRoute.roleName = 'Manual';
      } else if (this.routeToForm.controls.recipientId.value === '-6' || this.routeToForm.controls.recipientId.value === -6) {
        addnewRoute.roleId = +this.routeToForm.controls.recipientId.value;
        addnewRoute.roleName = 'Sub-ordinate';
      } else if (this.routeToForm.controls.recipientId.value === '-7' || this.routeToForm.controls.recipientId.value === -7) {
        addnewRoute.roleId = +this.routeToForm.controls.recipientId.value;
        addnewRoute.roleName = 'Sender';
      } else if (this.routeToForm.controls.recipientId.value === '-8' || this.routeToForm.controls.recipientId.value === -8) {
        addnewRoute.roleId = +this.routeToForm.controls.recipientId.value;
        addnewRoute.roleName = 'None';
      } else if (this.routeToForm.controls.recipientId.value === '-100' || this.routeToForm.controls.recipientId.value === -100) {
        for (let index = 0; index < this.listOfRoles.length; index++) {
          if (this.listOfRoles[index].id === +this.routeToForm.controls.roles.value) {
            addnewRoute.roleName = this.listOfRoles[index].name;
            addnewRoute.roleId = this.listOfRoles[index].id;
          }
        }
      } else if (this.routeToForm.controls.recipientId.value === '-101' || this.routeToForm.controls.recipientId.value === -101) {
        for (let index = 0; index < this.listOfRolesList.length; index++) {
          if (this.listOfRolesList[index].id === +this.routeToForm.controls.roles.value) {
            addnewRoute.roleName = this.listOfRolesList[index].name;
            addnewRoute.roleId = this.listOfRolesList[index].id + 9000000;
          }
        }
      }
      addnewRoute.purpose = null;
      addnewRoute.finishForm = this.routeToForm.controls.template.value;
      this.selectedRouteTo.push(addnewRoute);
      this.routeToForm.reset();
      this.routeToForm.patchValue({
        respoId: this.responseTypeList[0].id,
        recipientId: -1,
        activityId: this.listOfActivity[0].type.id,
        purpose: null,
        comment: null,
        signEnabled: 0
      });
    }
  }

  openOrgModel() {
    this.modalService.getModal('organizationList').open();
  }

  addOrganizationModel(tree) {
    this.selectedNode = tree;
  }


  patchOrganizationName() {
    if (this.selectedNode !== null) { // changed
      if (this.selectedNode) {
        this.workTypeForm.patchValue({ orgUnit: this.selectedNode.id, orgName: this.selectedNode.label });
      } else {
        this.workTypeForm.patchValue({ orgUnit: 0, orgName: 'HashECM Technologies Pvt Ltd' });
      }
    }


  }

  validateJSON() {
    this.spinner.show();
    this.schemaService.validateDraftWorkType(this.selectedWorkType.id).subscribe(data => { this.validateWorktype(data); this.spinner.hide(); }, error => { this.tr.error('', 'Invalid Definition'); this.spinner.hide(); });
  }

  validateWorktype(data) {
    this.tr.success('', 'Valid Definition');
  }

  commitJSON() {
    this.spinner.show();
    this.schemaService.commitDraftWorkType(this.selectedWorkType.id).subscribe(data => { this.commitWorkType(data); this.spinner.hide(); }, error => { this.spinner.hide(); this.tr.error('', 'Worktype couldn\'t be committed'); });
  }

  workTypeCommit() {
    this.schemaService.commitWorkType(this.selectedwList.docId).subscribe(data => this.commitWorkType(data), error => this.tr.error('', 'Worktype couldn\'t be committed'));
  }

  commitWorkType(data) {
    this.tr.success('', 'Worktype committed');
    this.schemaService.getWorkTypesForEdit().subscribe(dataRes => this.getWorkType(dataRes), error => { });
  }

  roleSelected(rolename) {
    const role = new Roles();
    role.id = rolename.id;
    role.name = rolename.name;
    role.orgId = rolename.orgId;
    role.orgName = rolename.orgName;
    role.privacy = rolename.privacy;
    role.adGroup = rolename.adGroup;
    role.type = this.permissionAccessType;
    let flag = 0;
    for (let response = 0; response < this.responsedPermissionRoles.length; response++) {
      if (role.id === this.responsedPermissionRoles[response].id) {
        flag = 1;
        break;
      }
    }
    for (let index = 0; index < this.selectedRoleList.length; index++) {
      if (role.id === this.selectedRoleList[index].id) {
        flag = 1;
        break;
      }
    }
    if (flag === 0) {
      this.selectedRoleList.push(role);
      this.rolenameSelected = null;
    } else {
      this.tr.warning('', 'Role already present');
    }
  }

  roleListSelected(rolename) {
    const role = rolename;
    const id = role.id * -1;
    role.id = id;
    role.type = this.permissionAccessType;
    let flag = 0;
    for (let response = 0; response < this.responsedPermissionRoles.length; response++) {
      if (role.id === this.responsedPermissionRoles[response].id) {
        flag = 1;
        break;
      }
    }
    for (let index = 0; index < this.selectedRoleList.length; index++) {
      if (role.id === this.selectedRoleList[index].id) {
        flag = 1;
        break;
      }
    }
    if (flag === 0) {
      this.selectedRoleList.push(role);
      this.roleListNameSelected = null;
    } else {
      this.tr.warning('', 'Role already present');
    }
  }

  removeSelectedRole(value) {
    this.showAlertToDeleteRole = false;
    for (let index = 0; index < this.selectedRoleList.length; index++) {
      if (value.id === this.selectedRoleList[index].id) {
        this.selectedRoleList.splice(index, 1);
      }

    }
  }
  removeResponseSelectedRole(response) {
    this.okToDelete = false;
    this.okToDelete = false;
    this.spinner.show();
    this.schemaService.removeWorkTypeRoles(response, this.selectedWorkType.objId).subscribe(data => {
      this.spinner.hide();
      this.schemaService.getWorkTypeRoles(this.selectedWorkType.objId).subscribe(dataRes => { this.workTypePermissionRoles(dataRes); this.spinner.hide(); }, error => { this.spinner.hide(); });
      this.tr.success('Role deleted');

    },
      error => {
        this.spinner.hide();
      });
  }

  initialActivityClicked(id) {
    this.initActivityTypeId = id;
  }

  deleteListActivity(i) {
    this.listOfActivity.splice(i, 1);
    this.deleteType = undefined;
    this.deleteIndex = -1000000;
  }

  initialRouteClicked(id) {
    this.initRoleId = id;
  }

  recipientIdChanged() {
    this.routeToForm.patchValue({
      roles: null
    });
  }

  initActivityTypeChanged(event) {
    let listOfBegineActivity;
    for (let index = 0; index < this.listOfActivity.length; index++) {
      if (this.listOfActivity[index].type.id === +this.workTypeForm.controls.initActivityType.value) {
        listOfBegineActivity = this.listOfActivity[index];
      }
    }
    this.listOfInitalRoute = [];
    if (listOfBegineActivity[0].type.responses.length > 0) {
      for (let j = 0; j < listOfBegineActivity[0].type.responses.length; j++) {
        this.listOfInitalRoute.push(listOfBegineActivity[0].type.responses[j]);
      }
    }
  }

  editActivityTabClicked(event) {
    if (event.tabTitle !== 'Activity') {
      this.isActivityActive = false;
    }
  }

  purposeChanged() {
    if (this.routeToForm.controls.purpose.value === 'EMAIL') {
      this.schemaService.getTemplates().subscribe(data => this.getTemplates(data));
    }
  }

  getTemplates(data) {
    const value = JSON.parse(data._body);
    this.listOftemplates = [];
    value.forEach(element => {
      const templateVal = {
        label: element.symName,
        value: element.docId
      };
      this.listOftemplates.push(templateVal);
    });
  }

  templateIdChanged() {
    for (let index = 0; index < this.listOftemplates.length; index++) {
      if (this.listOftemplates[index].value === this.workTypeForm.controls.templateId.value) {
        this.workTypeForm.patchValue({
          template: this.listOftemplates[index].label
        });
      }
    }
  }

  showClassChanged(event) {
    if (event.target.value === 'on') {
      this.showDocClass = true;
      this.documentAddFormModel.patchValue({ docClass: this.getDocClassResult[0].symName });
    } else {
      this.showDocClass = false;
    }
  }
  permissions(wList, permissionsAdd) {
    this.spinner.show();
    this.addPermissionRefreshed();
    this.schemaService.getWorkTypeRoles(wList.objId).subscribe(data => { this.workTypePermissionRoles(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
    this.selectedWorkType = wList;
    this.modalService.getModal('permissionsAdd').open();
  }
  addPermissionRefreshed() {
    this.selectedRoleList = [];
    this.responseTypeList = [];
    this.showAlertToDeleteRole = false;
    this.okToDelete = false;
    this.permissionAccessType = 0;
  }
  workTypePermissionRoles(data) {
    this.selectedRoleList = [];
    this.responsedPermissionRoles = [];
    this.responsedPermissionRoles = JSON.parse(data._body);
  }
  addPermissions() {
    if (this.selectedRoleList.length > 0) {
      const workType = {
        id: this.selectedWorkType.objId,
        roleid: this.selectedRoleList[this.value].id,
        type: this.selectedRoleList[this.value].type,
      };
      this.schemaService.addWorkTypeRoles(workType).subscribe(data => {
        if (this.value === (this.selectedRoleList.length - 1)) {
          this.value = 0;
          this.reloadThisPage(data);
        } else {
          this.value++;
          this.addPermissions();
        }
      });
    } else {
      this.tr.warning('Select atleast one role to add');
    }
  }
  reloadThisPage(data) {
    this.tr.success('', 'Worktype permission added');
    this.ngOnInit();
  }

  downloadWorkType(wList) {
    this.schemaService.exportWorkType(wList.objId).subscribe(data => this.downloadCurrentDocument(data));
  }

  importNewWorkType(worktypeUpload) {
    this.modalService.getModal('worktypeUpload').open();
  }

  submitImportDoc() {
    if (this.defid.queue.length > 0) {
      const defLen = this.defid.queue.length - 1;
      const string = this.defid.queue[defLen].file.name.split('.')[1];
      if (string === 'pti') {
        const docInfo = {
          docclass: '',
          docTitle: this.defid.queue[defLen].file.name,
          props: [{
            'name': 'Document Title',
            'symName': 'DocumentTitle',
            'dtype': 'STRING',
            'mvalues': [this.defid.queue[defLen].file.name],
            'mtype': 'N',
            'len': 255,
            'rOnly': 'false',
            'hidden': 'false',
            'req': 'false'
          }],
        };
        const formData = new FormData();
        formData.append('DocInfo', JSON.stringify(docInfo));
        formData.append('file', this.defid.queue[defLen]._file);
        this.defid.clearQueue();
        this.spinner.show();
        this.schemaService.importWorkTypeDefinition(formData).subscribe(data => { this.imported(data); this.spinner.hide(); }, error => {
          console.log(error); this.tr.error('Incorrect worktype file selected');
          this.defid.clearQueue(); this.spinner.hide();
        });
      } else {
        this.tr.error('Please select only .pti file');
      }
    } else {
      this.tr.error('Please select worktype to import');
    }
  }
  imported(data) {
    this.tr.success('Import Success');
    this.ngOnInit();
  }

  updateNewworkType(wList, checkinWorktype) {
    this.checkOutFile = wList;
    this.modalService.getModal('checkinWorktype').open();
  }

  checkedOut(data, checkinWorktype) {
    this.ds.checkOut(this.checkOutFile.docId).subscribe(dataRes => this.updateDocument(dataRes));
  }

  updateDocument(data) {
    if (this.checkIn.queue.length > 0) {
      const docInfo = {
        docclass: '',
        docTitle: this.checkIn.queue[0].file.name,
        id: this.checkOutFile.docId,
        props: [{
          'name': 'Document Title',
          'symName': 'DocumentTitle',
          'dtype': 'STRING',
          'mvalues': [this.checkIn.queue[0].file.name],
          'mtype': 'N',
          'len': 255,
          'rOnly': 'false',
          'hidden': 'false',
          'req': 'false'
        }],
      };
      const formData = new FormData();
      formData.append('DocInfo', JSON.stringify(docInfo));
      formData.append('file', this.checkIn.queue[0]._file);
      this.checkIn.clearQueue();
      this.spinner.show();
      this.ds.checkIn(formData).subscribe(dataRes => { this.workTypeUpdated(dataRes); this.spinner.hide(); }, error => {
        console.log(error); this.tr.error('Incorrect worktype file selected');
        this.checkIn.clearQueue(); this.spinner.hide(); this.ds.cancelCheckOut(this.checkOutFile.docId).subscribe();
      });
    } else {
      this.tr.error('Please select worktype to import');
    }
  }

  workTypeUpdated(data) {
    this.tr.success('', 'Worktype updated');
    this.ngOnInit();
  }

  defIdfileChanged(event) {
    if (this.defid.queue[0].file.name.includes('.pti')) {
    } else {
      this.tr.error('', 'Invalid file type, Please select only .pti files');
      this.defid.clearQueue();
      const file = <HTMLInputElement>document.getElementById('defIdVariable');
      file.value = '';
    }
  }
  permissionAccessTypeChanged(event) {
    this.permissionAccessType = +this.permissionAccessType;
  }

  deleteWorkType() {
    this.modalService.getModal('deleteWorkTypeAlert').open();

  }

  deleteInactivateWorkType(wList, deleteInactiveWorkType) {
    this.selectedwList = wList;
    this.modalService.getModal('deleteInactiveWorkType').open();

  }

  workTypeDelete() {
    if (this.selectedwList.objId > 0) {
      this.schemaService.inactiveWorkType(this.selectedwList.objId).subscribe(data => this.inactiveWorkType(data), error => this.tr.error('', 'Worktype couldn\'t be deleted'));
    } else {
      this.workTypeInactiveDelete();
    }
  }

  workTypeInactiveDelete() {
    this.schemaService.inactiveDraftWorkType(this.selectedwList.docId).subscribe(data => this.inactiveWorkType(data), error => this.tr.error('', 'Worktype couldn\'t be deleted'));
  }

  inactiveWorkType(data) {
    this.tr.success('', 'Worktype Deleted');
    this.ngOnInit();
  }
  addWorkTypeSymName(event) {
    const value = (event.target.value).toLowerCase();
    const symNameValue = value.replace(/[^a-z]/g, '');
    if (this.isAdd) {
      this.workTypeName.patchValue({ symName: 'WT_' + symNameValue });
      this.workTypeForm.patchValue({ symName: 'WT_' + symNameValue });
    }
  }
  addActivityTypeSymName(event) {
    const value = (event.target.value).toLowerCase();
    const symNameValue = value.replace(/[^a-z0-9_]/g, '');
    if (this.isAdd) {
      this.activityName.patchValue({ symName: symNameValue });
      this.activityName.patchValue({ symName: symNameValue });
    }
  }
  addActivitySymName(event) {
  }

  editRouteTo(routeTo, addNewRouteModel, ind) {
    console.log(routeTo);
    this.updateRoute = true;
    if (this.responseTypeList === undefined) {
      this.schemaService.getResponseTypes().subscribe(data => this.getResponseTypeData(data));
    }
    const routeEdit = routeTo;
    if (routeEdit.id !== undefined) {
      this.routeToForm.patchValue({
        respoId: routeEdit.id,
        activityId: routeEdit.routeToId,

        purpose: null,
        template: routeEdit.finishForm,
        comment: routeEdit.comment,
        signEnabled: routeEdit.signEnabled,
        id: routeEdit.id
      });
      if (routeEdit.roleId === 0) {
        this.routeToForm.patchValue({
          recipientId: -8,
        });
      } else {
        this.routeToForm.patchValue({
          recipientId: routeEdit.roleId,
        });
      }
      if (routeEdit.roleId > 0 && routeEdit.roleId < 9000000) {
        this.routeToForm.patchValue({
          recipientId: -100,
          roles: routeEdit.roleId
        });
      } else if (routeEdit.roleId > 9000000) {
        this.routeToForm.patchValue({
          recipientId: -101,
          roles: routeEdit.roleId - 9000000
        });
      }
    }
    this.modalService.getModal('addNewRouteModel').open();
  }

  getResponseTypeData(data) {
    this.responseTypeList = JSON.parse(data._body);
    const val1 = {
      label: 'Any',
      value: ''
    };
    this.responseDropDown.push(val1);
    this.responseTypeList.forEach(element => {
      const val = {
        label: element.name,
        value: element.id
      };
      this.responseDropDown.push(val);
    });
  }

  updateExistingRouetModel() {
    this.updateRoute = false;
    const addnewRoute = new Response();
    addnewRoute.id = +this.routeToForm.controls.respoId.value;
    addnewRoute.comment = this.routeToForm.controls.comment.value;
    if (this.routeToForm.controls.signEnabled.value) {
      addnewRoute.signEnabled = 1;
    } else {
      addnewRoute.signEnabled = 0;
    }
    for (let index = 0; index < this.responseTypeList.length; index++) {
      if (this.responseTypeList[index].id === +addnewRoute.id) {
        addnewRoute.name = this.responseTypeList[index].name;
      }
    }
    let flag = 0;
    for (let index = 0; index < this.selectedRouteTo.length; index++) {
      if (this.selectedRouteTo[index].name === addnewRoute.name) {
        flag = 1;
        break;
      }
    }
    addnewRoute.routeToId = this.routeToForm.controls.activityId.value;
    for (let index = 0; index < this.listOfActivity.length; index++) {
      if (this.listOfActivity[index].type.id === +this.routeToForm.controls.activityId.value) {
        addnewRoute.routeToName = this.listOfActivity[index].type.name;
      }
    }
    if (this.routeToForm.controls.recipientId.value === '-1' || this.routeToForm.controls.recipientId.value === -1) {
      addnewRoute.roleId = +this.routeToForm.controls.recipientId.value;
      addnewRoute.roleName = 'Manager';
    } else if (this.routeToForm.controls.recipientId.value === '-2' || this.routeToForm.controls.recipientId.value === -2) {
      addnewRoute.roleId = +this.routeToForm.controls.recipientId.value;
      addnewRoute.roleName = 'Initator';
    } else if (this.routeToForm.controls.recipientId.value === '-3' || this.routeToForm.controls.recipientId.value === -3) {
      addnewRoute.roleId = +this.routeToForm.controls.recipientId.value;
      addnewRoute.roleName = 'System';
    } else if (this.routeToForm.controls.recipientId.value === '-4' || this.routeToForm.controls.recipientId.value === -4) {
      addnewRoute.roleId = +this.routeToForm.controls.recipientId.value;
      addnewRoute.roleName = 'Self';
    } else if (this.routeToForm.controls.recipientId.value === '-5' || this.routeToForm.controls.recipientId.value === -5) {
      addnewRoute.roleId = +this.routeToForm.controls.recipientId.value;
      addnewRoute.roleName = 'Manual';
    } else if (this.routeToForm.controls.recipientId.value === '-6' || this.routeToForm.controls.recipientId.value === -6) {
      addnewRoute.roleId = +this.routeToForm.controls.recipientId.value;
      addnewRoute.roleName = 'Sub-ordinate';
    } else if (this.routeToForm.controls.recipientId.value === '-7' || this.routeToForm.controls.recipientId.value === -7) {
      addnewRoute.roleId = +this.routeToForm.controls.recipientId.value;
      addnewRoute.roleName = 'Sender';
    } else if (this.routeToForm.controls.recipientId.value === '-8' || this.routeToForm.controls.recipientId.value === -8) {
      addnewRoute.roleId = +this.routeToForm.controls.recipientId.value;
      addnewRoute.roleName = 'None';
    } else if (this.routeToForm.controls.recipientId.value === '-100' || this.routeToForm.controls.recipientId.value === -100) {
      addnewRoute.roleId = +this.routeToForm.controls.recipientId.value;
      for (let index = 0; index < this.listOfRoles.length; index++) {
        if (this.listOfRoles[index].id === +this.routeToForm.controls.roles.value) {
          addnewRoute.roleName = this.listOfRoles[index].name;
          addnewRoute.roleId = this.listOfRoles[index].id;
        }
      }
    } else if (this.routeToForm.controls.recipientId.value === '-101' || this.routeToForm.controls.recipientId.value === -101) {
      addnewRoute.roleId = +this.routeToForm.controls.recipientId.value;
      for (let index = 0; index < this.listOfRolesList.length; index++) {
        if (this.listOfRolesList[index].id === +this.routeToForm.controls.roles.value) {
          addnewRoute.roleName = this.listOfRolesList[index].name;
          addnewRoute.roleId = this.listOfRolesList[index].id * -1;
        }
      }
    }
    addnewRoute.purpose = null;
    addnewRoute.finishForm = this.routeToForm.controls.template.value;
    for (let index = 0; index < this.selectedRouteTo.length; index++) {
      if (this.selectedRouteTo[index].id === this.routeToForm.controls.id.value) {
        this.selectedRouteTo[index] = addnewRoute;
      }
    }

    this.routeToForm.reset();
    this.routeToForm.patchValue({
      respoId: this.responseTypeList[0].id,
      recipientId: -1,
      activityId: this.listOfActivity[0].type.id,
      purpose: null,
      comment: null,
      signEnabled: 0
    });
  }
  formPropReturn(value) {
    for (let i = 0; i < this.propertiesArray.length; i++) {
      if (this.propertiesArray[i].label === value) {
        return this.propertiesArray[i].value;
      }
    }
  }
  addMapping() {

    this.modalService.getModal('addMapAlert').open();
    if (this.workTypeForm.controls['mapping']) {
      const value = JSON.parse(this.workTypeForm.controls['mapping'].value);
      for (let i = 0; i < value.length; i++) {
        if (value[i].id === 1) {
          this.mapForm.patchValue({ mapProp1: this.formPropReturn(value[i].formprop) });
        }
        if (value[i].id === 2) {
          this.mapForm.patchValue({ mapProp2: this.formPropReturn(value[i].formprop) });
        }
        if (value[i].id === 3) {
          this.mapForm.patchValue({ mapProp3: this.formPropReturn(value[i].formprop) });
        }
        if (value[i].id === 4) {
          this.mapForm.patchValue({ mapProp4: this.formPropReturn(value[i].formprop) });
        }
      }
    }
  }
  addMapConfirmed() {
    const value = JSON.parse(this.workTypeForm.controls['mapping'].value);
    for (let i = 0; i < value.length; i++) {
      if (value[i].id === 1) {
        value[i].formprop = this.formPropMapLabelReturn(this.mapForm.controls['mapProp1'].value);
      }
      if (value[i].id === 2) {
        value[i].formprop = this.formPropMapLabelReturn(this.mapForm.controls['mapProp2'].value);
      }
      if (value[i].id === 3) {
        value[i].formprop = this.formPropMapLabelReturn(this.mapForm.controls['mapProp3'].value);
      }
      if (value[i].id === 4) {
        value[i].formprop = this.formPropMapLabelReturn(this.mapForm.controls['mapProp4'].value);
      }
    }
    console.log(JSON.stringify(value));
    this.workTypeForm.patchValue({ mapping: JSON.stringify(value) });
  }
  formPropMapLabelReturn(value) {
    for (let i = 0; i < this.propertiesArray.length; i++) {
      if (this.propertiesArray[i].value === value) {
        return this.propertiesArray[i].label;
      }
    }
  }
  addProperties() {
    this.modalService.getModal('addPropsAlert').open();
  }
  addPropsConfirmed() {
    let propVal;
    for (let i = 0; i < this.propertiesArray.length; i++) {
      if (this.propertiesArray[i].value === this.addSupPropesForm.value.subProps) {
        propVal = this.propertiesArray[i].label;
      }
    }
    const val = this.workTypeForm.value.subjectProps;
    const value = val + ',' + propVal;
    this.workTypeForm.patchValue({ subjectProps: value });
    this.addSupPropesForm.reset();
  }
  allToLowerCase(str) {
    if (str !== undefined) {
      return str.toLowerCase();
    } else {
      return str;
    }
  }
  addNewVersion() {
    this.modalService.getModal('addNewVersionAlert').open();
  }
  addNewVersionAlertConfirmed() {
    this.spinner.show();
    this.schemaService.addNewWorkTypeVersion(this.selectedWorkType.id).subscribe(data => { this.spinner.hide(); this.tr.success('Created New Version'); this.addedNewWrktypeVersion(data); }, error => { this.spinner.hide(); });
  }
  addedNewWrktypeVersion(data) {

  }
  deleteWorkTypeConfirmed() {
    this.spinner.show();
    this.schemaService.deleteWorkType(this.selectedWorkType).subscribe(data => { this.spinner.hide(); this.deletedWorkType(data); }, error => { this.spinner.hide(); });
  }
  deletedWorkType(data) {
    this.tr.success(this.selectedWorkType.name + ' is deleted');
    this.editWorkTypeToggle = false;
    this.callWorkTypes();
  }
  callWorkTypes() {
    this.spinner.show();
    this.schemaService.getWorkTypesForEdit().subscribe(data => { this.spinner.hide(); this.getWorkType(data); }, error => { this.spinner.hide(); });
  }
  addFilePatternChanged(event) {
    if (this.workTypeForm.controls.filePattern.value) {
      const value = this.workTypeForm.controls.filePattern.value.split('/');
      if (value.length > 0) {
        let pat = '';
        value.forEach(element => {
          if (pat === '') {
            pat = element;
          } else {
            pat = pat + '/' + element;
          }
        });
        pat = pat + '/' + event.value;
        this.workTypeForm.patchValue({ filePattern: pat, filePaternDropDown: null });
      }
    } else {
      this.workTypeForm.patchValue({ filePattern: event.value, filePaternDropDown: null });
    }
  }
  draftSupportChanged(event) {
    if (event === true) {
      this.workTypeForm.patchValue({ draftSupport: 1 });
    } else {
      this.workTypeForm.patchValue({ draftSupport: 0 });
    }
  }
  saveWorkType() {
    this.spinner.show();
    console.log(this.workTypeForm.value);
    this.schemaService.updateWorkType(this.workTypeForm.value).subscribe(data => { this.spinner.hide(); this.tr.success('Work Type is Updated'); }, error => { this.spinner.hide(); });
  }
  activityTypeChanged(event) {
    console.log(event);
  }
  nextScreenChanged(event) {
  }
  slaTypeChanged(event) {

  }
  recallChanged(event) {
    console.log(+event);
    const one = +event;
    console.log((!!one));
  }
  availExtChanged(event) {

  }
  updatePrimaryChanged(event) {

  }
  activityTypeSubmitted() {
    const activityType = new ActivityType();
    activityType.sla = this.activityTypeForm.controls.slaNumber.value;
    if (this.activityTypeForm.controls.canrecall.value === true || this.activityTypeForm.controls.canrecall.value === 1) {
      activityType.canRecall = 1;
    } else {
      activityType.canRecall = 0;
    }
    if (this.activityTypeForm.controls.availForExtUser.value === true || this.activityTypeForm.controls.availForExtUser.value === 1) {
      activityType.stayDraft = 1;
    } else {
      activityType.stayDraft = 0;
    }
    if (this.activityTypeForm.controls.updatePrimary.value === true || this.activityTypeForm.controls.updatePrimary.value === 1) {
      activityType.updatePrimary = 1;
    } else {
      activityType.updatePrimary = 0;
    }

    activityType.docTypes = this.activityInfoEdit.docTypes;
    activityType.instructions = this.activityTypeForm.controls.instruction.value;
    activityType.name = this.activityTypeForm.controls.name.value;
    activityType.symName = this.activityTypeForm.controls.symName.value;
    activityType.id = this.selectedActivity.type.id;
    activityType.nextScreen = this.activityTypeForm.controls.nextscreen.value;
    activityType.seqType = this.activityTypeForm.controls.activitytype.value,
      activityType.responses = this.activityInfoEdit.responses;
    activityType.routes = this.activityInfoEdit.routes;
    activityType.routeRules = this.activityInfoEdit.routeRules;
    activityType.sla = (this.activityTypeForm.controls.slaNumber.value); // + (+this.activityTypeForm.controls.slatype.value);
    activityType.ui = this.activityInfoEdit.ui;
    this.schemaService.updateActivityType(activityType).subscribe(data => { this.tr.success('Activity Info is Updated Successfully'); }, error => { });
    this.activeIndexForInfo = 0;
  }
  formAssociatedChanged(event) {
    for (let i = 0; i < this.listOfFormtemplate.length; i++) {
      if (this.listOfFormtemplate[i].docId === event.value) {
        this.documentTypeForm.patchValue({ templateName: this.listOftemplates[i].symName });
        break;
      }
    }
  }
  documentClassResult(data) {
    this.documentClassResultValue = JSON.parse(data._body);
    this.documentClassResultValue.forEach(element => {
      const val = {
        label: element.name,
        value: element.symName
      };
      this.documetClassDropDown.push(val);
    });
  }
  docClassNameChanged(event) {

  }
  primaryFlgChanged(event) {
    if (event === true) {
      this.documentTypeForm.patchValue({ primaryFlag: 1 });
    } else {
      this.documentTypeForm.patchValue({ primaryFlag: 0 });
    }
  }
  docTypeRequired(event) {
    if (event === true) {
      this.documentTypeForm.patchValue({ req: 1 });
    } else {
      this.documentTypeForm.patchValue({ req: 0 });
    }
  }
  docTypeEdit(event) {
    if (event === true) {
      this.documentTypeForm.patchValue({ showClass: 1 });
    } else {
      this.documentTypeForm.patchValue({ showClass: 0 });
    }
  }
  displayTypeChanged(event) {

  }
  clickedOnEditFormControls() {
    this.modalService.getModal('docTypeFormControlAlert').open();
  }
  docTypeSubmitted() {
    const value = {
      activityType: this.activityInfoEdit.id,
      id: this.documentTypeForm.value.id,
      name: this.documentTypeForm.value.name,
      type: this.documentTypeForm.value.type,
      signEnabled: this.documentTypeForm.value.signEnabled,
      primaryFlag: this.documentTypeForm.value.primaryFlag,
      req: this.documentTypeForm.value.req,
      template: this.documentTypeForm.value.template,
      templateName: this.documentTypeForm.value.templateName,
      docClass: this.documentTypeForm.value.docClass,
      showType: this.documentTypeForm.value.showType,
      showClass: this.documentTypeForm.value.showClass,
      formControl: this.documentTypeForm.value.formControl,
      keyId: this.documentTypeForm.value.keyId
    };
    if (this.documentTypeForm.controls.req.value === true) {
      value.req = 1;
    } else {
      value.req = 0;
    }
    // if (this.documentTypeForm.controls.primaryFlag.value === true) {
    //   value.primaryFlag = 1;
    // } else {
    //   value.primaryFlag = 0;
    // }
    if (this.documentTypeForm.controls.showClass.value === true) {
      value.showClass = 1;
    } else {
      value.showClass = 0;
    }
    if (this.selectedDocType === null) {
      this.schemaService.addDocTypeToActivityType(value).subscribe(data => { this.tr.success('New Document Type Added'); this.callActivityTypes(); this.showDocList = true; }, error => { });
    } else {
      this.schemaService.updateActivityTypeDocType(value).subscribe(data => { this.tr.success('Document Type Updated'); this.callActivityTypes(); this.showDocList = true; }, error => { });
    }

  }
  clickedOnEditRouteConditions() {
    this.modalService.getModal('routingConditionAlert').open();
    this.selectedRouteRuleConditionIndex = true;
  }

  clickedOnActions(value, index) {
    this.showActionsList = false;
    this.selectedAction = value;
    this.selectedActionsIndex = index;
    this.actionsForm.patchValue({
      action: value.action,
      id: value.id,
      param1: value.param1,
      param2: value.param2,
      param3: value.param3,
      param4: value.param4,
      responseId: value.response.id,
      responseName: value.response.name,
      responseValidate: value.response.validate,
      symName: this.activityInfoEdit.symName
    });
  }
  clickToAddNewAction() {
    this.showActionsList = false;
    this.selectedAction = null;
    this.selectedActionsIndex = -1;
  }
  actionSubmitted() {
    const value = {
      action: this.actionsForm.controls['action'].value,
      id: this.actionsForm.controls['id'].value,
      param1: this.actionsForm.controls['param1'].value,
      param2: this.actionsForm.controls['param2'].value,
      param3: this.actionsForm.controls['param3'].value,
      param4: this.actionsForm.controls['param4'].value,
      response: {
        id: this.actionsForm.controls['responseId'].value,
        name: this.actionsForm.controls['responseName'].value,
        validate: this.actionsForm.controls['responseValidate'].value
      },
      symName: this.activityInfoEdit.symName
    };
    if (this.selectedAction === null) {
      this.schemaService.addActionToActivityType(value).subscribe(data => {
        this.tr.success('Added New Action'); this.callActivityTypes(); this.showActionsList = true;
      }, error => { });
    } else {
      this.schemaService.updateActivityTypeAction(value).subscribe(data => {
        this.tr.success('Updated Action'); this.callActivityTypes(); this.showActionsList = true;
      }, error => { });
    }
  }
  clickedOnPermission(value, index) {
    this.showPermissionList = false;
    this.selectedPermission = value;
    this.selectedPermissionIndex = index;
    this.permissionForm.patchValue({
      id: this.workTypeActivity.id,
      accessType: value.accessType,
      accessMask: value.accessMask,
      granteeName: value.granteeName,
      granteeType: value.granteeType,
      permissionSource: value.permissionSource,
      inheritDepth: value.inheritDepth,
      accessLevel: value.accessLevel,
      action: value.action,
      depthName: value.depthName,
      roleId: value.roleId,
      aclId: value.aclId,
      objectId: value.objectId
    });
  }
  clickToAddNewPermission() {
    this.showPermissionList = false;
    this.selectedPermission = null;
    this.selectedPermissionIndex = -1;
  }
  permissionSubmitted() {
    const value = {
      id: this.workTypeActivity.id,
      accessType: this.permissionForm.controls['accessType'].value,
      accessMask: this.permissionForm.controls['accessMask'].value,
      granteeName: this.permissionForm.controls['granteeName'].value,
      granteeType: this.permissionForm.controls['granteeType'].value,
      permissionSource: this.permissionForm.controls['permissionSource'].value,
      inheritDepth: this.permissionForm.controls['inheritDepth'].value,
      accessLevel: this.permissionForm.controls['accessLevel'].value,
      action: this.permissionForm.controls['action'].value,
      depthName: this.permissionForm.controls['depthName'].value,
      roleId: this.permissionForm.controls['roleId'].value,
      aclId: this.permissionForm.controls['aclId'].value,
      // objectId: this.permissionForm.controls['objectId'].value,
      objectId: this.selectedWorkType.id
    };
    if (this.selectedPermission === null) {
      this.schemaService.addWorkTypePermission(value).subscribe(data => {
        this.tr.success('Added New Permission'); this.callWorkTypesFormId(); this.showPermissionList = true;
      }, error => { });
    } else {
      this.schemaService.updateWorkTypePermission(value).subscribe(data => {
        this.tr.success('Updated Permission'); this.callWorkTypesFormId(); this.showPermissionList = true;
      }, error => { });
    }
  }
  routesSubmitted() {
    const value = {
      id: this.routesForm.controls['id'].value,
      name: this.routesForm.controls['name'].value,
      activityType: this.activityInfoEdit.id,
      keyId: this.routesForm.controls['keyId'].value,
      purpose: this.routesForm.controls['purpose'].value,
      routeToId: this.routesForm.controls['routeToId'].value,
      routeToName: this.routesForm.controls['routeToName'].value,
      roleId: this.routesForm.controls['roleId'].value,
      roleName: this.routesForm.controls['roleName'].value,
      signEnabled: this.routesForm.controls['signEnabled'].value,
      comment: this.routesForm.controls['comment'].value
    };
    if (this.routesForm.controls['signEnabled'].value === true) {
      value.signEnabled = 1;
    } else {
      value.signEnabled = 0;
    }
    if (this.routesForm.controls['roleId'].value === -100) {
      value.roleId = this.routesForm.controls['roleSelectedId'].value;
      value.roleName = this.routesForm.controls['roleSelectedName'].value;
    } else if (this.routesForm.controls['roleId'].value === -101) {
      value.roleId = this.routesForm.controls['roleSelectedGrpId'].value;
      value.roleName = this.routesForm.controls['roleSelectedGrpName'].value;
    }
    if (this.selectedRoute === null) {
      this.schemaService.addResponseToActivityType(value).subscribe(data => {
        this.tr.success('New Routes Added'); this.callActivityTypes(); this.showRouteList = true;
      }, error => { });
    } else {
      this.schemaService.updateActivityTypeResponse(value).subscribe(data => {
        this.tr.success('Route Updated'); this.callActivityTypes(); this.showRouteList = true;
      }, error => { });
    }

  }
  routeRulesSubmitted() {
    const value = {
      id: this.routeRulesForm.controls['id'].value,
      response: {
        id: this.routeRulesForm.controls['responseId'].value,
        name: this.routeRulesForm.controls['responseName'].value,
        validate: this.routeRulesForm.controls['responseValidate'].value
      },
      routeCondtions: this.selectedRouteConditions,
      activityType: this.activityInfoEdit.id,
      roleId: this.routeRulesForm.controls['roleId'].value,
      routeToId: this.routeRulesForm.controls['routeToId'].value,
      routeToName: this.routeRulesForm.controls['routeToName'].value,
      condition: this.routeRulesForm.controls['condition'].value,
      routeRole: this.routeRulesForm.controls['routeRole'].value,
      routeRoleName: this.routeRulesForm.controls['routeRoleName'].value
    };
    if (this.routeRulesForm.controls['roleId'].value === -100) {
      value.routeRole = this.routeRulesForm.controls['roleSelectedId'].value;
      value.routeRoleName = this.routeRulesForm.controls['roleSelectedName'].value;
    } else if (this.routeRulesForm.controls['roleId'].value === -101) {
      value.routeRole = this.routeRulesForm.controls['roleSelectedGrpId'].value;
      value.routeRoleName = this.routeRulesForm.controls['roleSelectedGrpName'].value;
    }
    if (this.selectedRouteRules === null) {
      this.schemaService.addRouteRuleToActivityType(value).subscribe(data => {
        this.tr.success('New Route Rule Added'); this.callActivityTypes(); this.showRouteRulesList = true;
      }, error => { });
    } else {
      this.schemaService.updateActivityTypeRouteRule(value).subscribe(data => {
        this.tr.success('Route Rule Updated'); this.callActivityTypes(); this.showRouteRulesList = true;
      }, error => { });
    }
  }
  documentTypeChanged(event) {
    this.listOfDocumentTypes.forEach(element => {
      if (event.value === element.id) {
        this.documentTypeForm.patchValue({
          name: element.name,
          type: element.type
        });
        if (element.type !== 'FORM') {
          this.documentTypeForm.patchValue({
            template: null,
            templateName: ''
          });
        }
      }

    });
  }
  callActivityTypes() {
    this.schemaService.getActivityTypeFromID(this.selectedActivity.type.id).subscribe(dataRes => this.getActivityTypesInfo(dataRes), error => { });
  }
  roleResponseChanged(event) {
    this.responseTypeList.forEach(element => {
      if (event.value === element.id) {
        this.routesForm.patchValue({ name: element.name });
      }
    });
  }
  routeToActivityChanged(event) {
    this.listOfActivity.forEach(element => {
      if (event.value === element.id) {
        this.routesForm.patchValue({ routeToName: element.name });
      }
    });
  }
  roleIdChanged(event) {
    this.routeToRecipientList.forEach(element => {
      this.routesForm.patchValue({ roleName: element.label });
    });
    this.routesForm.patchValue({ roleSelectedId: null, roleSelectedName: '', roleSelectedGrpId: null, roleSelectedGrpName: '' });
    if (event.value === -100) {
      this.routesForm.controls['roleSelectedId'].enable();
    } else {
      this.routesForm.controls['roleSelectedId'].disable();
    }

    if (event.value === -101) {
      this.routesForm.controls['roleSelectedGrpId'].enable();
    } else {
      this.routesForm.controls['roleSelectedGrpId'].disable();
    }

  }
  roleChanged(event) {
    this.rolesDropDown.forEach(element => {
      if (event.value === element.value) {
        this.routesForm.patchValue({ roleSelectedName: element.name });
      }
    });
  }
  roleGrpChanged(event) {
    this.recipientGroupDropDown.forEach(element => {
      if (event.value === element.value) {
        this.routesForm.patchValue({ roleSelectedGrpName: element.label });
      }
    });
  }
  routeRoleIdChanged(event) {
    this.routeToRecipientList.forEach(element => {
      this.routeRulesForm.patchValue({ routeRoleName: element.label });
    });
    this.routeRulesForm.patchValue({ roleSelectedId: null, roleSelectedName: '', roleSelectedGrpId: null, roleSelectedGrpName: '' });
    if (event.value === -100) {
      this.routeRulesForm.controls['roleSelectedId'].enable();
    } else {
      this.routeRulesForm.controls['roleSelectedId'].disable();
    }

    if (event.value === -101) {
      this.routeRulesForm.controls['roleSelectedGrpId'].enable();
    } else {
      this.routeRulesForm.controls['roleSelectedGrpId'].disable();
    }

  }
  routeRoleChanged(event) {
    this.rolesDropDown.forEach(element => {
      if (event.value === element.value) {
        this.routeRulesForm.patchValue({ roleSelectedName: element.name });
      }
    });
  }
  routeRoleoleGrpChanged(event) {
    this.recipientGroupDropDown.forEach(element => {
      if (event.value === element.value) {
        this.routeRulesForm.patchValue({ roleSelectedGrpName: element.label });
      }
    });
  }
  deleteRouteRule() {
    this.schemaService.deleteActivityTypeRouteRule(this.selectedRouteRules).subscribe(data => {
      this.tr.success('Deleted Route Rule'); this.callActivityTypes();
    }, error => { });
  }
  routeRulesResponseIdChanged(event) {
    this.responseTypeList.forEach(element => {
      if (event.value === element.id) {
        this.routeRulesForm.patchValue({ name: element.name });
      }
    });
  }
  addRouteConditionSubmitted() {
    const value = {
      key: this.addRouteConditionsForm.controls['key'].value,
      dtype: this.addRouteConditionsForm.controls['dtype'].value,
      oper: this.addRouteConditionsForm.controls['oper'].value,
      value: this.addRouteConditionsForm.controls['value'].value,
      id: this.addRouteConditionsForm.controls['id'].value,
      ruleId: this.selectedRouteRules.id,
      property: this.addRouteConditionsForm.controls['property'].value
    };
    if (this.selectedRouteConditionOption === null) {
      this.schemaService.addConditionToRouteRule(value).subscribe(data => {
        this.tr.success('New Route Condition Added'); this.callActivityTypes();
      }, error => { });
    } else {
      this.schemaService.updateRouteRuleCondition(value).subscribe(data => {
        this.tr.success('Condition Updated'); this.callActivityTypes();
      }, error => { });
    }

  }
  clickToAddNewRouteCondition() {
    this.selectedRouteConditionOption = null;
    this.selectedRouteRuleConditionIndex = false;
  }
  clickedOnRouteCondtionOpt(value, index) {
    this.selectedRouteConditionOption = value;
    this.selectedRouteRuleConditionIndex = false;
    this.addRouteConditionsForm.patchValue({
      key: value.key,
      dtype: value.dtype,
      oper: value.oper,
      value: value.value,
      ruleId: value.ruleId,
      id: value.id,
      property: value.property
    });
  }
  deleteRouteAddedCondition() {
    this.schemaService.deleteRouteRuleCondition(this.selectedRouteConditionOption).subscribe(data => {
      this.tr.success('Deleted Route Condition'); this.callActivityTypes();
    }, error => { });
  }
  reponseActionChanged(event) {
    this.responseTypeList.forEach(element => {
      if (event.value === element.id) {
        this.actionsForm.patchValue({ responseName: element.name });
      }
    });
  }
  deleteAction() {
    this.schemaService.deleteActivityTypeAction(this.selectedAction.id).subscribe(data => {
      this.tr.success('Action Deleted'); this.callActivityTypes();
    }, error => { });
  }
  deletePermission() {
    this.schemaService.deleteWorkTypePermission(this.selectedPermission).subscribe(data => {
      this.tr.success('Permission Deleted'); this.callWorkTypesFormId();
    }, error => { });
  }
  permissionResponseChanged(event) {
    this.responseTypeList.forEach(element => {
      if (event.value === element.id) {
        this.permissionForm.patchValue({ granteeName: element.name, roleId: event.value });
      }
    });
  }
  permissionRoleGroupChanged(event) {
    this.recipientGroupDropDown.forEach(element => {
      if (event.value === element.value) {
        this.permissionForm.patchValue({ granteeName: element.label, roleId: event.value });
      }
    });
  }
  permissionDropDownChanged(event) {
    this.permissionDropDown.forEach(element => {
      if (event.value === element.value) {
        this.permissionForm.patchValue({ accessLevel: element.label });
      }
    });
  }
  updateDocTypeFormControl() {
    const value = JSON.parse(this.selectedDocType.formControl);
    value[this.docTypeFormControlSelectedIndex].value = this.docTypeFormControlForm.controls['value'].value;
    this.documentTypeForm.patchValue({ formControl: JSON.stringify(value) });
    this.tr.success('Form Control Updated');
    this.docTypeFormControlList = true;
    this.docTypeFormControlSelectedIndex = -1;

  }
  clickedOnDocTypeFormControl(value, index) {
    this.docTypeFormControlList = false;
    this.docTypeFormControlSelectedIndex = index;
    this.selectedDocTypeFormControl = value;
    this.docTypeFormControlForm.patchValue({
      key: value.key,
      value: value.value
    });
  }
}
