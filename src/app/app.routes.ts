import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { SigninComponent } from './components/app-components/signin/signin.component';
import { AdminLayoutComponent } from './components/app-components/layout/admin-layout/admin-layout.component';
import { AuthGuard } from './service/auth.guard';
import { UsersComponent } from './components/app-components/users/users.component';
import { OrgunitsComponent } from './components/app-components/orgunits/orgunits.component';
import { RolesComponent } from './components/app-components/roles/roles.component';
import { RolesListComponent } from './components/app-components/roles-list/roles-list.component';
import { DelegationsComponent } from './components/delegations/delegations.component';
import { DocumentTypesComponent } from './components/document-types/document-types.component';
import { ResponseTypesComponent } from './components/response-types/response-types.component';
import { TemplatesComponent } from './components/app-components/templates/templates.component';
import { RepositoryComponent } from './components/repository/repository.component';
import { PropertyTemplateComponent } from './components/property-template/property-template.component';
import { ClassesComponent } from './components/classes/classes.component';
import { RootFoldersComponent } from './components/root-folders/root-folders.component';
import { RecycleBinComponent } from './components/recycle-bin/recycle-bin.component';
import { ConfiguarationsComponent } from './components/configuarations/configuarations.component';
import { TranslationsComponent } from './components/translations/translations.component';
import { LicenseComponent } from './components/license/license.component';
import { ReportsComponent } from './components/reports/reports.component';
import { FilterFormsComponent } from './components/app-components/filter-forms/filter-forms.component';
import { FormsComponent } from './components/forms/forms.component';
import { WorkTypesComponent } from './components/work-types/work-types.component';


export const routes: Routes = [
  { path: '', component: SigninComponent },
  {
    path: '',
    component: AdminLayoutComponent, canActivate: [AuthGuard],
    //  canDeactivate:[CanDeactivateGuard],
    children: [
      // {path: 'admin', loadChildren: './components/app components/admin/admin.module#AdminModule', canActivateChild: [AuthGuard]},
      { path: 'orgunits', component: OrgunitsComponent },
      { path: 'users', component: UsersComponent },
      { path: 'roles', component: RolesComponent },
      { path: 'rolesList', component: RolesListComponent },
      { path: 'delegate', component: DelegationsComponent },
      { path: 'docTypes', component: DocumentTypesComponent },
      { path: 'responseTypes', component: ResponseTypesComponent },
      { path: 'workTypes', component: WorkTypesComponent },
      { path: 'templates', component: TemplatesComponent },
      { path: 'repository', component: RepositoryComponent },
      { path: 'propTemplate', component: PropertyTemplateComponent },
      { path: 'classes', component: ClassesComponent },
      { path: 'rootFolders', component: RootFoldersComponent },
      { path: 'recycleBin', component: RecycleBinComponent },
      { path: 'configuarations', component: ConfiguarationsComponent },
      { path: 'translations', component: TranslationsComponent },
      { path: 'license', component: LicenseComponent },
      { path: 'reports', component: ReportsComponent },
      { path: 'forms', component: FormsComponent },
      { path: 'filterForms', component: FilterFormsComponent }
    ]
  }
];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes);
