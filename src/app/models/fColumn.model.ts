import { FormProps } from './formProps.model';

export class Fcolumn {
    // public unique: Number;
    public heading: String;
    public width: String;
    public properties: FormProps[];
}
