export class Response {
    public routeToId: number;
    public routeToName: String;
    public roleId: number;
    public roleName: String;
    public signEnabled: number;
    public comment: String;
    public name: string;
    public id: number;
    public purpose: string;
    public finishForm: string;
}
