export class DocumentType {
    public id: number;
    public name: String;
    public type: String;
    public req: number;
    public template: String;
    public templateName: String;
    public signEnabled: number;
    public primaryFlag: number;
    public showClass: number;
    public docClass: string;
    public formControl: string;
}
