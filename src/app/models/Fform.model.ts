import { Fsection } from './Fsection.model';
import { Fdatatable } from './Fdatatable.model';

export class Fform {
    public id: Number;
    public name: String;
    public label: String;
    public formTemplate: String;
    public sections: Fsection[];
    public actions = [];
    public datatable: Fdatatable[];
}
