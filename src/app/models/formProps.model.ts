import { Flookup } from './Flookup.model';
export class FormProps {
    // public unique: Number;
    public name: String;
    public label: String;
    public value: Flookup[];
    public type: String;
    public length: Number;
    public rOnly = 'FALSE';
    public req = 'FALSE';
    public visible = 'TRUE';
    // public lookups = [];
    public width: String;
    public action: String;
    // public dblookup = {
    //   dburl: String,
    //   dbtype: String,
    //   user: String,
    //   password: String,
    //   sql: String,
    //   filter: String,
    //   namecol: String,
    //   valuecol: String
    // };
    public isDefaultDB = true;
    // public autofill = {
    //   dburl: String,
    //   dbtype: String,
    //   user: String,
    //   password: String,
    //   sql: String,
    //   fillprops: [],
    //   fillparams: [],
    // }

    // public condition = [];
    public dbvalue = [
        { id: Number },
        { itemName: String }
    ];
    // public calc = [];
    // public colcalc = {
    //   type: '',
    //   label: ''
    // };
    public actions = [

    ];
    public setVisible: {
        val: '',
        show: [null],
        hide: [null],
        ronly: [null],
        hidesec: [null],
        readonlysec: [null]
    };
    public uitype = {
        uitype: String,
        lookups: [],
        dblookup: {
            dburl: String,
            dbtype: String,
            user: String,
            password: String,
            sql: String,
            filter: String,
            namecol: String,
            valuecol: String
        },
        condition: [],
        autofill: {
            dburl: String,
            dbtype: String,
            user: String,
            password: String,
            sql: String,
            fillprops: [],
            fillparams: [],
        },

        calc: [],
        colcalc: {
            type: '',
            label: '',
            copyto: ''
        },
        // copytoProp: ''
    };
    public prop = {};
    public colcalcType: String;
    constructor() {

    }
}
