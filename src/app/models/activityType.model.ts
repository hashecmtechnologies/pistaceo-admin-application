import { Roles } from './roles.model';
import { DocumentType } from './documentType.model';
import { Response } from './Response.model';

export class ActivityType {

    public id: number;
    public name: string;
    public symName: string;
    public canRecall: number;
    public instructions: string;
    public seqType: string;
    public ui: any;
    public updatePrimary: number;
    public availForExtUser: number;
    public nextScreen: String;
    public stayDraft: number;
    public sla: number;
    public workType: any;
    public responses: Response[];
    public docTypes: DocumentType[];
    public roles: Roles[];
    public routes = [];
    public routeRules = [];

}
