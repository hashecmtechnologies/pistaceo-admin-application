import { ActivityType } from './ActivityType.model';
import { Roles } from './roles.model';

export class WorkActivityType {
    public id: number;
    public type: ActivityType;
    public mandatory = 0;
    public seqNo: string;
    public routeTo: Roles;
    public workStatus: string;

}
