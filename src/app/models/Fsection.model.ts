import { Fcolumn } from './Fcolumn.model';

export class Fsection {
    public heading: String;
    public name: String;
    public type: String;
    public showHeader = 'TRUE';
    public visible = 'TRUE';
    public rOnly = 'FALSE';
    public columns: Fcolumn[];
    public addRows = 'TRUE';
    public rows = [];
}
