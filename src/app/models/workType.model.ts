import { WorkActivityType } from './workActivityType.model';
import { Roles } from './roles.model';

export class WorkType {
    public id: number;
    public name: string;
    public symName: string;
    // public fixedFlow = 0;
    public initActivityType: number;
    public initRoute: number;
    public draftSupport: number;
    public archiveSupport: number;
    public multiFormSupport: number;
    public registerSupport: number;
    public registerOnCreate: number;
    public subjectProps: string;
    public template: String;
    public orgUnit: string;
    public refNoPrefix: string;
    public orgName: string;
    public roles: Roles[];
    public activityTypes: WorkActivityType[];
    public mapping: string;
    public filePattern: String;
    public filePaternDropDown: String;
    public permissions = [];
}
