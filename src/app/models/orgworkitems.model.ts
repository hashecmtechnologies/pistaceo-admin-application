export interface OrgWorkItms {
    id;
    subject;
    typeId;
    typeName;
    createdOn;
    createdBy;
    createdUserName;
    creatorRoleId;
    creatorRoleName;
    status ;
    refNo ;
    finishedOn ;
    pendingWith ;
    lastActionBy ;
    priorityId;
}
