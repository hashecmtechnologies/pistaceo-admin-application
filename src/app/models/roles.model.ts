export class Roles {
    public id: number;
    public name: string;
    public orgId: number;
    public adGroup: string;
    public privacy: number;
    public orgName: string;
    public type: number;
}
