import { Component, OnInit, OnChanges, AfterViewInit, ViewChild, Input, DoCheck, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DocumentService } from '../../../service/document.service';
import { SignatureComponent } from '../signature-pad/signature-pad.component';
import { MenuItem, Paginator } from 'primeng/primeng';
import { saveAs } from 'file-saver';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { BreadcrumbService } from '../../app-components/layout/breadcrumb/breadcrumb.service';
import { UserService } from '../../../service/user.service';
import { ToastrService } from 'ngx-toastr';
import { ContentService } from '../../../service/content.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-document-annotation',
  templateUrl: './document-annotation.component.html',
  styleUrls: ['./document-annotation.component.css']
})
export class DocumentAnnotationComponent implements OnChanges, AfterViewInit {

  @Input() docId;
  @Input() documentType;
  @Input() currentPage;
  @Input() annotationType;
  @Input() viewType;
  @Output() pageChangeEmit =  new EventEmitter();
  @Output() documentTypeChangeEmit =  new EventEmitter();
  @Output() completeActivityEmit =  new EventEmitter();
  @Input() timeStamp;
  @ViewChild('signature') signaturePad: SignatureComponent;
  @ViewChild('ppaginatoro') ppaginatoro: Paginator;
  @ViewChild('ppaginatort') ppaginatort: Paginator;
  @ViewChild('ppaginatorth') ppaginatorth: Paginator;

  public pageBackCount = 0;
  public PaginatorcurrentPage;
  public annotationList;
  public annotationTypeSelected;
  public previewImage;
  public imageToShow = '';
  public totalPages;
  public displayImage = true;
  public canvasWidth;
  public allSavedAnnotaion = [];
  public usernamedropdownText = [];
  public annotationStatusdropdownText = [];
  public usernamedropdownImage = [];
  public imageContext: MenuItem[];
  public textContext: MenuItem[];
  public imageannotationURL= [];
  public hideheading = true;
  public signatureIsEmpty = false;
  public blocks = [];
  public inBounds = true;
  public myBounds = {
    width: 200,
    height: 200
  };
  public edge = {
    top: true,
    bottom: true,
    left: true,
    right: true
  };
  public movingOffset = { x: 0, y: 0 };
  public endOffset = { x: 0, y: 0 };
  public annotationid = 1;
  public offsetX;
  public offsetY;
  public annotationStatusSelected;
  public statusSelected = '';
  public userSelected = '';
  public statusList = [];
  public userList = [];
  public hideAllAnnotation = false;
  public signUrl;
  public listOfDcumentAnnotation = [];
  public signatureDisabled;

  public imageAnnotationDisplay: boolean;
  public textAnnotationDisplay: boolean;

  constructor(private translate: TranslateService, private ds: DocumentService, private location: Location, public ngxSmartModal: NgxSmartModalService,
    public us: UserService, private tr: ToastrService, private cs: ContentService, private router: Router,
    private route: ActivatedRoute, public breadcrumbService: BreadcrumbService, private spinnerService: Ng4LoadingSpinnerService ) {
      this.imageToShow = '';
      // this.route.queryParams.subscribe(p => {this.ngOnChanges(); });
    this.annotationList = [
      {label: this.translate.instant('Image annotation'), value: 'imageAnnotation'},
      {label:  this.translate.instant('Text annotation'), value: 'textAnnotation'}
    ];

    this.imageContext = [
      {
          label: this.translate.instant('Download'),
          icon: 'ui-icon-file-download',
          command: (event: any) =>   this.downloadThisdocument()
      },
      {
          label: this.translate.instant('With annotation'),
          icon: 'ui-icon-file-download',
          command:  (event: any) => this.downloadannotedDoc()
      }
  ];
  }

  ngOnChanges() {
    this.spinnerService.show();
    setTimeout(() => {
      this.canvasWidth = document.getElementById('myP').clientWidth - 20;
    }, 10);
    this.pageBackCount = -1;
    this.imageAnnotationDisplay = false;
    this.textAnnotationDisplay = false;
    this.imageToShow = '';
    this.imageannotationURL = [];
    if (this.documentType === 'readonly') {
      this.signatureDisabled = 'readonly';
    }
   if ( this.documentType !== 'annotate') {
    this.textContext = [
      {
          label: this.translate.instant('Download'),
          icon: 'ui-icon-file-download',
          command: (event: any) =>   this.downloadThisdocument()
      },
      {
          label: this.translate.instant('With annotation'),
          icon: 'ui-icon-file-download',
          command:  (event: any) => this.downloadannotedDoc()
      }
  ];
   }else {
    this.textContext = [
      {
          label: this.translate.instant('Download'),
          icon: 'ui-icon-file-download',
          command: (event: any) =>   this.downloadThisdocument()
      },
      {
          label: this.translate.instant('With annotation'),
          icon: 'ui-icon-file-download',
          command:  (event: any) => this.downloadannotedDoc()
      }, {
        label: this.translate.instant('Add Annotation'),
        icon: 'ui-icon-note',
        command:  (event: any) => this.addTextAnnotationPosition(event)
    }
  ];
   }
   if (this.annotationType === 'IMAGE') {
    this.textAnnotationDisplay = false;
  //  this.signaturePad.clear(); // invoke functions from szimek/signature_pad API
    // this.signaturePad.fromDataURL(this.imageannotationURL);
    // document.getElementById('textAnnotation').style.display = 'none';
    // document.getElementById('imageAnnotation').style.display = 'block';

    this.annotationTypeSelected = 'imageAnnotation';
    setTimeout(() => {
      this.imageAnnotationDisplay = true;
    }, 1000);
   
    if (this.docId !== undefined) {
      this.ds.getAnnotations(this.docId).subscribe( resdata => this.getAllAnnotation(resdata), error => this.spinnerService.hide() );
    }
   }else {
    if (this.docId !== undefined) {
      this.ds.getAnnotations(this.docId).subscribe( resdata => this.getAllAnnotation(resdata), error => this.spinnerService.hide() );
    }
   }
  }

  ngAfterViewInit(): void {
    if (this.annotationType === 'IMAGE') {
      if (this.imageannotationURL.length > 0) {
      }
    }
  }

  getPreviewImage(data) {
    this.previewImage = JSON.parse(data._body);
    this.spinnerService.hide();
    this.imageToShow  = 'data:image/png;base64,' + this.previewImage.image;
    this.currentPage = this.previewImage.pageNo;
    this.PaginatorcurrentPage = this.currentPage - 1;
    this.totalPages = this.previewImage.pageCount;
    this.displayImage = false;
    if (this.annotationType === 'IMAGE') {
      this.imageAnnotationDisplay = true;
      this.textAnnotationDisplay = false;
    //  this.signaturePad.clear(); // invoke functions from szimek/signature_pad API
      // this.signaturePad.fromDataURL(this.imageannotationURL);
      setTimeout(() => {
        this.signaturePad.fromDataURL(this.imageannotationURL);
      }, 2000);
      this.annotationTypeSelected = 'imageAnnotation';
     }else {
      this.imageAnnotationDisplay = false;
      this.textAnnotationDisplay = true;
      this.annotationTypeSelected = 'textAnnotation';
     }
     this.spinnerService.hide();
  }

  paginate(event) {
    if(this.blocks.length === 0 && this.signatureIsEmpty === false){
      event.docId = this.docId;
      this.pageChangeEmit.emit(event);
    }else{
      this.tr.info('','Please save annotations before changing page');
      
    }
  }

  getAllAnnotation(data) {
    this.allSavedAnnotaion = JSON.parse(data._body);
    this.usernamedropdownText = [{label: this.translate.instant('Select'), value: ''}];
    this.annotationStatusdropdownText = [{label: this.translate.instant('Select'), value: ''}];
    this.usernamedropdownImage = [{label: this.translate.instant('Select'), value: ''}];
    const usernameSet = new Set();
    const usernameSetImage = new Set();
    const annotationStatus = new Set();
    for (let index = 0; index < this.allSavedAnnotaion.length; index++) {
      if (this.allSavedAnnotaion[index].type === 'COMMENT' && this.allSavedAnnotaion[index].pageNo === +this.currentPage) {
        usernameSet.add(this.allSavedAnnotaion[index].userName);
        annotationStatus.add(this.allSavedAnnotaion[index].status);
      }

      if (this.allSavedAnnotaion[index].type === 'IMAGE'  && this.allSavedAnnotaion[index].pageNo === +this.currentPage) {
        usernameSetImage.add(this.allSavedAnnotaion[index].userName);
      }
    }
    usernameSet.forEach(ele => {
      this.usernamedropdownText.push({label: this.translate.instant(ele), value: ele});
    });
    usernameSetImage.forEach(ele => {
      this.usernamedropdownImage.push({label: this.translate.instant(ele), value: ele});
    });
    annotationStatus.forEach(ele => {
      this.annotationStatusdropdownText.push({label: this.translate.instant(ele), value: ele});
    });

    if (this.annotationType === 'IMAGE') {
      const url = {
        url: this.ds.downloadPreviewPage(this.docId , this.currentPage)
        };
        this.imageannotationURL.push(url);
        for (
          let index = 0; index < this.allSavedAnnotaion.length; index++) {
          if (this.allSavedAnnotaion[index].type === 'IMAGE' && this.allSavedAnnotaion[index].pageNo === +this.currentPage) {
            const url2 = {
            url: this.ds.downloadAnnotation(this.allSavedAnnotaion[index].imageId , this.currentPage)
            };
            this.imageannotationURL.push(url2);
        }
      }
    }
    this.ds.getPreviewPage(this.docId, this.currentPage).subscribe(datares => this.getPreviewImage(datares), error =>  this.spinnerService.hide() );
  }

  annotationTypeChanged() {
    // if(this.viewType === 'activityScreen'){
      if(this.signatureIsEmpty === false && (this.blocks.length === 0) ){
        if(this.annotationTypeSelected !== 'imageAnnotation'){
          this.documentTypeChangeEmit.emit({docId: this.docId,
          pageno: this.currentPage,
         annoatationType: 'COMMENT'});
          localStorage.setItem('Default Annotation', 'COMMENT');
        }else{
          this.documentTypeChangeEmit.emit({docId: this.docId,
            pageno: this.currentPage,
            annoatationType: 'IMAGE'});
          localStorage.setItem('Default Annotation', 'IMAGE');

        }
      }else{
        setTimeout(() => {
          if(this.annotationTypeSelected === 'imageAnnotation'){
            this.annotationTypeSelected = 'textAnnotation';
          }else{
            this.annotationTypeSelected = 'imageAnnotation';
          }
        }, 1000);
        this.tr.error('','Please save annotations');
      }   
    // }
    // else{
    //   if (window.location.hash.split('/')[1] === 'documentview') {
    //     if (this.annotationTypeSelected !== 'imageAnnotation') {
    //       this.router.navigate(['/documentview/viewdocument'], { queryParams:
    //         { 'documentType': 'annotate' , 'docId': this.docId ,
    //           'currentPage': this.currentPage , 'annotationType': 'COMMENT'  }});
    //     } else {
    //       this.router.navigate(['/documentview/viewdocument'], { queryParams:
    //         { 'documentType': 'annotate' , 'docId': this.docId ,
    //           'currentPage': this.currentPage , 'annotationType': 'IMAGE'  }});
    //     }
    //   }else {
    //     if (this.documentType === 'annotate') {
    //       if (this.annotationTypeSelected !== 'imageAnnotation') {
    //         this.router.navigate([window.location.hash.split('/')[1] + '/documentview'], { queryParams:
    //           { 'documentType': 'annotate' , 'docId': this.docId ,
    //             'currentPage': this.currentPage , 'annotationType': 'COMMENT'  }});
    //       }else {
    //         this.router.navigate([window.location.hash.split('/')[1] + '/documentview'], { queryParams:
    //           { 'documentType': 'annotate' , 'docId': this.docId ,
    //             'currentPage': this.currentPage , 'annotationType': 'IMAGE'  }});
    //       }
    //   } else {
    //     if (this.annotationTypeSelected !== 'imageAnnotation') {
    //       this.router.navigate([window.location.hash.split('/')[1] + '/documentview'], { queryParams:
    //         { 'documentType': 'readonly' , 'docId': this.docId ,
    //           'currentPage': this.currentPage , 'annotationType': 'COMMENT'  }});
    //     }else {
    //       this.router.navigate([window.location.hash.split('/')[1] + '/documentview'], { queryParams:
    //         { 'documentType': 'readonly' , 'docId': this.docId ,
    //           'currentPage': this.currentPage , 'annotationType': 'IMAGE'  }});
    //     }
    //   }
    // }
    // }
      
  }
    //   if ( this.annotationTypeSelected !== 'imageAnnotation') {
    //     this.imageAnnotationDisplay = false;
    //     this.textAnnotationDisplay = true;

    //     // document.getElementById('textAnnotation').style.display = 'block';
    //     // document.getElementById('imageAnnotation').style.display = 'none';
    //   }else {
    //     this.imageAnnotationDisplay = true;
    //     this.textAnnotationDisplay = false;
    //     // document.getElementById('textAnnotation').style.display = 'none';
    //     // document.getElementById('imageAnnotation').style.display = 'block';
    //     this.imageannotationURL = [];
    //     const url = {
    //       url: this.ds.downloadPreviewPage(this.docId , this.currentPage)
    //       };
    //       this.imageannotationURL.push(url);
    //     for (let index = 0; index < this.allSavedAnnotaion.length; index++) {
    //         if (this.allSavedAnnotaion[index].type === 'IMAGE' && this.allSavedAnnotaion[index].pageNo === this.currentPage) {
    //           const url2 = {
    //           url: this.ds.downloadAnnotation(this.allSavedAnnotaion[index].imageId , this.currentPage)
    //           };
    //           this.imageannotationURL.push(url2);
    //         }
    //     }
    //     this.signaturePad.fromDataURL(this.imageannotationURL);
    //   }
    // }

    drawComplete() {
      // will be notified of szimek/signature_pad's onEnd event
      this.signatureIsEmpty = true;
    }

  backButtonClicked() {
  this.router.navigateByUrl(localStorage.getItem('backClick'));
  // this.location.back();
  }

  downloadThisdocument() {
    this.ds.downloadDocument(this.docId).subscribe(data => this.downloadCurrentDocument(data));
  }

  downloadannotedDoc() {
    this.ds.downloadAnnotatedDocument(this.docId).subscribe(data => this.downloadCurrentDocument(data));
  }

  downloadCurrentDocument(data) {
    let filename = '';
      const disposition = data.headers.get('Content-Disposition');
      const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        const matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1]) {
          filename = matches[1].replace(/['"]/g, '');
        }
        saveAs(data._body, filename);
  }

  removeAnnotation(id) {
    for (let index = 0; index < this.blocks.length; index++) {
      if (this.blocks[index].id === id) {
        this.blocks.splice(index , 1);
      }
    }
  }

  annotationText(event , id ) {
    for (let index = 0; index < this.blocks.length; index++) {
      if (this.blocks[index].id === id) {
        this.blocks[index].text = event.target.value;
      }
    }
  }

  addTextAnnotation(offsetX, offsetY) {
    this.blocks.push({ text: '',
    type: 'TEXT',
    imageId : '',
    previewUrl: '',
    id : this.annotationid,
    xPos: offsetX,
    yPos: offsetY,
    page: this.currentPage
   });
  this.annotationid++;
  }

  onStop(event , id) {
    for (let index = 0; index < this.blocks.length; index++) {
      if (this.blocks[index].id === id) {
        this.blocks[index].xPos = this.endOffset.x;
        this.blocks[index].yPos = this.endOffset.y;
      }
    }
  }

  onMoving(event) {
    this.movingOffset.x = event.x;
    this.movingOffset.y = event.y;
  }

  mouseDown(e) {
    const rect = e.currentTarget.getBoundingClientRect();
    this.offsetX = e.pageX - 90,
    this.offsetY = e.pageY;
  }


  addTextAnnotationPosition(event) {
    this.addTextAnnotation(this.offsetX, this.offsetY);
  }

  onMoveEnd(event , id) {
    for (let index = 0; index < this.blocks.length; index++) {
      if (this.blocks[index].id === id) {
        this.blocks[index].xPos = event.x;
        this.blocks[index].yPos = event.y;
      }
    }
  }

  statusDropdownChange(event) {
    this.annotationStatusSelected = this.statusSelected;
  }

  userDropdownChange(event) {
    const usernameSet = new Set();
    this.annotationStatusdropdownText = [];
    this.annotationStatusdropdownText = [{label: this.translate.instant('Select'), value: ''}];
    if (this.userSelected !== '' ) {
      for (let index = 0; index < this.allSavedAnnotaion.length; index++) {
        if (this.allSavedAnnotaion[index].type === 'COMMENT' && this.allSavedAnnotaion[index].userName === this.userSelected) {
          usernameSet.add(this.allSavedAnnotaion[index].status);
        }
      }
      usernameSet.forEach(ele => {
        this.annotationStatusdropdownText.push({label: this.translate.instant(ele), value: ele});
      });
    }else {
      for (let index = 0; index < this.allSavedAnnotaion.length; index++) {
        if (this.allSavedAnnotaion[index].type === 'COMMENT') {
          usernameSet.add(this.allSavedAnnotaion[index].status);
        }
      }
      usernameSet.forEach(ele => {
        this.annotationStatusdropdownText.push({label: this.translate.instant(ele), value: ele});
      });
    }

}

completeannotation(block) {
  this.cs.deleteAnnotation(block.id).subscribe(res => {this.annotationcompleted(res); this.tr.success('','Annotation deleted'); } );
  // this.ds.completeAnnotation(block.id).subscribe(res => this.annotationcompleted(res) );
}

annotationcompleted(res) {
  if (this.docId !== undefined) {
    this.ds.getAnnotations(this.docId).subscribe( resdata => this.getAllAnnotation(resdata)  );
  }
}

saveTextAnnotation() {
  if (this.blocks.length > 0 ) {
    for (let index = 0; index < this.blocks.length; index++) {
      let annoation;
      if (this.blocks[index].type === 'TEXT') {
         annoation = {
          docId: this.docId,
          comment: this.blocks[index].text,
          type: 'COMMENT',
          empNo: this.us.getCurrentUser().EmpNo,
          xPos: this.blocks[index].xPos,
          yPos: this.blocks[index].yPos,
          userName: this.us.getCurrentUser().userLogin,
          pageNo: this.blocks[index].page
        };
      }
      this.ds.saveAnnotation(annoation).subscribe(data => this.savedAnnotation(data , '')  );
  }
  }
}

savedAnnotation(data , docId) {
  this.tr.success('' , 'Saved Annotation' );
  this.imageannotationURL = [];
  this.blocks = [];
  if (this.annotationType === 'IMAGE' ) {
    const url = {
      url: this.ds.downloadPreviewPage(this.docId , this.currentPage)
      };
      this.imageannotationURL.push(url);
    for (let index = 0; index < this.allSavedAnnotaion.length; index++) {
        if (this.allSavedAnnotaion[index].type === 'IMAGE' && this.allSavedAnnotaion[index].pageNo === +this.currentPage) {
          const url1 = {
          url: this.ds.downloadAnnotation(this.allSavedAnnotaion[index].imageId , 1)
          };
          this.imageannotationURL.push(url1);
        }
    }
    const url2 = {
      url: this.ds.downloadAnnotation(docId , 1)
      };
      this.imageannotationURL.push(url2);
    this.signaturePad.fromDataURL(this.imageannotationURL);
  }
  this.annotationcompleted('');
  }

  clearSignaturepad() {
    this.signatureIsEmpty = false;
    this.signaturePad.clear();
  }


  saveImageAnnotation() {
    this.blocks = [];
    this.signUrl = this.signaturePad.toDataURL();
      const byteCharacters  = atob(this.signUrl.replace('data:image/png;base64,', ''));
            const byteNumbers = new Array(byteCharacters.length);
            for (let i = 0; i < byteCharacters.length; i++) {
                byteNumbers[i] = byteCharacters.charCodeAt(i);
              }
              const byteArray = new Uint8Array(byteNumbers);
            const blob = new Blob([byteArray] , { type: 'image/png' } );
            // saveAs(blob, this.activityinfo.refNo);
              let string: String;
              string = this.us.getCurrentUser().EmpNo + '-imageannotation.png';
            const docInfo = {
             docclass: 'ProductivitiDocument',
             props: [{
               'name': 'Document Title',
               'symName': 'DocumentTitle',
               'dtype': 'STRING',
               'mvalues': [ string ],
               'mtype': 'N',
               'len': 255,
               'rOnly': 'false',
               'hidden': 'false',
               'req': 'false'
             }],
             accessPolicies: []
           };
            const formData = new FormData();
           formData.append('DocInfo', JSON.stringify(docInfo));
          // formData.append('file', scannedPdfName);
           formData.append('document', blob , this.us.getCurrentUser().EmpNo + '-imageannotation.png');
            this.ds.addDocument(formData).subscribe(data => this.digitalSignAnnotation(data));
  }

  digitalSignAnnotation(data) {
    this.signatureIsEmpty = false;
    this.signaturePad.clear();
      const image = {
        docId: this.docId,
     imageId: data._body,
     type: 'IMAGE',
     empNo: this.us.getCurrentUser().EmpNo,
     xPos: 0,
     yPos: 0,
     userName: this.us.getCurrentUser().userLogin,
     pageNo: this.currentPage
     };
     this.annotationid++;
     this.ds.saveAnnotation(image).subscribe(resdata => this.savedAnnotation(resdata , data._body) );
  }

  userDropdownImageChange(event) {
    this.imageannotationURL = [];
    const url = {
      url: this.ds.downloadPreviewPage(this.docId , this.currentPage)
      };
      this.imageannotationURL.push(url);
    for (let index = 0; index < this.allSavedAnnotaion.length; index++) {
      if (this.allSavedAnnotaion[index].type === 'IMAGE' && this.allSavedAnnotaion[index].pageNo === +this.currentPage && this.allSavedAnnotaion[index].userName === this.userSelected) {
        const url1 = {
        url: this.ds.downloadAnnotation(this.allSavedAnnotaion[index].imageId , 1)
        };
        this.imageannotationURL.push(url1);
      }
  }
  if (this.userSelected === '') {
    for (let index = 0; index < this.allSavedAnnotaion.length; index++) {
      if (this.allSavedAnnotaion[index].type === 'IMAGE' && this.allSavedAnnotaion[index].pageNo === +this.currentPage ) {
        const url2 = {
        url: this.ds.downloadAnnotation(this.allSavedAnnotaion[index].imageId , 1)
        };
        this.imageannotationURL.push(url2);
      }
  }
}
    this.signaturePad.fromDataURL(this.imageannotationURL);
  }

  listOfannotationOnDocument(annotationDocumentList) {
    if (this.docId !== undefined) {
    this.cs.getAnnotations(this.docId).subscribe(data => this.getListOfAnnoattion(data));
    }
  }

  getListOfAnnoattion(data) {
    const resData = JSON.parse(data._body);
    if (resData.length === 0 ) {
      this.tr.info('', 'No annotation found on this document');
    }else {
      this.listOfDcumentAnnotation = JSON.parse(data._body);
      this.ngxSmartModal.getModal('annotationModelList').open();
    }
  }
  documnetAnnoation(pageNo , type) {
    this.currentPage = pageNo;
    this.annotationType = type;
    if (this.router.url.includes('inbox')) {
      this.documentType =  'annotate';
    }else if (this.router.url.includes('sent')) {
      this.documentType =   'readonly';
    }else if (this.router.url.includes('draft')) {
      this.documentType =  'annotate';
    }else if (this.router.url.includes('register')) {
      this.documentType =   'readonly';
    }else if (this.router.url.includes('archive')) {
      this.documentType =   'readonly';
    }else {
      this.documentType =  'annotate';
    }
    this.ngxSmartModal.getModal('annotationModelList').close();
    this.ngOnChanges();
}

  deleteAnnotation(annotation) {
    this.cs.deleteAnnotation(annotation.id).subscribe(data => {this.ngxSmartModal.getModal('annotationModelList').close();
    this.cs.getAnnotations(this.docId).subscribe(datares => this.getListOfAnnoattion(datares));
    this.ds.getAnnotations(this.docId).subscribe( resdata => this.getAllAnnotation(resdata) );
      });
  }

  annotationComplete(){
    if(this.signatureIsEmpty === false && (this.blocks.length === 0) ){
      const date = new Date();
      this.completeActivityEmit.emit(date);
    }else{
      this.tr.error('','Please save annotations');
    }

  }

}
